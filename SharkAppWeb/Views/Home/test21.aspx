﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title></title>
</head>
<body style="height: 249px">
    <form id="form1" runat="server">
    <div>
    
    </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [BCPA_TAX_ROLL]"></asp:SqlDataSource>
    </form>
</body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-us" xml:lang="en-US" xmlns="http://www.w3.org/1999/xhtml"><head id="Head1"><meta content="Document,Clerk Records" name="keywords" /><meta content="en,us" http-equiv="content-language" /><meta content="Broward County Clerk of Circuit Court" name="DESCRIPTION" id="MetaDescription" /><meta content="Copyright 2015 by Broward County Clerk of Circuit Court" name="COPYRIGHT" id="MetaCopyright" /><meta content="Broward County Clerk of Circuit Court" name="AUTHOR" id="MetaAuthor" /><meta content="DOCUMENT" name="RESOURCE-TYPE" /><meta content="GLOBAL" name="DISTRIBUTION" /><meta content="NOINDEX, NOFOLLOW" name="ROBOTS" /><meta content="1 DAYS" name="REVISIT-AFTER" /><meta content="GENERAL" name="RATING" /><meta content="RevealTrans(Duration=0,Transition=1)" http-equiv="PAGE-ENTER" /><meta content="IE=edge" http-equiv="X-UA-Compatible" /><title>
	COCE16024438 - Esummons Issuance
</title>
    <script src="../Scripts/jquery-2.2.3.js"></script>
    <style type="text/css">
        html, body, form
        {
            height: 100%;
            margin: 0px;
            padding: 0px;
            overflow: hidden;
        }
        body
        {
            background-image: none;
            background-color: #52b9e9;
        }
        #lblPageInfo
        {
            float: right;
            font-weight: bold;
            margin-right: 40px;
            padding: 10px 0;
            font-size: 1em;
        }
        #divSpin {
        background: url("../Content/assets/img/Website/spinner_28x28.gif") no-repeat scroll center center #FFF;
        position: absolute;
        height: 100%;
        width: 100%;
    }
    </style>

    <link rel="stylesheet" href="../Content/css/bootstrap.css" /><link class="Telerik_stylesheet" rel="stylesheet" type="text/css" href="/Web2/WebResource.axd?d=Eolpxceq3se1NJ978Plkor_veniO0VFjhx1iNPAIYPLYyanrrQTd8fHIGieyWNxAAiF4u703Y4cCnhcizmNXW-ubd525t5HcJXDyHjAvEK5iE0hjDamoAjQPo8r1AcxlI0RzCng97TAsKlxkJaTVBQ2&amp;t=635896822690120059" /><link class="Telerik_stylesheet" rel="stylesheet" type="text/css" href="/Web2/WebResource.axd?d=E4pdc1M13gllyATRsQMjVpcsdmELmmehqTHNdC-PqJIDDpWiiYgCjvW4OXBbqXoKYX37zeQY5PzpaS7DHBqvci5DusHi9zvu_c1Xv5uDZzr0MziwfGVWKOK4ff0Ampne93SDtiZRvYOzsS52TZc3IzkRIGxVZaZPZFIN52yaYpk1&amp;t=635896822690120059" /><link class="Telerik_stylesheet" rel="stylesheet" type="text/css" href="/Web2/WebResource.axd?d=_h3tqqHoL_scwqSdjPRDX9c2vGX5n5TsYDa76ln9qq9YBiUM0G_P0qpmouAboYuKUj73hrvEyHsZVnZJ8LVtDGjfisKYL3X9BCLfmBCvhCeSSkvuUeuB_9WeqFLVEZPdZudnQJJf7lGI4gKT5mcGQQ2&amp;t=635896822690120059" /><link class="Telerik_stylesheet" rel="stylesheet" type="text/css" href="/Web2/WebResource.axd?d=ISQ2nTkDyQ6UApHxVmBsh3D7nH1weX4fnAKg1q78Pg9RWlG9O0q-lm7F7r61Av0o4yxKW1KtcnuKBpBqRrIbifZ2W1xU2gHHgiV090kBhXr1KpKOnMyipZlVvwLtCH8Z-uDMFMOVTSMh7N-sVU1aOyQQGOiXR0psbkrenhCgYHc1&amp;t=635896822690120059" /><link class="Telerik_stylesheet" rel="stylesheet" type="text/css" href="/Web2/WebResource.axd?d=QKX1AO1vX8ebWKfbb4eOTD1jNPqnoeAm8EhabeyeDAIu1-oUC94VD5HOtNV93TwjnAYjmvkAjVWxCZDUxIZawz3uuh4iV6eJXddONqojhpzhCIZjRkmGOc1_hSyLAtCQ0&amp;t=635896822690120059" /><link class="Telerik_stylesheet" rel="stylesheet" type="text/css" href="/Web2/WebResource.axd?d=abSXP-RgNuAsfhlSYpnpbBsbryu3pqECRAXBnuBQOFuffufJQuB6UTM5XLC9D2uVelzTVAwVaR5JbaMeMgMFTjTlIOBCCLyPJBQYWT3rXWQ2HR11ojYj_se7IaZx1Wdj0L7NGIu5jVURKNoblvvQRA2&amp;t=635896822690120059" /></head>
<body>
    <form id="form1" action="./Document.aspx?CaseID=ODczMDA0NA%3d%3d-QdfPoRSp4TE%3d&amp;CaseNumber=COCE16024438&amp;Style=+Brentwood+Townhouse+Condominiums+Inc%0a++++++++Plaintiff%0a+vs.+%0a%0aElba+Bates%2c+et+al%0a++++++++Defendant&amp;FragmentID=MjI2NzQ4MDg%3d-gIoQ4pBIF%2fw%3d&amp;DtFile=11%2f16%2f2016&amp;DocName=eSummons+Issuance&amp;PgCnt=2&amp;UserName=&amp;UserType=Anonymous" method="post">
<div class="aspNetHidden">
<input type="hidden" value="" id="__EVENTTARGET" name="__EVENTTARGET" />
<input type="hidden" value="" id="__EVENTARGUMENT" name="__EVENTARGUMENT" />
<input type="hidden" value="ZlfPUDVuj6h0sohq9oGnkNSVmKsAcfd/Dz73nRmJZFxtxDYQn7UOZpvPFHyIoeEq1xMbKplWr53+tAs+IyKrsm7kN4H+Oe3veOCg008VglUzttlkQ0YkS5C8EwMw2b2zxLXMhgJVT8OXlubKEAWN6+e2zMOH2n+4vaBBlnvD/w5uBaRll+mYRNqRVWG+/PzAap+42XOvmftApkVxPf+XpqLinS4QDB5pTYcDSzIkRlVY+7+40oYHNeLcV9nqinTpdPmY87QuMpGk0GyjKN9piY5aSpqAF6W5KxfhX+0Ofy/Fb1AvOS0LZnUWpap7A/FwS6oRkP4KrHZ68AhSUuRVTg/aNzbvj78BrOAEV9puz6U6aYBXkEcYYXPLVz3vSRfsAuEm8j2q5N6/wpYHW1LGIGDVsyMnk2JZ4PZV7F13kDBme+NjFwZO9J/EKB41ew+99HxPThlnNIcFPtg+tXEDsv0A2ajyAOQMKmTe63W9h7+GZi1Czl2SC3oNKiyA5E/BclBd0Hi6s612rT09dufP2axkl4+5qDNbzT382rJ0Lucp6e2ASF/MFvFqTqHF+ASsdiBLUFkXR2od0uBGinfrF93lQ/chAIaxnGGuu5q8sxnmU7RX+kdZd2xO1N58Rp4XJtGzzd+16nUMhUTgTIapwuVixdc+V8jANEYyUofglOOyWT80CLCZk49+wcVQ5/p7ncPsYu0lq5s2JvSyDDz0qC0HmoY6A5wq51MCnHal0myohYdddKkX8BSpSPc7dT7cT/4WhrbeBd9KB1HwyG09huDKASAaOSK5UCnHEu5MAWwIdNyhqfUqNdxD+1dBnr1WEFZ0s5lpkjQcapKB0+HxdI+AirrWQs0oZPeB3X8MPJVDRLK0DPeGoMzq0qk6Tb3C8Y4XDSjiXKEVH7Dd9bWlK6X0sC5wt1xxaV3T0vbNilPDtVN+gs3hur/FLqdvC1O4lOy0Y+kzJjgMK0x/fWrLkO+JNmZQAYPr6X4+rXctcz6ekPY0vPi3qTAjcNdvpopKAX7EOMFTLQz+Mx3DONLpWTRT8TSyBmuX/GAcR0xgLnNgXO8tBtBl6X0fqnmiPp2qDL0nyRtLUwaHhuZS61E9/3p2bu/kNa2+bIlHJWTqWvbRqC5osgweCrzN5q1v5a+kl5uXt6cxtvmW8BFb5aSphZVUtOxHtWvvw++40gHCvna8iEKqfczynZ8xsdlTQjM+mD1uhO543gRzaddmac/59JXO43yrp2kLUe2chcpKr/R7xTOxDj4bt9S/VG6wcOG8anJRqQ2UHnoYyet4nvbyR+6+62VtFHN9nAzdiYejT4xX2ytZuh09vW+RX9I4swYk6qWeQ1A0R9hjaZBEpXqTEPn6wtKAo1urWp3cu/lLkPjTG2aFk9qJm7TTelea6HzuhpbrgcmUVZiCwdh+ZLuCagIUfb4DGCRXwI92i91xYidPgGCuy1pY7i6xz0/iCQNSGKzccYBZcpt4+la2Wdiy60hnHUE7owWGZ659rAWpTleuBJjw67m92hbRYNhRYyn+tzHVLNQzfJTBUBqet3W7Np9dA3aJojJsKnepkGkAVe8LMe5twRSj8ChcwJLAoQVJOJIpbJgVlMNT0FPrqfZXsG/ZvA6R1oxVcLarCMrXgz8fiOtbZMe97I+JFN3bFxcH1J6opdWxisDjuZN1PHzvc4Hh1Sbwycltx9xfxEOj8m4MG6HfTcRsFr6gjwPox+B9ARB2Ey8FyjEvYcsx/txLf0GEXsOvu8iGEi8VInuECX/tcTXf4aRf8+/ExTAcDPKXHqGpaGyM/cHp/puB30QrlvBkSHEc9AiQqqlc1kJgFDEQtfr0HiwpLk2jyRpBjslWeAdQzW5ilrxf2CgpF8+uxMfSNkGDhKyL0xkZDf0wSJ3PxTUESjuH5llYcJY+fHbAfFcs3/iTmFJzb9IM3fjS1RkV/qm6e5Ev7orPPn712ao9sKQPgpYVO1cvv1b77LNu" id="__VIEWSTATE" name="__VIEWSTATE" />
</div>

<script type="text/javascript">
//&lt;![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]&gt;
</script>


<script type="text/javascript" src="/Web2/WebResource.axd?d=pynGkmcFUV13He1Qd6_TZHtvhq-67bu6gYOxFFPNHzKUMM8W7ljDChZQeNVAxFMNk8KgHaVG8-JcjhryaapVcg2&amp;t=635875329181218729"></script>


<script type="text/javascript" src="/Web2/ScriptResource.axd?d=NJmAwtEo3Ipnlaxl6CMhvmdrRo104uh6ylpTKEeNjqthGJcaez48AzbccHn02Lbm787Q1n4RyUVlLoJJIJ1XoPOvJtcYetIc3m7aebBbXSRxBE8_WNRpzo3Oz-CbcaL1L8aF8aFH3bBm8ulSsnzUSCQ0iVsOVur3zQogVWGVDgU1&amp;t=ffffffffeea0dba9"></script>
<script type="text/javascript" src="/Web2/ScriptResource.axd?d=dwY9oWetJoJoVpgL6Zq8OECSGHU0f9IQ1Ty29_xOHn72FGQuuZfcy9u-Jn2WnYZ_MEq9yHAzJ-rPaDDMfw_RSMPigMTnFOf9xEa-AmHWcjSrjbX_TDin9fOXA9DVnEqVFvEzWNLjs4f7zoVMuJL30bwjIWd4TlJ1l9TW8bq9NoE1&amp;t=ffffffffeea0dba9"></script>
<script type="text/javascript" src="/Web2/ScriptResource.axd?d=TDVjdgRbdvNAY3tnbizUcA4dGTGxhYWNQEakctkEVyDxDnhTN3Jovf3xmJ3H_n7AmSjKmXjFvOnIqW7_YyHhRvG1C8s_LI12MNwMDiOP1zUiDIjDULAm3hFHRQCmRh1t0&amp;t=ffffffff83fd1515"></script>
<script type="text/javascript" src="/Web2/ScriptResource.axd?d=mbGZm65DzNC0tMTq0ElbcZKePjuy66KmAYP2ltWKt0EDezw271EV_SU2BljieofeBvcl5osJWxQSDJoWc4Dz367f9jD7JZJpzwOCi5TR6oxCwr9m8LG8k1wBHZfwLZ7RoOCEq-k7dRImc9VXHZ4vww2&amp;t=ffffffff83fd1515"></script>
<script type="text/javascript" src="/Web2/ScriptResource.axd?d=cmIcXiVJ__exjjpjrN730ma-Lzwj3hlekACur1f7oLlbp3aPu6fI8GlVxq393hzC5EYPl3aqgFeqriyeuSxdWkbLlcm2-cTBhKZmVIVF7XSN-JN815EZ0YEqx5mT-XP2DPcHZm4yhJdb_yKeqtZTOA2&amp;t=ffffffff83fd1515"></script>
<script type="text/javascript" src="/Web2/ScriptResource.axd?d=wM9Do_oVrPadTpWytatiZlTKINhP8a3GRZkV1CpfBjL58cVpnX2HxTHdSVpJ0LRlurXMqM_PZfQQAf5LmsgusUfqPBeN6uhTtZrxK6FmzWCW-qtefmSXQxblXfU9mbSRbbZrn_HL487wBdI2HodtTg2&amp;t=ffffffff83fd1515"></script>
<script type="text/javascript" src="/Web2/ScriptResource.axd?d=utaAJrK4EroNO-KNr3p19Eqen3Ofe9aN6RqrslWoLAS4ui8pcLTOiY1D-48aJqzzaUZ_xZfCIpF5aj9Xf8GomfiCd_O0v8QmJxRTupiMwL6YgG6Wh-wY4iShUDZUOhm1Y_YEjfnTp9xYIMflVHcUZA2&amp;t=ffffffff83fd1515"></script>
<script type="text/javascript" src="/Web2/ScriptResource.axd?d=aoSSiMnLNTC20U5L7LFIKugY51oJ9jDoADj-SsaWMRAymhvl20PlWrvTUopBJI4PT-gJYY_XrjKw8uECE6uDQuFHNidiqRYUvjmGib42nH2SusRNDJWLboLpCqQ94yw2P1EcML4J031P8qxlocpIm3Ts4hC-0YS9gUkRpZskBbM1&amp;t=ffffffff83fd1515"></script>
<script type="text/javascript" src="/Web2/ScriptResource.axd?d=uZyysmNAhs23bID72ZUaSpUvYj63xpPe8wwLeltQ1VUzsT7CpovnOT1QRNrMUpo5XZVwin1fEG7bMxA0vyxc1tqys8wHyowqrqHit1ZjSwzMOgu5g-jH_m5vADoB1_2-0-aUcSQdhfhvRsodlcfZaFa3A4LxTRnqIEJkGg8c30c1&amp;t=ffffffff83fd1515"></script>
<script type="text/javascript" src="/Web2/ScriptResource.axd?d=eKZhdBAoDXLAsQys5JFN8sXJSQVWBVhzDuzX2Um1GFWZr-MqDsEn1H_GSbd5Lyn29GyaNiL2acS52j3VdAgmPSVUAUzHDTMRpILOcHPV76qJMlYzny-k6Ml7OqJD1Wgvr4hlb-uqbHMeCCfkd8mgnWOMgUDJwsnDGsGW23dtrx01&amp;t=ffffffff83fd1515"></script>
<script type="text/javascript" src="/Web2/ScriptResource.axd?d=azZWDlPjvGQFzPBzbd9C4KdqmQJlfQsPU0Q6ZE3qFhxEIfq96QJNRmdnObJmYmodpOxrlrQgFZ-oOgg-TmcVQWGEYuw6Sw8m1iE6z14X5Y3lQz5WZijMETZ-XiWXRDk5SHsAMyClDyxlGJ9TFVZYWA2&amp;t=ffffffff83fd1515"></script>
<script type="text/javascript" src="/Web2/ScriptResource.axd?d=A22-SyJgKhJ5FWUJePaPlALhULPeP2S--0L1p22U5RSKMRpSGYwCHhGwnrYOT1NLtrkJSOuURtBi9eoij0Rf1ZCfpEGcjEkRw0EHLWwpCgSbceNX7kFwxh1eXansuXDcVkrrNnCyQL5VTFZ2EdaCKQ2&amp;t=ffffffff83fd1515"></script>
<script type="text/javascript" src="/Web2/ScriptResource.axd?d=ei_BvuPu_kfWuvcXX4wAOIlpIT10IQiVZRzNigm_qzyfOwrwjyLs29nPL-xotGD6jZyA2fBalhZWZvEoD_zbyLhGBuSnyPgKLKZdP_8hSabkc2RlPjTnK6XiAFqZU_A0y2UDdYh0KzcO5Cpo--DKfA2&amp;t=ffffffff83fd1515"></script>
<script type="text/javascript" src="/Web2/ScriptResource.axd?d=qsRpKh5Y3Y02fhrPxQCAxXUL3lAaLu799CiKU5sOA4-rCD_ubDmt2btJyAKkhwL_dGVFyJ9J-LAhgGzkqoUIzjtc0NjRc_Wyxl_vUAVPHLwzbX40etFfkuNQB1ZjdHV_whqySDJ96c65-IoQ9J8dzA2&amp;t=ffffffff83fd1515"></script>
<script type="text/javascript" src="/Web2/ScriptResource.axd?d=GUyLPMOEFhffuIMm6XZ0oBLA87KgJRehqHsHRQZVqozhmFFQO9aAeebTbUsNzNNGYbBsQ6HUxXO_3-k6PD2fSEutIImvhqhmlKj0j26B2epnDxpUfwXdpvXZuL71OLMRgBo3TP4jLoWqToSH1gV8WyDXCxh6JXamDTKi9otsIOM1&amp;t=ffffffff83fd1515"></script>
<script type="text/javascript" src="/Web2/ScriptResource.axd?d=wl5RKBCXTWDacHhxsSq8Q8tAjrKGFEcALQrOwKGiWScEfgPDUumyt0LV-YA8_g1QoRoUypp6Sailec-dIDJjDuiwqigRBZFCu0CMeohSJZ4BfYZCXdipX8vuHR_L43-MAmYJtXyBW-CnzfWxSU3nZQ2&amp;t=ffffffff83fd1515"></script>
<script type="text/javascript" src="/Web2/ScriptResource.axd?d=OhiQVM8i2feYV04aiMKgwqh9x607Swkt6aw3E1BEUNSEtfQxI4rf6fSBpEvznCpSgdKeNG8PsX18qNVWhXzPOxYfW5csfZQy69P3kXiR5XAcE8yNZuNrOfOA-3JiXLmHkygaBxjSgCOAAsWNveooAA2&amp;t=ffffffff83fd1515"></script>
<div class="aspNetHidden">

	<input type="hidden" value="F66DC1D8" id="__VIEWSTATEGENERATOR" name="__VIEWSTATEGENERATOR" />
</div>
    <script type="text/javascript">
//&lt;![CDATA[
Sys.WebForms.PageRequestManager._initialize('smAjax', 'form1', [], [], [], 90, '');
//]]&gt;
</script>

    <div id="mph"></div>
    <div style="height: 100%;" id="ParentDivElement">
        <div class="row align-center col-md-12" id="divSpin" style="display: none;">
        </div>
        <div style="width: 100%; height: 433px;" id="rsplMain"><!-- 2014.3.1209.40 --><div style="">
            <table style="width:1px;height:1px;border-left-width:1px;border-top-width:1px;" class="RadSplitter RadSplitter_Default" id="RAD_SPLITTER_rsplMain">
	<tbody><tr id="RAD_SPLITTER_PANE_TR_Navigation">
		<td style="border-right-width:1px;border-bottom-width:1px;" class="rspPaneHorizontal rspFirstItem" id="Navigation"><div style="width: 1364px; height: 30px; overflow: hidden;" id="RAD_SPLITTER_PANE_CONTENT_Navigation">
                <div style="z-index:9000;" class="RadToolBar RadToolBar_Horizontal RadToolBar_Default RadToolBar_Default_Horizontal" id="rdToolBar">
			<div class="rtbOuter">
				<div class="rtbMiddle">
					<div class="rtbInner">
						<ul class="rtbUL">
							<li class="rtbItem rtbBtn"><a href="#" class="rtbWrap" title="View Entire Document"><span class="rtbOut"><span class="rtbMid"><span class="rtbIn"><img class="rtbIcon" src="images/dwnload_idx.gif" alt="View Entire Document" /><span class="rtbText">View All Pages</span></span></span></span></a></li><li class="rtbSeparator"><span class="rtbText"></span></li><li class="rtbItem rtbBtn rtbDisabled"><a href="#" class="rtbWrap"><span class="rtbOut"><span class="rtbMid"><span class="rtbIn"><img class="rtbIcon" src="images/first.gif" alt="" /><span class="rtbText">First</span></span></span></span></a></li><li class="rtbItem rtbBtn rtbDisabled"><a href="#" class="rtbWrap"><span class="rtbOut"><span class="rtbMid"><span class="rtbIn"><img class="rtbIcon" src="images/previous.gif" alt="" /><span class="rtbText">Previous</span></span></span></span></a></li><li class="rtbItem rtbBtn"><a href="#" class="rtbWrap"><span class="rtbOut"><span class="rtbMid"><span class="rtbIn"><span class="rtbText">Next</span><img class="rtbIcon" src="images/next_h.gif" alt="" /></span></span></span></a></li><li class="rtbItem rtbBtn"><a href="#" class="rtbWrap"><span class="rtbOut"><span class="rtbMid"><span class="rtbIn"><span class="rtbText">Last</span><img class="rtbIcon" src="images/last_h.gif" alt="" /></span></span></span></a></li><li class="rtbSeparator"><span class="rtbText"></span></li><li class="rtbItem rtbBtn"><a href="http://www.adobe.com/products/acrobat/readstep2.html" class="rtbWrap" title="Get Adbode Acrobat Reader"><span class="rtbOut"><span class="rtbMid"><span class="rtbIn"><img class="rtbIcon" src="images/acrobat.gif" alt="Get Adbode Acrobat Reader" /></span></span></span></a></li><li class="rtbSeparator"><span class="rtbText"></span></li><li class="rtbItem rtbBtn"><a href="#" class="rtbWrap"><span class="rtbOut"><span class="rtbMid"><span class="rtbIn"><img class="rtbIcon" src="images/Redaction.gif" alt="" /><span class="rtbText">Request Information Removal</span></span></span></span></a></li><li class="rtbSeparator"><span class="rtbText"></span></li><li class="rtbItem rtbBtn"><a href="#" class="rtbWrap" title="Close this window"><span class="rtbOut"><span class="rtbMid"><span class="rtbIn"><img class="rtbIcon" src="images/Actions-window-close-icon.png" alt="Close this window" /><span class="rtbText">Close</span></span></span></span></a></li>
						</ul>
					</div>
				</div>
			</div><input type="hidden" name="rdToolBar_ClientState" id="rdToolBar_ClientState" autocomplete="off" />
		</div>
                <span style="vertical-align:top; text-align:center;" id="lblPageInfo">Page 1 of 2</span>
            </div><input type="hidden" name="Navigation_ClientState" id="Navigation_ClientState" value="{&quot;_originalWidth&quot;:&quot;&quot;,&quot;_originalHeight&quot;:&quot;35px&quot;,&quot;_collapsedDirection&quot;:1,&quot;_scrollLeft&quot;:0,&quot;_scrollTop&quot;:0,&quot;_expandedSize&quot;:0,&quot;width&quot;:1364,&quot;height&quot;:30,&quot;collapsed&quot;:false,&quot;contentUrl&quot;:&quot;&quot;,&quot;minWidth&quot;:20,&quot;maxWidth&quot;:10000,&quot;minHeight&quot;:30,&quot;maxHeight&quot;:70,&quot;locked&quot;:false}" autocomplete="off" /></td>
	</tr><tr id="RAD_SPLITTER_PANE_TR_pdfview">
		<td style="border-right-width:1px;border-bottom-width:1px;" class="rspPaneHorizontal rspLastItem" id="pdfview"><div style="width: 1364px; height: 400px; overflow: hidden;" id="RAD_SPLITTER_PANE_CONTENT_pdfview"><iframe frameborder="0" name="pdfview" id="RAD_SPLITTER_PANE_EXT_CONTENT_pdfview" src="/Web2/WebForms/PDF.aspx?page=1" style="border: 0px none; width: 1364px; height: 400px;" scrolling="auto"></iframe></div><input type="hidden" name="pdfview_ClientState" id="pdfview_ClientState" value="{&quot;_originalWidth&quot;:&quot;&quot;,&quot;_originalHeight&quot;:&quot;&quot;,&quot;_collapsedDirection&quot;:1,&quot;_scrollLeft&quot;:0,&quot;_scrollTop&quot;:0,&quot;_expandedSize&quot;:0,&quot;width&quot;:1364,&quot;height&quot;:400,&quot;collapsed&quot;:false,&quot;contentUrl&quot;:&quot;/Web2/WebForms/PDF.aspx?page=1&quot;,&quot;minWidth&quot;:400,&quot;maxWidth&quot;:10000,&quot;minHeight&quot;:400,&quot;maxHeight&quot;:10000,&quot;locked&quot;:false}" autocomplete="off" /></td>
	</tr>
</tbody></table><input type="hidden" name="rsplMain_ClientState" id="rsplMain_ClientState" autocomplete="off" /></div></div>
    </div>

        


    <script type="text/javascript">  
      
        var inCurrentPage = 1; var inPages = 2; 
        document.getElementById("lblPageInfo").innerHTML = "Page " + inCurrentPage + " of " + inPages;
        
        function buttonClicked(sender, args){
            var value = args.get_item().get_value();
            switch(value){
            case "b":
               goBack();
            break
            case "c":
                window.close();
            break
            case "f":
                inCurrentPage = 1;
                UpdatePage(1);
                UpdateButtonState();
                UpdatePage(inCurrentPage);   
            break
            case "p":
                if(inCurrentPage - 1 &gt;= 1){
                    inCurrentPage -= 1;
                }else{
                    inCurrentPage = 1;
                }
                UpdateButtonState();
                UpdatePage(inCurrentPage);   
            break
            case "n":
                if(inCurrentPage + 1 &lt;= inPages){
                    inCurrentPage += 1;
                }else{
                    inCurrentPage = inPages;
                }
                UpdateButtonState();
                UpdatePage(inCurrentPage);   
            break
            case "l":
                inCurrentPage = inPages;
                UpdateButtonState();
                UpdatePage(inCurrentPage);   
            break
            case "c":
               
                break
            case "r":                
                var url=args.get_item().get_commandArgument();
                OpenWindow(url);
                
                break
            case "gotoPage":
                var inPage = document.getElementById("txtPageNumber").value;
                if (parseInt(inPage)){        
                    if(inPage &gt; inPages){inPage = inPages;}
                    if(inPage &lt; 1){inPage = 1;}
                    inCurrentPage = parseInt(inPage);
                    UpdatePage(inPage);
                    document.getElementById("lblPageInfo").innerHTML = "Page Navigation: " + inCurrentPage + " of " + inPages;
                }else{
                    document.getElementById("txtPageNumber").value="";           
                    alert("You must enter a number"); 
                    document.getElementById("txtPageNumber").focus();
                    return false;
                }
            break
            case "d":
                var toolBar = $find("rdToolBar");
                var download = toolBar.findItemByValue("d");
                var first = toolBar.findItemByValue("f");
                var previous = toolBar.findItemByValue("p");
                var next = toolBar.findItemByValue("n");
                var last = toolBar.findItemByValue("l");
                if (!(download.get_checked())){
                    UpdateButtonState();
                    UpdatePage(inCurrentPage);
                    download.set_text("View All Pages");
                    document.getElementById("mph").innerHTML="";
                }else{
                      download.set_text("Single Page");
                    first.disable();
                    previous.disable();
                    next.disable();
                    last.disable();
                    UpdatePage('all');
                    document.getElementById("lblPageInfo").innerHTML="Displaying all Pages";
                    document.getElementById("mph").innerHTML="&lt;h4 style=\"color:red;\"&gt;(View All Pages) Depending on the file size, it could take a while for this process to complete. If the file size is too large, the operation may timeout and the file will not be displayed.&lt;/h4&gt;";
                }
                
            break
            case "x":
                var toolBar = $find("rdToolBar");
                var redact = toolBar.findItemByValue("x");
                var download = toolBar.findItemByValue("d");
                var first = toolBar.findItemByValue("f");
                var previous = toolBar.findItemByValue("p");
                var next = toolBar.findItemByValue("n");
                var last = toolBar.findItemByValue("l");
                var adobe = toolBar.findItemByValue("a");

                if (!(redact.get_checked())){
                    if (download)
                        download.enable();
                    if (adobe)
                        adobe.enable();
                    redact.set_text("Request Information Removal");
                    document.getElementById("mph").innerHTML="";
                    UpdateButtonState();
                    UpdatePage(inCurrentPage);
                }else{
                    redact.set_text("Back to previous page");
                    if (download)
                        download.disable();
                    if (first)
                        first.disable();
                    if (previous)
                        previous.disable();
                    if (next)
                        next.disable();
                    if (last)
                        last.disable();
                    if (adobe)
                        adobe.disable();
                    RedactPage(inCurrentPage);
                    document.getElementById("lblPageInfo").innerHTML="Request Information Removal";
                }
                
            break
            }
                 
        }
        function goBack(){            
             history.back(1);
        }
        
        function UpdateButtonState(){
            var toolBar = $find("rdToolBar");
            var first = toolBar.findItemByValue("f");
            var previous = toolBar.findItemByValue("p");
            var next = toolBar.findItemByValue("n");
            var last = toolBar.findItemByValue("l");
            if(inCurrentPage == 1){
                if (first)
                    first.disable();
                if (previous)
                    previous.disable();
            }else{
                if (first)
                    first.enable();
                if (previous)
                    previous.enable();
            }
            if (inCurrentPage == inPages){
                if (last)
                    last.disable();
                if (next)
                    next.disable();
            }else{
                if (last)
                    last.enable();
                if (next)
                    next.enable();
            }
            document.getElementById("lblPageInfo").innerHTML = "Page " + inCurrentPage + " of " + inPages; 
            return false;
        }

        function UpdatePage(page){
            var Url = "PDF.aspx"; 
            var splitter = $find("rsplMain");
            var pane = splitter.getPaneById("pdfview");
            var frame = pane.getExtContentElement();
           
            Url = Url + "?page=" + page ;
            frame.src=Url;
           // frame.contentWindow.location.replace(Url);           
        }
        
        function RedactPage(page){
            var Url = "Redact.aspx"; 
            var splitter = $find("rsplMain");
            var pane = splitter.getPaneById("pdfview");
            var frame = pane.getExtContentElement();
           
            Url = Url + "?page=" + page ;
            frame.src=Url;
            // frame.contentWindow.location.replace(Url);           
        } 
        
        function OpenWindow(url) {
            var wnd = window.radopen(url, null);
            wnd.setSize(600, 400);
            return false;
        }
        
     
    </script>
    
    <div style="display:none;" id="rdWindowMgr">
	<div style="display:none;" id="wVor">
		<div style="display:none;" id="wVor_C">

		</div><input type="hidden" name="wVor_ClientState" id="wVor_ClientState" autocomplete="off" />
	</div><div style="display:none;" id="rdWindowMgr_alerttemplate">
		<div class="rwDialogPopup radalert">			
			<div class="rwDialogText">
			{1}				
			</div>
			
			<div>
				<a href="javascript:void(0);" class="rwPopupButton" onclick="$find('{0}').close(true);">
					<span class="rwOuterSpan">
						<span class="rwInnerSpan">##LOC[OK]##</span>
					</span>
				</a>				
			</div>
		</div>
		</div><div style="display:none;" id="rdWindowMgr_prompttemplate">
		 <div class="rwDialogPopup radprompt">			
			    <div class="rwDialogText">
			    {1}				
			    </div>		
			    <div>
				    <script type="text/javascript">
				    function RadWindowprompt_detectenter(id, ev, input)
				    {							
					    if (!ev) ev = window.event;                
					    if (ev.keyCode == 13)
					    {															        
					        var but = input.parentNode.parentNode.getElementsByTagName("A")[0];					        
					        if (but)
						    {							
							    if (but.click) but.click();
							    else if (but.onclick)
							    {
							        but.focus(); var click = but.onclick; but.onclick = null; if (click) click.call(but);							 
							    }
						    }
					       return false;
					    } 
					    else return true;
				    }	 
				    </script>
				    <input type="text" value="{2}" class="rwDialogInput" onkeydown="return RadWindowprompt_detectenter('{0}', event, this);" title="Enter Value" />
			    </div>
			    <div>
				    <a href="javascript:void(0);" class="rwPopupButton" onclick="$find('{0}').close(this.parentNode.parentNode.getElementsByTagName('input')[0].value);"><span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span></span></a>
				    <a href="javascript:void(0);" class="rwPopupButton" onclick="$find('{0}').close(null);"><span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
			    </div>
		    </div>				       
		</div><div style="display:none;" id="rdWindowMgr_confirmtemplate">
		<div class="rwDialogPopup radconfirm">			
			<div class="rwDialogText">
			{1}				
			</div>						
			<div>
				<a href="javascript:void(0);" class="rwPopupButton" onclick="$find('{0}').close(true);"><span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span></span></a>
				<a href="javascript:void(0);" class="rwPopupButton" onclick="$find('{0}').close(false);"><span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
			</div>
		</div>		
		</div><input type="hidden" name="rdWindowMgr_ClientState" id="rdWindowMgr_ClientState" />
</div>
    

<script type="text/javascript">
//&lt;![CDATA[
Telerik.Web.UI.RadSplitter._preInitialize("rsplMain");Telerik.Web.UI.RadPane._preInitialize("Navigation", "rsplMain", "", "",  0, 0, "False");Telerik.Web.UI.RadPane._preInitialize("pdfview", "rsplMain", "", "",  1, 1, "True");Sys.Application.add_init(function() {
    $create(Telerik.Web.UI.RadToolBar, {"_cssClass":"","_skin":"Default","attributes":{},"clientStateFieldID":"rdToolBar_ClientState","collapseAnimation":"{\"duration\":450}","expandAnimation":"{\"duration\":450}","itemData":[{"value":"d","checkOnClick":true,"allowSelfUnCheck":true,"toolTip":"View Entire Document","cssClass":"rtbWrap","imageUrl":"images/dwnload_idx.gif"},{"isSeparator":true,"cssClass":"rtbSeparator"},{"value":"f","enabled":false,"cssClass":"rtbWrap","imageUrl":"images/first_h.gif","disabledImageUrl":"images/first.gif"},{"value":"p","enabled":false,"cssClass":"rtbWrap","imageUrl":"images/previous_h.gif","disabledImageUrl":"images/previous.gif"},{"value":"n","cssClass":"rtbWrap","imageUrl":"images/next_h.gif","disabledImageUrl":"images/next.gif","imagePosition":1},{"value":"l","cssClass":"rtbWrap","imageUrl":"images/last_h.gif","disabledImageUrl":"images/last.gif","imagePosition":1},{"isSeparator":true,"cssClass":"rtbSeparator"},{"value":"a","toolTip":"Get Adbode Acrobat Reader","cssClass":"rtbWrap","imageUrl":"images/acrobat.gif"},{"isSeparator":true,"cssClass":"rtbSeparator"},{"value":"x","checkOnClick":true,"allowSelfUnCheck":true,"cssClass":"rtbWrap","imageUrl":"images/Redaction.gif"},{"isSeparator":true,"cssClass":"rtbSeparator"},{"value":"c","toolTip":"Close this window","cssClass":"rtbWrap","imageUrl":"images/Actions-window-close-icon.png"}]}, {"buttonClicked":buttonClicked}, null, $get("rdToolBar"));
});
Sys.Application.add_init(function() {
    $create(Telerik.Web.UI.RadPane, {"_collapsedDirection":1,"_expandedSize":0,"_height":"35px","_originalHeight":"35px","_originalWidth":"","_scrollLeft":0,"_scrollTop":0,"_splitterOrientation":0,"clientStateFieldID":"Navigation_ClientState","maxHeight":70,"minHeight":30,"scrolling":4}, null, null, $get("Navigation"));
});
Sys.Application.add_init(function() {
    $create(Telerik.Web.UI.RadPane, {"_collapsedDirection":1,"_expandedSize":0,"_originalHeight":"","_originalWidth":"","_scrollLeft":0,"_scrollTop":0,"_splitterOrientation":0,"clientStateFieldID":"pdfview_ClientState","contentUrl":"/Web2/WebForms/PDF.aspx?page=1","minHeight":400,"minWidth":400}, null, null, $get("pdfview"));
});
Sys.Application.add_init(function() {
    $create(Telerik.Web.UI.RadSplitter, {"_attachResizeHandler":true,"_height":"100%","_isNested":false,"_orientation":0,"_registerWithScriptManager":true,"_width":"100%","clientStateFieldID":"rsplMain_ClientState"}, null, null, $get("rsplMain"));
});
Sys.Application.add_init(function() {
    $create(Telerik.Web.UI.RadWindow, {"_dockMode":false,"behaviors":4,"clientStateFieldID":"wVor_ClientState","formID":"form1","iconUrl":"images/Info-icon.png","minimizeIconUrl":"images/Info-icon.png","modal":true,"name":"wVor","skin":"Default","visibleStatusbar":false}, null, null, $get("wVor"));
});
Sys.Application.add_init(function() {
    $create(Telerik.Web.UI.RadWindowManager, {"behaviors":4,"clientStateFieldID":"rdWindowMgr_ClientState","formID":"form1","iconUrl":"images/Info-icon.png","minimizeIconUrl":"images/Info-icon.png","modal":true,"name":"rdWindowMgr","skin":"Default","visibleStatusbar":false,"windowControls":"['wVor']"}, null, {"child":"wVor"}, $get("rdWindowMgr"));
});
//]]&gt;
</script>
</form>


<script type="text/javascript">
    $('#divSpin').fadeOut(2000);
</script>
</body></html>