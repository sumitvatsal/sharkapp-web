﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SharkAppWeb.Common
{
    public class Settings
    {
        public static string GetValue(string key)
        {
            return WebConfigurationManager.AppSettings[key];
        }

        public static string Username
        {
            get { return GetValue("username"); }
        }

        public static string Password
        {
            get { return GetValue("password"); }
        }

        public static string Url
        {
            get { return GetValue("url"); }
        }
        public static string Url1
        {
            get { return GetValue("url1"); }
        }
       

        public static string uname
        {
            get { return GetValue("uname"); }
        }
        public static string pass
        {
            get { return GetValue("pass"); }
        }


        public static string u1
        {
            get { return GetValue("u1"); }
        }
        public static string u2
        {
            get { return GetValue("u2"); }
        }
    }
}