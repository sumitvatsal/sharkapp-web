﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Text.RegularExpressions;

using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using SharkAppWeb.Models;
using System.Reflection;
using System.Collections.Generic;
using System.Collections.Specialized;

using System.Xml;
using System.Security.Cryptography;



namespace SharkAppWeb.Common
{
    public class IEBrowser : System.Windows.Forms.ApplicationContext
    {
       
        Thread thrd;
        string FolioUrl;
        string FinalHtml;
        //string ReportID;
        //string FinalUrl;
        //string htmlInputTable;
        //string htmlScriptTable;
        //string chkPostSetting;
        //int loginCount;
        int navigationCounter;
        string html = string.Empty;
        WebBrowser ieBrowser;
        Form form;
        //int index = 0;
        int radioValue = 0;
        string _Streetnumber;
        string _StreetDirection;
        string _StreetName;
        string _StreetType;
        string _StreetCity;
        string _StreetPostDir;
        string _StreetUnitNumber;

        public string HtmlResult
        {
            get { return FinalHtml; }
        }

        public IEBrowser(bool visible, AutoResetEvent resultEvent, string Url, int radioVal, string PropertyAddress)
        {
            FolioUrl = Url;
            if(radioVal==2){
                 string[] _address = PropertyAddress.Split(',');
                 _Streetnumber = _address[0];
                 _StreetDirection = _address[1];
                 _StreetName = _address[2];
                 _StreetType = _address[3];
                 _StreetPostDir=_address[4];
                 _StreetUnitNumber = _address[5];
                 _StreetCity = _address[6];
            }
            radioValue=radioVal;
            thrd = new Thread(new ThreadStart(
               delegate
               {
                   Init(visible);

                   System.Windows.Forms.Application.Run(this);

               }));
            thrd.SetApartmentState(ApartmentState.STA);
            thrd.Start();
        }

        [DllImport("wininet.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool InternetSetCookie(string lpszUrlName, string lbszCookieName, string lpszCookieData);
        [ComVisible(true)]
        private void Init(bool visible)
        {

            ieBrowser = new WebBrowser();

            ieBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(IEBrowser_DocumentCompleted);
            ieBrowser.Navigating += new WebBrowserNavigatingEventHandler(IEBrowser_Navigating);
            if (visible)
            {
                form = new System.Windows.Forms.Form();
                ieBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
                form.Controls.Add(ieBrowser);
            }

            ieBrowser.ScriptErrorsSuppressed = true;
            //loginCount = 0;
            navigationCounter = 0;
            ieBrowser.AllowNavigation = true;
            ieBrowser.Navigate(FolioUrl);

        }


        void IEBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            string url = FolioUrl;

            try
            {
                if (ieBrowser.Document != null)
                {
                    if (radioValue == 1)
                    {
                        HtmlElementCollection collections = ieBrowser.Document.GetElementsByTagName("body");
                        foreach (System.Windows.Forms.HtmlElement collection in collections)
                        {
                            FinalHtml = collection.InnerHtml;
                        }
                    }
                    else  if (radioValue == 3)
                    {
                        HtmlElementCollection collections = ieBrowser.Document.GetElementsByTagName("body");
                        foreach (System.Windows.Forms.HtmlElement collection in collections)
                        {
                            FinalHtml = collection.InnerHtml;
                        }
                    }
                       
                    else
                    {
                        HtmlElementCollection collections = ieBrowser.Document.GetElementsByTagName("body");
                        foreach (System.Windows.Forms.HtmlElement collection in collections)
                        {
                            FinalHtml = collection.InnerHtml;
                            HtmlElementCollection Inputs = ieBrowser.Document.GetElementsByTagName("input");
                            foreach (System.Windows.Forms.HtmlElement _input in Inputs)
                            {
                                if (_input.GetAttribute("name") == "Situs_Street_Number")
                                {
                                    _input.InnerText = _Streetnumber;
                                }
                                if (_input.GetAttribute("name") == "Situs_Street_Name")
                                {
                                    _input.InnerText = _StreetName;
                                }
                                if (_input.GetAttribute("name") == "Situs_Unit_Number")
                                {
                                    _input.InnerText = _StreetUnitNumber;
                                }

                            }

                            HtmlElementCollection Select = ieBrowser.Document.GetElementsByTagName("select");
                            foreach (System.Windows.Forms.HtmlElement _select in Select)
                            {
                                if (_select.GetAttribute("name") == "Situs_Street_Direction")
                                {
                                    _select.InnerText = _StreetDirection;
                                }
                                if (_select.GetAttribute("name") == "Situs_Street_Type")
                                {
                                    _select.InnerText = _StreetType;
                                }
                                if (_select.GetAttribute("name") == "Situs_City")
                                {
                                    _select.InnerText = _StreetCity;
                                }
                                if (_select.GetAttribute("name") == "Situs_Street_Post_Dir")
                                {
                                    _select.InnerText = _StreetPostDir;
                                }

                            }
                            HtmlElementCollection Forms = ieBrowser.Document.GetElementsByTagName("form");
                            foreach (System.Windows.Forms.HtmlElement _forms in Forms)
                            {
                                if (_forms.GetAttribute("name") == "homeind")
                                {
                                    _forms.InvokeMember("submit");
                                    break;
                                }
                            }
                            break;
                        }
                    }


                    if (e.Url.AbsolutePath.Equals("/RecInfo.asp"))
                    {
                        HtmlElementCollection collections = ieBrowser.Document.GetElementsByTagName("body");
                        foreach (System.Windows.Forms.HtmlElement collection in collections)
                        {
                            FinalHtml = collection.InnerHtml;
                            break;
                        }
                    }
                    else if(e.Url.AbsoluteUri.Equals("http://www.bcpa.net/RecSearch.asp")){
                         FinalHtml = "Incomplete Address";
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        void IEBrowser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            navigationCounter++;
            if (form != null) form.Text = e.Url.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            if (thrd != null)
            {
                thrd.Abort();
                thrd = null;
                return;
            }
            System.Runtime.InteropServices.Marshal.Release(ieBrowser.Handle);
            ieBrowser.Dispose();
            if (form != null) form.Dispose();
            base.Dispose(disposing);
        }
    }
}
