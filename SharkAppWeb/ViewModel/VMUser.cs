﻿using SharkAppWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SharkAppWeb.ViewModel
{
    public class VMUser
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string UserPassword { get; set; }
        public Nullable<bool> IsActive { get; set; }


        public VMUser LoginUser(string userEmail, string userPassword)
        {
            VMUser user = new VMUser();
            using(var db =   new BrowardclerkEntities())
            {
                user = db.tblUsers.AsEnumerable().Where(w => w.UserEmail == userEmail && w.UserPassword == userPassword && w.IsActive == true).Select(s => new VMUser
                {
                    IsActive = s.IsActive,
                    UserEmail = s.UserEmail,
                    UserId = s.UserId,
                    UserName = s.UserName,
                    UserPassword = s.UserPassword
                }).FirstOrDefault();
            }

            return user;
        }
    }
}