﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SharkAppWeb;
using SharkAppWeb.Models;
using System.Data.SqlClient;
using System.Data;
using System.Data.Objects;
using System.Globalization;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using System.Configuration;

namespace SharkAppWeb.ViewModel
{
    public class VMCase
    {
        BrowardclerkEntities db = new BrowardclerkEntities();
        public int CaseID { get; set; }
        public string CaseNumber { get; set; }
        public string CaseStyle { get; set; }
        public string CaseType { get; set; }
        public DateTime FilingDate { get; set; }
        public Boolean CaseStatus { get; set; }
        public List<Defandant> defandantList { get; set; }
        public string StateRptnumber { get; set; }
        public string CaseStatusT { get; set; }
        public string CourtType { get; set; }
        public string CourtLocation { get; set; }
        public string JudgeIdName { get; set; }
        public string MagistrateIdName { get; set; }
        public Nullable<DateTime> SearchedDate { get; set; }
        public string Plantiff { get; set; }
        public string AttornyAddress { get; set; }
        public List<SearchedPdf> searchPDFs { get; set; }
        public List<Citation> CitationList { get; set; }

        public List<tblAllPagesAddress> tblAllPagesAddress { get; set; }

        public void SaveSeacrhHistory(DateTime? SearchDateTo, DateTime? SearchDateFrom, String SearchCourtType, DateTime SearchedDate, string BusinessName)
        {

            db.sp_InsertSearchHistory(SearchDateTo, SearchDateFrom, SearchCourtType, SearchedDate, BusinessName);

        }

        public void SaveCaseSearchHistory(DateTime CaseSearchDate, String CaseNumber, String CaseStyle, String CaseType, DateTime Fillingdate, String CaseStatus, string SearchType)
        {
            db.sp_InsertCaseNumberHistory(CaseNumber, CaseSearchDate, CaseStyle, CaseType, Fillingdate, CaseStatus, null, true, SearchType);

        }

        public void SaveCitationSeacrhHistory(DateTime CitationSearchDate, String CitationNumber, String CaseNumber, String CaseStyle, String CaseType, DateTime Fillingdate, String CaseStatus)
        {

            db.sp_InsertCitationNumberHistory(CitationNumber, CitationSearchDate, CaseNumber, CaseStyle, CaseType, Fillingdate, CaseStatus);

        }

        public void SavePartySeacrhHistory(string PartyFirstName, string PartyLastName, DateTime? PartyDateTo, DateTime? PartyDateFrom, String PartyCourtType, DateTime PartySearchedDate)
        {

            db.sp_InsertPartySearchHistory(PartyFirstName, PartyLastName, PartyCourtType, PartyDateFrom, PartyDateTo, PartySearchedDate);

        }

        public void SaveCaseDetials(VMCase _case)
        {
            ObjectParameter _param_caseID = new ObjectParameter("CaseID", typeof(int));

            //_param_caseID.ParameterType.o = ParameterDirection.Output;
            db.sp_InsertUpdateCaseDetials(_case.CaseNumber, _case.CaseType, _case.FilingDate, _case.CaseStatusT, _case.CourtType, _case.CourtLocation, _case.JudgeIdName, _case.MagistrateIdName, _case.Plantiff, _case.AttornyAddress, _case.StateRptnumber, _case.SearchedDate, _param_caseID);
            int caseid = Convert.ToInt32(_param_caseID.Value.ToString());
            var _Defandantlst = db.tblDefandants.Where(w => w.CaseID == caseid).ToList();
            if (_Defandantlst != null)
            {
                foreach (var item in _Defandantlst)
                {

                    db.tblDefandants.Remove(item);
                    db.SaveChanges();
                }
            }
            foreach (var def in _case.defandantList)
            {
                tblDefandant _newDefandant = new tblDefandant();
                _newDefandant.CaseNumber = def.CaseNumber;
                _newDefandant.PartyName = def.PartyName;
                _newDefandant.CaseID = caseid;
                db.tblDefandants.Add(_newDefandant);
                db.SaveChanges();

            }
            if (_case.CitationList != null)
            {
                var _Citationlst = db.tblCitations.Where(w => w.CaseID == caseid).ToList();
                if (_Citationlst != null)
                {
                    foreach (var item in _Citationlst)
                    {
                        db.tblCitations.Remove(item);
                        db.SaveChanges();
                    }
                }
                foreach (var Citn in _case.CitationList)
                {
                    tblCitation _newCitation = new tblCitation();
                    _newCitation.CitaionNumber = Citn.CitaionNumber;
                    _newCitation.OffenseDate = Citn.OffenseDate;
                    _newCitation.CaseID = caseid;
                    db.tblCitations.Add(_newCitation);
                    db.SaveChanges();
                }
            }
        }

        public void SavePalmBeachcasesList(DataTable _dtCaseView)
        {
            try
            {
                foreach (DataRow _dr in _dtCaseView.Rows)
                {
                    string CaseNumber = _dr["CaseNumber"].ToString();
                    string CourtType = _dr["CourtType"].ToString();
                    string CaseType = _dr["CaseType"].ToString();
                    string ArrestDate = _dr["ArrestDate"].ToString();
                    string FileDate = _dr["FileDate"].ToString();
                    string PartyType = _dr["PartyType"].ToString();
                    string FullName = _dr["FullName"].ToString();
                    string DOB = _dr["DOB"].ToString();
                    string Status = _dr["Status"].ToString();
                    db.Sp_InsertPalmCaseViewList(CaseNumber, CourtType, CaseType, ArrestDate, FileDate, PartyType, FullName, DOB, Status);
                }
            }
            catch
            {

            }
        }

        public void SavePalmPbcDetails(Int64 PalmBeachLPCaseID, string PCN, string OfficialBook, string SaleDate, string Subdiv, string LegalDesc, DataTable _dtSaleinfo, string Units, string TotalSqft, string Acres, string Usecode, string Zoning, DataTable _dtAppraisal, DataTable _dtAssesed, DataTable _dtTaxes, string LocationAddress, string MailingAddress)
        {
            try
            {
                int CntPCBPalm = db.tblPCBPalmDetails.AsEnumerable().Where(x => x.PalmBeachLPCaseID == PalmBeachLPCaseID).Count();
                if (CntPCBPalm == 0)
                {
                    ObjectParameter _param_PCBPalmDetailsID = new ObjectParameter("PCBPalmDetailsID", typeof(Int64));
                   // db.Sp_InsertPalmPCBDetails(PalmBeachLPCaseID, PCN, OfficialBook, SaleDate, Subdiv, LegalDesc, Units, TotalSqft, Acres, Usecode, Zoning, LocationAddress, MailingAddress, true, _param_PCBPalmDetailsID);
                    Int64 PCBPalmDetailsID = Convert.ToInt64(_param_PCBPalmDetailsID.Value.ToString());
                    if (PCBPalmDetailsID > 0)
                    {
                        tblPCBSaleInfo _newtblPCBSaleInfo = new tblPCBSaleInfo();
                        foreach (DataRow _tblAddr in _dtSaleinfo.Rows)
                        {
                            _newtblPCBSaleInfo.PCBPalmDetailsID = PCBPalmDetailsID;
                            _newtblPCBSaleInfo.SaleDate = _tblAddr["SaleDate"].ToString();
                            _newtblPCBSaleInfo.Price = _tblAddr["Price"].ToString();
                            _newtblPCBSaleInfo.BookPage = _tblAddr["BookPage"].ToString();
                            _newtblPCBSaleInfo.SaleType = _tblAddr["SaleType"].ToString();
                            _newtblPCBSaleInfo.Owner = _tblAddr["Owner"].ToString();
                            db.tblPCBSaleInfoes.Add(_newtblPCBSaleInfo);
                            db.SaveChanges();
                        }
                        tblPCBAppraisal _newtblPCBAppraisal = new tblPCBAppraisal();
                        foreach (DataRow _tblAddr in _dtAppraisal.Rows)
                        {
                            _newtblPCBAppraisal.PCBPalmDetailsID = PCBPalmDetailsID;
                            _newtblPCBAppraisal.TaxYear = _tblAddr["TaxYear"].ToString();
                            _newtblPCBAppraisal.ImprovementVal = _tblAddr["ImprovementVal"].ToString();
                            _newtblPCBAppraisal.LandValue = _tblAddr["LandValue"].ToString();
                            _newtblPCBAppraisal.MarketValue = _tblAddr["MarketValue"].ToString();
                            db.tblPCBAppraisals.Add(_newtblPCBAppraisal);
                            db.SaveChanges();
                        }

                        tblPCBTaxAssesed _newtblPCBTaxAssesed = new tblPCBTaxAssesed();
                        foreach (DataRow _tblAddr in _dtAssesed.Rows)
                        {
                            _newtblPCBTaxAssesed.PCBPalmDetailsID = PCBPalmDetailsID;
                            _newtblPCBTaxAssesed.TaxAssdYear = _tblAddr["TaxAssdYear"].ToString();
                            _newtblPCBTaxAssesed.AssesedVal = _tblAddr["AssesedVal"].ToString();
                            _newtblPCBTaxAssesed.ExemptionAmt = _tblAddr["ExemptionAmt"].ToString();
                            _newtblPCBTaxAssesed.TaxableValue = _tblAddr["TaxableValue"].ToString();
                            db.tblPCBTaxAsseseds.Add(_newtblPCBTaxAssesed);
                            db.SaveChanges();
                        }
                        tblPCBTotalTax _newtblPCBTotalTax = new tblPCBTotalTax();
                        foreach (DataRow _tblAddr in _dtTaxes.Rows)
                        {
                            _newtblPCBTotalTax.PCBPalmDetailsID = PCBPalmDetailsID;
                            _newtblPCBTotalTax.TaxesTax = _tblAddr["TaxesTax"].ToString();
                            _newtblPCBTotalTax.AdValorem = _tblAddr["AdValorem"].ToString();
                            _newtblPCBTotalTax.NonAdValorem = _tblAddr["NonAdValorem"].ToString();
                            _newtblPCBTotalTax.TotalTaxes = _tblAddr["TotalTaxes"].ToString();
                            db.tblPCBTotalTaxes.Add(_newtblPCBTotalTax);
                            db.SaveChanges();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                string exec = ex.ToString();


            }
        }

        public string InsertsrchDefendant(String DocumentType, String RecordDate, String Plaintiff, String Defendant, String BookType, String CaseNumber, String RefNum, String _Url, bool IsResidential, DateTime _SearchedDeffDate)
        {
            string Response = "";
            ObjectParameter _param_DefendantID = new ObjectParameter("srchDefendantIDOut", typeof(int));

           // db.Sp_InsertUSrchDefandant(DocumentType, Plaintiff, Defendant, BookType, CaseNumber, RefNum, _SearchedDeffDate, _Url, IsResidential, RecordDate, true, true, true, _param_DefendantID);
            Response = "Success";

            return Response;
        }

        public string InsertOtherPropertyDetails(int SrchDefendantID, string _SiteAddress, string _PropertOwner, string _mailingAddress, string _FolioID, string _Milage, string _use, string _LegalDesc, string LandFactor, string AdvBldgSF, string UnitsBEDBath, string FolioSiteLink, DataTable _dt, bool Compareable, DataTable _dtSaleHistory, DataTable _dtTaxable, string CardUrl, string sketchUrl, string _spliturl,string latitude,string longitude)
        {
            string Response = "";
            ObjectParameter _param_AddressID = new ObjectParameter("OTHAddressID", typeof(int));
            DateTime _SearchedDate = DateTime.Now;
          //  db.sp_InsertUOtherProperty(_param_AddressID, SrchDefendantID, _SiteAddress, _PropertOwner, _mailingAddress, _LegalDesc, _FolioID, _Milage, _use, LandFactor, AdvBldgSF, UnitsBEDBath, FolioSiteLink, _SearchedDate, Compareable, CardUrl, sketchUrl, _spliturl,latitude,longitude);
            int _OTHAddressID = Convert.ToInt32(_param_AddressID.Value.ToString());
            if (_OTHAddressID != 0)//null)
            {
                tblOtherAssessedValue _newtblAssesedValue = new tblOtherAssessedValue();
                foreach (DataRow _tblAddr in _dt.Rows)
                {
                    _newtblAssesedValue.OTHAddressID = _OTHAddressID;
                    _newtblAssesedValue.OthYear = Convert.ToInt32(_tblAddr["Year"].ToString().Trim());
                    _newtblAssesedValue.OthLand = _tblAddr["Land"].ToString().Trim();
                    _newtblAssesedValue.OthBuilding = _tblAddr["Building"].ToString().Trim();
                    _newtblAssesedValue.OthMarketValue = _tblAddr["MarketValue"].ToString();
                    _newtblAssesedValue.OthAssessedValue = _tblAddr["AssessedValue"].ToString().Trim();
                    _newtblAssesedValue.OthAssessedTax = _tblAddr["AssessedTax"].ToString().Trim();
                    db.tblOtherAssessedValues.Add(_newtblAssesedValue);
                    db.SaveChanges();
                }

                tblOtherSaleHistory _newtblSaleHistory = new tblOtherSaleHistory();
                foreach (DataRow _tblsale in _dtSaleHistory.Rows)
                {
                    if (_tblsale["SaleType"].ToString().Contains("WD"))
                    {
                        _newtblSaleHistory.OTHAddressID = _OTHAddressID;
                        _newtblSaleHistory.OthSaleDate = Convert.ToDateTime(_tblsale["SaleDate"].ToString().Trim());
                        _newtblSaleHistory.OthSaleType = _tblsale["SaleType"].ToString().Trim();
                        _newtblSaleHistory.OthSalePrice = _tblsale["SalePrice"].ToString().Trim();
                        _newtblSaleHistory.OthSaleBookPage = _tblsale["SaleBookPage"].ToString().Trim();
                        db.tblOtherSaleHistories.Add(_newtblSaleHistory);
                        db.SaveChanges();
                    }
                }

                tblExemptionTAX _newtExempTAX = new tblExemptionTAX();
                foreach (DataRow _tbltax in _dtTaxable.Rows)
                {
                    _newtExempTAX.OTHAddressID = _OTHAddressID;
                    _newtExempTAX.EXTitle = _tbltax["Title"].ToString().Trim();
                    _newtExempTAX.EXCounty = _tbltax["County"].ToString().Trim();
                    _newtExempTAX.EXSchoolBoard = _tbltax["SchoolBoard"].ToString().Trim();
                    _newtExempTAX.EXMuncipal = _tbltax["Muncipal"].ToString().Trim();
                    _newtExempTAX.EXIndependent = _tbltax["Independent"].ToString().Trim();
                    db.tblExemptionTAXes.Add(_newtExempTAX);
                    db.SaveChanges();

                }

                //if (Compareable == false)
                //{
                //    tblCompareProperty _newttblCompareProperty = new tblCompareProperty();
                //    _newttblCompareProperty.CmpFolioByID = CmpAddressID;
                //    _newttblCompareProperty.CmpFloioToID = _AddressID;
                //    _newttblCompareProperty.CompareDate = CompareDate;
                //    db.tblCompareProperties.Add(_newttblCompareProperty);
                //    db.SaveChanges();
                //}
                Response = "Success";
            }
            return Response;

        }

        public string InsertAssessedDetails(int? CmpAddressID, DateTime? CompareDate, string _SiteAddress, string _PropertOwner, string _mailingAddress, string _FolioID, string _Milage, string _use, string _LegalDesc, string LandFactor, string AdvBldgSF, string UnitsBEDBath, string FolioSiteLink, DataTable _dt, bool Compareable, DataTable _dtSaleHistory)
        {
            string Response = "";
            ObjectParameter _param_AddressID = new ObjectParameter("AddressID", typeof(int));
            DateTime _SearchedDate = DateTime.Now;
            db.sp_InsertUPropertyFolio(_param_AddressID, _SiteAddress, _PropertOwner, _mailingAddress, _LegalDesc, _FolioID, _Milage, _use, LandFactor, AdvBldgSF, UnitsBEDBath, FolioSiteLink, _SearchedDate, Compareable, null, null, null, null);
            int _AddressID = Convert.ToInt32(_param_AddressID.Value.ToString());
            if (_AddressID != 0)//null)
            {
                tblAssesedValue _newtblAssesedValue = new tblAssesedValue();
                foreach (DataRow _tblAddr in _dt.Rows)
                {
                    _newtblAssesedValue.AddressID = _AddressID;
                    _newtblAssesedValue.Year = Convert.ToInt32(_tblAddr["Year"].ToString());
                    _newtblAssesedValue.Land = _tblAddr["Land"].ToString();
                    _newtblAssesedValue.Building = _tblAddr["Building"].ToString();
                    _newtblAssesedValue.MarketValue = _tblAddr["MarketValue"].ToString();
                    _newtblAssesedValue.AssessedValue = _tblAddr["AssessedValue"].ToString();
                    _newtblAssesedValue.AssessedTax = _tblAddr["AssessedTax"].ToString();
                    db.tblAssesedValues.Add(_newtblAssesedValue);
                    db.SaveChanges();
                }

                tblSaleHistory _newtblSaleHistory = new tblSaleHistory();
                foreach (DataRow _tblsale in _dtSaleHistory.Rows)
                {
                    if (_tblsale["SaleType"].ToString().Contains("WD"))
                    {
                        _newtblSaleHistory.AddressID = _AddressID;
                        _newtblSaleHistory.SaleDate = Convert.ToDateTime(_tblsale["SaleDate"].ToString());
                        _newtblSaleHistory.SaleType = _tblsale["SaleType"].ToString();
                        _newtblSaleHistory.SalePrice = _tblsale["SalePrice"].ToString();
                        _newtblSaleHistory.SaleBookPage = _tblsale["SaleBookPage"].ToString();
                        db.tblSaleHistories.Add(_newtblSaleHistory);
                        db.SaveChanges();
                    }
                }
                if (Compareable == false)
                {
                    tblCompareProperty _newttblCompareProperty = new tblCompareProperty();
                    _newttblCompareProperty.CmpFolioByID = CmpAddressID;
                    _newttblCompareProperty.CmpFloioToID = _AddressID;
                    _newttblCompareProperty.CompareDate = CompareDate;
                    db.tblCompareProperties.Add(_newttblCompareProperty);
                    db.SaveChanges();
                }
                Response = "Success";
            }
            return Response;

        }

        public void SavePalmBeach(DataTable _dt)
        {
            try
            {
                var Palmdetails = new SqlParameter("tblPalmBeach", SqlDbType.Structured);
                Palmdetails.Value = _dt;
                Palmdetails.TypeName = "TypePalmBeach";  ///type 
                db.Database.ExecuteSqlCommand("EXEC Sp_InsertPalmBeachCase @tblPalmBeach", Palmdetails);

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        public void SaveOtherBCPARecords(int SubJectAddressID, string[] Rowhtml)
        {
            DateTime _SearchedDate = DateTime.Now;
            for (int i = 0; i < Rowhtml.Length; i++)
            {
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(Rowhtml[i].ToString());

                string _FolioID = "";
                string _SiteAddress = "";
                string _PropertOwner = "";
                string _mailingAddress = "";
                string _LegalDesc = "";
                string _Milage = "";
                string _use = "";
                string LandFactor = "";
                string AdvBldgSF = "";
                string UnitsBEDBath = "";
                string FolioSiteLink = "";
                string SalePrice = "";
                string MarketSqFt = "";
                string SalePricePrSqFt = "";
                string AssessedValue = "";

                string _SALEDATE = "";
                string _DEEDTYPE = "";
                string _SALECIN = "";



                bool Compareable = false;
                int _countColumn = 1;
                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//tr/td"))
                {
                    var text = node.InnerText;
                    var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    switch (_countColumn)
                    {
                        case 1:
                            _FolioID = _text;
                            FolioSiteLink = "http://www.bcpa.net/RecInfo.asp?URL_Folio=" + _text;
                            break;
                        case 2:
                            _SiteAddress = _text;
                            break;
                        case 3:
                            LandFactor = _text;
                            break;
                        case 4:
                            AdvBldgSF = _text;
                            break;
                        case 5:
                            SalePrice = _text;
                            break;
                        case 6:
                            MarketSqFt = _text;
                            break;
                        case 7:
                            SalePricePrSqFt = _text;
                            break;

                        case 8:
                            AssessedValue = _text;
                            break;
                        case 9:
                            _mailingAddress = _text;
                            break;
                        case 10:
                            _PropertOwner = _text;
                            break;
                        case 11:
                            _LegalDesc = _text;
                            break;
                        case 12:
                            UnitsBEDBath = _text;
                            break;
                        case 13:
                            _Milage = _text;
                            break;
                        case 14:
                            _use = _text;
                            break;
                        case 15:
                            _SALEDATE = _text;
                            break;
                        case 16:
                            _DEEDTYPE = _text;
                            break;
                        case 17:
                            _SALECIN = _text;
                            break;

                    }
                    _countColumn++;
                }

                ObjectParameter _param_AddressID = new ObjectParameter("OTHAddressID", typeof(int));

              //  db.sp_InsertUOtherProperty(_param_AddressID, null, _SiteAddress, _PropertOwner, _mailingAddress, _LegalDesc, _FolioID, _Milage, _use, LandFactor, AdvBldgSF, UnitsBEDBath, FolioSiteLink, _SearchedDate, Compareable, null, null, null);
                int _AddressID = Convert.ToInt32(_param_AddressID.Value.ToString());
                if (_AddressID != 0)//null)
                {
                    tblOtherAssessedValue _newtblAssesedValue = new tblOtherAssessedValue();
                    _newtblAssesedValue.OTHAddressID = _AddressID;
                    _newtblAssesedValue.OthYear = Convert.ToInt32(_SearchedDate.Year);
                    _newtblAssesedValue.OthMarketValue = MarketSqFt;
                    _newtblAssesedValue.OthAssessedValue = AssessedValue;
                    db.tblOtherAssessedValues.Add(_newtblAssesedValue);
                    db.SaveChanges();


                    tblOtherSaleHistory _newtblSaleHistory = new tblOtherSaleHistory();
                    _newtblSaleHistory.OTHAddressID = _AddressID;
                    _newtblSaleHistory.OthSaleDate = Convert.ToDateTime(_SALEDATE);
                    _newtblSaleHistory.OthSaleType = _DEEDTYPE;
                    _newtblSaleHistory.OthSalePrice = SalePrice;
                    _newtblSaleHistory.OthSaleBookPage = _SALECIN;
                    db.tblOtherSaleHistories.Add(_newtblSaleHistory);
                    db.SaveChanges();

                    if (Compareable == false)
                    {
                        tblOTHCompareFolio _newttblCompareProperty = new tblOTHCompareFolio();
                        _newttblCompareProperty.OTHCmpFolioByID = SubJectAddressID;
                        _newttblCompareProperty.OTHCmpFloioToID = _AddressID;
                        _newttblCompareProperty.OTHCmpDate = _SearchedDate;
                        db.tblOTHCompareFolios.Add(_newttblCompareProperty);
                        db.SaveChanges();
                    }
                }
            }
        }

        public void SaveDefaultProperty(int SubJectAddressID, int OTHAddressID, string _SiteAddress)
        {
            if (SubJectAddressID != 0)//null)
            {
                var _newSrchDefandant = db.tblSrchDefandants.Find(SubJectAddressID);
                if (_newSrchDefandant != null)
                {
                    _newSrchDefandant.BCPaAddress = _SiteAddress;
                    _newSrchDefandant.IsGetAddress = false;
                    _newSrchDefandant.IsSrchProperty = false;
                    _newSrchDefandant.IsNewsCase = false;
                    db.Entry(_newSrchDefandant).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            var _newOtherProperty = db.tblOtherProperties.Find(OTHAddressID);
            if (_newOtherProperty != null)
            {
                _newOtherProperty.IsOTHCorrectProperty = true;
                db.Entry(_newOtherProperty).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void SaveBCPARecords(int SubJectAddressID, string[] Rowhtml, bool Compareable, int? ChkBcpa)
        {
            DateTime _SearchedDate = DateTime.Now;
            for (int i = 0; i < Rowhtml.Length; i++)
            {
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(Rowhtml[i].ToString());

                string _FolioID = "";
                string _SiteAddress = "";
                string _PropertOwner = "";
                string _mailingAddress = "";
                string _LegalDesc = "";
                string _Milage = "";
                string _use = "";
                string LandFactor = "";
                string AdvBldgSF = "";
                string UnitsBEDBath = "";
                string FolioSiteLink = "";
                string SalePrice = "";
                string AssedMarketSqFt = "";
                string SalePricePrSqFt = "";

                string _SALEDATE = "";
                string _DEEDTYPE = "";
                string _SALECIN = "";




                int _countColumn = 1;
                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//tr/td"))
                {
                    var text = node.InnerText;
                    var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    switch (_countColumn)
                    {
                        case 1:
                            _FolioID = _text;
                            FolioSiteLink = "http://www.bcpa.net/RecInfo.asp?URL_Folio=" + _text;
                            break;
                        case 2:
                            _SiteAddress = _text;
                            break;
                        case 3:
                            LandFactor = _text;
                            break;
                        case 4:
                            AdvBldgSF = _text;
                            break;
                        case 5:
                            SalePrice = _text;
                            break;
                        case 6:
                            AssedMarketSqFt = _text;
                            break;
                        case 7:
                            SalePricePrSqFt = _text;
                            break;
                        case 8:
                            _mailingAddress = _text;
                            break;
                        case 9:
                            _PropertOwner = _text;
                            break;
                        case 10:
                            _LegalDesc = _text;
                            break;
                        case 11:
                            UnitsBEDBath = _text;
                            break;
                        case 12:
                            _Milage = _text;
                            break;
                        case 13:
                            _use = _text;
                            break;
                        case 14:
                            _SALEDATE = _text;
                            break;
                        case 15:
                            _DEEDTYPE = _text;
                            break;
                        case 16:
                            _SALECIN = _text;
                            break;

                    }
                    _countColumn++;
                }

                ObjectParameter _param_AddressID = new ObjectParameter("AddressID", typeof(int));

                db.sp_InsertUPropertyFolio(_param_AddressID, _SiteAddress, _PropertOwner, _mailingAddress, _LegalDesc, _FolioID, _Milage, _use, LandFactor, AdvBldgSF, UnitsBEDBath, FolioSiteLink, _SearchedDate, Compareable, SubJectAddressID, null, null, null);
                int _AddressID = Convert.ToInt32(_param_AddressID.Value.ToString());
                if (_AddressID !=0)// null)
                {
                    tblAssesedValue _newtblAssesedValue = new tblAssesedValue();
                    _newtblAssesedValue.AddressID = _AddressID;
                    _newtblAssesedValue.Year = Convert.ToInt32(_SearchedDate.Year);
                    _newtblAssesedValue.MarketValue = AssedMarketSqFt;
                    db.tblAssesedValues.Add(_newtblAssesedValue);
                    db.SaveChanges();


                    tblSaleHistory _newtblSaleHistory = new tblSaleHistory();
                    _newtblSaleHistory.AddressID = _AddressID;
                    _newtblSaleHistory.SaleDate = Convert.ToDateTime(_SALEDATE);
                    _newtblSaleHistory.SaleType = _DEEDTYPE;
                    _newtblSaleHistory.SalePrice = SalePrice;
                    _newtblSaleHistory.SaleBookPage = _SALECIN;
                    db.tblSaleHistories.Add(_newtblSaleHistory);
                    db.SaveChanges();

                    if (ChkBcpa != null)
                    {
                        var _newSrchDefandant = db.tblSrchDefandants.Find(SubJectAddressID);
                        if (_newSrchDefandant != null)
                        {
                            _newSrchDefandant.BCPaAddress = _SiteAddress;
                            _newSrchDefandant.IsGetAddress = false;
                            _newSrchDefandant.IsSrchProperty = false;
                            db.Entry(_newSrchDefandant).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }

                    if (Compareable == false)
                    {
                        tblCompareProperty _newttblCompareProperty = new tblCompareProperty();
                        _newttblCompareProperty.CmpFolioByID = SubJectAddressID;
                        _newttblCompareProperty.CmpFloioToID = _AddressID;
                        _newttblCompareProperty.CompareDate = _SearchedDate;
                        db.tblCompareProperties.Add(_newttblCompareProperty);
                        db.SaveChanges();
                    }
                }
            }
        }

        public string SaveCorrectPropertyAddress(int SrchDefendantID, int OthAddressID, string SiteAddress)
        {
            string Result = "";
            try
            {
                var _newSrchDefandant = db.tblSrchDefandants.Find(SrchDefendantID);
                if (_newSrchDefandant != null)
                {
                    _newSrchDefandant.BCPaAddress = SiteAddress;
                    _newSrchDefandant.IsSrchProperty = false;
                    db.Entry(_newSrchDefandant).State = EntityState.Modified;
                    db.SaveChanges();
                }
                var _newOTHSrch = db.tblOtherProperties.Find(OthAddressID);
                if (_newOTHSrch != null)
                {
                    _newOTHSrch.IsOTHCorrectProperty = true;
                    db.Entry(_newOTHSrch).State = EntityState.Modified;
                    db.SaveChanges();
                    Result = "Success";
                }
                return Result;
            }
            catch
            {
                return null;
            }


        }

        public void SaveLpSetAsDefault(int SrchDefendentID, int OTHAddressID, string _Text)
        {
            try
            {
                var _newSrchDefandant = db.tblSrchDefandants.Find(SrchDefendentID);
                if (_newSrchDefandant != null)
                {
                    _newSrchDefandant.BCPaAddress = _Text;
                    _newSrchDefandant.IsGetAddress = false;
                    _newSrchDefandant.IsSrchProperty = false;
                    _newSrchDefandant.IsNewsCase = false;
                    db.Entry(_newSrchDefandant).State = EntityState.Modified;
                    db.SaveChanges();
                }
                var _newOTHSrch = db.tblOtherProperties.Find(OTHAddressID);
                if (_newOTHSrch != null)
                {
                    _newOTHSrch.IsOTHCorrectProperty = true;
                    db.Entry(_newOTHSrch).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch
            {

            }
        }

        public void SaveAddressResult(VMCase _case, int? SrchDefendantID)
        {
            if (_case.searchPDFs != null && _case.searchPDFs.Count > 0)
            {
                tblAllPagesAddress _newAllPagesAddress = new tblAllPagesAddress();

                DataTable _dt = new DataTable();

                _dt.Columns.Add("ToDefendant", typeof(String));
                _dt.Columns.Add("LocatedAt", typeof(String));
                _dt.Columns.Add("OnDefendant", typeof(String));
                _dt.Columns.Add("ToWit", typeof(String));
                _dt.Columns.Add("SearchedAddressID", typeof(String));
                _dt.Columns.Add("CopyFurnished", typeof(String));
                _dt.Columns.Add("CopyFurnished1", typeof(String));
                _dt.Columns.Add("PrincipalBalance", typeof(String));
                _dt.Columns.Add("NewAddress", typeof(String));
                if (SrchDefendantID != null)
                {
                    SaveAddressRecords(_case, _dt, SrchDefendantID);
                }
                else
                {
                    SaveAddressList(_case, _newAllPagesAddress, _dt);
                }
            }
        }

        private void SaveAddressRecords(VMCase _case, DataTable _dt, int? SrchDefendantID)
        {
            foreach (var item in _case.searchPDFs)
            {
                if (item.pdfAddresslist != null && item.pdfAddresslist.Count > 0)
                {
                    foreach (var addr in item.pdfAddresslist)
                    {
                        tblNewAllAddress _tblNewAllAddress = new tblNewAllAddress();
                        if (addr.AddressType.ToString() == "Principal Balance:")
                        {
                            _tblNewAllAddress.PrincipalBalance = addr.Pdfaddress.ToString();
                        }
                        else
                        {
                            _tblNewAllAddress.Defendantaddress = addr.Pdfaddress.ToString();
                        }
                        _tblNewAllAddress.SrchDefendantID = SrchDefendantID;
                        db.tblNewAllAddresses.Add(_tblNewAllAddress);
                        db.SaveChanges();
                    }
                }
            }
        }

        private void SaveAddressList(VMCase _case, tblAllPagesAddress _newAllPagesAddress, DataTable _dt)
        {
            foreach (var item in _case.searchPDFs)
            {
                if (item.pdfAddresslist != null && item.pdfAddresslist.Count > 0)
                {
                    ObjectParameter _param_SearchedAddressID = new ObjectParameter("SearchedAddressID", typeof(int));
                    db.sp_InsertSrchAddressResult(item.CaseNumberID, item.CaseNumber, item.SerchedDate, item.Pdfname, _param_SearchedAddressID);
                    int SearchedAddressID = Convert.ToInt32(_param_SearchedAddressID.Value.ToString());
                    var _tblAllPagesAddresses = db.tblAllPagesAddresses.Where(w => w.SearchedAddressID == SearchedAddressID).ToList();
                    if (_tblAllPagesAddresses != null && _tblAllPagesAddresses.Count > 0)
                    {
                        foreach (var _allAdrs in _tblAllPagesAddresses)
                        {
                            db.tblAllPagesAddresses.Remove(_allAdrs);
                            db.SaveChanges();
                        }
                    }

                    DataRow dr = _dt.NewRow();
                    dr["SearchedAddressID"] = SearchedAddressID;
                    int _cnt = 1;
                    foreach (var addr in item.pdfAddresslist)
                    {
                        if (_cnt == 7)
                        {
                            _dt.Rows.Add(dr);
                            dr = _dt.NewRow();
                            _cnt = 1;
                        }
                        switch (_cnt)
                        {
                            case 1:
                                if (addr.AddressType.ToString() == "Principal Balance:")
                                {
                                    dr["PrincipalBalance"] = addr.Pdfaddress.ToString();

                                }
                                dr["ToDefendant"] = addr.Pdfaddress.ToString();
                                break;
                            case 2:
                                dr["OnDefendant"] = addr.Pdfaddress.ToString();
                                break;
                            case 3:
                                dr["LocatedAt"] = addr.Pdfaddress.ToString();
                                break;
                            case 4:
                                dr["ToWit"] = addr.Pdfaddress.ToString();
                                break;
                            case 5:
                                dr["CopyFurnished"] = addr.Pdfaddress.ToString();
                                break;
                            case 6:
                                dr["CopyFurnished1"] = addr.Pdfaddress.ToString();
                                break;
                        }


                        //switch (addr.AddressType.ToString())
                        //{
                        //    case "TO DEFENDANT:":
                        //        dr["ToDefendant"] = addr.Pdfaddress.ToString();
                        //        break;
                        //    case "On Defendant.":
                        //        dr["OnDefendant"] = addr.Pdfaddress.ToString();
                        //        break;
                        //    case "Located at:":
                        //        dr["LocatedAt"] = addr.Pdfaddress.ToString();
                        //        break;
                        //    case "TO-wit:":
                        //        dr["ToWit"] = addr.Pdfaddress.ToString();
                        //        break;
                        //    case "COPIES FURNISHED TO:":
                        //        dr["CopyFurnished"] = addr.Pdfaddress.ToString();
                        //        break;
                        //    case "COPIES FURNISHED TO 2:":
                        //        dr["CopyFurnished1"] = addr.Pdfaddress.ToString();
                        //        break;
                        //    case "Principal Balance:":
                        //        dr["PrincipalBalance"] = addr.Pdfaddress.ToString();
                        //        break;
                        //}
                        _cnt++;

                    }
                    _dt.Rows.Add(dr);

                }
            }

            foreach (DataRow _tblAddr in _dt.Rows)
            {
                if (_tblAddr["CopyFurnished"].ToString() != null && _tblAddr["CopyFurnished"].ToString() != "")
                {
                    _newAllPagesAddress.SearchedAddressID = Convert.ToInt32(_tblAddr["SearchedAddressID"].ToString());
                    _newAllPagesAddress.ToDefendant = _tblAddr["ToDefendant"].ToString();
                    _newAllPagesAddress.OnDefendant = _tblAddr["OnDefendant"].ToString();
                    _newAllPagesAddress.ToWit = _tblAddr["ToWit"].ToString();
                    _newAllPagesAddress.LocatedAt = _tblAddr["LocatedAt"].ToString();
                    _newAllPagesAddress.CopyFurnished = _tblAddr["CopyFurnished"].ToString().Trim();
                    _newAllPagesAddress.PrincipalBalance = _tblAddr["PrincipalBalance"].ToString();

                    db.tblAllPagesAddresses.Add(_newAllPagesAddress);
                    db.SaveChanges();

                    if (_tblAddr["CopyFurnished1"].ToString() != null && _tblAddr["CopyFurnished1"].ToString() != "")
                    {
                        _newAllPagesAddress.SearchedAddressID = Convert.ToInt32(_tblAddr["SearchedAddressID"].ToString());
                        _newAllPagesAddress.CopyFurnished = _tblAddr["CopyFurnished1"].ToString();
                        db.tblAllPagesAddresses.Add(_newAllPagesAddress);
                        db.SaveChanges();
                    }
                }
                else
                {
                    _newAllPagesAddress.SearchedAddressID = Convert.ToInt32(_tblAddr["SearchedAddressID"].ToString());
                    _newAllPagesAddress.ToDefendant = _tblAddr["ToDefendant"].ToString();
                    _newAllPagesAddress.OnDefendant = _tblAddr["OnDefendant"].ToString();
                    _newAllPagesAddress.ToWit = _tblAddr["ToWit"].ToString();
                    _newAllPagesAddress.LocatedAt = _tblAddr["LocatedAt"].ToString();
                    _newAllPagesAddress.CopyFurnished = _tblAddr["CopyFurnished"].ToString().Trim();
                    _newAllPagesAddress.PrincipalBalance = _tblAddr["PrincipalBalance"].ToString();

                    db.tblAllPagesAddresses.Add(_newAllPagesAddress);
                    db.SaveChanges();
                }
            }
        }

        public void UpdatedLpAddressListSaved(int SrchDefendantID)
        {
            try
            {
                var _newSavedAddress = db.tblAllPagesAddresses.Find(SrchDefendantID);

                if (_newSavedAddress != null)
                {
                    var _newSrchDefandant = db.tblSrchDefandants.Find(SrchDefendantID);
                    if (_newSrchDefandant != null)
                    {
                        _newSrchDefandant.IsGetAddress = true;
                        db.Entry(_newSrchDefandant).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
            }
            catch
            {

            }
        }

        public void SaveAddressInLPList(int SrchDefendantID, tblSelectedAddress obj)
        {
            try
            {
                db.Sp_InsertUSelectedAddress(SrchDefendantID, obj.StreetNumber, obj.StreetDirection, obj.StreetName, obj.StreetType, obj.PostDirection, obj.UnitNumber, obj.City, obj.CaseNumberID, 1);

                var _newSrchDefandant = db.tblSrchDefandants.Find(SrchDefendantID);
                if (_newSrchDefandant != null)
                {
                    string Finaladdress = obj.StreetNumber + " " + obj.StreetDirection + " " + obj.StreetName + " " + obj.StreetType + " " + obj.PostDirection + " " + obj.UnitNumber + " " + obj.City;
                    Regex regex1 = new Regex(@"\W+");
                    string outputAddress = regex1.Replace(Finaladdress, " ");
                    _newSrchDefandant.BCPaAddress = outputAddress;
                    _newSrchDefandant.IsSrchProperty = true;
                    _newSrchDefandant.IsGetAddress = false;
                    db.Entry(_newSrchDefandant).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        public void SaveOwedOnMortgageInPalm(long PCBPalmDetailsID, string Matchdate, string OwedOnMortgage)
        {
            try
            {
                var _newOthProperty = db.tblPCBPalmDetails.Find(PCBPalmDetailsID);

                if (_newOthProperty != null)
                {
                    _newOthProperty.OwedOnMortgage = OwedOnMortgage;
                    //_newOthProperty.DateOFMortgage = Matchdate;
                    db.Entry(_newOthProperty).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        public void SaveOwedOnMortgageInOTH(int OTHAddressID, string Matchdate, string OwedOnMortgage)
        {
            try
            {
                var _newOthProperty = db.tblOtherProperties.Find(OTHAddressID);

                if (_newOthProperty != null)
                {
                    _newOthProperty.OwedOnMortgage = OwedOnMortgage;
                    _newOthProperty.DateOFMortgage = Matchdate;
                    db.Entry(_newOthProperty).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        public void SaveAndUppdateContactInfo(int OTHAddressID, string Mobile, string Email, string Landline, string Landline1, string Mobile2, string Email1, string Email2, string Email3, string NOTESBOX)
        {
            try
            {
                db.Sp_InsertUContactInfo(OTHAddressID, Mobile, Email, Landline, NOTESBOX, Mobile2, Landline1, Email1, Email2, Email3);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }


        public void SaveUserSpokioDetails(int OTHAddressID, string UName, string TxtBirthDate, string TxtTelephone, string TxtEmail, string TxtFBLink, string _url, DataTable _dtCourtDtails)
        {
            try
            {
                ObjectParameter _param_SpokioDetailsID = new ObjectParameter("SpokioDetailsIDOut", typeof(Int64));
                //_param_SpokioDetailsID.ParameterType = ParameterDirection.Output;
                db.Sp_InsertSpokioDetails(2064, UName, TxtBirthDate, TxtTelephone, TxtEmail, TxtFBLink, _url,null,null,null, _param_SpokioDetailsID);
                Int64 _SpokioDetailsID = Convert.ToInt64(_param_SpokioDetailsID.Value.ToString());
                if (_SpokioDetailsID > 0)
                {
                    if (_dtCourtDtails.Rows.Count > 0)
                    {
                        tblCourtDetail newtblCourt = new tblCourtDetail();
                        foreach (DataRow _tblcourt in _dtCourtDtails.Rows)
                        {
                            newtblCourt.SpokioDetailsID = _SpokioDetailsID;
                            newtblCourt.CaseCategory = _tblcourt["Category"].ToString();
                            newtblCourt.OffenseDate = _tblcourt["OffenseDate"].ToString();
                            newtblCourt.OffenseCode = _tblcourt["OffenseCode"].ToString();
                            newtblCourt.Disposition = _tblcourt["Disposition"].ToString();
                            newtblCourt.DispositionDate = _tblcourt["DispositionDate"].ToString();
                            newtblCourt.SourceState = _tblcourt["SourceState"].ToString();
                            newtblCourt.SourceName = _tblcourt["SourceName"].ToString();
                            newtblCourt.CaseNo = _tblcourt["CaseNo"].ToString();
                            newtblCourt.ArrestDate = _tblcourt["ArrestDate"].ToString();
                            newtblCourt.Racewhite = _tblcourt["Racewhite"].ToString();
                            db.tblCourtDetails.Add(newtblCourt);
                            db.SaveChanges();
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }
        public List<SrchDefandant> GetOtherPropertyMatched(int SrchDefendantID)
        {
            List<SrchDefandant> _VMSrchDefent = new List<SrchDefandant>();

            _VMSrchDefent = db.sp_GetMatchedOTherProperty(SrchDefendantID).Select(s => new SrchDefandant
            {
                SrchDefendantID = Convert.ToInt32(s.SrchDefendantID),
                Plaintiff = Regex.Replace(s.Plaintiff, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                Defendant = Regex.Replace(s.Defendant, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                CaseNumber = s.CaseNumber,
                OtherFolio = s.OTHFolioId.ToString().Replace(" ", ""),
                CompareOTHAddressID = Convert.ToInt32(s.OTHAddressID),
                OTHMailingAddress = s.OTHMailingAddress,
                OTHSiteAddressSrch = s.OTHSiteAddressSrch,
                OTHAbbreviatedLegalDesc = s.OTHAbbreviatedLegalDesc,
                OTHLandFactor = s.OTHLandFactor,
                LegalDescMetched = s.LegalDescMetched,
                AddessMatched = s.AddessMatched,
                ViewPDF = s.DocResultSitUrl,

            }).ToList();

            return _VMSrchDefent;
        }

        public List<SrchDefandant> DefandantFilterByAssesd(int UseCodeCategoryID, string SubTabId, int? AssesedFrom, int? AssesedTo, decimal? LandFactorFrom, decimal? LandFactorTo, int? Zipcode, decimal? UnitsMin, decimal? UnitsMax, decimal? BedroomMin, decimal? BedroomMax, decimal? BathroomMin, decimal? BathroomMax,string LocationAddress, string MailingAddress, string FolioID, string Plaintiff, string Defendant, DateTime? RecordDateFrom, DateTime? RecordDateTo, DateTime? SaleDateFrom, DateTime? SaleDateTo, int? SalePriceFrom, int? SalePriceTo, decimal? EstimatedMortFrom, decimal? EstimatedMortTo, decimal? EstOwnerFrom, decimal? EstOwnerTo,string FilterStatus,string TaxIssueStatus,string HOAfeemin,string HOAfeemax)
        {
            try
            {
                List<SrchDefandant> _VMSrchDefent = new List<SrchDefandant>();
                _VMSrchDefent = db.sp_GetDefFilterByAssesd(AssesedFrom, AssesedTo, LandFactorFrom, LandFactorTo, Zipcode, UnitsMin, UnitsMax, BedroomMin, BedroomMax, BathroomMin, BathroomMax, UseCodeCategoryID,FolioID, null, LocationAddress, MailingAddress, Defendant, Plaintiff, SubTabId, null, null, null, null, null, null, RecordDateFrom, RecordDateTo, SaleDateFrom, SaleDateTo, SalePriceFrom, SalePriceTo, EstimatedMortFrom, EstimatedMortTo, EstOwnerFrom, EstOwnerTo, FilterStatus, TaxIssueStatus,HOAfeemin,HOAfeemax).Select(s => new SrchDefandant
                {
                    SrchDefendantID = s.SrchDefendantID,
                    DocumentType = s.DocumentType,
                    Plaintiff = Regex.Replace(s.Plaintiff, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                    Defendant = Regex.Replace(s.Defendant, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                    BookType = s.BookType,
                    CaseNumber = s.CaseNumber,
                    RefNum = s.RefNum,
                    SearchedDeffDate = s.SearchedDeffDate,
                    DocResultSitUrl = s.DocResultSitUrl,
                    IsResidential = s.IsResidential,
                    RecordDate = s.RecordDate,
                    IsNewsCase = s.IsNewsCase,
                    IsOtherFolio = Convert.ToBoolean(s.IsOtherFolio),
                    OtherFolio = s.OTHFolioId,
                    IsGetAddress = s.IsGetAddress,
                    IsSrchProperty = s.IsSrchProperty,
                    HasCompareable = Convert.ToBoolean(s.HasCompareable),
                    CompareOTHAddressID = Convert.ToInt32(s.OTHAddressID),
                    OTHAddressID = s.OTHAddressID,
                    BCPaAddress = s.BCPaAddress,
                    MatchedWord = Convert.ToBoolean(s.MatchedWord),
                    OTHMailingAddress = s.OTHMailingAddress,
                    OthAssesedValue = s.OthAssessedValue,
                    PropertyType = s.propertyType,
                    OthMarketValue = s.OthMarketValue,
                    OTHSiteAddressSrch = s.OTHSiteAddressSrch,
                    OTHAbbreviatedLegalDesc = s.OTHAbbreviatedLegalDesc,
                    OwedOnMortgage = s.OwedOnMortgage,
                   // SalesPriceSqFt = string.Format(CultureInfo.InvariantCulture, "{0:N2}", Convert.ToDecimal((string.IsNullOrEmpty(s.OthSalePrice) ? 0 : Convert.ToDecimal(s.OthSalePrice.Replace(",", "").Replace("$", "")) / (string.IsNullOrEmpty(s.OTHAdvBldgSF) ? 1 : Convert.ToDecimal(s.OTHAdvBldgSF))))),
                    //AssessedMarketSqFt = string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal((string.IsNullOrEmpty(s.OthMarketValue) ? 0 : Convert.ToDecimal(s.OthMarketValue.Replace(",", "").Replace("$", "")) / (string.IsNullOrEmpty(s.OTHAdvBldgSF) ? 1 : Convert.ToDecimal(s.OTHAdvBldgSF))))),
                    EstimatedMortgage = s.OwedOnMortgage,
                    EstOwnerEquity = s.OwedOnMortgage == "FULLY PAID" ? s.OwedOnMortgage : string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal((string.IsNullOrEmpty(s.OthMarketValue) ? 0 : Convert.ToDecimal(s.OthMarketValue.Replace(",", "").Replace("$", "")) - (string.IsNullOrEmpty(s.OwedOnMortgage) ? 0 : Convert.ToDecimal(s.OwedOnMortgage))))),
                    EstMarketValue = s.OthMarketValue == null ? "0" : Convert.ToString((Convert.ToInt32(s.OthMarketValue.Replace(",", "").Replace("$", "")) * 15 / 100) + Convert.ToInt32(s.OthMarketValue.Replace(",", "").Replace("$", ""))),
                    IsspokioDetails = s.IsSpokioDetails ?? false,
                    Latitude = s.Latitude,
                    Longitude = s.Longitude,
                    cntPlaintiffs = Convert.ToBoolean(s.cntPlaintiffs),
                    RedfinPrice = s.RedfinPrice,
                    ZillowPrice = s.ZillowPrice,
                    IssunbizDetailsID = Convert.ToBoolean(s.IssunbizDetails),
                    IsBrowardLinkedInID = Convert.ToBoolean(s.IsBrowardLinkedInID),
                    IsBrowardTwitterID = Convert.ToBoolean(s.IsTwitterBrowardID),
                    RedfinPricedetail = s.RedfinPricedetail,
                    ZillowPricedetail = s.ZillowPricedetail,
                    RedfinUrl = s.RedfinUrl,
                    zillowUrl = s.zillowUrl,
                    LoopnetPrice = s.LoopnetPrice,
                    LoopnetUrl = s.LoopnetUrl,
                    NonpaymentStatus = s.NonpaymentStatus,
                    HOARedfinDue = s.HOARedfinDue,
                    Rentzestimate = s.Rentzestimate,
                    Refipayment = s.Refipayment,
                    OTHPropertyOwner = s.OTHPropertyOwner,
                    saleprice = s.OthSalePrice,
                    SaleDate = s.OthSaleDate.HasValue ? s.OthSaleDate.Value.ToString() : string.Empty,
                    TaxIssue = s.TaxIssueStatus,
                    OTHLandFactor=s.OTHLand,
                    DateModified = s.SearchedDeffDate.ToString(),
                    Casedate = s.SearchedDeffDate.ToString(),
                    Redfindays=s.Redfindays,
                    Zillowdays=s.Zillowdays

                }).ToList();


                return _VMSrchDefent;
            }
            catch(Exception ex)
            {
                return null;
            }
        }


        public List<SunbizDetail> GetSunbizsearchFilteredData(string LocationAddress, string MailingAddress, string Defendant, string FilterStatus,string DirectorName)
        {
            try
            {

                List<SunbizDetail> _VMSunbizDetailLpcases = new List<SunbizDetail>();

                _VMSunbizDetailLpcases = db.sp_GetDefFilterByManualSearch(LocationAddress, MailingAddress, Defendant, FilterStatus, DirectorName).AsEnumerable().Select(s => new SunbizDetail
                {
                    SunbizCompanyID = s.SunbizCompanyID,
                    Defendant = s.Defendant,
                    FEIEIN = s.FEIEIN,
                    //DateFiled=s.DateFiled,
                    State = s.State,
                    Status = s.Status,
                    PrincipalAddress = s.PrincipalAddress,
                    MailingAddress = s.MailingAddress,
                    RegisteredagentNameAddress = s.RegisteredagentNameAddress,
                    url = s.url,
                    DocumnentNumber = s.DocumnentNumber,
                    DirectorName = s.DirectorName,
                    LinkedInID = s.LinkedInID,
                    TwitterID = s.TwitterID

                }).ToList();

                return _VMSunbizDetailLpcases;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public List<SrchDefandant> GetSaerchDefendent(int UseCodeCategoryID, int? SrchDefendantID, int? CntRecord, string SubTabId)
        {
            List<SrchDefandant> _VMSrchDefent = new List<SrchDefandant>();
            List<SrchDefandant> _VMSrchDefenttt = new List<SrchDefandant>();
            var List = "";
            try
            {
                //_VMSrchDefent = db.sp_GetserchDefendantList(UseCodeCategoryID, SrchDefendantID, CntRecord, SubTabId).Select(s => new SrchDefandant
                _VMSrchDefent = db.sp_GetserchDefendantList1(UseCodeCategoryID, SrchDefendantID, CntRecord, SubTabId).Select(s => new SrchDefandant
                {
                    SrchDefendantID = s.SrchDefendantID,
                    DocumentType = s.DocumentType,
                    Plaintiff = Regex.Replace(s.Plaintiff, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                    Defendant = Regex.Replace(s.Defendant, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                    BookType = s.BookType,
                    CaseNumber = s.CaseNumber,
                    OTHAddressID = s.OTHAddressID,
                    RefNum = s.RefNum,
                    OtherFolio = s.OTHFolioId,
                    TaxIssue = s.TaxIssueStatus,
                    DateModified = s.SearchedDeffDate.ToString(),
                    SearchedDeffDate = s.SearchedDeffDate,
                    DocResultSitUrl = s.DocResultSitUrl,
                    IsResidential = s.IsResidential,
                        RecordDate = s.RecordDate,
                        IsNewsCase = s.IsNewsCase,
                        IsOtherFolio = Convert.ToBoolean(s.IsOtherFolio),
                        IsGetAddress = s.IsGetAddress,
                        IsSrchProperty = s.IsSrchProperty,
                        HasCompareable = Convert.ToBoolean(s.HasCompareable),
                        CompareOTHAddressID = Convert.ToInt32(s.OTHAddressID),
                        BCPaAddress = s.BCPaAddress,
                        OTHMailingAddress = s.OTHMailingAddress,
                        MatchedWord = Convert.ToBoolean(s.MatchedWord),
                        PropertyType = s.propertyType,
                        OTHSrchUse = s.OTHSrchUse,
                        OthMarketValue = s.OthMarketValue,
                        OTHSiteAddressSrch = s.OTHSiteAddressSrch,
                        OTHAbbreviatedLegalDesc = s.OTHAbbreviatedLegalDesc,
                        EstMarketValue = string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal(string.IsNullOrEmpty(s.OthMarketValue) ? 0 : Convert.ToDecimal(((Convert.ToDecimal(s.OthMarketValue.Replace(",", "").Replace("$", "")) * 15 / 100) + Convert.ToDecimal(s.OthMarketValue.Replace(",", "").Replace("$", "")))))),
                        OwedOnMortgage = s.OwedOnMortgage,
                        EstOwnerEquity = s.OwedOnMortgage == "FULLY PAID" ? s.OwedOnMortgage : string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal((string.IsNullOrEmpty(s.OthMarketValue) ? 0 : Convert.ToDecimal(s.OthMarketValue.Replace(",", "").Replace("$", "")) - (string.IsNullOrEmpty(s.OwedOnMortgage) ? 0 : Convert.ToDecimal(s.OwedOnMortgage))))),
                        //DateOFMortgage=s.DateOFMortgage,
                        CntOther = Convert.ToInt32(s.CntOther),
                        Excemption = string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal(s.Excemption == null ? 0 : s.Excemption)),
                        saleprice = s.OthSalePrice,
                        SaleDate = s.OthSaleDate.HasValue ? s.OthSaleDate.Value.ToString() : string.Empty,
                        SaleType = s.OthSaleType,
                        SaleBookPage = s.OthSaleBookPage,
                        AdvBldgSF = s.OTHAdvBldgSF,
                        SalesPriceSqFt = string.Format(CultureInfo.InvariantCulture, "{0:N2}", Convert.ToDecimal((string.IsNullOrEmpty(s.OthSalePrice) ? 0 : Convert.ToDecimal(s.OthSalePrice.Replace(",", "").Replace("$", "")) / (string.IsNullOrEmpty(s.OTHAdvBldgSF) ? 1 : Convert.ToDecimal(s.OTHAdvBldgSF))))),
                        AssessedMarketSqFt = string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal((string.IsNullOrEmpty(s.OthMarketValue) ? 0 : Convert.ToDecimal(s.OthMarketValue.Replace(",", "").Replace("$", "")) / (string.IsNullOrEmpty(s.OTHAdvBldgSF) ? 1 : Convert.ToDecimal(s.OTHAdvBldgSF))))),
                        EstimatedMortgage = s.OwedOnMortgage == "FULLY PAID" ? s.OwedOnMortgage : string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal((string.IsNullOrEmpty(s.OwedOnMortgage) ? 0 : Convert.ToDecimal(s.OwedOnMortgage.Replace(",", "").Replace("$", ""))))),
                        IsspokioDetails = Convert.ToBoolean(s.IsspokioDetails),
                        IssunbizDetailsID = Convert.ToBoolean(s.IssunbizDetailsID),
                        IsBrowardLinkedInID=Convert.ToBoolean(s.IsBrowardLinkedInID),
                        IsBrowardTwitterID=Convert.ToBoolean(s.IsBrowardTwitterID),
                        cntPlaintiffs = Convert.ToBoolean(s.cntPlaintiffs),
                        Latitude = s.Latitude,
                        Longitude = s.Longitude,
                    RedfinPrice = (s.RedfinPrice),
                    ZillowPrice = (s.ZillowPrice),
                    SpokioStatus = s.SpokioStatus,
                    RedfinPricedetail=s.RedfinPricedetail,
                    ZillowPricedetail=s.ZillowPricedetail,
                    RedfinUrl=s.RedfinUrl,
                    zillowUrl=s.zillowUrl,
                    LoopnetPrice=s.LoopnetPrice,
                    LoopnetUrl=s.LoopnetUrl,
                    NonpaymentStatus=s.NonpaymentStatus,
                   HOARedfinDue=s.HOARedfinDue,
                   Rentzestimate=s.Rentzestimate,
                   Refipayment=s.Refipayment,
                   OTHPropertyOwner=s.OTHPropertyOwner,
                    OTHLandFactor = s.OTHLand,
                   OthAssesedValue=s.OthAssessedValue,
                   Casedate= s.SearchedDeffDate.ToString(),
                    Redfindays = s.Redfindays,
                    Zillowdays = s.Zillowdays



                }).ToList();

                //var op = _VMSrchDefent.Select(x => x.OTHAddressID).Distinct().ToList();
                //var spokeo = new List<tblSpokioDetail>();
                //var count1 = new List<addID_count>();
                //op.ForEach(x=>
                //{
                //foreach(var x in op)
                //{
                //    addID_count ac = new addID_count();
                //    var reslt = (from s in db.tblSpokioDetails
                //                 join p in db.tblOtherProperties on s.OTHAddressID equals p.OTHAddressID
                //                 join code in db.tblUseCodeTypes on p.OTHSrchUse equals code.UseCode
                //                 join cat in db.tblUseCodeCategories on code.UseCodeCategoryID equals cat.UseCodeCategoryID
                //                 where p.OTHSrchUse == "01" && s.OTHAddressID == x && cat.UseCodeCategoryID == 1
                //                 select new
                //                 {
                //                     s
                //                 }
                //              ).Count();
                //    // ac.count = db.tblSpokioDetails.Where(y => y.OTHAddressID == x).Count();
                //    if(reslt==0)
                //    {
                //        ac.count = reslt;
                //        ac.addressID =Convert.ToInt32(x);
                //        count1.Add(ac);
                //    }

                //}

                //});
                _VMSrchDefenttt = _VMSrchDefent;
                //_VMSrchDefenttt = _VMSrchDefenttt.OrderBy(x => x.OTHAddressID).ToList();

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return _VMSrchDefenttt;
        }


        public List<SrchDefandant> GetFinancialDataDetail(int? OTHAddressID)
        {
            List<SrchDefandant> _VMSrchDefent = new List<SrchDefandant>();
            List<SrchDefandant> _VMSrchDefenttt = new List<SrchDefandant>();
            var List = "";
            try
            {

                _VMSrchDefent = db.Sp_GetDetailsforFinancialData(OTHAddressID).Select(s => new SrchDefandant
                {
                    SrchDefendantID = s.srchdefendantid,
                    DocumentType = s.Documenttype,
                    Plaintiff = Regex.Replace(s.Plaintiff, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                    //Defendant = Regex.Replace(s.defe, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                    CaseNumber = s.casenumber,
                    OTHAddressID = s.othaddressid,
                    OtherFolio = s.othfolioid,
                    OTHMailingAddress = s.OTHSiteAddressSrch,
                    PropertyType = s.propertyType,
                    OthMarketValue = s.othmarketvalue,
                    OwedOnMortgage = s.OwedOnMortgage,
                   EstOwnerEquity = s.OwedOnMortgage == "FULLY PAID" ? s.OwedOnMortgage : string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal((string.IsNullOrEmpty(s.othmarketvalue) ? 0 : Convert.ToDecimal(s.othmarketvalue.Replace(",", "").Replace("$", "")) - (string.IsNullOrEmpty(s.OwedOnMortgage) ? 0 : Convert.ToDecimal(s.OwedOnMortgage))))),
                    //DateOFMortgage=s.DateOFMortgage,
                    saleprice = s.OthSalePrice,
                    SaleDate = s.OthSaleDate.HasValue ? s.OthSaleDate.Value.ToString() : string.Empty,
                    SaleType = s.OthSaleType,
                     AdvBldgSF = s.othadvbldgsf,
                    SalesPriceSqFt = string.Format(CultureInfo.InvariantCulture, "{0:N2}", Convert.ToDecimal((string.IsNullOrEmpty(s.OthSalePrice) ? 0 : Convert.ToDecimal(s.OthSalePrice.Replace(",", "").Replace("$", "")) / (string.IsNullOrEmpty(s.othadvbldgsf) ? 1 : Convert.ToDecimal(s.othadvbldgsf))))),
                    RedfinPricedetail = s.Redfinpricedetail,
                    ZillowPricedetail = s.Zillowpricedetail,
                      HOARedfinDue = s.HoaRedfindue,
                    Rentzestimate = s.Rentzestimate,
                    OTHPropertyOwner = s.othpropertyowner,
                    OTHLandFactor = s.OTHLandFactor,
                    instrumentnumber=s.instrumentnumber,
                    OTHEffYearBuilt=s.OTHEffYearBuilt,
                    UnitsBedBath=s.UnitBedBath,
                    Usecodetype=s.usecodetype,
                    Totalsqft=s.Totalsqft,
                    OthLand=s.OthLand,
                    othbuilding=s.othbuilding,
                    othAssessedtax=s.othAssessedtax,
                    Refipayment=s.Refipayment,
                    ZillMarkettemp=s.ZillMarkettemp,
                    MedianZEstimate=s.MedianZEstimate,
                    ZillPastMonths=s.ZillPastMonths

                }).ToList();

             
                _VMSrchDefenttt = _VMSrchDefent;
                //_VMSrchDefenttt = _VMSrchDefenttt.OrderBy(x => x.OTHAddressID).ToList();

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return _VMSrchDefenttt;
        }
        public class addID_count
        {
            public int addressID { get; set; }
            public int count { get; set; }
        }

        public ContactInfo GetContactInfo(int OTHAddressID)
        {

            ContactInfo ContactInf = new ContactInfo();

            try
            {
                ContactInf = db.spGetContactInfo(OTHAddressID).Select(s => new ContactInfo
                {
                    OTHAddressID = OTHAddressID,
                    NOTESBOX = s.NOTESBOX,
                    Email = string.IsNullOrEmpty(s.Emails) ? string.Empty : (s.Emails.Split(',').Length > 0 ? s.Emails.Split(',')[0] : string.Empty),
                    Email1 = string.IsNullOrEmpty(s.Emails) ? string.Empty : (s.Emails.Split(',').Length > 1 ? s.Emails.Split(',')[1] : string.Empty),
                    Email2 = string.IsNullOrEmpty(s.Emails) ? string.Empty : (s.Emails.Split(',').Length > 2 ? s.Emails.Split(',')[2] : string.Empty),
                    Email3 = string.IsNullOrEmpty(s.Emails) ? string.Empty : (s.Emails.Split(',').Length > 3 ? s.Emails.Split(',')[3] : string.Empty),
                    Mobile = string.IsNullOrEmpty(s.Mobiles) ? string.Empty : (s.Mobiles.Split(',').Length > 0 ? s.Mobiles.Split(',')[0] : string.Empty),
                    Mobile2 = string.IsNullOrEmpty(s.Mobiles) ? string.Empty : (s.Mobiles.Split(',').Length > 1 ? s.Mobiles.Split(',')[1] : string.Empty),
                    Landline = string.IsNullOrEmpty(s.Landlines) ? string.Empty : (s.Landlines.Split(',').Length > 0 ? s.Landlines.Split(',')[0] : string.Empty),
                    Landline1 = string.IsNullOrEmpty(s.Landlines) ? string.Empty : (s.Landlines.Split(',').Length > 1 ? s.Landlines.Split(',')[1] : string.Empty)
                }).FirstOrDefault() ?? new ContactInfo()
                {
                    OTHAddressID = OTHAddressID,
                };

                //ContactInf = db.tblContactInfoes.Where(x => x.OTHAddressID == OTHAddressID).Select(p => new ContactInfo
                //{
                //    OTHAddressID = OTHAddressID,
                //    Mobile = p.Mobile,
                //    Email = p.Email,
                //    Landline = p.Landline,
                //    NOTESBOX = p.NOTESBOX,
                //    Mobile2 = p.Mobile2,
                //    Landline1 = p.Landline1,
                //    Email1 = p.Email1,
                //    Email2 = p.Email2,
                //    Email3 = p.Email3,
                //}).FirstOrDefault() ?? new ContactInfo()
                //{
                //    OTHAddressID = OTHAddressID,
                //};

                return ContactInf;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return new ContactInfo();
            }
        }

       


        public int GetFolioBySrchDefendantID(int SrchDefendantID)
        {
            //PropertyFolio _PropertyFolio= new PropertyFolio();
            var _PropertyFolio = db.tblSrchFolioIDs.AsEnumerable().Where(x => x.SrchDefendantID == SrchDefendantID).Select(x => x.AddressID).FirstOrDefault();
            return _PropertyFolio;
        }

        public List<OtherAssessedValue> OtherPriviousYearsDetails(int AddressID)
        {
            try
            {
                List<OtherAssessedValue> _VMAssessedValuelist = new List<OtherAssessedValue>();

                _VMAssessedValuelist = db.tblOtherAssessedValues.AsEnumerable().Where(x => x.OTHAddressID == AddressID).Select(s => new OtherAssessedValue
                {
                    OthPropertyAssesID = s.OthPropertyAssesID,
                    OTHAddressID = s.OTHAddressID,
                    OthYear = s.OthYear,
                    OthLand = s.OthLand,
                    OthBuilding = s.OthBuilding,
                    OthMarketValue = s.OthMarketValue,
                    OthAssessedValue = s.OthAssessedValue,
                    OthAssessedTax = s.OthAssessedTax


                }).ToList();
                return _VMAssessedValuelist;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public List<AssesedValueslist> PriviousYearsDetails(int AddressID)
        {
            try
            {
                List<AssesedValueslist> _VMDefendantlist = new List<AssesedValueslist>();

                _VMDefendantlist = db.tblAssesedValues.AsEnumerable().Where(x => x.AddressID == AddressID).Select(s => new AssesedValueslist
                {
                    PropertyAssesID = s.PropertyAssesID,
                    AddressID = s.AddressID,
                    Year = s.Year,
                    Land = s.Land,
                    Building = s.Building,
                    MarketValue = s.MarketValue,
                    AssessedValue = s.AssessedValue,
                    AssessedTax = s.AssessedTax


                }).ToList();
                return _VMDefendantlist;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public List<OtherProperty> GetOtherPropertyList(int SrchDefendantID)
        {
            try
            {
                List<OtherProperty> _VMOtherPropertyList = new List<OtherProperty>();

                _VMOtherPropertyList = db.sp_GetOtherPropertyList(SrchDefendantID).Select(s => new OtherProperty
                {
                    OTHAddressID = Convert.ToInt32(s.OTHAddressID),
                    OTHSiteAddressSrch = s.OTHSiteAddressSrch,
                    OTHPropertyOwner = s.OTHPropertyOwner,
                    OTHMailingAddress = s.OTHMailingAddress,
                    OTHAbbreviatedLegalDesc = s.OTHAbbreviatedLegalDesc,
                    OTHFolioId = s.OTHFolioId,
                    OTHMilage = s.OTHMilage,
                    OTHSrchUse = s.OTHSrchUse,
                    OTHLandFactor = s.OTHLandFactor,
                    OTHAdvBldgSF = s.OTHAdvBldgSF,
                    UnitsBEDBath = s.UnitsBEDBath,
                    OTHFolioSiteLink = s.OTHFolioSiteLink,
                    OTHSearchDate = s.OTHSearchDate,
                    OTHCompareable = Convert.ToBoolean(s.OTHCompareable),
                    SaleBookPage = s.SaleBookPage,
                    SrchDefendantID = s.SrchDefendantID,
                    IsOTHCorrectProperty = s.IsOTHCorrectProperty,
                }).ToList();
                return _VMOtherPropertyList;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public List<SrchDefandant> GetLpCasesearchProperty()
        {

            try
            {
                List<SrchDefandant> _SrchDefandant = new List<SrchDefandant>();

                _SrchDefandant = db.sp_GetLpForCorrectProperty().AsEnumerable().Select(s => new SrchDefandant
                {
                    SrchDefendantID = s.SrchDefendantID,
                    DocumentType = s.DocumentType,
                    Plaintiff = s.Plaintiff,
                    Defendant = s.Defendant,
                    BookType = s.BookType,
                    CaseNumber = s.CaseNumber,
                    RefNum = s.RefNum,
                    SearchedDeffDate = s.SearchedDeffDate,
                    DocResultSitUrl = s.DocResultSitUrl,
                    IsResidential = s.IsResidential,
                    RecordDate = s.RecordDate,
                    IsNewsCase = s.IsNewsCase,
                    BCPaAddress = s.BCPaAddress,
                    IsSrchProperty = s.IsSrchProperty,
                    IsGetAddress = s.IsGetAddress,
                    IsOtherProperty = s.IsOtherProperty,
                    IsOfficialPDfDownload = s.IsOfficialPDfDownload,
                    OTHAddressID = s.OTHAddressID,
                }).ToList();
                return _SrchDefandant;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }

        }

        public List<PropertyFolio> GetFolioPropertyList()
        {
            try
            {
                List<PropertyFolio> _VMPropertyFolioList = new List<PropertyFolio>();
                _VMPropertyFolioList = db.sp_GetFolioPropertyList().Select(s => new PropertyFolio
                {
                    AddressID = s.AddressID,
                    SiteAddressSrch = s.SiteAddressSrch,
                    PropertyOwner = s.PropertyOwner,
                    MailingAddress = s.MailingAddress,
                    AbbreviatedLegalDesc = s.AbbreviatedLegalDesc,
                    FolioId = s.FolioId,
                    Milage = s.Milage,
                    SrchUse = s.SrchUse,
                    LandFactor = s.LandFactor,
                    AdvBldgSF = s.AdvBldgSF,
                    UnitsBEDBath = s.UnitsBEDBath,
                    FolioSiteLink = s.FolioSiteLink,
                    SearchDate = s.SearchDate,
                    Compareable = Convert.ToBoolean(s.Compareable),
                    SaleBookPage = s.SaleBookPage
                }).ToList();
                return _VMPropertyFolioList;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }


        public List<tblCourtDetail> GetCourtList(int SpokioDetailsID)
        {
            try
            {
                List<tblCourtDetail> _VMCourtList = new List<tblCourtDetail>();
                _VMCourtList = db.tblCourtDetails.AsEnumerable().Where(x => x.SpokioDetailsID == SpokioDetailsID).Select(s => new tblCourtDetail
                {
                    SpokioDetailsID = Convert.ToInt32(s.SpokioDetailsID),
                    CourtDetailsID = Convert.ToInt32(s.CourtDetailsID),
                    RecordsNumber = s.RecordsNumber,
                    OffenseDate = s.OffenseDate,
                    OffenseCode = s.OffenseCode,
                    OffenseDesc = s.OffenseDesc,
                    Disposition = s.Disposition,
                    DispositionDate = s.DispositionDate,
                    SourceState = s.SourceState,
                    SourceName = s.SourceName,
                    CaseNo = s.CaseNo,
                    ArrestDate = s.ArrestDate,
                    Racewhite = s.Racewhite,
                }).ToList();

                return _VMCourtList;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public List<SpokioDetail> GetSpokioOwnerList(int OTHAddressID)
        {
            try
            {
                List<SpokioDetail> _VMSpokioList = new List<SpokioDetail>();
                List<SpokioDetail> _VMSpokioList1 = new List<SpokioDetail>();
                var reslt = db.tblSpokioDetails.Where(x => x.OTHAddressID == OTHAddressID).Select(x => x.SpkUName).Distinct().ToList();
                _VMSpokioList = db.tblSpokioDetails.AsEnumerable().Where(x => x.OTHAddressID == OTHAddressID).Select(s => new SpokioDetail
                {
                    OTHAddressID = Convert.ToInt32(s.OTHAddressID),
                    SpkUName = s.SpkUName,
                    SpkBirthDate = s.SpkBirthDate,
                    SpkEmail = s.SpkEmail,
                    SpkTelephone = s.SpkTelephone,
                    SpkFBLink = s.SpkFBLink,
                    SpkUrl = s.SpkUrl,
                    SpkMaritalStatus=s.MaritalStatus,
                    SpkZodiacSign=s.ZodiacSign,
                    SpkEthnicity=s.Ethnicity,
                    SpokioDetailsID = Convert.ToInt32(s.SpokioDetailsID)
                }).ToList();
                int i = 0;
                foreach (var r in reslt)
                {
                    foreach (var _v in _VMSpokioList)
                    {
                        if(_v.SpkUName==r && i==0)
                        {
                            _VMSpokioList1.Add(_v);
                            i++;
                        }
                    }
                    i = 0;
                }
                return _VMSpokioList1;
            }
            catch(Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }
        public SpokioDetail GetSpokioOwnerDetails(long spokioDetailsID, int othAddressID)
        {
            try
            {
                SpokioDetail _VMSpokioList = new SpokioDetail();
                _VMSpokioList = db.tblSpokioDetails.AsEnumerable().Where(x => x.OTHAddressID == othAddressID && x.SpokioDetailsID == spokioDetailsID).Select(s => new SpokioDetail
                {
                    OTHAddressID = Convert.ToInt32(s.OTHAddressID),
                    SpkUName = s.SpkUName,
                    SpkBirthDate = s.SpkBirthDate,
                    SpkEmail = s.SpkEmail,
                    SpkTelephone = s.SpkTelephone,
                    SpkFBLink = s.SpkFBLink,
                    SpkUrl = s.SpkUrl,
                    SpkMaritalStatus = s.MaritalStatus,
                    SpkZodiacSign = s.ZodiacSign,
                    SpkEthnicity = s.Ethnicity,
                    SpokioDetailsID = Convert.ToInt32(s.SpokioDetailsID),
                    CntCourtDetails = db.tblCourtDetails.AsEnumerable().Where(p => p.SpokioDetailsID == s.SpokioDetailsID).ToList().Count(),
                    SpkEmails = db.tblSpokeoEmails.AsEnumerable().Where(w => w.SpokeoDetailsID == s.SpokioDetailsID).Select(x => new SpokioEmails
                    {
                        EmailID = x.EmailID,
                        EmailType = x.EmailType,
                        SpokeoDetailsID = x.SpokeoDetailsID,
                        SpokeoEmailsID = x.SpokeoEmailsID
                    }).ToList(),
                    SpkTelephones = db.tblSpokeoTelephones.AsEnumerable().Where(w => w.SpokeoDetailsID == s.SpokioDetailsID).Select(x => new SpokioTelephones
                    {
                        Network = x.Network,
                        PhoneType = x.PhoneType,
                        SpokeoDetailsID = x.SpokeoDetailsID,
                        SpokeoTelephonesID = x.SpokeoTelephonesID,
                        Telephone = x.Telephone
                    }).ToList(),
                    Addresses = db.tblSpokeoAddresses.AsEnumerable().Where(w => w.SpokeoDetailsID == s.SpokioDetailsID).Select(x => new SpokeoAddresses
                    {
                        AddressYear = x.AddressYear,
                        SpokeoAddress = x.SpokeoAddress,
                        SpokeoAddressID = x.SpokeoAddressID,
                        SpokeoDetailsID = x.SpokeoDetailsID,
                        Residents = db.tblSpokeoResidents.AsEnumerable().Where(w => w.SpokeoAddressID == x.SpokeoAddressID).Select(r => new SpokeoResidents
                        {
                            Ethnicity = r.Ethnicity,
                            MaritalStatus = r.MaritalStatus,
                            ResidentDOB = r.ResidentDOB,
                            ResidentID = r.ResidentID,
                            ResidentName = r.ResidentName,
                            ResidentUrl = r.ResidentUrl,
                            SpokeoAddressID = r.SpokeoAddressID,
                            ZodiacSign = r.ZodiacSign
                        }).ToList()
                    }).ToList(),
                    Relatives = db.tblSpokeoRelatives.AsEnumerable().Where(w => w.SpokeoDetailsID == s.SpokioDetailsID).Select(x => new SpokeoRelatives
                    {
                        AddressUrl = x.AddressUrl,
                        RelativeAddress = x.RelativeAddress,
                        RelativeID = x.RelativeID,
                        RelativeName = x.RelativeName,
                        RelativeUrl = x.RelativeUrl,
                        SpokeoDetailsID = x.SpokeoDetailsID
                    }).ToList(),
                    SocialProfiles = db.tblSpokeoSocialLinks.AsEnumerable().Where(w => w.SpokeoDetailsID == s.SpokioDetailsID).Select(x => new SpokeoSocialProfiles
                    {
                        SocialLinkUrl = x.SocialLinkUrl,
                        SocialName = x.SocialName,
                        SocialSiteName = x.SocialSiteName,
                        SocialSiteType = x.SocialSiteType,
                        SpokeoDetailsID = x.SpokeoDetailsID,
                        SpokeoSocialLinkID = x.SpokeoSocialLinkID
                    }).ToList()

                }).FirstOrDefault();

                return _VMSpokioList;
            }
            catch
            {
                return null;
            }
        }

        public OtherProperty OtherPropertiesDetails(int AddressID)
        {
            try
            {
                OtherProperty _VMOtherPropertyList = new OtherProperty();
                _VMOtherPropertyList = db.sp_GetOtherPropertydetailsByID(AddressID).Select(s => new OtherProperty
                {
                    OTHAddressID = s.OTHAddressID,
                    OTHAbbreviatedLegalDesc = s.OTHAbbreviatedLegalDesc,
                    OTHAdvBldgSF = s.OTHAdvBldgSF,
                    OTHFolioId = s.OTHFolioId,
                    OTHFolioSiteLink = s.OTHFolioSiteLink,
                    OTHLandFactor = s.OTHLandFactor,
                    OTHMailingAddress = s.OTHMailingAddress,
                    OTHMilage = s.OTHMilage,
                    OTHPropertyOwner = s.OTHPropertyOwner,
                    OTHSearchDate = s.OTHSearchDate,
                    OTHSiteAddressSrch = s.OTHSiteAddressSrch,
                    OTHSrchUse = s.OTHSrchUse,
                    UnitsBEDBath = s.UnitsBEDBath,
                    SalePrice = s.SalePrice,
                    SalePricePrSqFt = String.Format("{0:0.00}", s.SalePricePrSqFt),
                    AssedMarketSqFt = s.OthMarketValue,
                    SrchDefendantID = Convert.ToInt32(s.SrchDefendantID),
                    OthSaleDate = s.OthSaleDate,
                    OthSaleBookPage = s.OthSaleBookPage,
                    OthSaleType = s.OthSaleType,
                    OTHCardUrl = s.OTHCardUrl,
                    OTHSketchUrl = s.OTHSketchUrl,
                    OTHEffYearBuilt = s.OTHEffYearBuilt,
                    UseCodeCategoryID=s.UseCodeCategoryID

                }).FirstOrDefault();
                return _VMOtherPropertyList;
            }
            catch
            {
                return null;
            }
        }


        public tblPCBSaleInfo OtherPalmPropertiesDetails(int PCBPalmDetailsID)
        {
            try
            {
                tblPCBSaleInfo _VMOtherPropertyList = new tblPCBSaleInfo();
                _VMOtherPropertyList = db.sp_GetpalmOtherPropertydetailsByID(PCBPalmDetailsID).Select(s => new tblPCBSaleInfo
                {
                    PCBSaleInfoID=s.PCBSaleInfoID,
                    PCBPalmDetailsID=s.PCBPalmDetailsID,
                    SaleDate=s.SaleDate,
                    Price=s.Price,
                    BookPage=s.BookPage,
                    SaleType=s.SaleType,
                    Owner=s.Owner,
                    UseCodeCategpryID = s.UseCodeCategpryID

                }).FirstOrDefault();
                return _VMOtherPropertyList;
            }
            catch(Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }


        public PropertyFolio PropertiesDetails(int AddressID)
        {
            try
            {
                PropertyFolio _VMPropertyFolioList = new PropertyFolio();
                _VMPropertyFolioList = db.sp_GetPropertydetailsByID(AddressID).Select(s => new PropertyFolio
                {
                    AddressID = s.AddressID,
                    AbbreviatedLegalDesc = s.AbbreviatedLegalDesc,
                    AdvBldgSF = s.AdvBldgSF,
                    FolioId = s.FolioId,
                    FolioSiteLink = s.FolioSiteLink,
                    LandFactor = s.LandFactor,
                    MailingAddress = s.MailingAddress,
                    Milage = s.Milage,
                    PropertyOwner = s.PropertyOwner,
                    SearchDate = s.SearchDate,
                    SiteAddressSrch = s.SiteAddressSrch,
                    SrchUse = s.SrchUse,
                    UnitsBEDBath = s.UnitsBEDBath,
                    SalePrice = s.SalePrice,
                    SalePricePrSqFt = String.Format("{0:0.00}", s.SalePricePrSqFt),
                    AssedMarketSqFt = s.MarketValue,
                    SrchDefendantID = Convert.ToInt32(s.SrchDefendantID)
                }).FirstOrDefault();
                return _VMPropertyFolioList;
            }
            catch
            {
                return null;
            }
        }

        public tblSelectedAddress GetAddressFileds(int? SrchDefendantID)
        {
            tblSelectedAddress _NewSelectedAddress = new tblSelectedAddress();

            _NewSelectedAddress = db.tblSelectedAddresses.AsEnumerable().Where(x => x.SrchDefendantID == SrchDefendantID).Select(s => new tblSelectedAddress
            {
                SelctedAddressId = s.SelctedAddressId,
                StreetNumber = s.StreetNumber == null ? "" : s.StreetNumber,
                StreetName = s.StreetName == null ? "" : s.StreetName,
                StreetDirection = s.StreetDirection == null ? "" : s.StreetDirection,
                StreetType = s.StreetType == null ? "" : s.StreetType,
                SrchDefendantID = s.SrchDefendantID,
                PostDirection = s.PostDirection == null ? "" : s.PostDirection,
                UnitNumber = s.UnitNumber == null ? "" : s.UnitNumber,
                City = s.City == null ? "" : s.City
            }).FirstOrDefault();

            return _NewSelectedAddress;
        }

        public List<CompareFolioID> GetFolioFormFields(int AddressID)
        {
            List<CompareFolioID> _lst = new List<CompareFolioID>();
            var objCmpFolio = (from c in db.tblCompareProperties
                               select c).Where(c => c.CmpFolioByID == AddressID).ToList();
            foreach (var item in objCmpFolio)
            {
                CompareFolioID _lstFolioProp = db.tblSrchFolioIDs.AsEnumerable().Where(s => s.AddressID == item.CmpFloioToID).Select(s => new CompareFolioID
                 {
                     AddressID = s.AddressID,
                     CmpAbbreviatedLegalDesc = s.AbbreviatedLegalDesc,
                     CmpAdvBldgSF = s.AdvBldgSF,
                     CmpFolioId = s.FolioId.Replace(" ", ""),
                     CmpFolioSiteLink = s.FolioSiteLink,
                     CmpLandFactor = s.LandFactor,
                     CmpMailingAddress = s.MailingAddress,
                     CmpMilage = s.Milage,
                     CmpPropertyOwner = s.PropertyOwner,
                     CmpSearchDate = s.SearchDate,
                     CmpSiteAddressSrch = s.SiteAddressSrch,
                     CmpSrchUse = s.SrchUse,
                     CmpUnitsBEDBath = s.UnitsBEDBath,
                     CmpCompareable = Convert.ToBoolean(s.Compareable),
                 }).FirstOrDefault();
                _lst.Add(_lstFolioProp);
            }
            return _lst;
        }

        public List<DataRow> GetOtherFolioFormRows(int AddressID)
        {
            DataTable dtViewComparision = new DataTable();

            DataColumnCollection columns = dtViewComparision.Columns;
            dtViewComparision.Columns.Add("Comparison", typeof(string));
            dtViewComparision.Columns.Add("FolioId", typeof(string));
            dtViewComparision.Columns.Add("SiteAddress", typeof(string));
            dtViewComparision.Columns.Add("LandFactor", typeof(string));
            dtViewComparision.Columns.Add("AdvBldgSF", typeof(string));
            dtViewComparision.Columns.Add("SalePrice", typeof(string));
            dtViewComparision.Columns.Add("MarketValueSqFt", typeof(string));
            dtViewComparision.Columns.Add("SalePricePrSqFt", typeof(string));
            dtViewComparision.Columns.Add("AssessedValue", typeof(string));
            dtViewComparision.Columns.Add("MailingAddress", typeof(string));
            dtViewComparision.Columns.Add("PropertyOwner", typeof(string));
            dtViewComparision.Columns.Add("AbbreviatedLegalDescription", typeof(string));
            dtViewComparision.Columns.Add("UnitsBEDBath", typeof(string));
            dtViewComparision.Columns.Add("Milage", typeof(string));
            dtViewComparision.Columns.Add("Use", typeof(string));
            dtViewComparision.Columns.Add("ViewProperty", typeof(string));


            var objCmpFolio = (from c in db.tblOTHCompareFolios
                               select c).Where(c => c.OTHCmpFolioByID == AddressID).ToList();


            var getData1 = db.sp_GetOtherPropertydetailsByID(AddressID);
            //var getData1 = db.tblSrchFolioIDs.AsEnumerable().Where(s => s.AddressID == AddressID).FirstOrDefault();

            OtherProperty _VMPropertyFolio = new OtherProperty();

            _VMPropertyFolio = db.sp_GetOtherPropertydetailsByID(AddressID).Select(s => new OtherProperty
            {
                OTHAddressID = s.OTHAddressID,
                OTHAbbreviatedLegalDesc = s.OTHAbbreviatedLegalDesc,
                OTHAdvBldgSF = s.OTHAdvBldgSF,
                OTHFolioId = s.OTHFolioId,
                OTHFolioSiteLink = s.OTHFolioSiteLink,
                OTHLandFactor = s.OTHLandFactor,
                OTHMailingAddress = s.OTHMailingAddress,
                OTHMilage = s.OTHMilage,
                OTHPropertyOwner = s.OTHPropertyOwner,
                OTHSearchDate = s.OTHSearchDate,
                OTHSiteAddressSrch = s.OTHSiteAddressSrch,
                OTHSrchUse = s.OTHSrchUse,
                UnitsBEDBath = s.UnitsBEDBath,
                SalePrice = s.SalePrice,
                SalePricePrSqFt = String.Format("{0:0.00}", s.SalePricePrSqFt),
                AssedMarketSqFt = s.OthMarketValue,
                AssessedValue = s.OthAssessedValue
            }).FirstOrDefault();
            if (_VMPropertyFolio != null)
            {

                DataRow dr = dtViewComparision.NewRow();
                dr["Comparison"] = "Subject Property";
                dr["FolioId"] = _VMPropertyFolio.OTHFolioId.Replace(" ", "");
                dr["SiteAddress"] = _VMPropertyFolio.OTHSiteAddressSrch;
                dr["LandFactor"] = _VMPropertyFolio.OTHLandFactor;
                dr["AdvBldgSF"] = _VMPropertyFolio.OTHAdvBldgSF;
                dr["SalePrice"] = _VMPropertyFolio.SalePrice;
                dr["MarketValueSqFt"] = _VMPropertyFolio.AssedMarketSqFt;
                dr["SalePricePrSqFt"] = _VMPropertyFolio.SalePricePrSqFt;
                dr["AssessedValue"] = _VMPropertyFolio.AssessedValue;
                dr["MailingAddress"] = _VMPropertyFolio.OTHMailingAddress;
                dr["PropertyOwner"] = _VMPropertyFolio.OTHPropertyOwner;
                dr["AbbreviatedLegalDescription"] = _VMPropertyFolio.OTHAbbreviatedLegalDesc;
                dr["UnitsBEDBath"] = _VMPropertyFolio.UnitsBEDBath;
                dr["Milage"] = _VMPropertyFolio.OTHMilage;
                dr["Use"] = _VMPropertyFolio.OTHSrchUse;
                dr["ViewProperty"] = _VMPropertyFolio.OTHFolioSiteLink;

                dtViewComparision.Rows.Add(dr);
            }
            int cnt = 1;
            foreach (var item in objCmpFolio)
            {
                //var getData = db.tblSrchFolioIDs.AsEnumerable().Where(s => s.AddressID == item.CmpFloioToID).FirstOrDefault();

                OtherProperty getData = new OtherProperty();

                getData = db.sp_GetOtherPropertydetailsByID(item.OTHCmpFloioToID).Select(s => new OtherProperty
                {
                    OTHAddressID = s.OTHAddressID,
                    OTHAbbreviatedLegalDesc = s.OTHAbbreviatedLegalDesc,
                    OTHAdvBldgSF = s.OTHAdvBldgSF,
                    OTHFolioId = s.OTHFolioId,
                    OTHFolioSiteLink = s.OTHFolioSiteLink,
                    OTHLandFactor = s.OTHLandFactor,
                    OTHMailingAddress = s.OTHMailingAddress,
                    OTHMilage = s.OTHMilage,
                    OTHPropertyOwner = s.OTHPropertyOwner,
                    OTHSearchDate = s.OTHSearchDate,
                    OTHSiteAddressSrch = s.OTHSiteAddressSrch,
                    OTHSrchUse = s.OTHSrchUse,
                    UnitsBEDBath = s.UnitsBEDBath,
                    SalePrice = s.SalePrice,
                    SalePricePrSqFt = String.Format("{0:0.00}", s.SalePricePrSqFt),
                    AssedMarketSqFt = s.OthMarketValue,
                    AssessedValue = s.OthAssessedValue

                }).FirstOrDefault();

                if (getData != null)
                {
                    DataRow dr = dtViewComparision.NewRow();
                    dr["Comparison"] = "Property " + cnt;
                    dr["FolioId"] = getData.OTHFolioId.Replace(" ", "");
                    dr["SiteAddress"] = getData.OTHSiteAddressSrch;
                    dr["LandFactor"] = getData.OTHLandFactor;
                    dr["AdvBldgSF"] = getData.OTHAdvBldgSF;
                    dr["SalePrice"] = getData.SalePrice;
                    dr["MarketValueSqFt"] = getData.AssedMarketSqFt;
                    dr["SalePricePrSqFt"] = getData.SalePricePrSqFt;
                    dr["AssessedValue"] = getData.AssessedValue;
                    dr["MailingAddress"] = getData.OTHMailingAddress;
                    dr["PropertyOwner"] = getData.OTHPropertyOwner;
                    dr["AbbreviatedLegalDescription"] = getData.OTHAbbreviatedLegalDesc;
                    dr["UnitsBEDBath"] = getData.UnitsBEDBath != "" ? getData.UnitsBEDBath.ToString() : "";
                    dr["Milage"] = getData.OTHMilage;
                    dr["Use"] = getData.OTHSrchUse;
                    dr["ViewProperty"] = getData.OTHFolioSiteLink;

                    dtViewComparision.Rows.Add(dr);
                }
                cnt++;
            }

            DataTable dtComparision = PivoteDataTable.Pivot(dtViewComparision);
            List<DataRow> lstComparision = dtComparision.AsEnumerable().ToList();

            return lstComparision;
        }

        public List<DataRow> GetFolioFormRows(int AddressID)
        {
            DataTable dtViewComparision = new DataTable();

            DataColumnCollection columns = dtViewComparision.Columns;
            dtViewComparision.Columns.Add("Comparison", typeof(string));
            dtViewComparision.Columns.Add("FolioId", typeof(string));
            dtViewComparision.Columns.Add("SiteAddress", typeof(string));
            dtViewComparision.Columns.Add("LandFactor", typeof(string));
            dtViewComparision.Columns.Add("AdvBldgSF", typeof(string));
            dtViewComparision.Columns.Add("SalePrice", typeof(string));
            dtViewComparision.Columns.Add("MarketValueSqFt", typeof(string));
            dtViewComparision.Columns.Add("SalePricePrSqFt", typeof(string));
            dtViewComparision.Columns.Add("AssessedValue", typeof(string));
            dtViewComparision.Columns.Add("MailingAddress", typeof(string));
            dtViewComparision.Columns.Add("PropertyOwner", typeof(string));
            dtViewComparision.Columns.Add("AbbreviatedLegalDescription", typeof(string));
            dtViewComparision.Columns.Add("UnitsBEDBath", typeof(string));
            dtViewComparision.Columns.Add("Milage", typeof(string));
            dtViewComparision.Columns.Add("Use", typeof(string));
            dtViewComparision.Columns.Add("ViewProperty", typeof(string));

            List<CompareFolioID> _lst = new List<CompareFolioID>();
            var objCmpFolio = (from c in db.tblCompareProperties
                               select c).Where(c => c.CmpFolioByID == AddressID).ToList();


            var getData1 = db.sp_GetPropertydetailsByID(AddressID);
            //var getData1 = db.tblSrchFolioIDs.AsEnumerable().Where(s => s.AddressID == AddressID).FirstOrDefault();

            PropertyFolio _VMPropertyFolio = new PropertyFolio();

            _VMPropertyFolio = db.sp_GetPropertydetailsByID(AddressID).Select(s => new PropertyFolio
            {
                AddressID = s.AddressID,
                AbbreviatedLegalDesc = s.AbbreviatedLegalDesc,
                AdvBldgSF = s.AdvBldgSF,
                FolioId = s.FolioId,
                FolioSiteLink = s.FolioSiteLink,
                LandFactor = s.LandFactor,
                MailingAddress = s.MailingAddress,
                Milage = s.Milage,
                PropertyOwner = s.PropertyOwner,
                SearchDate = s.SearchDate,
                SiteAddressSrch = s.SiteAddressSrch,
                SrchUse = s.SrchUse,
                UnitsBEDBath = s.UnitsBEDBath,
                SalePrice = s.SalePrice,
                SalePricePrSqFt = String.Format("{0:0.00}", s.SalePricePrSqFt),
                AssedMarketSqFt = s.MarketValue,
                AssessedValue = s.AssessedValue

            }).FirstOrDefault();
            if (_VMPropertyFolio != null)
            {

                DataRow dr = dtViewComparision.NewRow();
                dr["Comparison"] = "Subject Property";
                dr["FolioId"] = _VMPropertyFolio.FolioId.Replace(" ", "");
                dr["SiteAddress"] = _VMPropertyFolio.SiteAddressSrch;
                dr["LandFactor"] = _VMPropertyFolio.LandFactor;
                dr["AdvBldgSF"] = _VMPropertyFolio.AdvBldgSF;
                dr["SalePrice"] = _VMPropertyFolio.SalePrice;
                dr["MarketValueSqFt"] = _VMPropertyFolio.AssedMarketSqFt;
                dr["SalePricePrSqFt"] = _VMPropertyFolio.SalePricePrSqFt;
                dr["AssessedValue"] = _VMPropertyFolio.AssessedValue;
                dr["MailingAddress"] = _VMPropertyFolio.MailingAddress;
                dr["PropertyOwner"] = _VMPropertyFolio.PropertyOwner;
                dr["AbbreviatedLegalDescription"] = _VMPropertyFolio.AbbreviatedLegalDesc;
                dr["UnitsBEDBath"] = _VMPropertyFolio.UnitsBEDBath;
                dr["Milage"] = _VMPropertyFolio.Milage;
                dr["Use"] = _VMPropertyFolio.SrchUse;
                dr["ViewProperty"] = _VMPropertyFolio.FolioSiteLink;

                dtViewComparision.Rows.Add(dr);
            }
            int cnt = 1;
            foreach (var item in objCmpFolio)
            {
                //var getData = db.tblSrchFolioIDs.AsEnumerable().Where(s => s.AddressID == item.CmpFloioToID).FirstOrDefault();

                PropertyFolio getData = new PropertyFolio();

                getData = db.sp_GetPropertydetailsByID(item.CmpFloioToID).Select(s => new PropertyFolio
                {
                    AddressID = s.AddressID,
                    AbbreviatedLegalDesc = s.AbbreviatedLegalDesc,
                    AdvBldgSF = s.AdvBldgSF,
                    FolioId = s.FolioId,
                    FolioSiteLink = s.FolioSiteLink,
                    LandFactor = s.LandFactor,
                    MailingAddress = s.MailingAddress,
                    Milage = s.Milage,
                    PropertyOwner = s.PropertyOwner,
                    SearchDate = s.SearchDate,
                    SiteAddressSrch = s.SiteAddressSrch,
                    SrchUse = s.SrchUse,
                    UnitsBEDBath = s.UnitsBEDBath,
                    SalePrice = s.SalePrice,
                    SalePricePrSqFt = String.Format("{0:0.00}", s.SalePricePrSqFt),
                    AssedMarketSqFt = s.MarketValue,
                    AssessedValue = s.AssessedValue

                }).FirstOrDefault();

                if (getData != null)
                {
                    DataRow dr = dtViewComparision.NewRow();
                    dr["Comparison"] = "Property " + cnt;
                    dr["FolioId"] = getData.FolioId.Replace(" ", "");
                    dr["SiteAddress"] = getData.SiteAddressSrch;
                    dr["LandFactor"] = getData.LandFactor;
                    dr["LandFactor"] = getData.LandFactor;
                    dr["AdvBldgSF"] = getData.AdvBldgSF;
                    dr["SalePrice"] = getData.SalePrice;
                    dr["MarketValueSqFt"] = getData.AssedMarketSqFt;
                    dr["SalePricePrSqFt"] = getData.SalePricePrSqFt;
                    dr["AssessedValue"] = getData.AssessedValue;
                    dr["MailingAddress"] = getData.MailingAddress;
                    dr["PropertyOwner"] = getData.PropertyOwner;
                    dr["AbbreviatedLegalDescription"] = getData.AbbreviatedLegalDesc;
                    dr["UnitsBEDBath"] = getData.UnitsBEDBath != "" ? getData.UnitsBEDBath.ToString() : "";
                    dr["Milage"] = getData.Milage;
                    dr["Use"] = getData.SrchUse;
                    dr["ViewProperty"] = getData.FolioSiteLink;

                    dtViewComparision.Rows.Add(dr);
                }
                cnt++;
            }

            DataTable dtComparision = PivoteDataTable.Pivot(dtViewComparision);
            List<DataRow> lstComparision = dtComparision.AsEnumerable().ToList();

            return lstComparision;
        }

        public List<DataRow> GetOtherSubjectProperty(int AddressID)
        {
            DataTable dtViewComparision = new DataTable();

            DataColumnCollection columns = dtViewComparision.Columns;
            dtViewComparision.Columns.Add("Adjustments", typeof(string));
            dtViewComparision.Columns.Add("FolioId", typeof(string));
            dtViewComparision.Columns.Add("SiteAddress", typeof(string));
            dtViewComparision.Columns.Add("LandFactor", typeof(string));
            dtViewComparision.Columns.Add("AdvBldgSF", typeof(string));
            dtViewComparision.Columns.Add("SalePrice", typeof(string));
            dtViewComparision.Columns.Add("MarketValueSqFt", typeof(string));
            dtViewComparision.Columns.Add("SalePricePrSqFt", typeof(string));
            dtViewComparision.Columns.Add("AssessedValue", typeof(string));
            dtViewComparision.Columns.Add("MailingAddress", typeof(string));
            dtViewComparision.Columns.Add("PropertyOwner", typeof(string));
            dtViewComparision.Columns.Add("AbbreviatedLegalDescription", typeof(string));
            dtViewComparision.Columns.Add("Units", typeof(string));
            dtViewComparision.Columns.Add("BedRoom", typeof(string));
            dtViewComparision.Columns.Add("BathRoom", typeof(string));
            dtViewComparision.Columns.Add("Milage", typeof(string));
            dtViewComparision.Columns.Add("Use", typeof(string));
            dtViewComparision.Columns.Add("ViewProperty", typeof(string));
            dtViewComparision.Columns.Add("AdjustedValues", typeof(string));

            List<tblOTHCompareFolio> _lst = new List<tblOTHCompareFolio>();

            var objCmpFolio = (from c in db.tblOTHCompareFolios
                               select c).Where(c => c.OTHCmpFolioByID == AddressID).ToList();

            var getData1 = db.sp_GetOtherPropertydetailsByID(AddressID);
            //var getData1 = db.tblSrchFolioIDs.AsEnumerable().Where(s => s.AddressID == AddressID).FirstOrDefault();

            OtherProperty _VMPropertyFolio = new OtherProperty();

            var _adjust = db.tblAdjustments.FirstOrDefault();

            _VMPropertyFolio = db.sp_GetOtherPropertydetailsByID(AddressID).Select(s => new OtherProperty
            {
                OTHAddressID = s.OTHAddressID,
                OTHAbbreviatedLegalDesc = s.OTHAbbreviatedLegalDesc,
                OTHAdvBldgSF = s.OTHAdvBldgSF,
                OTHFolioId = s.OTHFolioId,
                OTHFolioSiteLink = s.OTHFolioSiteLink,
                OTHLandFactor = s.OTHLandFactor,
                OTHMailingAddress = s.OTHMailingAddress,
                OTHMilage = s.OTHMilage,
                OTHPropertyOwner = s.OTHPropertyOwner,
                OTHSearchDate = s.OTHSearchDate,
                OTHSiteAddressSrch = s.OTHSiteAddressSrch,
                OTHSrchUse = s.OTHSrchUse,
                UnitsBEDBath = s.UnitsBEDBath,
                SalePrice = s.SalePrice,
                SalePricePrSqFt = String.Format("{0:0.00}", s.SalePricePrSqFt),
                AssedMarketSqFt = s.OthMarketValue,
                AssessedValue = s.OthAssessedValue

            }).FirstOrDefault();
            if (_VMPropertyFolio != null)
            {
                DataRow dr1 = dtViewComparision.NewRow();
                dr1["Adjustments"] = "Adjustment";
                dr1["FolioId"] = "";
                dr1["SiteAddress"] = "";
                dr1["LandFactor"] = "";
                dr1["AdvBldgSF"] = "";
                dr1["SalePrice"] = "";
                dr1["MarketValueSqFt"] = "";
                dr1["SalePricePrSqFt"] = "";
                dr1["AssessedValue"] = "";
                dr1["MailingAddress"] = "";
                dr1["PropertyOwner"] = "";
                dr1["AbbreviatedLegalDescription"] = "";
                dr1["Units"] = _adjust == null ? 4000 : _adjust.AdjUnits;
                dr1["BedRoom"] = _adjust == null ? 4000 : _adjust.AdjBedRoom;
                dr1["BathRoom"] = _adjust == null ? 4000 : _adjust.AdjBathRoom;
                dr1["Milage"] = "";
                dr1["Use"] = "";
                dr1["ViewProperty"] = "";

                dtViewComparision.Rows.Add(dr1);

                DataRow dr = dtViewComparision.NewRow();

                dr["Adjustments"] = "Subject";
                dr["FolioId"] = _VMPropertyFolio.OTHFolioId.Replace(" ", "");
                dr["SiteAddress"] = _VMPropertyFolio.OTHSiteAddressSrch;
                dr["LandFactor"] = _VMPropertyFolio.OTHLandFactor;
                dr["AdvBldgSF"] = _VMPropertyFolio.OTHAdvBldgSF;
                dr["SalePrice"] = _VMPropertyFolio.SalePrice;
                dr["MarketValueSqFt"] = _VMPropertyFolio.AssedMarketSqFt;
                dr["SalePricePrSqFt"] = _VMPropertyFolio.SalePricePrSqFt;
                dr["AssessedValue"] = _VMPropertyFolio.AssessedValue;
                dr["MailingAddress"] = _VMPropertyFolio.OTHMailingAddress;
                dr["PropertyOwner"] = _VMPropertyFolio.OTHPropertyOwner;
                dr["AbbreviatedLegalDescription"] = _VMPropertyFolio.OTHAbbreviatedLegalDesc;
                string[] _ArrUnits = _VMPropertyFolio.UnitsBEDBath.Split('/');
                if (_ArrUnits.Length > 0)
                {
                    dr["Units"] = _ArrUnits[0] == "" ? "0" : _ArrUnits[0].ToString();
                }
                if (_ArrUnits.Length > 1)
                {
                    dr["BedRoom"] = _ArrUnits[1] == "" ? "0" : _ArrUnits[1].ToString();
                }
                if (_ArrUnits.Length > 2)
                {
                    dr["BathRoom"] = _ArrUnits[2] == "" ? "0" : _ArrUnits[2].ToString();
                }
                dr["Milage"] = _VMPropertyFolio.OTHMilage;
                dr["Use"] = _VMPropertyFolio.OTHSrchUse;
                dr["ViewProperty"] = _VMPropertyFolio.OTHFolioSiteLink;

                dtViewComparision.Rows.Add(dr);

            }
            int cnt = 1;
            int _Units = 0;
            int _BedRoom = 0;
            int _BathRoom = 0;
            int _Lanfactor = 0;
            int _AdvBldgSF = 0;
            int _SalePrice = 0;
            int _Milage = 0;
            int _Use = 0;
            int _AssedMarketSqFt = 0;
            decimal _SalePricePrSqFt = 0;
            string _UnitVal = "";
            string _BedVal = "";
            string _BathVal = "";
            foreach (var item in objCmpFolio)
            {

                OtherProperty getData = new OtherProperty();

                getData = db.sp_GetOtherPropertydetailsByID(item.OTHCmpFloioToID).Select(s => new OtherProperty
                {
                    OTHAddressID = s.OTHAddressID,
                    OTHAbbreviatedLegalDesc = s.OTHAbbreviatedLegalDesc,
                    OTHAdvBldgSF = s.OTHAdvBldgSF,
                    OTHFolioId = s.OTHFolioId,
                    OTHFolioSiteLink = s.OTHFolioSiteLink,
                    OTHLandFactor = s.OTHLandFactor,
                    OTHMailingAddress = s.OTHMailingAddress,
                    OTHMilage = s.OTHMilage,
                    OTHPropertyOwner = s.OTHPropertyOwner,
                    OTHSearchDate = s.OTHSearchDate,
                    OTHSiteAddressSrch = s.OTHSiteAddressSrch,
                    OTHSrchUse = s.OTHSrchUse,
                    UnitsBEDBath = s.UnitsBEDBath,
                    SalePrice = s.SalePrice,
                    SalePricePrSqFt = String.Format("{0:0.00}", s.SalePricePrSqFt),
                    AssedMarketSqFt = s.OthMarketValue,
                    AssessedValue = s.OthAssessedValue

                }).FirstOrDefault();

                if (getData != null)
                {
                    DataRow dr = dtViewComparision.NewRow();
                    dr["Adjustments"] = "Comp." + cnt;
                    dr["FolioId"] = getData.OTHFolioId.Replace(" ", "");
                    dr["SiteAddress"] = getData.OTHSiteAddressSrch;
                    dr["LandFactor"] = getData.OTHLandFactor;
                    dr["AdvBldgSF"] = getData.OTHAdvBldgSF;
                    dr["SalePrice"] = getData.SalePrice;
                    dr["MarketValueSqFt"] = getData.AssedMarketSqFt;
                    dr["SalePricePrSqFt"] = getData.SalePricePrSqFt;
                    dr["AssessedValue"] = getData.AssessedValue;
                    dr["MailingAddress"] = getData.OTHMailingAddress;
                    dr["PropertyOwner"] = getData.OTHPropertyOwner;
                    dr["AbbreviatedLegalDescription"] = getData.OTHAbbreviatedLegalDesc;
                    dr["Units"] = getData.UnitsBEDBath.Split('/')[0];
                    dr["BedRoom"] = getData.UnitsBEDBath.Split('/')[1];
                    dr["BathRoom"] = getData.UnitsBEDBath.Split('/')[2];
                    dr["Milage"] = getData.OTHMilage;
                    dr["Use"] = getData.OTHSrchUse;
                    dr["ViewProperty"] = getData.OTHFolioSiteLink;
                    dr["AdjustedValues"] = "Adjusted Price";
                    dtViewComparision.Rows.Add(dr);

                    int lanfactor = Convert.ToInt32(Convert.ToDecimal(_VMPropertyFolio.OTHLandFactor == "" ? "0" : _VMPropertyFolio.OTHLandFactor.Replace(",", ""))) - Convert.ToInt32(Convert.ToDecimal(getData.OTHLandFactor.Replace(",", "")));
                    string[] _ArrUnt = _VMPropertyFolio.UnitsBEDBath.Split('/');

                    if (_ArrUnt.Length > 0)
                    {
                        _UnitVal = _ArrUnt[0] == "" ? "0" : _ArrUnt[0].ToString();
                    }
                    if (_ArrUnt.Length > 1)
                    {
                        _BedVal = _ArrUnt[1] == "" ? "0" : _ArrUnt[1].ToString();
                    }
                    if (_ArrUnt.Length > 2)
                    {
                        _BathVal = _ArrUnt[2] == "" ? "0" : _ArrUnt[2].ToString();
                    }

                    int Units = (Convert.ToInt32(Math.Round(Convert.ToDouble(_UnitVal == "" ? "0" : _UnitVal))) - Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[0])))) * Convert.ToInt32(_adjust == null ? 4000 : _adjust.AdjUnits);
                    int BedRoom = (Convert.ToInt32(Math.Round(Convert.ToDouble(_BedVal == "" ? "0" : _BedVal))) - Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[1])))) * Convert.ToInt32(_adjust == null ? 4000 : _adjust.AdjBedRoom);
                    int BathRoom = (Convert.ToInt32(Math.Round(Convert.ToDouble(_BathVal == "" ? "0" : _BathVal))) - Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[2])))) * Convert.ToInt32(_adjust == null ? 4000 : _adjust.AdjBathRoom);
                    _Units += Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[0])));
                    _BedRoom += Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[1])));
                    _BathRoom += Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[2])));
                    _Lanfactor += Convert.ToInt32(Convert.ToDecimal(getData.OTHLandFactor.Replace(",", "")));
                    _AdvBldgSF += Convert.ToInt32(getData.OTHAdvBldgSF.Replace(",", "").Replace("$", ""));
                    _SalePrice += Convert.ToInt32(getData.SalePrice.Replace(",", "").Replace("$", ""));
                    _AssedMarketSqFt += Convert.ToInt32(getData.AssedMarketSqFt.Replace(",", "").Replace("$", ""));
                    _SalePricePrSqFt += Convert.ToDecimal(getData.SalePricePrSqFt.Replace(",", "").Replace("$", ""));
                    _Milage += Convert.ToInt32(getData.OTHMilage.Replace(",", "").Replace("$", ""));
                    _Use += Convert.ToInt32(getData.OTHSrchUse.Replace(",", "").Replace("$", ""));

                    DataRow dr1 = dtViewComparision.NewRow();
                    dr1["Adjustments"] = "Adustment.#";
                    dr1["FolioId"] = "";
                    dr1["SiteAddress"] = "";
                    dr1["LandFactor"] = lanfactor;
                    dr1["AdvBldgSF"] = "";
                    dr1["SalePrice"] = "";
                    dr1["MarketValueSqFt"] = "";
                    dr1["SalePricePrSqFt"] = "";
                    dr1["AssessedValue"] = "";
                    dr1["MailingAddress"] = "";
                    dr1["PropertyOwner"] = "";
                    dr1["AbbreviatedLegalDescription"] = "";
                    dr1["Units"] = Units;
                    dr1["BedRoom"] = BedRoom;
                    dr1["BathRoom"] = BathRoom;
                    dr1["Milage"] = "";
                    dr1["Use"] = "";
                    dr1["ViewProperty"] = "";
                    dr1["AdjustedValues"] = lanfactor + Units + BedRoom + BathRoom;

                    dtViewComparision.Rows.Add(dr1);
                }
                cnt++;
            }
            int _AvgUnits = ((Convert.ToInt32(Math.Round(Convert.ToDouble(_UnitVal == "" ? "0" : _UnitVal))) + _Units) / cnt);
            int _AvgBedRoom = ((Convert.ToInt32(Math.Round(Convert.ToDouble(_BedVal == "" ? "0" : _BedVal))) + _BedRoom) / cnt);
            int _AvgBathRoom = ((Convert.ToInt32(Math.Round(Convert.ToDouble(_BathVal == "" ? "0" : _BathVal))) + _BathRoom) / cnt);
            int _AvgLandfactor = ((Convert.ToInt32(_VMPropertyFolio.OTHLandFactor == "" ? "0" : _VMPropertyFolio.OTHLandFactor.Replace(",", "")) + _Lanfactor) / cnt);

            int _AVGAdvBldgSF = ((Convert.ToInt32(_VMPropertyFolio.OTHAdvBldgSF.Replace(",", "")) + _AdvBldgSF) / cnt);
            int _AVGSalePrice = ((Convert.ToInt32(_VMPropertyFolio.SalePrice == "" ? "0" : _VMPropertyFolio.SalePrice.Replace(",", "").Replace(",", "").Replace("$", "")) + _SalePrice) / cnt);
            int _AVGAssedMarketSqFt = ((Convert.ToInt32(_VMPropertyFolio.AssedMarketSqFt.Replace(",", "").Replace(",", "").Replace("$", "")) + _AssedMarketSqFt) / cnt);
            decimal _AVGSalePricePrSqFt = ((Convert.ToDecimal(String.Format("{0:0.00}", _VMPropertyFolio.SalePricePrSqFt).Replace(",", "").Replace("$", "")) + _SalePricePrSqFt) / cnt);
            int _AVGMilage = ((Convert.ToInt32(_VMPropertyFolio.OTHMilage.Replace(",", "")) + _Milage) / cnt);
            int _AVGUse = ((Convert.ToInt32(_VMPropertyFolio.OTHSrchUse.Replace(",", "")) + _Use) / cnt);

            DataRow dr2 = dtViewComparision.NewRow();
            dr2["Adjustments"] = "Average";
            dr2["FolioId"] = "";
            dr2["SiteAddress"] = "";
            dr2["LandFactor"] = _AvgLandfactor;
            dr2["AdvBldgSF"] = _AVGAdvBldgSF;
            dr2["SalePrice"] = _AVGSalePrice;
            dr2["MarketValueSqFt"] = _AVGAssedMarketSqFt;
            dr2["SalePricePrSqFt"] = String.Format("{0:0.00}", _AVGSalePricePrSqFt);
            dr2["AssessedValue"] = "";
            dr2["MailingAddress"] = "";
            dr2["PropertyOwner"] = "";
            dr2["AbbreviatedLegalDescription"] = "";
            dr2["Units"] = _AvgUnits;
            dr2["BedRoom"] = _AvgBedRoom;
            dr2["BathRoom"] = _AvgBathRoom;
            dr2["Milage"] = _AVGMilage;
            dr2["Use"] = _AVGUse;
            dr2["ViewProperty"] = "";
            dr2["AdjustedValues"] = "";

            dtViewComparision.Rows.Add(dr2);

            DataTable dtComparision = PivoteDataTable.Pivot(dtViewComparision);
            List<DataRow> lstComparision = dtComparision.AsEnumerable().ToList();

            return lstComparision;
        }

        public List<DataRow> GetSubjectProperty(int AddressID)
        {
            DataTable dtViewComparision = new DataTable();

            DataColumnCollection columns = dtViewComparision.Columns;
            dtViewComparision.Columns.Add("Adjustments", typeof(string));
            dtViewComparision.Columns.Add("FolioId", typeof(string));
            dtViewComparision.Columns.Add("SiteAddress", typeof(string));
            dtViewComparision.Columns.Add("LandFactor", typeof(string));
            dtViewComparision.Columns.Add("AdvBldgSF", typeof(string));
            dtViewComparision.Columns.Add("SalePrice", typeof(string));
            dtViewComparision.Columns.Add("MarketValueSqFt", typeof(string));
            dtViewComparision.Columns.Add("SalePricePrSqFt", typeof(string));
            dtViewComparision.Columns.Add("AssessedValue", typeof(string));
            dtViewComparision.Columns.Add("MailingAddress", typeof(string));
            dtViewComparision.Columns.Add("PropertyOwner", typeof(string));
            dtViewComparision.Columns.Add("AbbreviatedLegalDescription", typeof(string));
            dtViewComparision.Columns.Add("Units", typeof(string));
            dtViewComparision.Columns.Add("BedRoom", typeof(string));
            dtViewComparision.Columns.Add("BathRoom", typeof(string));
            dtViewComparision.Columns.Add("Milage", typeof(string));
            dtViewComparision.Columns.Add("Use", typeof(string));
            dtViewComparision.Columns.Add("ViewProperty", typeof(string));
            dtViewComparision.Columns.Add("AdjustedValues", typeof(string));

            List<CompareFolioID> _lst = new List<CompareFolioID>();

            var objCmpFolio = (from c in db.tblCompareProperties
                               select c).Where(c => c.CmpFolioByID == AddressID).ToList();

            var getData1 = db.sp_GetPropertydetailsByID(AddressID);
            //var getData1 = db.tblSrchFolioIDs.AsEnumerable().Where(s => s.AddressID == AddressID).FirstOrDefault();

            PropertyFolio _VMPropertyFolio = new PropertyFolio();

            var _adjust = db.tblAdjustments.FirstOrDefault();

            _VMPropertyFolio = db.sp_GetPropertydetailsByID(AddressID).Select(s => new PropertyFolio
            {
                AddressID = s.AddressID,
                AbbreviatedLegalDesc = s.AbbreviatedLegalDesc,
                AdvBldgSF = s.AdvBldgSF,
                FolioId = s.FolioId,
                FolioSiteLink = s.FolioSiteLink,
                LandFactor = s.LandFactor,
                MailingAddress = s.MailingAddress,
                Milage = s.Milage,
                PropertyOwner = s.PropertyOwner,
                SearchDate = s.SearchDate,
                SiteAddressSrch = s.SiteAddressSrch,
                SrchUse = s.SrchUse,
                UnitsBEDBath = s.UnitsBEDBath,
                SalePrice = s.SalePrice,
                SalePricePrSqFt = String.Format("{0:0.00}", s.SalePricePrSqFt),
                AssedMarketSqFt = s.MarketValue,
                AssessedValue = s.AssessedValue

            }).FirstOrDefault();
            if (_VMPropertyFolio != null)
            {
                DataRow dr1 = dtViewComparision.NewRow();
                dr1["Adjustments"] = "Adjustment";
                dr1["FolioId"] = "";
                dr1["SiteAddress"] = "";
                dr1["LandFactor"] = "";
                dr1["AdvBldgSF"] = "";
                dr1["SalePrice"] = "";
                dr1["MarketValueSqFt"] = "";
                dr1["SalePricePrSqFt"] = "";
                dr1["AssessedValue"] = "";
                dr1["MailingAddress"] = "";
                dr1["PropertyOwner"] = "";
                dr1["AbbreviatedLegalDescription"] = "";
                dr1["Units"] = _adjust == null ? 4000 : _adjust.AdjUnits;
                dr1["BedRoom"] = _adjust == null ? 4000 : _adjust.AdjBedRoom;
                dr1["BathRoom"] = _adjust == null ? 4000 : _adjust.AdjBathRoom;
                dr1["Milage"] = "";
                dr1["Use"] = "";
                dr1["ViewProperty"] = "";

                dtViewComparision.Rows.Add(dr1);

                DataRow dr = dtViewComparision.NewRow();

                dr["Adjustments"] = "Subject";
                dr["FolioId"] = _VMPropertyFolio.FolioId.Replace(" ", "");
                dr["SiteAddress"] = _VMPropertyFolio.SiteAddressSrch;
                dr["LandFactor"] = _VMPropertyFolio.LandFactor;
                dr["AdvBldgSF"] = _VMPropertyFolio.AdvBldgSF;
                dr["SalePrice"] = _VMPropertyFolio.SalePrice;
                dr["MarketValueSqFt"] = _VMPropertyFolio.AssedMarketSqFt;
                dr["SalePricePrSqFt"] = _VMPropertyFolio.SalePricePrSqFt;
                dr["AssessedValue"] = _VMPropertyFolio.SalePricePrSqFt;
                dr["MailingAddress"] = _VMPropertyFolio.MailingAddress;
                dr["PropertyOwner"] = _VMPropertyFolio.PropertyOwner;
                dr["AbbreviatedLegalDescription"] = _VMPropertyFolio.AbbreviatedLegalDesc;
                dr["Units"] = _VMPropertyFolio.UnitsBEDBath.Split('/')[0];
                dr["BedRoom"] = _VMPropertyFolio.UnitsBEDBath.Split('/')[1];
                dr["BathRoom"] = _VMPropertyFolio.UnitsBEDBath.Split('/')[2];
                dr["Milage"] = _VMPropertyFolio.Milage;
                dr["Use"] = _VMPropertyFolio.SrchUse;
                dr["ViewProperty"] = _VMPropertyFolio.FolioSiteLink;

                dtViewComparision.Rows.Add(dr);

                //"<input type='text' id='SubAdjust-@count' class='form-control' />"
            }
            int cnt = 1;
            int _Units = 0;
            int _BedRoom = 0;
            int _BathRoom = 0;
            int _Lanfactor = 0;
            int _AdvBldgSF = 0;
            int _SalePrice = 0;
            int _Milage = 0;
            int _Use = 0;
            int _AssedMarketSqFt = 0;
            decimal _SalePricePrSqFt = 0;
            foreach (var item in objCmpFolio)
            {
                //var getData = db.tblSrchFolioIDs.AsEnumerable().Where(s => s.AddressID == item.CmpFloioToID).FirstOrDefault();

                PropertyFolio getData = new PropertyFolio();

                getData = db.sp_GetPropertydetailsByID(item.CmpFloioToID).Select(s => new PropertyFolio
                {
                    AddressID = s.AddressID,
                    AbbreviatedLegalDesc = s.AbbreviatedLegalDesc,
                    AdvBldgSF = s.AdvBldgSF,
                    FolioId = s.FolioId,
                    FolioSiteLink = s.FolioSiteLink,
                    LandFactor = s.LandFactor,
                    MailingAddress = s.MailingAddress,
                    Milage = s.Milage,
                    PropertyOwner = s.PropertyOwner,
                    SearchDate = s.SearchDate,
                    SiteAddressSrch = s.SiteAddressSrch,
                    SrchUse = s.SrchUse,
                    UnitsBEDBath = s.UnitsBEDBath,
                    SalePrice = s.SalePrice,
                    SalePricePrSqFt = String.Format("{0:0.00}", s.SalePricePrSqFt),
                    AssedMarketSqFt = s.MarketValue,
                    AssessedValue = s.AssessedValue

                }).FirstOrDefault();

                if (getData != null)
                {
                    DataRow dr = dtViewComparision.NewRow();
                    dr["Adjustments"] = "Comp." + cnt;
                    dr["FolioId"] = getData.FolioId.Replace(" ", "");
                    dr["SiteAddress"] = getData.SiteAddressSrch;
                    dr["LandFactor"] = getData.LandFactor;
                    dr["LandFactor"] = getData.LandFactor;
                    dr["AdvBldgSF"] = getData.AdvBldgSF;
                    dr["SalePrice"] = getData.SalePrice;
                    dr["MarketValueSqFt"] = getData.AssedMarketSqFt;
                    dr["SalePricePrSqFt"] = getData.SalePricePrSqFt;
                    dr["AssessedValue"] = getData.AssessedValue;
                    dr["MailingAddress"] = getData.MailingAddress;
                    dr["PropertyOwner"] = getData.PropertyOwner;
                    dr["AbbreviatedLegalDescription"] = getData.AbbreviatedLegalDesc;
                    dr["Units"] = getData.UnitsBEDBath.Split('/')[0];
                    dr["BedRoom"] = getData.UnitsBEDBath.Split('/')[1];
                    dr["BathRoom"] = getData.UnitsBEDBath.Split('/')[2];
                    dr["Milage"] = getData.Milage;
                    dr["Use"] = getData.SrchUse;
                    dr["ViewProperty"] = getData.FolioSiteLink;
                    dr["AdjustedValues"] = "Adjusted Price";
                    dtViewComparision.Rows.Add(dr);

                    int lanfactor = Convert.ToInt32(_VMPropertyFolio.LandFactor == "" ? "0" : _VMPropertyFolio.LandFactor.Replace(",", "")) - Convert.ToInt32(getData.LandFactor.Replace(",", ""));
                    int Units = (Convert.ToInt32(Math.Round(Convert.ToDouble(_VMPropertyFolio.UnitsBEDBath.Split('/')[0]))) - Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[0])))) * Convert.ToInt32(_adjust == null ? 4000 : _adjust.AdjUnits);
                    int BedRoom = (Convert.ToInt32(Math.Round(Convert.ToDouble(_VMPropertyFolio.UnitsBEDBath.Split('/')[1]))) - Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[1])))) * Convert.ToInt32(_adjust == null ? 4000 : _adjust.AdjBedRoom);
                    int BathRoom = (Convert.ToInt32(Math.Round(Convert.ToDouble(_VMPropertyFolio.UnitsBEDBath.Split('/')[2]))) - Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[2])))) * Convert.ToInt32(_adjust == null ? 4000 : _adjust.AdjBathRoom);
                    _Units += Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[0])));
                    _BedRoom += Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[1])));
                    _BathRoom += Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[2])));
                    _Lanfactor += Convert.ToInt32(getData.LandFactor.Replace(",", ""));
                    _AdvBldgSF += Convert.ToInt32(getData.AdvBldgSF.Replace(",", "").Replace("$", ""));
                    _SalePrice += Convert.ToInt32(getData.SalePrice.Replace(",", "").Replace("$", ""));
                    _AssedMarketSqFt += Convert.ToInt32(getData.AssedMarketSqFt.Replace(",", "").Replace("$", ""));
                    _SalePricePrSqFt += Convert.ToDecimal(getData.SalePricePrSqFt.Replace(",", "").Replace("$", ""));
                    _Milage += Convert.ToInt32(getData.Milage.Replace(",", "").Replace("$", ""));
                    _Use += Convert.ToInt32(getData.SrchUse.Replace(",", "").Replace("$", ""));

                    DataRow dr1 = dtViewComparision.NewRow();
                    dr1["Adjustments"] = "Adustment.#";
                    dr1["FolioId"] = "";
                    dr1["SiteAddress"] = "";
                    dr1["LandFactor"] = lanfactor;
                    dr1["AdvBldgSF"] = "";
                    dr1["SalePrice"] = "";
                    dr1["MarketValueSqFt"] = "";
                    dr1["SalePricePrSqFt"] = "";
                    dr["AssessedValue"] = "";
                    dr1["MailingAddress"] = "";
                    dr1["PropertyOwner"] = "";
                    dr1["AbbreviatedLegalDescription"] = "";
                    dr1["Units"] = Units;
                    dr1["BedRoom"] = BedRoom;
                    dr1["BathRoom"] = BathRoom;
                    dr1["Milage"] = "";
                    dr1["Use"] = "";
                    dr1["ViewProperty"] = "";
                    dr1["AdjustedValues"] = lanfactor + Units + BedRoom + BathRoom;

                    dtViewComparision.Rows.Add(dr1);
                }
                cnt++;
            }
            int _AvgUnits = ((Convert.ToInt32(Math.Round(Convert.ToDouble(_VMPropertyFolio.UnitsBEDBath.Split('/')[0]))) + _Units) / cnt);
            int _AvgBedRoom = ((Convert.ToInt32(Math.Round(Convert.ToDouble(_VMPropertyFolio.UnitsBEDBath.Split('/')[1]))) + _BedRoom) / cnt);
            int _AvgBathRoom = ((Convert.ToInt32(Math.Round(Convert.ToDouble(_VMPropertyFolio.UnitsBEDBath.Split('/')[2]))) + _BathRoom) / cnt);
            int _AvgLandfactor = ((Convert.ToInt32(_VMPropertyFolio.LandFactor == "" ? "0" : _VMPropertyFolio.LandFactor.Replace(",", "")) + _Lanfactor) / cnt);

            int _AVGAdvBldgSF = ((Convert.ToInt32(_VMPropertyFolio.AdvBldgSF.Replace(",", "")) + _AdvBldgSF) / cnt);
            int _AVGSalePrice = ((Convert.ToInt32(_VMPropertyFolio.SalePrice.Replace(",", "").Replace(",", "").Replace("$", "")) + _SalePrice) / cnt);
            int _AVGAssedMarketSqFt = ((Convert.ToInt32(_VMPropertyFolio.AssedMarketSqFt.Replace(",", "").Replace(",", "").Replace("$", "")) + _AssedMarketSqFt) / cnt);
            decimal _AVGSalePricePrSqFt = ((Convert.ToDecimal(String.Format("{0:0.00}", _VMPropertyFolio.SalePricePrSqFt).Replace(",", "").Replace("$", "")) + _SalePricePrSqFt) / cnt);
            int _AVGMilage = ((Convert.ToInt32(_VMPropertyFolio.Milage.Replace(",", "")) + _Milage) / cnt);
            int _AVGUse = ((Convert.ToInt32(_VMPropertyFolio.SrchUse.Replace(",", "")) + _Use) / cnt);

            DataRow dr2 = dtViewComparision.NewRow();
            dr2["Adjustments"] = "Average";
            dr2["FolioId"] = "";
            dr2["SiteAddress"] = "";
            dr2["LandFactor"] = _AvgLandfactor;
            dr2["AdvBldgSF"] = _AVGAdvBldgSF;
            dr2["SalePrice"] = _AVGSalePrice;
            dr2["MarketValueSqFt"] = _AVGAssedMarketSqFt;
            dr2["SalePricePrSqFt"] = String.Format("{0:0.00}", _AVGSalePricePrSqFt);
            dr2["AssessedValue"] = "";
            dr2["MailingAddress"] = "";
            dr2["PropertyOwner"] = "";
            dr2["AbbreviatedLegalDescription"] = "";
            dr2["Units"] = _AvgUnits;
            dr2["BedRoom"] = _AvgBedRoom;
            dr2["BathRoom"] = _AvgBathRoom;
            dr2["Milage"] = _AVGMilage;
            dr2["Use"] = _AVGUse;
            dr2["ViewProperty"] = "";
            dr2["AdjustedValues"] = "";

            dtViewComparision.Rows.Add(dr2);

            DataTable dtComparision = PivoteDataTable.Pivot(dtViewComparision);
            List<DataRow> lstComparision = dtComparision.AsEnumerable().ToList();

            return lstComparision;
        }

        public List<DataRow> GetOtherCalculateAdjustProperty(int AddressID, int? txtAdvBldgSF, int? txtSalePrice, int? txtAssedMarketSqFt, int? txtUnits, int? txtBedRoom, int? txtBathRoom, int? txtMilage, int? txtUse)
        {
            DataTable dtViewComparision = new DataTable();

            DataColumnCollection columns = dtViewComparision.Columns;
            dtViewComparision.Columns.Add("Adjustments", typeof(string));
            dtViewComparision.Columns.Add("FolioId", typeof(string));
            dtViewComparision.Columns.Add("SiteAddress", typeof(string));
            dtViewComparision.Columns.Add("LandFactor", typeof(string));
            dtViewComparision.Columns.Add("AdvBldgSF", typeof(string));
            dtViewComparision.Columns.Add("SalePrice", typeof(string));
            dtViewComparision.Columns.Add("MarketValueSqFt", typeof(string));
            dtViewComparision.Columns.Add("SalePricePrSqFt", typeof(string));
            dtViewComparision.Columns.Add("AssessedValue", typeof(string));
            dtViewComparision.Columns.Add("MailingAddress", typeof(string));
            dtViewComparision.Columns.Add("PropertyOwner", typeof(string));
            dtViewComparision.Columns.Add("AbbreviatedLegalDescription", typeof(string));
            dtViewComparision.Columns.Add("Units", typeof(string));
            dtViewComparision.Columns.Add("BedRoom", typeof(string));
            dtViewComparision.Columns.Add("BathRoom", typeof(string));
            dtViewComparision.Columns.Add("Milage", typeof(string));
            dtViewComparision.Columns.Add("Use", typeof(string));
            dtViewComparision.Columns.Add("ViewProperty", typeof(string));
            dtViewComparision.Columns.Add("AdjustedValues", typeof(string));


            var objCmpFolio = (from c in db.tblOTHCompareFolios
                               select c).Where(c => c.OTHCmpFolioByID == AddressID).ToList();

            var getData1 = db.sp_GetOtherPropertydetailsByID(AddressID);
            //var getData1 = db.tblSrchFolioIDs.AsEnumerable().Where(s => s.AddressID == AddressID).FirstOrDefault();
            var _adjust = db.tblAdjustments.FirstOrDefault();

            OtherProperty _VMPropertyFolio = new OtherProperty();

            _VMPropertyFolio = db.sp_GetOtherPropertydetailsByID(AddressID).Select(s => new OtherProperty
            {
                OTHAddressID = s.OTHAddressID,
                OTHAbbreviatedLegalDesc = s.OTHAbbreviatedLegalDesc,
                OTHAdvBldgSF = s.OTHAdvBldgSF,
                OTHFolioId = s.OTHFolioId,
                OTHFolioSiteLink = s.OTHFolioSiteLink,
                OTHLandFactor = s.OTHLandFactor,
                OTHMailingAddress = s.OTHMailingAddress,
                OTHMilage = s.OTHMilage,
                OTHPropertyOwner = s.OTHPropertyOwner,
                OTHSearchDate = s.OTHSearchDate,
                OTHSiteAddressSrch = s.OTHSiteAddressSrch,
                OTHSrchUse = s.OTHSrchUse,
                UnitsBEDBath = s.UnitsBEDBath,
                SalePrice = s.SalePrice,
                SalePricePrSqFt = String.Format("{0:0.00}", s.SalePricePrSqFt),
                AssedMarketSqFt = s.OthMarketValue,
                AssessedValue = s.OthAssessedValue

            }).FirstOrDefault();

            string _UnitVal = "";
            string _BedVal = "";
            string _BathVal = "";

            if (_VMPropertyFolio != null)
            {
                DataRow dr1 = dtViewComparision.NewRow();
                dr1["Adjustments"] = "Adjustment";
                dr1["FolioId"] = "";
                dr1["SiteAddress"] = "";
                dr1["LandFactor"] = "";
                dr1["AdvBldgSF"] = txtAdvBldgSF;
                dr1["SalePrice"] = txtSalePrice;
                dr1["MarketValueSqFt"] = txtAssedMarketSqFt;
                dr1["SalePricePrSqFt"] = "";
                dr1["AssessedValue"] = "";
                dr1["MailingAddress"] = "";
                dr1["PropertyOwner"] = "";
                dr1["AbbreviatedLegalDescription"] = "";
                dr1["Units"] = txtUnits;
                dr1["BedRoom"] = txtBedRoom;
                dr1["BathRoom"] = txtBathRoom;
                dr1["Milage"] = txtMilage;
                dr1["Use"] = txtUse;
                dr1["ViewProperty"] = "";

                dtViewComparision.Rows.Add(dr1);

                DataRow dr = dtViewComparision.NewRow();

                dr["Adjustments"] = "Subject";
                dr["FolioId"] = _VMPropertyFolio.OTHFolioId.Replace(" ", "");
                dr["SiteAddress"] = _VMPropertyFolio.OTHSiteAddressSrch;
                dr["LandFactor"] = _VMPropertyFolio.OTHLandFactor;
                dr["AdvBldgSF"] = _VMPropertyFolio.OTHAdvBldgSF;
                dr["SalePrice"] = _VMPropertyFolio.SalePrice;
                dr["MarketValueSqFt"] = _VMPropertyFolio.AssedMarketSqFt;
                dr["SalePricePrSqFt"] = _VMPropertyFolio.SalePricePrSqFt;
                dr["AssessedValue"] = _VMPropertyFolio.AssessedValue;
                dr["MailingAddress"] = _VMPropertyFolio.OTHMailingAddress;
                dr["PropertyOwner"] = _VMPropertyFolio.OTHPropertyOwner;
                dr["AbbreviatedLegalDescription"] = _VMPropertyFolio.OTHAbbreviatedLegalDesc;
                string[] _ArrUnt = _VMPropertyFolio.UnitsBEDBath.Split('/');

                if (_ArrUnt.Length > 0)
                {
                    _UnitVal = _ArrUnt[0] == "" ? "0" : _ArrUnt[0].ToString();
                }
                if (_ArrUnt.Length > 1)
                {
                    _BedVal = _ArrUnt[1] == "" ? "0" : _ArrUnt[1].ToString();
                }
                if (_ArrUnt.Length > 2)
                {
                    _BathVal = _ArrUnt[2] == "" ? "0" : _ArrUnt[2].ToString();
                }

                dr["Units"] = _UnitVal;
                dr["BedRoom"] = _BedVal;
                dr["BathRoom"] = _BathVal;
                dr["Milage"] = _VMPropertyFolio.OTHMilage;
                dr["Use"] = _VMPropertyFolio.OTHSrchUse;
                dr["ViewProperty"] = _VMPropertyFolio.OTHFolioSiteLink;

                dtViewComparision.Rows.Add(dr);

                //"<input type='text' id='SubAdjust-@count' class='form-control' />"
            }
            int cnt = 1;
            int _Units = 0;
            int _BedRoom = 0;
            int _BathRoom = 0;
            int _Lanfactor = 0;
            int _AdvBldgSF = 0;
            int _SalePrice = 0;
            int _Milage = 0;
            int _Use = 0;
            int _AssedMarketSqFt = 0;
            decimal _SalePricePrSqFt = 0;
            foreach (var item in objCmpFolio)
            {
                //var getData = db.tblSrchFolioIDs.AsEnumerable().Where(s => s.AddressID == item.CmpFloioToID).FirstOrDefault();

                OtherProperty getData = new OtherProperty();

                getData = db.sp_GetOtherPropertydetailsByID(item.OTHCmpFloioToID).Select(s => new OtherProperty
                {
                    OTHAddressID = s.OTHAddressID,
                    OTHAbbreviatedLegalDesc = s.OTHAbbreviatedLegalDesc,
                    OTHAdvBldgSF = s.OTHAdvBldgSF,
                    OTHFolioId = s.OTHFolioId,
                    OTHFolioSiteLink = s.OTHFolioSiteLink,
                    OTHLandFactor = s.OTHLandFactor,
                    OTHMailingAddress = s.OTHMailingAddress,
                    OTHMilage = s.OTHMilage,
                    OTHPropertyOwner = s.OTHPropertyOwner,
                    OTHSearchDate = s.OTHSearchDate,
                    OTHSiteAddressSrch = s.OTHSiteAddressSrch,
                    OTHSrchUse = s.OTHSrchUse,
                    UnitsBEDBath = s.UnitsBEDBath,
                    SalePrice = s.SalePrice,
                    SalePricePrSqFt = String.Format("{0:0.00}", s.SalePricePrSqFt),
                    AssedMarketSqFt = s.OthMarketValue,
                    AssessedValue = s.OthAssessedValue

                }).FirstOrDefault();

                if (getData != null)
                {
                    DataRow dr = dtViewComparision.NewRow();
                    dr["Adjustments"] = "Comp." + cnt;
                    dr["FolioId"] = getData.OTHFolioId.Replace(" ", "");
                    dr["SiteAddress"] = getData.OTHSiteAddressSrch;
                    dr["LandFactor"] = getData.OTHLandFactor;
                    dr["AdvBldgSF"] = getData.OTHAdvBldgSF;
                    dr["SalePrice"] = getData.SalePrice;
                    dr["MarketValueSqFt"] = getData.AssedMarketSqFt;
                    dr["SalePricePrSqFt"] = getData.SalePricePrSqFt;
                    dr["AssessedValue"] = getData.AssessedValue;
                    dr["MailingAddress"] = getData.OTHMailingAddress;
                    dr["PropertyOwner"] = getData.OTHPropertyOwner;
                    dr["AbbreviatedLegalDescription"] = getData.OTHAbbreviatedLegalDesc;
                    dr["Units"] = getData.UnitsBEDBath.Split('/')[0];
                    dr["BedRoom"] = getData.UnitsBEDBath.Split('/')[1];
                    dr["BathRoom"] = getData.UnitsBEDBath.Split('/')[2];
                    dr["Milage"] = getData.OTHMilage;
                    dr["Use"] = getData.OTHSrchUse;
                    dr["ViewProperty"] = getData.OTHFolioSiteLink;
                    dr["AdjustedValues"] = "Adjusted Price";
                    dtViewComparision.Rows.Add(dr);

                    int lanfactor = Convert.ToInt32(_VMPropertyFolio.OTHLandFactor == "" ? "0" : _VMPropertyFolio.OTHLandFactor.Replace(",", "")) - Convert.ToInt32(getData.OTHLandFactor.Replace(",", ""));
                    int Units = (Convert.ToInt32(Math.Round(Convert.ToDouble(_UnitVal == "" ? "0" : _UnitVal))) - Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[0])))) * (txtUnits != null ? Convert.ToInt32(txtUnits) : Convert.ToInt32(_adjust == null ? 4000 : _adjust.AdjBedRoom));
                    int BedRoom = (Convert.ToInt32(Math.Round(Convert.ToDouble(_BedVal == "" ? "0" : _BedVal))) - Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[1])))) * (txtBedRoom != null ? Convert.ToInt32(txtBedRoom) : Convert.ToInt32(_adjust == null ? 4000 : _adjust.AdjBedRoom));
                    int BathRoom = (Convert.ToInt32(Math.Round(Convert.ToDouble(_BathVal == "" ? "0" : _BathVal))) - Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[2])))) * (txtBathRoom != null ? Convert.ToInt32(txtBathRoom) : Convert.ToInt32(_adjust == null ? 4000 : _adjust.AdjBathRoom));
                    _Units += Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[0])));
                    _BedRoom += Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[1])));
                    _BathRoom += Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[2])));
                    _Lanfactor += Convert.ToInt32(getData.OTHLandFactor.Replace(",", ""));
                    _AdvBldgSF += Convert.ToInt32(getData.OTHAdvBldgSF.Replace(",", "").Replace("$", ""));
                    _SalePrice += Convert.ToInt32(getData.SalePrice.Replace(",", "").Replace("$", ""));
                    _AssedMarketSqFt += Convert.ToInt32(getData.AssedMarketSqFt.Replace(",", "").Replace("$", ""));
                    _SalePricePrSqFt += Convert.ToDecimal(getData.SalePricePrSqFt.Replace(",", "").Replace("$", ""));
                    _Milage += Convert.ToInt32(getData.OTHMilage.Replace(",", "").Replace("$", ""));
                    _Use += Convert.ToInt32(getData.OTHSrchUse.Replace(",", "").Replace("$", ""));
                    DataRow dr1 = dtViewComparision.NewRow();
                    dr1["Adjustments"] = "Adustment.#";
                    dr1["FolioId"] = "";
                    dr1["SiteAddress"] = "";
                    dr1["LandFactor"] = lanfactor;
                    dr1["AdvBldgSF"] = "";
                    dr1["SalePrice"] = "";
                    dr1["MarketValueSqFt"] = "";
                    dr1["SalePricePrSqFt"] = "";
                    dr1["AssessedValue"] = "";
                    dr1["MailingAddress"] = "";
                    dr1["PropertyOwner"] = "";
                    dr1["AbbreviatedLegalDescription"] = "";
                    dr1["Units"] = Units;
                    dr1["BedRoom"] = BedRoom;
                    dr1["BathRoom"] = BathRoom;
                    dr1["Milage"] = "";
                    dr1["Use"] = "";
                    dr1["ViewProperty"] = "";
                    dr1["AdjustedValues"] = lanfactor + Units + BedRoom + BathRoom;

                    dtViewComparision.Rows.Add(dr1);
                }
                cnt++;
            }
            int _AvgUnits = ((Convert.ToInt32(Math.Round(Convert.ToDouble(_UnitVal == "" ? "0" : _UnitVal))) + _Units) / cnt);
            int _AvgBedRoom = ((Convert.ToInt32(Math.Round(Convert.ToDouble(_BedVal == "" ? "0" : _BedVal))) + _BedRoom) / cnt);
            int _AvgBathRoom = ((Convert.ToInt32(Math.Round(Convert.ToDouble(_BathVal == "" ? "0" : _BathVal))) + _BathRoom) / cnt);
            int _AvgLandfactor = ((Convert.ToInt32(_VMPropertyFolio.OTHLandFactor == "" ? "0" : _VMPropertyFolio.OTHLandFactor.Replace(",", "")) + _Lanfactor) / cnt);

            int _AVGAdvBldgSF = ((Convert.ToInt32(_VMPropertyFolio.OTHAdvBldgSF.Replace(",", "")) + _AdvBldgSF) / cnt);
            int _AVGSalePrice = ((Convert.ToInt32(_VMPropertyFolio.SalePrice == "" ? "0" : _VMPropertyFolio.SalePrice.Replace(",", "").Replace(",", "").Replace("$", "")) + _SalePrice) / cnt);
            int _AVGAssedMarketSqFt = ((Convert.ToInt32(_VMPropertyFolio.AssedMarketSqFt == "" ? "0" : _VMPropertyFolio.AssedMarketSqFt.Replace(",", "").Replace(",", "").Replace("$", "")) + _AssedMarketSqFt) / cnt);
            decimal _AVGSalePricePrSqFt = ((Convert.ToDecimal(String.Format("{0:0.00}", _VMPropertyFolio.SalePricePrSqFt).Replace(",", "").Replace("$", "")) + _SalePricePrSqFt) / cnt);
            int _AVGMilage = ((Convert.ToInt32(_VMPropertyFolio.OTHMilage.Replace(",", "")) + _Milage) / cnt);
            int _AVGUse = ((Convert.ToInt32(_VMPropertyFolio.OTHSrchUse.Replace(",", "")) + _Use) / cnt);

            DataRow dr2 = dtViewComparision.NewRow();
            dr2["Adjustments"] = "Average";
            dr2["FolioId"] = "";
            dr2["SiteAddress"] = "";
            dr2["LandFactor"] = _AvgLandfactor;
            dr2["AdvBldgSF"] = _AVGAdvBldgSF;
            dr2["SalePrice"] = _AVGSalePrice;
            dr2["MarketValueSqFt"] = _AVGAssedMarketSqFt;
            dr2["SalePricePrSqFt"] = String.Format("{0:0.00}", _AVGSalePricePrSqFt);
            dr2["AssessedValue"] = "";
            dr2["MailingAddress"] = "";
            dr2["PropertyOwner"] = "";
            dr2["AbbreviatedLegalDescription"] = "";
            dr2["Units"] = _AvgUnits;
            dr2["BedRoom"] = _AvgBedRoom;
            dr2["BathRoom"] = _AvgBathRoom;
            dr2["Milage"] = _AVGMilage;
            dr2["Use"] = _AVGUse;
            dr2["ViewProperty"] = "";
            dr2["AdjustedValues"] = "";

            dtViewComparision.Rows.Add(dr2);

            db.sp_InsertUpAdjustment(txtUnits, txtBathRoom, txtBedRoom, txtMilage, txtUse, txtAdvBldgSF, txtSalePrice, txtAssedMarketSqFt);

            DataTable dtComparision = PivoteDataTable.Pivot(dtViewComparision);
            List<DataRow> lstComparision = dtComparision.AsEnumerable().ToList();

            return lstComparision;
        }

        public List<DataRow> GetCalculateAdjustProperty(int AddressID, int? txtAdvBldgSF, int? txtSalePrice, int? txtAssedMarketSqFt, int? txtUnits, int? txtBedRoom, int? txtBathRoom, int? txtMilage, int? txtUse)
        {
            DataTable dtViewComparision = new DataTable();

            DataColumnCollection columns = dtViewComparision.Columns;
            dtViewComparision.Columns.Add("Adjustments", typeof(string));
            dtViewComparision.Columns.Add("FolioId", typeof(string));
            dtViewComparision.Columns.Add("SiteAddress", typeof(string));
            dtViewComparision.Columns.Add("LandFactor", typeof(string));
            dtViewComparision.Columns.Add("AdvBldgSF", typeof(string));
            dtViewComparision.Columns.Add("SalePrice", typeof(string));
            dtViewComparision.Columns.Add("MarketValueSqFt", typeof(string));
            dtViewComparision.Columns.Add("SalePricePrSqFt", typeof(string));
            dtViewComparision.Columns.Add("AssessedValue", typeof(string));
            dtViewComparision.Columns.Add("MailingAddress", typeof(string));
            dtViewComparision.Columns.Add("PropertyOwner", typeof(string));
            dtViewComparision.Columns.Add("AbbreviatedLegalDescription", typeof(string));
            dtViewComparision.Columns.Add("Units", typeof(string));
            dtViewComparision.Columns.Add("BedRoom", typeof(string));
            dtViewComparision.Columns.Add("BathRoom", typeof(string));
            dtViewComparision.Columns.Add("Milage", typeof(string));
            dtViewComparision.Columns.Add("Use", typeof(string));
            dtViewComparision.Columns.Add("ViewProperty", typeof(string));
            dtViewComparision.Columns.Add("AdjustedValues", typeof(string));

            List<CompareFolioID> _lst = new List<CompareFolioID>();

            var objCmpFolio = (from c in db.tblCompareProperties
                               select c).Where(c => c.CmpFolioByID == AddressID).ToList();

            var getData1 = db.sp_GetPropertydetailsByID(AddressID);
            //var getData1 = db.tblSrchFolioIDs.AsEnumerable().Where(s => s.AddressID == AddressID).FirstOrDefault();
            var _adjust = db.tblAdjustments.FirstOrDefault();

            PropertyFolio _VMPropertyFolio = new PropertyFolio();

            _VMPropertyFolio = db.sp_GetPropertydetailsByID(AddressID).Select(s => new PropertyFolio
            {
                AddressID = s.AddressID,
                AbbreviatedLegalDesc = s.AbbreviatedLegalDesc,
                AdvBldgSF = s.AdvBldgSF,
                FolioId = s.FolioId,
                FolioSiteLink = s.FolioSiteLink,
                LandFactor = s.LandFactor,
                MailingAddress = s.MailingAddress,
                Milage = s.Milage,
                PropertyOwner = s.PropertyOwner,
                SearchDate = s.SearchDate,
                SiteAddressSrch = s.SiteAddressSrch,
                SrchUse = s.SrchUse,
                UnitsBEDBath = s.UnitsBEDBath,
                SalePrice = s.SalePrice,
                SalePricePrSqFt = String.Format("{0:0.00}", s.SalePricePrSqFt),
                AssedMarketSqFt = s.MarketValue,
                AssessedValue = s.AssessedValue

            }).FirstOrDefault();


            if (_VMPropertyFolio != null)
            {
                DataRow dr1 = dtViewComparision.NewRow();
                dr1["Adjustments"] = "Adjustment";
                dr1["FolioId"] = "";
                dr1["SiteAddress"] = "";
                dr1["LandFactor"] = "";
                dr1["AdvBldgSF"] = txtAdvBldgSF;
                dr1["SalePrice"] = txtSalePrice;
                dr1["MarketValueSqFt"] = txtAssedMarketSqFt;
                dr1["SalePricePrSqFt"] = "";
                dr1["AssessedValue"] = "";
                dr1["MailingAddress"] = "";
                dr1["PropertyOwner"] = "";
                dr1["AbbreviatedLegalDescription"] = "";
                dr1["Units"] = txtUnits;
                dr1["BedRoom"] = txtBedRoom;
                dr1["BathRoom"] = txtBathRoom;
                dr1["Milage"] = txtMilage;
                dr1["Use"] = txtUse;
                dr1["ViewProperty"] = "";

                dtViewComparision.Rows.Add(dr1);

                DataRow dr = dtViewComparision.NewRow();

                dr["Adjustments"] = "Subject";
                dr["FolioId"] = _VMPropertyFolio.FolioId.Replace(" ", "");
                dr["SiteAddress"] = _VMPropertyFolio.SiteAddressSrch;
                dr["LandFactor"] = _VMPropertyFolio.LandFactor;
                dr["AdvBldgSF"] = _VMPropertyFolio.AdvBldgSF;
                dr["SalePrice"] = _VMPropertyFolio.SalePrice;
                dr["MarketValueSqFt"] = _VMPropertyFolio.AssedMarketSqFt;
                dr["SalePricePrSqFt"] = _VMPropertyFolio.SalePricePrSqFt;
                dr["AssessedValue"] = _VMPropertyFolio.AssessedValue;
                dr["MailingAddress"] = _VMPropertyFolio.MailingAddress;
                dr["PropertyOwner"] = _VMPropertyFolio.PropertyOwner;
                dr["AbbreviatedLegalDescription"] = _VMPropertyFolio.AbbreviatedLegalDesc;
                dr["Units"] = _VMPropertyFolio.UnitsBEDBath.Split('/')[0];
                dr["BedRoom"] = _VMPropertyFolio.UnitsBEDBath.Split('/')[1];
                dr["BathRoom"] = _VMPropertyFolio.UnitsBEDBath.Split('/')[2];
                dr["Milage"] = _VMPropertyFolio.Milage;
                dr["Use"] = _VMPropertyFolio.SrchUse;
                dr["ViewProperty"] = _VMPropertyFolio.FolioSiteLink;

                dtViewComparision.Rows.Add(dr);

                //"<input type='text' id='SubAdjust-@count' class='form-control' />"
            }
            int cnt = 1;
            int _Units = 0;
            int _BedRoom = 0;
            int _BathRoom = 0;
            int _Lanfactor = 0;
            int _AdvBldgSF = 0;
            int _SalePrice = 0;
            int _Milage = 0;
            int _Use = 0;
            int _AssedMarketSqFt = 0;
            decimal _SalePricePrSqFt = 0;
            foreach (var item in objCmpFolio)
            {
                //var getData = db.tblSrchFolioIDs.AsEnumerable().Where(s => s.AddressID == item.CmpFloioToID).FirstOrDefault();

                PropertyFolio getData = new PropertyFolio();

                getData = db.sp_GetPropertydetailsByID(item.CmpFloioToID).Select(s => new PropertyFolio
                {
                    AddressID = s.AddressID,
                    AbbreviatedLegalDesc = s.AbbreviatedLegalDesc,
                    AdvBldgSF = s.AdvBldgSF,
                    FolioId = s.FolioId,
                    FolioSiteLink = s.FolioSiteLink,
                    LandFactor = s.LandFactor,
                    MailingAddress = s.MailingAddress,
                    Milage = s.Milage,
                    PropertyOwner = s.PropertyOwner,
                    SearchDate = s.SearchDate,
                    SiteAddressSrch = s.SiteAddressSrch,
                    SrchUse = s.SrchUse,
                    UnitsBEDBath = s.UnitsBEDBath,
                    SalePrice = s.SalePrice,
                    SalePricePrSqFt = String.Format("{0:0.00}", s.SalePricePrSqFt),
                    AssedMarketSqFt = s.MarketValue,
                    AssessedValue = s.AssessedValue

                }).FirstOrDefault();

                if (getData != null)
                {
                    DataRow dr = dtViewComparision.NewRow();
                    dr["Adjustments"] = "Comp." + cnt;
                    dr["FolioId"] = getData.FolioId.Replace(" ", "");
                    dr["SiteAddress"] = getData.SiteAddressSrch;
                    dr["LandFactor"] = getData.LandFactor;
                    dr["LandFactor"] = getData.LandFactor;
                    dr["AdvBldgSF"] = getData.AdvBldgSF;
                    dr["SalePrice"] = getData.SalePrice;
                    dr["MarketValueSqFt"] = getData.AssedMarketSqFt;
                    dr["SalePricePrSqFt"] = getData.SalePricePrSqFt;
                    dr["AssessedValue"] = getData.AssessedValue;
                    dr["MailingAddress"] = getData.MailingAddress;
                    dr["PropertyOwner"] = getData.PropertyOwner;
                    dr["AbbreviatedLegalDescription"] = getData.AbbreviatedLegalDesc;
                    dr["Units"] = getData.UnitsBEDBath.Split('/')[0];
                    dr["BedRoom"] = getData.UnitsBEDBath.Split('/')[1];
                    dr["BathRoom"] = getData.UnitsBEDBath.Split('/')[2];
                    dr["Milage"] = getData.Milage;
                    dr["Use"] = getData.SrchUse;
                    dr["ViewProperty"] = getData.FolioSiteLink;
                    dr["AdjustedValues"] = "Adjusted Price";
                    dtViewComparision.Rows.Add(dr);
                    int lanfactor = Convert.ToInt32(_VMPropertyFolio.LandFactor == "" ? "0" : _VMPropertyFolio.LandFactor.Replace(",", "")) - Convert.ToInt32(getData.LandFactor.Replace(",", ""));
                    int Units = (Convert.ToInt32(Math.Round(Convert.ToDouble(_VMPropertyFolio.UnitsBEDBath.Split('/')[0]))) - Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[0])))) * (txtUnits != null ? Convert.ToInt32(txtUnits) : Convert.ToInt32(_adjust == null ? 4000 : _adjust.AdjUnits));
                    int BedRoom = (Convert.ToInt32(Math.Round(Convert.ToDouble(_VMPropertyFolio.UnitsBEDBath.Split('/')[1]))) - Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[1])))) * (txtBedRoom != null ? Convert.ToInt32(txtBedRoom) : Convert.ToInt32(_adjust == null ? 4000 : _adjust.AdjBedRoom));
                    int BathRoom = (Convert.ToInt32(Math.Round(Convert.ToDouble(_VMPropertyFolio.UnitsBEDBath.Split('/')[2]))) - Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[2])))) * (txtBathRoom != null ? Convert.ToInt32(txtBathRoom) : Convert.ToInt32(_adjust == null ? 4000 : _adjust.AdjBathRoom));
                    _Units += Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[0])));
                    _BedRoom += Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[1])));
                    _BathRoom += Convert.ToInt32(Math.Round(Convert.ToDouble(getData.UnitsBEDBath.Split('/')[2])));
                    _Lanfactor += Convert.ToInt32(getData.LandFactor.Replace(",", ""));
                    _AdvBldgSF += Convert.ToInt32(getData.AdvBldgSF.Replace(",", "").Replace("$", ""));
                    _SalePrice += Convert.ToInt32(getData.SalePrice.Replace(",", "").Replace("$", ""));
                    _AssedMarketSqFt += Convert.ToInt32(getData.AssedMarketSqFt.Replace(",", "").Replace("$", ""));
                    _SalePricePrSqFt += Convert.ToDecimal(getData.SalePricePrSqFt.Replace(",", "").Replace("$", ""));
                    _Milage += Convert.ToInt32(getData.Milage.Replace(",", "").Replace("$", ""));
                    _Use += Convert.ToInt32(getData.SrchUse.Replace(",", "").Replace("$", ""));
                    DataRow dr1 = dtViewComparision.NewRow();
                    dr1["Adjustments"] = "Adustment.#";
                    dr1["FolioId"] = "";
                    dr1["SiteAddress"] = "";
                    dr1["LandFactor"] = lanfactor;
                    dr1["AdvBldgSF"] = "";
                    dr1["SalePrice"] = "";
                    dr1["MarketValueSqFt"] = "";
                    dr1["SalePricePrSqFt"] = "";
                    dr1["AssessedValue"] = "";
                    dr1["MailingAddress"] = "";
                    dr1["PropertyOwner"] = "";
                    dr1["AbbreviatedLegalDescription"] = "";
                    dr1["Units"] = Units;
                    dr1["BedRoom"] = BedRoom;
                    dr1["BathRoom"] = BathRoom;
                    dr1["Milage"] = "";
                    dr1["Use"] = "";
                    dr1["ViewProperty"] = "";
                    dr1["AdjustedValues"] = lanfactor + Units + BedRoom + BathRoom;

                    dtViewComparision.Rows.Add(dr1);
                }
                cnt++;
            }
            int _AvgUnits = ((Convert.ToInt32(Math.Round(Convert.ToDouble(_VMPropertyFolio.UnitsBEDBath.Split('/')[0]))) + _Units) / cnt);
            int _AvgBedRoom = ((Convert.ToInt32(Math.Round(Convert.ToDouble(_VMPropertyFolio.UnitsBEDBath.Split('/')[1]))) + _BedRoom) / cnt);
            int _AvgBathRoom = ((Convert.ToInt32(Math.Round(Convert.ToDouble(_VMPropertyFolio.UnitsBEDBath.Split('/')[2]))) + _BathRoom) / cnt);
            int _AvgLandfactor = ((Convert.ToInt32(_VMPropertyFolio.LandFactor == "" ? "0" : _VMPropertyFolio.LandFactor.Replace(",", "")) + _Lanfactor) / cnt);

            int _AVGAdvBldgSF = ((Convert.ToInt32(_VMPropertyFolio.AdvBldgSF.Replace(",", "")) + _AdvBldgSF) / cnt);
            int _AVGSalePrice = ((Convert.ToInt32(_VMPropertyFolio.SalePrice.Replace(",", "").Replace(",", "").Replace("$", "")) + _SalePrice) / cnt);
            int _AVGAssedMarketSqFt = ((Convert.ToInt32(_VMPropertyFolio.AssedMarketSqFt.Replace(",", "").Replace(",", "").Replace("$", "")) + _AssedMarketSqFt) / cnt);
            decimal _AVGSalePricePrSqFt = ((Convert.ToDecimal(String.Format("{0:0.00}", _VMPropertyFolio.SalePricePrSqFt).Replace(",", "").Replace("$", "")) + _SalePricePrSqFt) / cnt);
            int _AVGMilage = ((Convert.ToInt32(_VMPropertyFolio.Milage.Replace(",", "")) + _Milage) / cnt);
            int _AVGUse = ((Convert.ToInt32(_VMPropertyFolio.SrchUse.Replace(",", "")) + _Use) / cnt);

            DataRow dr2 = dtViewComparision.NewRow();
            dr2["Adjustments"] = "Average";
            dr2["FolioId"] = "";
            dr2["SiteAddress"] = "";
            dr2["LandFactor"] = _AvgLandfactor;
            dr2["AdvBldgSF"] = _AVGAdvBldgSF;
            dr2["SalePrice"] = _AVGSalePrice;
            dr2["MarketValueSqFt"] = _AVGAssedMarketSqFt;
            dr2["SalePricePrSqFt"] = String.Format("{0:0.00}", _AVGSalePricePrSqFt);
            dr2["AssessedValue"] = "";
            dr2["MailingAddress"] = "";
            dr2["PropertyOwner"] = "";
            dr2["AbbreviatedLegalDescription"] = "";
            dr2["Units"] = _AvgUnits;
            dr2["BedRoom"] = _AvgBedRoom;
            dr2["BathRoom"] = _AvgBathRoom;
            dr2["Milage"] = _AVGMilage;
            dr2["Use"] = _AVGUse;
            dr2["ViewProperty"] = "";
            dr2["AdjustedValues"] = "";

            dtViewComparision.Rows.Add(dr2);

            db.sp_InsertUpAdjustment(txtUnits, txtBathRoom, txtBedRoom, txtMilage, txtUse, txtAdvBldgSF, txtSalePrice, txtAssedMarketSqFt);

            DataTable dtComparision = PivoteDataTable.Pivot(dtViewComparision);
            List<DataRow> lstComparision = dtComparision.AsEnumerable().ToList();

            return lstComparision;
        }

        public List<UseCodeCategory> GetPropertyTypeTab()
        {
            try
            {
                List<UseCodeCategory> _lst = new List<UseCodeCategory>();
                _lst = db.tblUseCodeCategories.AsEnumerable().Select(s => new UseCodeCategory
                {
                    UseCodeCategoryID = Convert.ToInt32(s.UseCodeCategoryID),
                    UseCodeCategoryName = s.UseCodeCategoryName,
                }).ToList();
                return _lst;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public DataTable GetSubTabsCount()
        {
            DataTable dt = new DataTable();

            try
            {
                //using (SqlConnection myConnection = new SqlConnection("data source=DC01;initial catalog=Browardclerk;user id=sa;password=Dbserver@ats6;multipleactiveresultsets=True;"))
                //using (SqlConnection myConnection = new SqlConnection("data source=lla-mvc.c08neocvkgdj.us-west-2.rds.amazonaws.com;initial catalog=Browardclerk;user id=guysimani;password=kjhUUY786*6RTY%TYRF;multipleactiveresultsets=True;"))
                using (SqlConnection myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBrowardclerk"].ToString()))
                {
                    using (SqlCommand cmd = new SqlCommand("sp_GetSubtabNumberingFilter", myConnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        myConnection.Open();

                        dt.Load(cmd.ExecuteReader());

                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }

        }

        public TabsCountedLp GetCountTabsLpCase()
        {
            try
            {
                TabsCountedLp _lst = new TabsCountedLp();
                _lst = db.sp_CountTabsLpCaseList(null,null,null,null,null,null,null,null,null,null,null,null,null).Select(s => new TabsCountedLp
                {
                    Residential = Convert.ToInt32(s.Residential),
                    Commercial = Convert.ToInt32(s.Commercial),
                    Industrial = Convert.ToInt32(s.Industrial),
                    Agricultural = Convert.ToInt32(s.Agricultural),
                    Institutional = Convert.ToInt32(s.Institutional),
                    Government = Convert.ToInt32(s.Government),
                    Miscellaneous = Convert.ToInt32(s.Miscellaneous),
                    totalMiscellaneous = (Convert.ToInt32(s.Agricultural) + Convert.ToInt32(s.Institutional) + Convert.ToInt32(s.Government) + Convert.ToInt32(s.Miscellaneous) + Convert.ToInt32(s.CentrallyAssessed) + Convert.ToInt32(s.Non_Agricultural)),
                    CentrallyAssessed = Convert.ToInt32(s.CentrallyAssessed),
                    NonAgricultural = Convert.ToInt32(s.Non_Agricultural),
                    ALLLEADS = Convert.ToInt32(s.ALLLEADS),
                    MultiProperty = Convert.ToInt32(s.MultiProperty),
                    SetSubjtProperty = Convert.ToInt32(s.SetSubjtProperty),
                    NotFound = Convert.ToInt32(s.NotFound),
                }).FirstOrDefault();
                return _lst;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public List<SrchDefandant> GetLpDefandentMapRecord(string[] showDocReportLp1)
        {
            try
            {
                List<SrchDefandant> _lstsrch = new List<SrchDefandant>();

                if (showDocReportLp1 != null)
                {

                    for (int i = 1; i < showDocReportLp1.Length; i++)
                    {
                        SrchDefandant _srch = new SrchDefandant();
                        //int OTHAddressID = 0;
                        int _countColumn = 1;
                        HtmlDocument doc = new HtmlDocument();
                        doc.LoadHtml(showDocReportLp1[i].ToString());

                        foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//tr/td"))
                        {
                            var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();

                            switch (_countColumn)
                            {
                                //case 1:
                                //    OTHAddressID = Convert.ToInt32(_text);
                                //    break;
                                case 1:
                                    _srch.SrchDefendantID = Convert.ToInt32(_text);
                                    _srch.Plaintiff = node.Elements("input").Skip(1).FirstOrDefault().Attributes["value"].Value;
                                    _srch.CaseNumber = node.Elements("input").Skip(2).FirstOrDefault().Attributes["value"].Value;
                                    _srch.PropertyType = node.Elements("input").Skip(3).FirstOrDefault().Attributes["value"].Value;
                                    _srch.DocResultSitUrl = node.Elements("input").Skip(4).FirstOrDefault().Attributes["value"].Value;
                                    _srch.Latitude = node.Elements("input").Skip(5).FirstOrDefault().Attributes["value"].Value;
                                    _srch.Longitude = node.Elements("input").Skip(6).FirstOrDefault().Attributes["value"].Value;
                                    break;
                                case 6:
                                    _srch.Defendant = _text;
                                    break;
                                case 7:
                                    _srch.OTHSiteAddressSrch = _text;
                                    break;
                                case 8:
                                    _srch.OTHMailingAddress = _text;
                                    break;
                                case 9:
                                    _srch.OthMarketValue = _text;
                                    break;
                                case 11:
                                    _srch.EstMarketValue = _text;
                                    break;
                                case 12:
                                    _srch.EstOwnerEquity = _text.Replace("$", "");
                                    break;
                                case 13:
                                    _srch.DocumentType = _text;
                                    break;
                                case 15:
                                    _srch.Excemption = _text;
                                    break;
                                //case 26:
                                //    _srch.Plaintiff = _text;
                                //    break;
                                //case 27:
                                //    _srch.CaseNumber = _text;
                                //    break;
                                //case 28:
                                //    _srch.PropertyType = _text;
                                //    break;
                                //case 29:
                                //    _srch.DocResultSitUrl = _text;
                                //    break;

                               
                            }
                           

                            //break;
                            _countColumn++;

                        }
                       // _srch = GetMapRecordBySrchID(OTHAddressID).FirstOrDefault();
                        _lstsrch.Add(_srch); 
                    }

                }
                return _lstsrch;

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public List<SrchDefandant> GetMapRecordBySrchID(int OTHAddressID)
        {
            try
            {
                List<SrchDefandant> _lstsrch = new List<SrchDefandant>();
                _lstsrch = db.sp_GetMapOTHPropertyByID(OTHAddressID).Select(s => new SrchDefandant
             {
                 SrchDefendantID = Convert.ToInt32(s.SrchDefendantID),
                 Plaintiff = Regex.Replace(s.Plaintiff, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                 Defendant = Regex.Replace(s.Defendant, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                 DocumentType = s.DocumentType,
                 CaseNumber = s.CaseNumber,
                 PropertyType = s.propertyType,
                 OTHMailingAddress = s.OTHMailingAddress,
                 OTHSiteAddressSrch=s.OTHSiteAddressSrch,
                 DocResultSitUrl = s.DocResultSitUrl,
                 TaxIssue = s.TaxIssueStatus,
                 OthMarketValue = s.MarketValue,
                 EstMarketValue ="$"+ string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal(s.MarketValue == null ? 0 : Convert.ToDecimal(((Convert.ToDecimal(s.MarketValue.Replace(",", "").Replace("$", "")) * 15 / 100) + Convert.ToDecimal(s.MarketValue.Replace(",", "").Replace("$", "")))))),
                 OwedOnMortgage = s.OwedOnMortgage,
                 EstOwnerEquity = (s.OwedOnMortgage == null || s.OwedOnMortgage == "FULLY PAID") ? s.OwedOnMortgage : (string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal((string.IsNullOrEmpty(s.MarketValue) ? 0 : Convert.ToDecimal(s.MarketValue.Replace(",", "").Replace("$", "")) - (string.IsNullOrEmpty(s.OwedOnMortgage) ? 0 : Convert.ToDecimal(s.OwedOnMortgage)))))),
                 Excemption = "$" + string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal(s.Excemption == null ? 0 : s.Excemption)),
                 Latitude=s.Latitude,
                 Longitude=s.Longitude

             }).ToList();

                return _lstsrch;
            }
            catch
            {
                return null;
            }
        }

        public string ConvertPropertyType(int SrchDefendantID)
        {
            string response = "";
            try
            {
                int i = db.sp_ConvertPropertyType(SrchDefendantID);
                if (i >= 1)
                    response = "Property has been moved successfully.";
                else
                    response = "Property has been not moved.";
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }
            return response;
        }

        public List<tblCaseNumberSearch> GetCaseNumberHasAddress()
        {

            List<tblCaseNumberSearch> _VMCaseNumberSearch = new List<tblCaseNumberSearch>();

            _VMCaseNumberSearch = db.sp_GetCaseWithAdderess().Select(s => new tblCaseNumberSearch
            {
                CaseNumberID = s.CaseNumberID,
                CaseSearchDate = s.CaseSearchDate,
                CaseStatus = s.CaseStatus,
                CaseNumber = s.CaseNumber,
                CaseStyle = s.CaseStyle,
                CaseType = s.CaseType,
                FillingDate = s.FillingDate,

            }).ToList();

            return _VMCaseNumberSearch;
        }

        public List<VMCase> GetCaseCitaionRowsDetails(int? Rows)
        {
            try
            {
                List<VMCase> _VMCase = new List<VMCase>();

                _VMCase = db.tblCaseDetails.AsEnumerable().OrderByDescending(x => x.CaseID).Select(s => new VMCase
                {
                    CaseID = s.CaseID,
                    CaseNumber = s.CaseNumber,
                    CaseStatusT = s.CaseStatusT,
                    CaseType = s.CaseType,
                    CourtLocation = s.CourtLocation,
                    CourtType = s.CourtType,
                    FilingDate = Convert.ToDateTime(s.FilingDate),
                    JudgeIdName = s.JudgeIdName,
                    MagistrateIdName = s.MagistrateIdName,
                    StateRptnumber = s.StateRptnumber,
                    AttornyAddress = s.AttornyAddress,
                    SearchedDate = s.SearchedDate,

                }).ToList();


                if (_VMCase != null && _VMCase.Count > 0)
                {
                    foreach (var _case in _VMCase)
                    {
                        _case.defandantList = db.tblDefandants.AsEnumerable().Where(w => w.CaseID == _case.CaseID).Select(s => new Defandant
                        {
                            CaseNumber = s.CaseNumber,
                            PartyName = s.PartyName,
                        }).ToList();

                        _case.CitationList = db.tblCitations.AsEnumerable().Where(w => w.CaseID == _case.CaseID).Select(s => new Citation
                        {
                            CitaionNumber = s.CitaionNumber,
                            OffenseDate = Convert.ToDateTime(s.OffenseDate),
                        }).ToList();
                    }
                }
                return _VMCase;
            }
            catch
            {
                return null;
            }

        }

        public List<VMCase> GetCaseDetails(DateTime _SearchedDate)
        {
            try
            {
                List<VMCase> _VMCase = new List<VMCase>();

                _VMCase = db.tblCaseDetails.AsEnumerable().Where(x => x.SearchedDate == _SearchedDate).Select(s => new VMCase
                {
                    CaseID = s.CaseID,
                    CaseNumber = s.CaseNumber,
                    CaseStatusT = s.CaseStatusT,
                    CaseType = s.CaseType,
                    CourtLocation = s.CourtLocation,
                    CourtType = s.CourtType,
                    FilingDate = Convert.ToDateTime(s.FilingDate),
                    JudgeIdName = s.JudgeIdName,
                    MagistrateIdName = s.MagistrateIdName,
                    StateRptnumber = s.StateRptnumber,
                    AttornyAddress = s.AttornyAddress,
                    SearchedDate = s.SearchedDate,

                }).ToList();

                if (_VMCase != null && _VMCase.Count > 0)
                {
                    foreach (var _case in _VMCase)
                    {
                        _case.defandantList = db.tblDefandants.AsEnumerable().Where(w => w.CaseID == _case.CaseID).Select(s => new Defandant
                        {
                            CaseNumber = s.CaseNumber,
                            PartyName = s.PartyName,
                        }).ToList();

                        _case.CitationList = db.tblCitations.AsEnumerable().Where(w => w.CaseID == _case.CaseID).Select(s => new Citation
                        {
                            CitaionNumber = s.CitaionNumber,
                            OffenseDate = Convert.ToDateTime(s.OffenseDate),
                        }).ToList();
                    }
                }
                return _VMCase;
            }
            catch
            {
                return null;
            }

        }

        public List<SearchedPdf> GetPDFResultRecord(int SrchDefendantID)
        {
            try
            {
                List<SearchedPdf> _VMSearchedPdf = new List<SearchedPdf>();

                _VMSearchedPdf = db.tblSrchDefandants.AsEnumerable().Where(x => x.SrchDefendantID == SrchDefendantID).Select(s => new SearchedPdf
                {
                    SrchDefendantID = s.SrchDefendantID,
                    CaseNumber = s.CaseNumber,
                    SerchedDate = s.SearchedDeffDate,
                }).ToList();

                if (_VMSearchedPdf != null && _VMSearchedPdf.Count > 0)
                {
                    foreach (var _srchpdf in _VMSearchedPdf)
                    {
                        _srchpdf.tblNewAllAddress = db.tblNewAllAddresses.AsEnumerable().Where(w => w.SrchDefendantID == _srchpdf.SrchDefendantID).Select(s => new tblNewAllAddress
                        {
                            SrchDefendantID = s.SrchDefendantID,
                            NewAllAddressID = s.NewAllAddressID,
                            PrincipalBalance = s.PrincipalBalance,
                            Defendantaddress = s.Defendantaddress,
                        }).ToList();
                    }
                }
                return _VMSearchedPdf;
            }
            catch
            {
                return null;
            }
        }

        public List<SearchedPdf> GetPDFResultList(int CaseNumberID)
        {
            try
            {
                List<SearchedPdf> _VMSearchedPdf = new List<SearchedPdf>();

                _VMSearchedPdf = db.tblSrchAddressResults.AsEnumerable().Where(x => x.CaseNumberID == CaseNumberID).Select(s => new SearchedPdf
                {
                    SearchedAddressID = s.SearchedAddressID,
                    CaseNumber = s.CaseNumber,
                    Pdfname = s.PageNumber,
                    SerchedDate = s.SearchedDate,
                }).ToList();

                if (_VMSearchedPdf != null && _VMSearchedPdf.Count > 0)
                {
                    foreach (var _srchpdf in _VMSearchedPdf)
                    {
                        _srchpdf.tblAllPagesAddress = db.tblAllPagesAddresses.AsEnumerable().Where(w => w.SearchedAddressID == _srchpdf.SearchedAddressID).Select(s => new tblAllPagesAddress
                        {
                            SearchedAddressID = s.SearchedAddressID,
                            SrchAllAdreessId = s.SrchAllAdreessId,
                            ToDefendant = s.ToDefendant,
                            OnDefendant = s.OnDefendant,
                            LocatedAt = s.LocatedAt,
                            ToWit = s.ToWit,
                            CopyFurnished = s.CopyFurnished,
                        }).ToList();
                    }
                }
                return _VMSearchedPdf;
            }
            catch
            {
                return null;
            }
        }

        public List<SrchDefandant> GetLpCaseforsearch()
        {

            try
            {
                List<SrchDefandant> _SrchDefandant = new List<SrchDefandant>();

                _SrchDefandant = db.tblSrchDefandants.AsEnumerable().Where(x => x.IsNewsCase == true).Select(s => new SrchDefandant
                {
                    SrchDefendantID = s.SrchDefendantID,
                    DocumentType = s.DocumentType,
                    Plaintiff = s.Plaintiff,
                    Defendant = s.Defendant,
                    BookType = s.BookType,
                    CaseNumber = s.CaseNumber,
                    RefNum = s.RefNum,
                    SearchedDeffDate = s.SearchedDeffDate,
                    DocResultSitUrl = s.DocResultSitUrl,
                    IsResidential = s.IsResidential,
                    RecordDate = s.RecordDate,
                    IsNewsCase = s.IsNewsCase,
                }).ToList();
                return _SrchDefandant;
            }
            catch
            {
                return null;
            }

        }

        public List<SearchedPdf> GetsearchPDFResult(string CaseNumber, DateTime _SearchedDate, int CaseNumberID)
        {
            try
            {
                List<SearchedPdf> _VMSearchedPdf = new List<SearchedPdf>();

                _VMSearchedPdf = db.tblSrchAddressResults.AsEnumerable().Where(x => x.CaseNumberID == CaseNumberID && x.CaseNumber == CaseNumber && x.SearchedDate == _SearchedDate).Select(s => new SearchedPdf
                {
                    SearchedAddressID = s.SearchedAddressID,
                    CaseNumber = s.CaseNumber,
                    Pdfname = s.PageNumber,
                    SerchedDate = s.SearchedDate,
                }).ToList();
                if (_VMSearchedPdf != null && _VMSearchedPdf.Count > 0)
                {
                    foreach (var _srchpdf in _VMSearchedPdf)
                    {
                        _srchpdf.tblAllPagesAddress = db.tblAllPagesAddresses.AsEnumerable().Where(w => w.SearchedAddressID == _srchpdf.SearchedAddressID).Select(s => new tblAllPagesAddress
                        {
                            SearchedAddressID = s.SearchedAddressID,
                            SrchAllAdreessId = s.SrchAllAdreessId,
                            ToDefendant = s.ToDefendant,
                            OnDefendant = s.OnDefendant,
                            LocatedAt = s.LocatedAt,
                            ToWit = s.ToWit,
                            CopyFurnished = s.CopyFurnished,
                        }).ToList();
                    }
                }
                return _VMSearchedPdf;
            }
            catch
            {
                return null;
            }
        }

        public string DeleteOtherCmpProperty(int CmpFolioTOId)
        {
            string response = "";
            try
            {
                int i = db.sp_DeleteCmpProperty(CmpFolioTOId);
                if (i >= 1)
                    response = "Record has been deleted successfully.";
                else
                    response = "Record has been not deleted.";
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }
            return response;
        }

        public string DeleteCmpProperty(int CmpFolioTOId)
        {
            string response = "";
            try
            {
                int i = db.sp_DeleteCmpProperty(CmpFolioTOId);
                if (i >= 1)
                    response = "Record has been deleted successfully.";
                else
                    response = "Record has been not deleted.";
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }
            return response;
        }

        public string DeleteProperty(int AddressID)
        {
            string response = "";
            try
            {
                int i = db.sp_DeleteProperty(AddressID);
                if (i >= 1)
                    response = "Record has been deleted successfully.";
                else
                    response = "Record has been not deleted.";
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }
            return response;
        }

        public string DeleteOtherProperty(int AddressID)
        {
            string response = "";
            try
            {
                int i = db.sp_DeleteOtherProperty(AddressID);
                if (i >= 1)
                    response = "Record has been deleted successfully.";
                else
                    response = "Record has been not deleted.";
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }
            return response;
        }

        public string deletedefandant(int SrchDefendantID)
        {
            string response = "";
            try
            {
                int i = db.sp_deleteDefendantlist(SrchDefendantID);
                if (i >= 1)
                    response = "Record has been deleted successfully.";
                else
                    response = "Record has been not deleted.";
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }
            return response;
        }

        public string DeleteSelectedLPCases(string[] RowLpCaseId)
        {
            string Result = "";
            try
            {
                for (int i = 0; i < RowLpCaseId.Length; i++)
                {
                    int SrchDefendantID = Convert.ToInt32(RowLpCaseId[i].ToString());
                    db.sp_deleteDefendantlist(SrchDefendantID);
                }
                Result = "Success";

            }
            catch
            {
                Result = "Error";

            }
            return Result;
        }

        public List<SrchDefandant> GetOtherDefendantPalintiff(int SrchDefendantID)
        {
            try
            {

                List<SrchDefandant> _tblPalmBeach = new List<SrchDefandant>();

                _tblPalmBeach = db.spGetOTHPlaintiffDefend(SrchDefendantID).Select(s => new SrchDefandant
                {
                    SrchDefendantID = s.SrchDefendantID,
                    Plaintiff = Regex.Replace(s.Plaintiff, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                    Defendant = Regex.Replace(s.Defendant, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                    DocResultSitUrl = s.DocResultSitUrl,
                    RecordDate = s.RecordDate,
                    DocumentType = s.DocumentType,
                    BookType = s.BookType
                }).ToList();
                return _tblPalmBeach;
            }
            catch
            {
                return null;
            }
        }
    }

    public static class PivoteDataTable
    {

        public static DataTable Pivot(this DataTable tbl)
        {
            var tblPivot = new DataTable();
            tblPivot.Columns.Add(tbl.Columns[0].ColumnName);
            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                tblPivot.Columns.Add(Convert.ToString(i));
            }
            for (int col = 0; col < tbl.Columns.Count; col++)
            {
                var r = tblPivot.NewRow();
                r[0] = tbl.Columns[col].ToString();
                for (int j = 1; j <= tbl.Rows.Count; j++)
                    r[j] = tbl.Rows[j - 1][col];

                tblPivot.Rows.Add(r);
            }
            return tblPivot;
        }
    }

    public class Defandant
    {
        public string CaseNumber { get; set; }
        public string PartyName { get; set; }
    }
    public class Citation
    {
        public string CitaionNumber { get; set; }
        public DateTime OffenseDate { get; set; }
    }

    public class PDFAddress
    {

        public string AddressType { get; set; }
        public string Pdfaddress { get; set; }
    }
    public class SearchedPdf
    {
        public string Pdfname { get; set; }
        public string PrincipalBalance { get; set; }
        public int SearchedAddressID { get; set; }
        public int CaseNumberID { get; set; }
        public int SrchDefendantID { get; set; }
        public Nullable<DateTime> SerchedDate { get; set; }
        public string CaseNumber { get; set; }
        public List<PDFAddress> pdfAddresslist { get; set; }
        public List<tblAllPagesAddress> tblAllPagesAddress { get; set; }
        public List<tblNewAllAddress> tblNewAllAddress { get; set; }
    }
    public partial class CaseNumberSearch
    {
        public int SearchedAddressID { get; set; }
        public int CaseNumberID { get; set; }
        public string CaseNumber { get; set; }
        public Nullable<System.DateTime> CaseSearchDate { get; set; }
        public string CaseStyle { get; set; }
        public string CaseType { get; set; }
        public Nullable<System.DateTime> FillingDate { get; set; }
        public string CaseStatus { get; set; }
    }

    public partial class OtherAssessedValue
    {
        public int OthPropertyAssesID { get; set; }
        public Nullable<int> OTHAddressID { get; set; }
        public Nullable<int> OthYear { get; set; }
        public string OthLand { get; set; }
        public string OthBuilding { get; set; }
        public string OthMarketValue { get; set; }
        public string OthAssessedValue { get; set; }
        public string OthAssessedTax { get; set; }
    }
    public class AssesedValueslist
    {
        public int PropertyAssesID { get; set; }
        public Nullable<int> AddressID { get; set; }
        public Nullable<int> Year { get; set; }
        public string Land { get; set; }
        public string Building { get; set; }
        public string MarketValue { get; set; }
        public string AssessedValue { get; set; }
        public string AssessedTax { get; set; }
    }
    public partial class OtherProperty
    {
        public int OTHAddressID { get; set; }
        public Nullable<int> SrchDefendantID { get; set; }
        public string OTHSiteAddressSrch { get; set; }
        public string OTHPropertyOwner { get; set; }
        public string OTHMailingAddress { get; set; }
        public string OTHAbbreviatedLegalDesc { get; set; }
        public string OTHFolioId { get; set; }
        public string OTHMilage { get; set; }
        public string OTHSrchUse { get; set; }
        public string OTHLandFactor { get; set; }
        public string OTHAdvBldgSF { get; set; }
        public string UnitsBEDBath { get; set; }
        public string Units { get; set; }
        public string BedRoom { get; set; }
        public string BathRoom { get; set; }
        public string SalePrice { get; set; }
        public string AssedMarketSqFt { get; set; }
        public string SalePricePrSqFt { get; set; }
        public string AssessedValue { get; set; }
        public string OTHFolioSiteLink { get; set; }
        public Nullable<System.DateTime> OTHSearchDate { get; set; }
        public Nullable<bool> OTHCompareable { get; set; }
        public Nullable<bool> IsOTHCorrectProperty { get; set; }
        public string SaleBookPage { get; set; }
        public Nullable<System.DateTime> OthSaleDate { get; set; }
        public string OthSaleBookPage { get; set; }
        public string OthSaleType { get; set; }
        public string OTHCardUrl { get; set; }
        public string OTHSketchUrl { get; set; }
        public string OTHEffYearBuilt { get; set; }
        public int UseCodeCategoryID { get; set; }

        public virtual tblSrchDefandant tblSrchDefandant { get; set; }
    }

    public partial class SpokioDetail
    {

        public long SpokioDetailsID { get; set; }
        public Nullable<int> OTHAddressID { get; set; }
        public string SpkUName { get; set; }
        public string SpkBirthDate { get; set; }
        public string SpkTelephone { get; set; }
        public string SpkEmail { get; set; }
        public string SpkUrl { get; set; }
        public string SpkFBLink { get; set; }
        public Nullable<int> CntCourtDetails { get; set; }
        public List<SpokioTelephones> SpkTelephones { get; set; }
        public List<SpokioEmails> SpkEmails { get; set; }
        public string SpkMaritalStatus { get; set; }
        public string SpkZodiacSign { get; set; }
        public string SpkEthnicity { get; set; }
        public List<SpokeoAddresses> Addresses { get; set; }
        public List<SpokeoRelatives> Relatives { get; set; }
        public List<SpokeoSocialProfiles> SocialProfiles { get; set; }
    }

    public class SpokioTelephones
    {
        public long SpokeoTelephonesID { get; set; }
        public Nullable<long> SpokeoDetailsID { get; set; }
        public string Telephone { get; set; }
        public string Network { get; set; }
        public string PhoneType { get; set; }

    }

    public class SpokioEmails
    {
        public long SpokeoEmailsID { get; set; }
        public Nullable<long> SpokeoDetailsID { get; set; }
        public string EmailID { get; set; }
        public string EmailType { get; set; }

    }

    public class SpokeoAddresses
    {
        public long SpokeoAddressID { get; set; }
        public Nullable<long> SpokeoDetailsID { get; set; }
        public string SpokeoAddress { get; set; }
        public string AddressYear { get; set; }
        public List<SpokeoResidents> Residents { get; set; }
    }

    public class SpokeoResidents
    {
        public long ResidentID { get; set; }
        public Nullable<long> SpokeoAddressID { get; set; }
        public string ResidentName { get; set; }
        public string ResidentDOB { get; set; }
        public string ResidentUrl { get; set; }
        public string MaritalStatus { get; set; }
        public string ZodiacSign { get; set; }
        public string Ethnicity { get; set; }
    }

    public class SpokeoRelatives
    {
        public long RelativeID { get; set; }
        public Nullable<long> SpokeoDetailsID { get; set; }
        public string RelativeName { get; set; }
        public string RelativeDOB { get; set; }
        public string RelativeUrl { get; set; }
        public string MaritalStatus { get; set; }
        public string ZodiacSign { get; set; }
        public string Ethnicity { get; set; }
        public string RelativeAddress { get; set; }
        public string AddressUrl { get; set; }
    }

    public class SpokeoSocialProfiles
    {
        public long SpokeoSocialLinkID { get; set; }
        public Nullable<long> SpokeoDetailsID { get; set; }
        public Nullable<long> SpokeoResidentID { get; set; }
        public Nullable<long> SpokeoRelativeID { get; set; }
        public string SocialLinkUrl { get; set; }
        public string SocialSiteType { get; set; }
        public string SocialSiteName { get; set; }
        public string SocialName { get; set; }
    }

    public class PropertyFolio
    {
        public int AddressID { get; set; }
        public string SiteAddressSrch { get; set; }
        public string PropertyOwner { get; set; }
        public string MailingAddress { get; set; }
        public string AbbreviatedLegalDesc { get; set; }
        public string FolioId { get; set; }
        public string Milage { get; set; }
        public string SrchUse { get; set; }
        public string LandFactor { get; set; }
        public string AdvBldgSF { get; set; }
        public string UnitsBEDBath { get; set; }
        public string Units { get; set; }
        public string BedRoom { get; set; }
        public string BathRoom { get; set; }
        public string SalePrice { get; set; }
        public string AssedMarketSqFt { get; set; }
        public string SalePricePrSqFt { get; set; }
        public string AssessedValue { get; set; }
        public string FolioSiteLink { get; set; }
        public Boolean Compareable { get; set; }
        public string SaleBookPage { get; set; }
        public int SrchDefendantID { get; set; }
        public Nullable<System.DateTime> SearchDate { get; set; }

        public virtual ICollection<tblAssesedValue> tblAssesedValues { get; set; }
    }

    public class CompareFolioID
    {
        public int CompareFID { get; set; }
        public Nullable<int> AddressID { get; set; }
        public string CmpSiteAddressSrch { get; set; }
        public string CmpPropertyOwner { get; set; }
        public string CmpMailingAddress { get; set; }
        public string CmpAbbreviatedLegalDesc { get; set; }
        public string CmpFolioId { get; set; }
        public string CmpMilage { get; set; }
        public string CmpSrchUse { get; set; }
        public string CmpLandFactor { get; set; }
        public string CmpAdvBldgSF { get; set; }
        public string CmpUnitsBEDBath { get; set; }
        public string CmpFolioSiteLink { get; set; }
        public Boolean CmpCompareable { get; set; }
        public Nullable<System.DateTime> CmpSearchDate { get; set; }
    }
    public partial class SrchDefandant
    {
        public int SrchDefendantID { get; set; }
        public string DocumentType { get; set; }
        public string Plaintiff { get; set; }
        public string Defendant { get; set; }
        public string BookType { get; set; }
        public string CaseNumber { get; set; }
        public string RefNum { get; set; }
        public Nullable<System.DateTime> SearchedDeffDate { get; set; }
        public string DateModified { get; set; }
        public string DocResultSitUrl { get; set; }
        public Nullable<bool> IsResidential { get; set; }
        public string RecordDate { get; set; }
        public Nullable<bool> IsNewsCase { get; set; }
        public string BCPaAddress { get; set; }
        public Nullable<bool> IsSrchProperty { get; set; }
        public Nullable<bool> IsGetAddress { get; set; }
        public Nullable<bool> IsOtherProperty { get; set; }
        public Nullable<bool> IsOtherFolio { get; set; }
        public Nullable<bool> HasCompareable { get; set; }
        public Nullable<bool> MatchedWord { get; set; }
        public string OthAssesedValue { get; set; }
        public Nullable<int> OTHAddressID { get; set; }

        public Nullable<int> CompareOTHAddressID { get; set; }
        public string OtherFolio { get; set; }
        public string OTHMailingAddress { get; set; }
        public string OTHSiteAddressSrch { get; set; }
        public string OTHAbbreviatedLegalDesc { get; set; }
        public string OTHLandFactor { get; set; }
        public string LegalDescMetched { get; set; }
        public string AddessMatched { get; set; }
        public string ViewPDF { get; set; }
        public string PropertyType { get; set; }
        public string OTHSrchUse { get; set; }
        public int CntOther { get; set; }
        public string EstMarketValue { get; set; }

        public string OthMarketValue { get; set; }

        public string OwedOnMortgage { get; set; }
        public string EstOwnerEquity { get; set; }

        public string DateOFMortgage { get; set; }
        public string TaxIssue { get; set; }
        public string Excemption { get; set; }

        public string saleprice { get; set; }
        public string SaleDate { get; set; }
        public string SaleType { get; set; }
        public string SaleBookPage { get; set; }
        public string AdvBldgSF { get; set; }
        public string SalesPriceSqFt { get; set; }
        public string AssessedMarketSqFt { get; set; }
        public string EstimatedMortgage { get; set; }
        
        public Nullable<bool> IsspokioDetails { get; set; }
        public Nullable<bool> IssunbizDetailsID { get; set; }

        public Nullable<bool> IsBrowardLinkedInID { get; set; }
        public Nullable<bool> IsBrowardTwitterID { get; set; }
        public ContactInfo ContactInf { get; set; }

        public Nullable<bool> IsOfficialPDfDownload { get; set; }
        public bool cntPlaintiffs { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int? RedfinPrice { get; set; }
        public int? ZillowPrice { get; set; }
        public int? SpokioStatus { get; set; }

        public string RedfinPricedetail { get; set; }
        public string ZillowPricedetail { get; set; }
        public string RedfinUrl { get; set; }
        public string zillowUrl { get; set; }
        public string LoopnetPrice { get; set; }
        public string LoopnetUrl { get; set; }

        public string NonpaymentStatus { get; set; }

        public string HOARedfinDue { get; set; }

        public string Rentzestimate { get; set; }
        public string Refipayment { get; set; }
        public string OTHPropertyOwner { get; set; }

        public string Casedate { get; set; }
        public string Redfindays { get; set; }
        public string Zillowdays { get; set; }

        public string instrumentnumber { get; set; }

        public string OTHEffYearBuilt { get; set; }

        public string UnitsBedBath { get; set; }
        public string Usecodetype { get; set; }

        public string Totalsqft { get; set; }
        public string OthLand { get; set; }
        public string othbuilding { get; set; }
        public string othAssessedtax { get; set; }


        public string ZillMarkettemp { get; set; }
        public string MedianZEstimate { get; set; }
        public string ZillPastMonths { get; set; }

    }
    public partial class UseCodeCategory
    {
        public int UseCodeCategoryID { get; set; }
        public string UseCodeCategoryName { get; set; }

    }

    public partial class TabsCountedLp
    {
        public int Residential { get; set; }
        public int Commercial { get; set; }
        public int Industrial { get; set; }
        public int Agricultural { get; set; }
        public int Institutional { get; set; }
        public int Government { get; set; }
        public int Miscellaneous { get; set; }
        public int totalMiscellaneous { get; set; }
        public int CentrallyAssessed { get; set; }
        public int NonAgricultural { get; set; }
        public int ALLLEADS { get; set; }
        public int MultiProperty { get; set; }
        public int SetSubjtProperty { get; set; }
        public int NotFound { get; set; }
    }
    public partial class SubTabsCount
    {
        public int CountSubTab { get; set; }
        public string OTHSrchUse { get; set; }
    }

    public class LoanAmortiZd
    {
        public string Month { get; set; }
        public int RepaymentNumber { get; set; }
        public decimal OpeningBalance { get; set; }
        public decimal LoanRepayment { get; set; }
        public decimal InterestCharged { get; set; }
        public decimal CapitalRepaid { get; set; }
        public decimal ClosingBalance { get; set; }
        public string CapitalOutstanding { get; set; }
        public decimal InterestRate { get; set; }

        public string OpeningBalanceNew { get; set; }
        public string LoanRepaymentNew { get; set; }
        public string InterestChargedNew { get; set; }
        public string CapitalRepaidNew { get; set; }
        public string ClosingBalanceNew { get; set; }
        public string InterestRateNew { get; set; }
    }

    public class ContactInfo
    {
        public int ContactIndoID { get; set; }
        public Nullable<int> OTHAddressID { get; set; }
        public Nullable<long> PCBPalmDetailsID { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Landline { get; set; }
        public string NOTESBOX { get; set; }
        public string Mobile2 { get; set; }
        public string Landline1 { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string Email3 { get; set; }
    }

}