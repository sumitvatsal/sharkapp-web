using System;
using System.Collections.Generic;
using System.Text;


namespace SharkAppWeb.ViewModel
{
    class CreateExcelDoc
    {
        
        private Microsoft.Office.Interop.Excel.Application app = null;
        private Microsoft.Office.Interop.Excel.Workbook workbook = null;
        private Microsoft.Office.Interop.Excel.Worksheet worksheet = null;
        private Microsoft.Office.Interop.Excel.Range workSheet_range = null;
        public CreateExcelDoc()
        {
            createDoc();
        }
        public void createDoc()
        {
            try
            {
                app = new Microsoft.Office.Interop.Excel.Application();
                app.Visible = true;
                workbook = app.Workbooks.Add(1);
                worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets[1];

            }
            catch(Exception ex)
            {
                string msg = ex.Message;
            }
            finally
            {

            }
        }

        public void createHeaders(int row, int col, string htext, string cell1, string cell2, int mergeColumns, string b, bool font, int size, string fcolor)
        {
            worksheet.Cells[row, col] = htext;
            workSheet_range = worksheet.get_Range(cell1, cell2);
            workSheet_range.Merge(mergeColumns);
            switch (b)
            {
                case "LemonChiffon":
                    workSheet_range.Interior.Color = System.Drawing.Color.LemonChiffon.ToArgb();
                    break;
                case "Blue":
                    workSheet_range.Interior.Color = System.Drawing.Color.Blue.ToArgb();
                    break;
                case "LightCoral":
                    workSheet_range.Interior.Color = System.Drawing.Color.CadetBlue.ToArgb();
                    break;
                case "LightSkyBlue":
                    workSheet_range.Interior.Color = System.Drawing.Color.LightSkyBlue.ToArgb();
                    break;
                case "LightCyan":
                    workSheet_range.Interior.Color = System.Drawing.Color.LightCyan.ToArgb();
                    break;
                case "WHITE":
                    workSheet_range.Interior.Color = System.Drawing.Color.White.ToArgb();
                    break;
                default:
                    //  workSheet_range.Interior.Color = System.Drawing.Color..ToArgb();
                    break;
                    
            }

            workSheet_range.Borders.Color = System.Drawing.Color.LightCoral.ToArgb();
            workSheet_range.Font.Bold = font;
            workSheet_range.ColumnWidth = size;
            if (fcolor.Equals(""))
            {
                workSheet_range.Font.Color = System.Drawing.Color.White.ToArgb();
            }
            else
            {
                workSheet_range.Font.Color = System.Drawing.Color.Black.ToArgb();
            }

        }

        public void createTitle(int row, int col, string htext, string cell1, string cell2, int mergeColumns, string b, bool font, int size, string fcolor)
        {
            worksheet.Cells[row, col] = htext;
            workSheet_range = worksheet.get_Range(cell1, cell2);
            workSheet_range.Merge(mergeColumns);
            workSheet_range.Font.Bold = font;
            workSheet_range.Font.Size = 18;
            workSheet_range.ColumnWidth = size;
            
            if (fcolor.Equals(""))
            {
                workSheet_range.Font.Color = System.Drawing.Color.White.ToArgb();
            }
            else
            {
                workSheet_range.Font.Color = System.Drawing.Color.Black.ToArgb();
            }
            workSheet_range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;

        }
        public void addData(int row, int col, string data, string cell1, string cell2, string format, int size ,string b)
        {
            worksheet.Cells[row, col] = data;
            workSheet_range = worksheet.get_Range(cell1, cell2);
            workSheet_range.Borders.Color = System.Drawing.Color.LightCoral.ToArgb();
            workSheet_range.NumberFormat = format;
            workSheet_range.ColumnWidth = size;
            workSheet_range.WrapText=true;
            switch (b)
            {
                case "LightCyan":
                    workSheet_range.Interior.Color = System.Drawing.Color.LightCyan.ToArgb();
                    break;
                case "LemonChiffon":
                    workSheet_range.Interior.Color = System.Drawing.Color.LemonChiffon.ToArgb();
                    break;
            }
           
        }

        public void addDateData(int row, int col, string data, string cell1, string cell2, string format, int size, string b)
        {
            worksheet.Cells[row, col] = data;
            workSheet_range = worksheet.get_Range(cell1, cell2);
            workSheet_range.Borders.Color = System.Drawing.Color.LightCoral.ToArgb();
            workSheet_range.ColumnWidth = size;
            workSheet_range.WrapText = true;
            workSheet_range.EntireColumn.NumberFormat = "MM/DD/YYYY";
            switch (b)
            {
                case "LightCyan":
                    workSheet_range.Interior.Color = System.Drawing.Color.LightCyan.ToArgb();
                    break;
                case "LemonChiffon":
                    workSheet_range.Interior.Color = System.Drawing.Color.LemonChiffon.ToArgb();
                    break;
            }

        }


        public void addPercentageData(int row, int col, string data, string cell1, string cell2, string format, int size, string b)
        {
            worksheet.Cells[row, col] = data;
            workSheet_range = worksheet.get_Range(cell1, cell2);
            workSheet_range.Borders.Color = System.Drawing.Color.LightCoral.ToArgb();
            workSheet_range.ColumnWidth = size;
            workSheet_range.WrapText = true;
            worksheet.Cells[row, col].NumberFormat = "0.00%";
            //workSheet_range.EntireRow.NumberFormat = "###,##%";
            switch (b)
            {
                case "LightCyan":
                    workSheet_range.Interior.Color = System.Drawing.Color.LightCyan.ToArgb();
                    break;
                case "LemonChiffon":
                    workSheet_range.Interior.Color = System.Drawing.Color.LemonChiffon.ToArgb();
                    break;
            }

        }
    }
}
