﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;

namespace SharkAppWeb.ViewModel
{
    public class ComparePropertyMSAccess
    {
        VMCase _vmCase = new VMCase();
        public DataSet GetCopareableProperty(int SrchDefendantID, string SiteAddressSrch, string MailingAddress, string LandFactor, int count, int othCnt)
        {
            string _city = "";
            string _Lfactor = LandFactor.Replace(",", "").Trim();
            int landFactor = _Lfactor == "" ? 5000 : Convert.ToInt32(_Lfactor);
            DataSet ds = new DataSet();
            OleDbConnection con;
            OleDbDataAdapter adapter;
            if (othCnt == 1)
            {
                _city = SiteAddressSrch.Split(',')[1].ToLower().ToString().Trim();
            }
            else
            {
                var _objSelectedAddress = _vmCase.GetAddressFileds(SrchDefendantID);
                _city = _objSelectedAddress.City.ToLower().ToString();
            }
            string[] Addres = MailingAddress.Split(' ');

            string Newstring = MailingAddress.Remove(MailingAddress.LastIndexOf(" "));
            string Newstr1 = Newstring.Remove(Newstring.LastIndexOf(' '));
            string _finally = "";
            string CityAddress = "";
            int Last1 = 0;
            if (_city != null && _city != "")
            {
                if (Newstr1.ToLower().Contains(_city))
                {
                    _finally = Newstr1.ToLower().Replace(_city, "");
                    Last1 = _finally.LastIndexOf(" ");
                }
                else
                {
                    Last1 = Newstr1.LastIndexOf(" ");
                }

                CityAddress = _city.ToUpper();
            }
            else
            {
                Last1 = Newstr1.LastIndexOf(" ");
                CityAddress = Newstr1.Split(' ').Last();
            }
            int FirstIndex = MailingAddress.IndexOf(" ");
            string FinalAddress = Newstr1.Substring(FirstIndex, Last1 - FirstIndex).Trim();
            int decLanfactor = Convert.ToInt32((landFactor - 500));
            int inclandFactor = Convert.ToInt32(landFactor + 500);
            con = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\BCPA_TAX_ROLL.mdb");
            //	7830 NW 125 LANE, PARKLAND   SELECT * FROM BCPA_TAX_ROLL where FOLIO_NUMBER='474131031280'
            if (FinalAddress.Contains("#"))
            {
                int tt = FinalAddress.LastIndexOf("#");
                FinalAddress = FinalAddress.Substring(1, tt - 1).Trim();
            }
            if (_Lfactor != null && _Lfactor != "")
            {
                adapter = new OleDbDataAdapter("SELECT * FROM BCPA_TAX_ROLL where ADDRESS_LINE_1 LIKE '%" + FinalAddress + "%' and CITY='" + CityAddress + "' and LAND_CALC_FACT_1 between " + decLanfactor + "and " + inclandFactor + "", con);
            }
            else
            {
                adapter = new OleDbDataAdapter("SELECT * FROM BCPA_TAX_ROLL where ADDRESS_LINE_1 LIKE '%" + FinalAddress + "%' and CITY='" + CityAddress + "'", con);
            }

            adapter.Fill(ds, "BCPA_TAX_ROLL");

            return ds;
        }



        public DataSet GetCompareableProperty()
        {
            string SiteAddressSrch = "4810 NW 42 AVENUE, TAMARAC ";
            string MailingAddress = "4810 NW 42 AVE TAMARAC FL 33319-3702";
            string LandFactor = "";
            string _city = "";
            string _Lfactor = LandFactor.Replace(",", "").Trim();
            int landFactor = _Lfactor == "" ? 5000 : Convert.ToInt32(_Lfactor);
            DataSet ds = new DataSet();
            OleDbConnection con;
            OleDbDataAdapter adapter;
            int othCnt = 1;
            if (othCnt == 1)
            {
                _city = SiteAddressSrch.Split(',')[1].ToLower().ToString().Trim();
            }
            else
            {
                //var _objSelectedAddress = _vmCase.GetAddressFileds(SrchDefendantID);
                //_city = _objSelectedAddress.City.ToLower().ToString();
            }
            string[] Addres = MailingAddress.Split(' ');

            string Newstring = MailingAddress.Remove(MailingAddress.LastIndexOf(" "));
            string Newstr1 = Newstring.Remove(Newstring.LastIndexOf(' '));
            string _finally = "";
            string CityAddress = "";
            int Last1 = 0;
            if (_city != null && _city != "")
            {
                if (Newstr1.ToLower().Contains(_city))
                {
                    _finally = Newstr1.ToLower().Replace(_city, "");
                    Last1 = _finally.LastIndexOf(" ");
                }
                else
                {
                    Last1 = Newstr1.LastIndexOf(" ");
                }
              
                CityAddress = _city.ToUpper();
            }
            else
            {
                Last1 = Newstr1.LastIndexOf(" ");
                CityAddress = Newstr1.Split(' ').Last();
            }
            int FirstIndex = MailingAddress.IndexOf(" ");
            string FinalAddress = Newstr1.Substring(FirstIndex, Last1 - FirstIndex).Trim();
            int decLanfactor = Convert.ToInt32((landFactor - 500));
            int inclandFactor = Convert.ToInt32(landFactor + 500);
            con = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\BCPA_TAX_ROLL.mdb");
            //	7830 NW 125 LANE, PARKLAND   SELECT * FROM BCPA_TAX_ROLL where FOLIO_NUMBER='474131031280'
            if (FinalAddress.Contains("#"))
            {
                int tt = FinalAddress.LastIndexOf("#");
                FinalAddress = FinalAddress.Substring(1, tt - 1).Trim();
            }
            if (_Lfactor != null && _Lfactor != "")
            {
                adapter = new OleDbDataAdapter("SELECT * FROM BCPA_TAX_ROLL where ADDRESS_LINE_1 LIKE '%" + FinalAddress + "%' and CITY='" + CityAddress + "' and LAND_CALC_FACT_1 between " + decLanfactor + "and " + inclandFactor + "", con);
            }
            else
            {
                adapter = new OleDbDataAdapter("SELECT * FROM BCPA_TAX_ROLL where ADDRESS_LINE_1 LIKE '%" + FinalAddress + "%' and CITY='" + CityAddress + "'", con);
            }

            adapter.Fill(ds, "BCPA_TAX_ROLL");

            return ds;
        }



        public DataSet GetMatchedPlantiffInBCPA(int SrchDefendantID, string Plaintiff)
        {

            DataSet ds = new DataSet();
            OleDbConnection con;
            OleDbDataAdapter adapter;

            con = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\BCPA_TAX_ROLL.mdb");
            string[] Filterbr = Plaintiff.Split(new string[] { "<br>" }, StringSplitOptions.None);
            string NewPlaintiff = "";
            if (Filterbr.Length > 0)
            {

                string Newstring = Filterbr[0].ToString();
                if (Newstring != null)
                {
                    NewPlaintiff = Newstring.Remove(Newstring.LastIndexOf(" "));
                }
            }

            // NAME_LINE_1 LEGAL_LINE_1
            adapter = new OleDbDataAdapter("SELECT * FROM BCPA_TAX_ROLL where LEGAL_LINE_1 LIKE '%" + NewPlaintiff + "%'", con);
            adapter.Fill(ds, "BCPA_TAX_ROLL");
            if (ds.Tables["BCPA_TAX_ROLL"].Rows.Count > 0)
            {
                return ds;
            }
            else
            {
                if (Filterbr.Length > 1)
                {

                    string Newstring = Filterbr[1].ToString();
                    if (Newstring != null)
                    {
                        NewPlaintiff = Newstring.Remove(Newstring.LastIndexOf(" "));
                        adapter = new OleDbDataAdapter("SELECT * FROM BCPA_TAX_ROLL where LEGAL_LINE_1 LIKE '%" + NewPlaintiff + "%'", con);
                        adapter.Fill(ds, "BCPA_TAX_ROLL");
                    }
                }
                return ds;
            }
        }


        public DataSet GetCmpPropertyBCPA(string FOLIO_NUMBER)
        {

            DataSet ds = new DataSet();
            OleDbConnection con;
            //OleDbCommand cmd;
            OleDbDataAdapter adapter;

            con = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\BCPA_TAX_ROLL.mdb");
            //	7830 NW 125 LANE, PARKLAND   SELECT * FROM BCPA_TAX_ROLL where FOLIO_NUMBER='474131031280'
            adapter = new OleDbDataAdapter("SELECT * FROM BCPA_TAX_ROLL where FOLIO_NUMBER=" + FOLIO_NUMBER + "", con);
            adapter.Fill(ds, "BCPA_TAX_ROLL");

            return ds;
        }

    }
}