﻿using SharkAppWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SharkAppWeb.ViewModel
{
    public class VMBulkEmails
    {
        public List<string> EmailList { get; set; }

        public List<string> GetEmailList(string County,int? UseCodeCategoryID, int? AssesedFrom, int? AssesedTo, decimal? LandFactorFrom, decimal? LandFactorTo, int? Zipcode, decimal? UnitsMin, decimal? UnitsMax, decimal? BedroomMin, decimal? BedroomMax, decimal? BathroomMin, decimal? BathroomMax)
        {
            List<string> result = new List<string>();
            using(BrowardclerkEntities db=new BrowardclerkEntities())
            {
                result = db.spGetAllEmails(County, AssesedFrom, AssesedTo, LandFactorFrom, LandFactorTo, Zipcode, UnitsMin, UnitsMax, BedroomMin, BedroomMax, BathroomMin, BathroomMax, UseCodeCategoryID, null, null, null, null, null, null).Select(s => s.EmailID).ToList();
            }

            return result;
        }
    }

    
}