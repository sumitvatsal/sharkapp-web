﻿using SharkAppWeb;
using SharkAppWeb.Models;
using Ghostscript.NET;
using iTextSharp.text.pdf;
using MODI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Objects;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Globalization;
using HtmlAgilityPack;
using System.Data.SqlClient;
using System.Configuration;

namespace SharkAppWeb.ViewModel
{
    class VMPalmCase
    {
        BrowardclerkEntities db = new BrowardclerkEntities();

        public List<PalmBeachLpcases> GetPalmSaveableLPCase()
        {
            try
            {

                List<PalmBeachLpcases> _tblPalmBeach = new List<PalmBeachLpcases>();

                _tblPalmBeach = db.tblPalmBeaches.AsEnumerable().Select(s => new PalmBeachLpcases
                {
                    PalmBeachLPCaseID = s.PalmBeachLPCaseID,
                    Name = s.Name,
                    CrossName = s.CrossName,
                    DetailsByUrl = s.DetailsByUrl,
                    CaseDate = s.CaseDate,
                    CaseType = s.CaseType,
                    Book = s.Book,
                    PageNumber = s.PageNumber,
                    CFN = s.CFN,
                    Legal = s.Legal,
                    IsPDFDownloaded = s.IsPDFDownloaded,
                    IsPDFRead = s.IsPDFRead,
                    IsGetDetails = s.IsGetDetails,
                    IsCorrectAddress = s.IsCorrectAddress,
                    IsPCBDetails = s.IsPCBDetails,
                }).ToList();
                return _tblPalmBeach;
            }
            catch
            {
                return null;
            }
        }

        public List<PalmBeachLpcases> GetOtherPalintiffLP(int PalmBeachLPCaseID)
        {
            try
            {

                List<PalmBeachLpcases> _tblPalmBeach = new List<PalmBeachLpcases>();

                _tblPalmBeach = db.sp_GetOTHPlaintiffLPcase(PalmBeachLPCaseID).Select(s => new PalmBeachLpcases
                {
                    PalmBeachLPCaseID = s.PalmBeachLPCaseID,
                    Name = s.Name,
                    CrossName = s.CrossName,
                    DetailsByUrl = s.DetailsByUrl,
                    CaseDate = Convert.ToDateTime(s.CaseDate.Value.ToShortDateString()),
                    CaseType = s.CaseType,
                    Book = s.Book,
                    PageNumber = s.PageNumber,
                    CFN = s.CFN,
                    Legal = s.Legal,
                }).ToList();
                return _tblPalmBeach;
            }
            catch
            {
                return null;
            }
        }

        public List<PalmBeachLpcases> GetLPFiltersINPalm(int UseCodeCategoryID, string SubTabId, int? AssesedFrom, int? AssesedTo, decimal? LandFactorFrom, decimal? LandFactorTo, int? Zipcode, decimal? UnitsMin, decimal? UnitsMax, decimal? BedroomMin, decimal? BedroomMax, decimal? BathroomMin, decimal? BathroomMax, string LocationAddress, string MailingAddress, string Plaintiff, string Defendant, DateTime? RecordDateFrom, DateTime? RecordDateTo, DateTime? SaleDateFrom, DateTime? SaleDateTo, int? SalePriceFrom, int? SalePriceTo, decimal? EstimatedMortFrom, decimal? EstimatedMortTo, decimal? EstOwnerFrom, decimal? EstOwnerTo,string HOAFilterStatus, string HOAfeemin, string HOAfeemax)
        {
            try
            {
              
                List<PalmBeachLpcases> _VMPalmBeachLpcases = new List<PalmBeachLpcases>();

                _VMPalmBeachLpcases = db.sp_GetPalmLpcasesByFilter(AssesedFrom, AssesedTo, LandFactorFrom, LandFactorTo, Zipcode, UnitsMin, UnitsMax, BedroomMin, BedroomMax, BathroomMin, BathroomMax, UseCodeCategoryID, SubTabId,null,null,Defendant ,Plaintiff,LocationAddress, MailingAddress, null,null,null,null,null,null, RecordDateFrom, RecordDateTo, SaleDateFrom, SaleDateTo, SalePriceFrom, SalePriceTo, EstimatedMortFrom, EstimatedMortTo, EstOwnerFrom, EstOwnerTo, HOAFilterStatus,HOAfeemin,HOAfeemax).AsEnumerable().Select(s => new PalmBeachLpcases
                {
                    PalmBeachLPCaseID = s.PalmBeachLPCaseID,
                    Name = Regex.Replace(s.Name, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                    CrossName = Regex.Replace(s.CrossName, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                    DetailsByUrl = s.DetailsByUrl,
                    CaseDate = s.CaseDate,
                    CaseType = s.CaseType,
                    Book = s.Book,
                    PageNumber = s.PageNumber,
                    CFN = s.CFN,
                    Legal = s.Legal,
                    IsPDFDownloaded = s.IsPDFDownloaded,
                    IsPDFRead = s.IsPDFRead,
                    IsGetDetails = s.IsGetDetails,
                    IsCorrectAddress = s.IsCorrectAddress,
                    ISCorrectPalmPCB = s.ISCorrectPalmPCB,
                    IsPCBDetails = s.IsPCBDetails,
                    PCNumber = s.PCNumber,
                    PCBPalmDetailsID = s.PCBPalmDetailsID,
                    LocationAddress = s.LocationAddress,
                    MailingAddress = s.MailingAddress,
                    LegalDesc = s.LegalDesc,
                    ImprovementVal = s.ImprovementVal,
                    LandValue = s.LandValue,
                    MarketValue = s.MarketValue,
                    SaleDate = s.SaleDate,
                    Price = s.Price,
                    BookPage = s.BookPage,
                    SaleType = s.SaleType,
                    Owner = s.Owner,
                    AssesedVal = s.AssesedVal,
                    ExemptionAmt = s.ExemptionAmt,
                    TaxableValue = s.TaxableValue,
                    IsOtherPalm = Convert.ToBoolean(s.IsOtherPalm),
                    AdValorem = s.AdValorem,
                    NonAdValorem = s.NonAdValorem,
                    TotalTaxes = s.TotalTaxes,
                    EstMarketValue = s.MarketValue == null ? "" : string.Format(CultureInfo.InvariantCulture, "{0:N0}", (Convert.ToInt32(s.MarketValue.Replace(",", "").Replace("$", "")) * 15 / 100) + Convert.ToInt32(s.MarketValue.Replace(",", "").Replace("$", ""))),
                    OwedOnMortgage = s.OwedOnMortgage,
                    SalesPriceSqFt = string.Format(CultureInfo.InvariantCulture, "{0:N2}", Convert.ToDecimal((string.IsNullOrEmpty(s.Price) ? 0 : Convert.ToDecimal(s.Price.Replace(",", "").Replace("$", "")) / ((string.IsNullOrEmpty(s.totalsqft) || Convert.ToDecimal(s.totalsqft) == 0) ? 1 : Convert.ToDecimal(s.totalsqft))))),
                    AssessedMarketSqFt = string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal((string.IsNullOrEmpty(s.MarketValue) ? 0 : Convert.ToDecimal(s.MarketValue.Replace(",", "").Replace("$", "")) / ((string.IsNullOrEmpty(s.totalsqft) || Convert.ToDecimal(s.totalsqft) == 0) ? 1 : Convert.ToDecimal(s.totalsqft))))),
                    EstOwnerEquity = s.OwedOnMortgage == "FULLY PAID" ? s.OwedOnMortgage : string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal((s.MarketValue == null ? 0 : Convert.ToDecimal(s.MarketValue.Replace(",", "").Replace("$", "")) - (s.OwedOnMortgage == null ? 0 : Convert.ToDecimal(s.OwedOnMortgage))))),
                    cntPlaintiffs = Convert.ToBoolean(s.cntPlaintiffs),
                    IsspokioDetails = s.IsSpokioDetails ?? false,
                    Latitude=s.Latitude,
                    Longitude=s.Longitude,
                    RedfinPrice=s.RedfinPrice,
                    ZillowPrice=s.ZillowPrice,
                    issunbizdetailedID = s.issunbizdetailedID,
                    IsPalmLinkedIn = s.IsPalmLinkedIn,
                    IsTwitterPalm = s.IsTwitterPalm,
                    RedfinPricedetail = s.RedfinPricedetail,
                    ZillowPricedetail = s.ZillowPricedetail,
                    RedfinUrl = s.RedfinUrl,
                    zillowUrl = s.zillowUrl,
                    LoopnetPrice = s.LoopnetPrice,
                    LoopnetUrl = s.LoopnetUrl,
                    NonpaymentStatus = s.NonpaymentStatus,
                    HOARedfinDue = s.hoaredfindue,
                    Rentzestimate = s.Rentzestimate,
                    Refipayment = s.Refipayment,
                    PropertyOwner = s.PropertyOwner,
                    PropertyType=s.propertyType,
                    Redfindays=s.Redfindays,
                    Zillowdays=s.Zillowdays


                }).ToList();

                return _VMPalmBeachLpcases;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

         

        public List<PalmBeachLpcases> GetPalmBeachCases(int? PageSize)
        {
            try
            {

                List<PalmBeachLpcases> _VMPalmBeachLpcases = new List<PalmBeachLpcases>();

                _VMPalmBeachLpcases = db.sp_GetPalmBeachLpcases(PageSize).AsEnumerable().Select(s => new PalmBeachLpcases
                {
                    PalmBeachLPCaseID = s.PalmBeachLPCaseID,
                    Name = Regex.Replace(s.Name, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                    CrossName = s.CrossName==null?"": Regex.Replace(s.CrossName, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                    DetailsByUrl = s.DetailsByUrl,
                    CaseDate = s.CaseDate,
                    CaseType = s.CaseType,
                    Book = s.Book,
                    PageNumber = s.PageNumber,
                    CFN = s.CFN,
                    Legal = s.Legal,
                    IsPDFDownloaded = s.IsPDFDownloaded,
                    IsPDFRead = s.IsPDFRead,
                    IsGetDetails = s.IsGetDetails,
                    IsCorrectAddress = s.IsCorrectAddress,
                    ISCorrectPalmPCB = s.ISCorrectPalmPCB,
                    IsPCBDetails = s.IsPCBDetails,
                    PCNumber = s.PCNumber,
                    PCBPalmDetailsID = s.PCBPalmDetailsID,
                    LocationAddress = s.LocationAddress,
                    MailingAddress = s.MailingAddress,
                    LegalDesc = s.LegalDesc,
                    ImprovementVal = s.ImprovementVal,
                    LandValue = s.LandValue,
                    MarketValue = s.MarketValue,
                    SaleDate = s.SaleDate,
                    Price = s.Price,
                    BookPage = s.BookPage,
                    SaleType = s.SaleType,
                    Owner = s.Owner,
                    AssesedVal = s.AssesedVal,
                    ExemptionAmt = s.ExemptionAmt,
                    TaxableValue = s.TaxableValue,
                    IsOtherPalm = Convert.ToBoolean(s.IsOtherPalm),
                    AdValorem = s.AdValorem,
                    NonAdValorem = s.NonAdValorem,
                    TotalTaxes = s.TotalTaxes,
                    EstMarketValue = s.MarketValue == null ? "" : string.Format(CultureInfo.InvariantCulture, "{0:N0}", (Convert.ToInt32(s.MarketValue.Replace(",", "").Replace("$", "")) * 15 / 100) + Convert.ToInt32(s.MarketValue.Replace(",", "").Replace("$", ""))),
                    OwedOnMortgage = s.OwedOnMortgage,
                    SalesPriceSqFt = string.Format(CultureInfo.InvariantCulture, "{0:N2}", Convert.ToDecimal((string.IsNullOrEmpty(s.Price) ? 0 : Convert.ToDecimal(s.Price.Replace(",", "").Replace("$", "")) / ((string.IsNullOrEmpty(s.totalsqft) || Convert.ToDecimal(s.totalsqft) == 0) ? 1 : Convert.ToDecimal(s.totalsqft))))),
                    AssessedMarketSqFt = string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal((string.IsNullOrEmpty(s.MarketValue) ? 0 : Convert.ToDecimal(s.MarketValue.Replace(",", "").Replace("$", "")) / ((string.IsNullOrEmpty(s.totalsqft) || Convert.ToDecimal(s.totalsqft) == 0) ? 1 : Convert.ToDecimal(s.totalsqft))))),
                    EstOwnerEquity = s.OwedOnMortgage == "FULLY PAID" ? s.OwedOnMortgage : string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal((s.MarketValue == null ? 0 : Convert.ToDecimal(s.MarketValue.Replace(",", "").Replace("$", "")) - (s.OwedOnMortgage == null ? 0 : Convert.ToDecimal(s.OwedOnMortgage))))),
                    cntPlaintiffs = Convert.ToBoolean(s.cntPlaintiffs),
                    IsspokioDetails = s.IsSpokioDetails ?? false,
                    issunbizdetailedID = s.issunbizdetailedID,
                    IsPalmLinkedIn = s.IsPalmLinkedIn,
                    IsTwitterPalm = s.IsTwitterPalm,
                    Latitude = s.Latitude,
                    Longitude = s.Longitude,

                    RedfinPricedetail = s.RedfinPricedetail,
                    ZillowPricedetail = s.ZillowPricedetail,
                    RedfinUrl = s.RedfinUrl,
                    zillowUrl = s.zillowUrl,
                    LoopnetPrice = s.LoopnetPrice,
                    LoopnetUrl = s.LoopnetUrl,
                    NonpaymentStatus = s.NonpaymentStatus,
                    HOARedfinDue = s.HOARedfinDue,
                    Rentzestimate = s.Rentzestimate,
                    Refipayment = s.Refipayment,
                    PropertyOwner = s.PropertyOwner,
                    Redfindays = s.Redfindays,
                    Zillowdays = s.Zillowdays

                }).ToList();

                return _VMPalmBeachLpcases;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public List<PCBPalmDetail> GetPCBPalmDetail(Int64 PCBPalmDetailsID)
        {
            try
            {
                List<PCBPalmDetail> _PCBPalmDetail = new List<PCBPalmDetail>();
                _PCBPalmDetail = db.tblPCBPalmDetails.AsEnumerable().Where(x => x.PCBPalmDetailsID == PCBPalmDetailsID).Select(s => new PCBPalmDetail
                {
                    PCBPalmDetailsID = s.PCBPalmDetailsID,
                    PalmBeachLPCaseID = s.PalmBeachLPCaseID,
                    PCNumber = s.PCNumber,
                    OfficialBook = s.OfficialBook,
                    SaleDate = s.SaleDate,
                    Subdivision = s.Subdivision,
                    LegalDesc = s.LegalDesc,
                    Units = s.Units,
                    TotalSqft = s.TotalSqft,
                    Acres = s.Acres,
                    Usecode = s.Usecode,
                    Zoning = s.Zoning,
                    MailingAddress = s.MailingAddress,
                    PCBAppraisal = db.tblPCBAppraisals.AsEnumerable().Where(x => x.PCBPalmDetailsID == s.PCBPalmDetailsID).ToList(),
                    PCBSaleInfo = db.tblPCBSaleInfoes.AsEnumerable().Where(x => x.PCBPalmDetailsID == s.PCBPalmDetailsID).ToList(),
                    PCBTaxAssesed = db.tblPCBTaxAsseseds.AsEnumerable().Where(x => x.PCBPalmDetailsID == s.PCBPalmDetailsID).ToList(),
                    PCBTotalTax = db.tblPCBTotalTaxes.AsEnumerable().Where(x => x.PCBPalmDetailsID == s.PCBPalmDetailsID).ToList(),
                }).ToList();

                return _PCBPalmDetail;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public List<PCBPalmDetail> GetPCBPalmMapRecord(string[] showDocReportLp1)
        {
            try
            {
                List<PCBPalmDetail> _lstsrch = new List<PCBPalmDetail>();

                if (showDocReportLp1 != null)
                {

                    for (int i = 1; i < showDocReportLp1.Length; i++)
                    {
                        PCBPalmDetail _srch = new PCBPalmDetail();
                        int _countColumn = 1;
                        HtmlDocument doc = new HtmlDocument();
                        doc.LoadHtml(showDocReportLp1[i].ToString());

                        foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//tr/td"))
                        {
                            var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();

                            switch (_countColumn)
                            {

                                ////case 36:
                                ////    _srch.PCBPalmDetailsID = Convert.ToInt32(_text);
                                ////    break;
                                //case 1:
                                //    _srch.PalmBeachLPCaseID = Convert.ToInt32(_text);
                                //    break;
                                //case 39:
                                //    _srch.PCNumber = _text;
                                //    break;
                                //case 25:
                                //    _srch.OfficialBook = _text;
                                //    break;
                                //case 18:
                                //    _srch.SaleDate = _text;
                                //    break;
                                //case 14:
                                //    _srch.EstOwnerEquity = _text.Replace("$","");
                                //    break;
                                //case 9:
                                //    _srch.LocationAddress = _text;
                                //    break;
                                //case 10:
                                //    _srch.MailingAddress = _text;
                                //    break;
                                //case 3:
                                //    _srch.Plaintiff = _text;
                                //    break;
                                //case 8:
                                //    _srch.Defendant = _text;
                                //    break;
                                //case 40:
                                //    _srch.SalePrice = _text;
                                //    break;
                                //case 11:
                                //    _srch.MarketValue = _text;
                                //    break;
                                //case 16:
                                //    _srch.Exemption = _text;
                                //    break;
                                //case 17:
                                //    _srch.Tax = _text;
                                //    break;

                                case 1:
                                    _srch.PalmBeachLPCaseID = Convert.ToInt32(_text);
                                    _srch.Plaintiff = node.Elements("input").Skip(1).FirstOrDefault().Attributes["value"].Value;
                                    _srch.CFN = node.Elements("input").Skip(2).FirstOrDefault().Attributes["value"].Value;
                                    _srch.CaseType = node.Elements("input").Skip(3).FirstOrDefault().Attributes["value"].Value;
                                    _srch.DetailsByUrl = node.Elements("input").Skip(4).FirstOrDefault().Attributes["value"].Value;
                                    _srch.Latitude = node.Elements("input").Skip(5).FirstOrDefault().Attributes["value"].Value;
                                    _srch.Longitude = node.Elements("input").Skip(6).FirstOrDefault().Attributes["value"].Value;
                                    _srch.PCNumber = node.Elements("input").Skip(7).FirstOrDefault().Attributes["value"].Value;
                                    _srch.SaleDate = node.Elements("input").Skip(8).FirstOrDefault().Attributes["value"].Value;
                                    _srch.SalePrice = node.Elements("input").Skip(9).FirstOrDefault().Attributes["value"].Value;

                                    break;
                                case 6:
                                    _srch.Defendant = _text;
                                    break;
                                case 7:
                                    _srch.LocationAddress = _text;
                                    break;
                                case 8:
                                    _srch.MailingAddress = _text;
                                    break;
                                case 9:
                                    _srch.MarketValue = _text;
                                    break;
                                case 11:
                                    _srch.EstMarketValue = _text;
                                    break;
                                case 12:
                                    _srch.EstOwnerEquity = _text.Replace("$", "");
                                    break;
                                case 13:
                                    _srch.PropertyType = _text;
                                    break;
                                case 15:
                                    _srch.Exemption = _text;
                                    break;
                               
                            }
                            _countColumn++;

                        }
                        if (!string.IsNullOrEmpty(_srch.MailingAddress))
                        _lstsrch.Add(_srch);
                    }

                }
                return _lstsrch;

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public List<PCBPalmDetail> GetPCBPalmForMap(Int64 PCBPalmDetailsID)
        {
            try
            {
                List<PCBPalmDetail> _PCBPalmDetail = new List<PCBPalmDetail>();
                _PCBPalmDetail = db.sp_GetPalmLPForMap(PCBPalmDetailsID).Select(s => new PCBPalmDetail
                {
                    PCBPalmDetailsID = s.PCBPalmDetailsID,
                    PalmBeachLPCaseID = s.PalmBeachLPCaseID,
                    PCNumber = s.PCNumber,
                    OfficialBook = s.OfficialBook,
                    SaleDate = s.SaleDate,
                    Subdivision = s.Subdivision,
                    LegalDesc = s.LegalDesc,
                    Units = s.Units,
                    TotalSqft = s.TotalSqft,
                    Acres = s.Acres,
                    Usecode = s.Usecode,
                    Zoning = s.Zoning,
                    MailingAddress = s.MailingAddress,
                    LocationAddress=s.LocationAddress,
                    Plaintiff = Regex.Replace(s.name, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                    Defendant = Regex.Replace(s.CrossName, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                    SalePrice = s.price,
                    MarketValue = s.MarketValue,
                    Exemption = s.ExemptionAmt,
                    Tax = s.TotalTaxes,
                    OwedOnMortgage = s.OwedOnMortgage,
                    EstOwnerEquity =(s.OwedOnMortgage==null || s.OwedOnMortgage == "FULLY PAID") ? s.OwedOnMortgage : string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal((s.MarketValue == null ? 0 : Convert.ToDecimal(s.MarketValue.Replace(",", "").Replace("$", "")) - (s.OwedOnMortgage == null ? 0 : Convert.ToDecimal(s.OwedOnMortgage))))),
                  Latitude=s.Latitude,
                  Longitude=s.Longitude

                }).ToList();

                return _PCBPalmDetail;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public string DeletePalmBeachCase(int palmBeachLPCaseID)
        {
            string response = "";
            try
            {
                int i = db.sp_deletePalmlist(palmBeachLPCaseID);
                if (i >= 1)
                    response = "Record has been deleted successfully.";
                else
                    response = "Record has been not deleted.";
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }
            return response;
        }
        public string DeletePalmLPCases(string[] RowLpCaseId)
        {
            string Result = "";
            try
            {
                for (int i = 0; i < RowLpCaseId.Length; i++)
                {
                    int PalmBeachLPCaseID = Convert.ToInt32(RowLpCaseId[i].ToString());
                    db.sp_deletePalmlist(PalmBeachLPCaseID);
                }
                Result = "Success";

            }
            catch
            {
                Result = "Error";

            }
            return Result;
        }

        public List<PalmBeachLpcases> GetOtherPalmList(int PalmBeachLPCaseID)
        {
            List<PalmBeachLpcases> _PalmLPCase = new List<PalmBeachLpcases>();
            try
            {
                _PalmLPCase = db.tblPCBPalmDetails.Where(x => x.PalmBeachLPCaseID == PalmBeachLPCaseID).Select(s => new PalmBeachLpcases()
                {
                    PCBPalmDetailsID = s.PCBPalmDetailsID,
                    PCNumber = s.PCNumber,
                    MailingAddress = s.MailingAddress,
                    SaleDate = s.SaleDate,
                }).ToList();

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return _PalmLPCase;

        }

        
        public void SetDefaultPalmPBC(Int64 PCBPalmDetailsID)
        {
            try
            {

                var _newPalmDetails = db.tblPCBPalmDetails.Find(PCBPalmDetailsID);
                if (_newPalmDetails != null)
                {
                    _newPalmDetails.ISCorrectPalmPCB = true;
                    db.Entry(_newPalmDetails).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        public ContactInfo GetPalmContactInfo(long? PCBPalmDetailsID)
        {

            ContactInfo ContactInf = new ContactInfo();

            try
            {

                ContactInf = db.spGetPalmContactInfo(PCBPalmDetailsID).Select(s => new ContactInfo
                {
                    PCBPalmDetailsID = PCBPalmDetailsID,
                    NOTESBOX = s.NOTESBOX,
                    Email = string.IsNullOrEmpty(s.Emails) ? string.Empty : (s.Emails.Split(',').Length > 0 ? s.Emails.Split(',')[0] : string.Empty),
                    Email1 = string.IsNullOrEmpty(s.Emails) ? string.Empty : (s.Emails.Split(',').Length > 1 ? s.Emails.Split(',')[1] : string.Empty),
                    Email2 = string.IsNullOrEmpty(s.Emails) ? string.Empty : (s.Emails.Split(',').Length > 2 ? s.Emails.Split(',')[2] : string.Empty),
                    Email3 = string.IsNullOrEmpty(s.Emails) ? string.Empty : (s.Emails.Split(',').Length > 3 ? s.Emails.Split(',')[3] : string.Empty),
                    Mobile = string.IsNullOrEmpty(s.Mobiles) ? string.Empty : (s.Mobiles.Split(',').Length > 0 ? s.Mobiles.Split(',')[0] : string.Empty),
                    Mobile2 = string.IsNullOrEmpty(s.Mobiles) ? string.Empty : (s.Mobiles.Split(',').Length > 1 ? s.Mobiles.Split(',')[1] : string.Empty),
                    Landline = string.IsNullOrEmpty(s.Landlines) ? string.Empty : (s.Landlines.Split(',').Length > 0 ? s.Landlines.Split(',')[0] : string.Empty),
                    Landline1 = string.IsNullOrEmpty(s.Landlines) ? string.Empty : (s.Landlines.Split(',').Length > 1 ? s.Landlines.Split(',')[1] : string.Empty)
                }).FirstOrDefault() ?? new ContactInfo()
                {
                    PCBPalmDetailsID = PCBPalmDetailsID,
                };

                //ContactInf = db.tblPalmContactInfoes.Where(x => x.PCBPalmDetailsID == PCBPalmDetailsID).Select(p => new ContactInfo
                //{
                //    PCBPalmDetailsID = PCBPalmDetailsID,
                //    Mobile = p.Mobile,
                //    Email = p.Email,
                //    Landline = p.Landline,
                //    NOTESBOX = p.NOTESBOX,
                //    Mobile2 = p.Mobile2,
                //    Landline1 = p.Landline1,
                //    Email1 = p.Email1,
                //    Email2 = p.Email2,
                //    Email3 = p.Email3,
                //}).FirstOrDefault() ?? new ContactInfo()
                //{
                //    PCBPalmDetailsID = PCBPalmDetailsID,
                //};

                return ContactInf;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return new ContactInfo();
            }
        }

        public void SaveAndUpdatePalmContactInfo(long PCBPalmDetailsID, string Mobile, string Email, string Landline, string Landline1, string Mobile2, string Email1, string Email2, string Email3, string NOTESBOX)
        {
            try
            {
                db.Sp_InsertUPalmContactInfo(PCBPalmDetailsID, Mobile, Email, Landline, NOTESBOX, Mobile2, Landline1, Email1, Email2, Email3);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        public List<tblPalmCourtDetail> GetCourtList(int SpokioDetailsID)
        {
            try
            {
                List<tblPalmCourtDetail> _VMCourtList = new List<tblPalmCourtDetail>();
                _VMCourtList = db.tblPalmCourtDetails.AsEnumerable().Where(x => x.SpokioDetailsID == SpokioDetailsID).Select(s => new tblPalmCourtDetail
                {
                    SpokioDetailsID = Convert.ToInt32(s.SpokioDetailsID),
                    CourtDetailsID = Convert.ToInt32(s.CourtDetailsID),
                    RecordsNumber = s.RecordsNumber,
                    OffenseDate = s.OffenseDate,
                    OffenseCode = s.OffenseCode,
                    OffenseDesc = s.OffenseDesc,
                    Disposition = s.Disposition,
                    DispositionDate = s.DispositionDate,
                    SourceState = s.SourceState,
                    SourceName = s.SourceName,
                    CaseNo = s.CaseNo,
                    ArrestDate = s.ArrestDate,
                    Racewhite = s.Racewhite,
                }).ToList();

                return _VMCourtList;
            }
            catch
            {
                return null;
            }
        }

        public List<SpokioDetail> GetSpokioOwnerList(int PCBPalmDetailsID)
        {
            try
            {
                List<SpokioDetail> _VMSpokioList = new List<SpokioDetail>();
                _VMSpokioList = db.tblPalmSpokioDetails.AsEnumerable().Where(x => x.PCBPalmDetailsID == PCBPalmDetailsID).Select(s => new SpokioDetail
                {
                    OTHAddressID = Convert.ToInt32(s.PCBPalmDetailsID),
                    SpkUName = s.SpkUName,
                    SpkBirthDate = s.SpkBirthDate,
                    SpkUrl = s.SpkUrl,
                    SpkMaritalStatus = s.MaritalStatus,
                    SpkZodiacSign = s.ZodiacSign,
                    SpkEthnicity = s.Ethnicity,
                    SpokioDetailsID = Convert.ToInt32(s.SpokioDetailsID)

                }).ToList();

                return _VMSpokioList;
            }
            catch
            {
                return null;
            }
        }
        public SpokioDetail GetPalmSpokioOwnerDetails(long spokioDetailsID, int PCBPalmDetailsID)
        {
            try
            {
                SpokioDetail _VMSpokioList = new SpokioDetail();
                _VMSpokioList = db.tblPalmSpokioDetails.AsEnumerable().Where(x => x.PCBPalmDetailsID == PCBPalmDetailsID && x.SpokioDetailsID == spokioDetailsID).Select(s => new SpokioDetail
                {
                    OTHAddressID = Convert.ToInt32(s.PCBPalmDetailsID),
                    SpkUName = s.SpkUName,
                    SpkBirthDate = s.SpkBirthDate,
                    SpkUrl = s.SpkUrl,
                    SpkMaritalStatus = s.MaritalStatus,
                    SpkZodiacSign = s.ZodiacSign,
                    SpkEthnicity = s.Ethnicity,
                    SpokioDetailsID = Convert.ToInt32(s.SpokioDetailsID),
                    CntCourtDetails = db.tblCourtDetails.AsEnumerable().Where(p => p.SpokioDetailsID == s.SpokioDetailsID).ToList().Count(),
                    SpkEmails = db.tblPalmSpokeoEmails.AsEnumerable().Where(w => w.SpokeoDetailsID == s.SpokioDetailsID).Select(x => new SpokioEmails
                    {
                        EmailID = x.EmailID,
                        EmailType = x.EmailType,
                        SpokeoDetailsID = x.SpokeoDetailsID,
                        SpokeoEmailsID = x.SpokeoEmailsID
                    }).ToList(),
                    SpkTelephones = db.tblPalmSpokeoTelephones.AsEnumerable().Where(w => w.SpokeoDetailsID == s.SpokioDetailsID).Select(x => new SpokioTelephones
                    {
                        Network = x.Network,
                        PhoneType = x.PhoneType,
                        SpokeoDetailsID = x.SpokeoDetailsID,
                        SpokeoTelephonesID = x.SpokeoTelephonesID,
                        Telephone = x.Telephone
                    }).ToList(),
                    Addresses = db.tblPalmSpokeoAddresses.AsEnumerable().Where(w => w.SpokeoDetailsID == s.SpokioDetailsID).Select(x => new SpokeoAddresses
                    {
                        AddressYear = x.AddressYear,
                        SpokeoAddress = x.SpokeoAddress,
                        SpokeoAddressID = x.SpokeoAddressID,
                        SpokeoDetailsID = x.SpokeoDetailsID,
                        Residents = db.tblPalmSpokeoResidents.AsEnumerable().Where(w => w.SpokeoAddressID == x.SpokeoAddressID).Select(r => new SpokeoResidents
                        {
                            Ethnicity = r.Ethnicity,
                            MaritalStatus = r.MaritalStatus,
                            ResidentDOB = r.ResidentDOB,
                            ResidentID = r.ResidentID,
                            ResidentName = r.ResidentName,
                            ResidentUrl = r.ResidentUrl,
                            SpokeoAddressID = r.SpokeoAddressID,
                            ZodiacSign = r.ZodiacSign
                        }).ToList()
                    }).ToList(),
                    Relatives = db.tblPalmSpokeoRelatives.AsEnumerable().Where(w => w.SpokeoDetailsID == s.SpokioDetailsID).Select(x => new SpokeoRelatives
                    {
                        AddressUrl = x.AddressUrl,
                        RelativeAddress = x.RelativeAddress,
                        RelativeID = x.RelativeID,
                        RelativeName = x.RelativeName,
                        RelativeUrl = x.RelativeUrl,
                        SpokeoDetailsID = x.SpokeoDetailsID
                    }).ToList(),
                    SocialProfiles = db.tblPalmSpokeoSocialLinks.AsEnumerable().Where(w => w.SpokeoDetailsID == s.SpokioDetailsID).Select(x => new SpokeoSocialProfiles
                    {
                        SocialLinkUrl = x.SocialLinkUrl,
                        SocialName = x.SocialName,
                        SocialSiteName = x.SocialSiteName,
                        SocialSiteType = x.SocialSiteType,
                        SpokeoDetailsID = x.SpokeoDetailsID,
                        SpokeoSocialLinkID = x.SpokeoSocialLinkID
                    }).ToList()

                }).FirstOrDefault();

                return _VMSpokioList;
            }
            catch
            {
                return null;
            }
        }

        public TabsCountedLp GetCountTabsLpCase()
        {
            try
            {
                TabsCountedLp _lst = new TabsCountedLp();
                _lst = db.sp_CountTabsPBCaseList(null, null, null, null, null, null, null, null, null, null, null, null, null).Select(s => new TabsCountedLp
                {
                    Residential = Convert.ToInt32(s.Residential),
                    Commercial = Convert.ToInt32(s.Commercial),
                    Industrial = Convert.ToInt32(s.Industrial),
                    Miscellaneous = Convert.ToInt32(s.Miscellaneous),
                    ALLLEADS = Convert.ToInt32(s.ALLLEADS),
                    MultiProperty = Convert.ToInt32(s.MultiProperty),
                    SetSubjtProperty = Convert.ToInt32(s.SetSubjtProperty),
                    NotFound = Convert.ToInt32(s.NotFound),
                }).FirstOrDefault();
                return _lst;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public DataTable GetSubTabsCount()
        {
            DataTable dt = new DataTable();

            try
            {
                using (SqlConnection myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBrowardclerk"].ToString()))
                {
                    using (SqlCommand cmd = new SqlCommand("sp_GetPalmSubtabNumbering", myConnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        myConnection.Open();

                        dt.Load(cmd.ExecuteReader());

                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }

        }

        public List<UseCodeCategory> GetPropertyTypeTab()
        {
            try
            {
                List<UseCodeCategory> _lst = new List<UseCodeCategory>();
                _lst = db.tblPalmUseCodeCategories.AsEnumerable().Select(s => new UseCodeCategory
                {
                    UseCodeCategoryID = Convert.ToInt32(s.UseCodeCategpryID),
                    UseCodeCategoryName = s.UseCodeCategoryName,
                }).ToList();
                return _lst;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }


        public List<PalmBeachLpcases> GetSerachPalmBeachCases(int UseCodeCategoryID, int? PalmBeachLPCaseID, int? CntRecord, string SubTabId,bool groupByCFN)
        {
            try
            {

                List<PalmBeachLpcases> _VMPalmBeachLpcases = new List<PalmBeachLpcases>();

                _VMPalmBeachLpcases = db.spGetSearchPalmBeach(UseCodeCategoryID, PalmBeachLPCaseID, CntRecord, SubTabId, groupByCFN).AsEnumerable().Select(s => new PalmBeachLpcases
                {
                    PalmBeachLPCaseID = s.PalmBeachLPCaseID,
                    PlantiffName = Regex.Replace(s.PlantiffName, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                    Name = Regex.Replace(s.Name, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                    CrossName =  s.CrossName == null ? "" : Regex.Replace(s.CrossName, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim(),
                    DetailsByUrl = s.DetailsByUrl,
                    CaseDate = s.CaseDate,
                    CaseType = s.CaseType,
                    Book = s.Book,
                    PageNumber = s.PageNumber,
                    CFN = s.CFN,
                    Legal = s.Legal,
                    IsPDFDownloaded = s.IsPDFDownloaded,
                    IsPDFRead = s.IsPDFRead,
                    IsGetDetails = s.IsGetDetails,
                    IsCorrectAddress = s.IsCorrectAddress,
                    ISCorrectPalmPCB = s.ISCorrectPalmPCB,
                    IsPCBDetails = s.IsPCBDetails,
                    PCNumber = s.PCNumber,
                    PCBPalmDetailsID = s.PCBPalmDetailsID,
                    LocationAddress = s.LocationAddress,
                    MailingAddress = s.MailingAddress,
                    LegalDesc = s.LegalDesc,
                    ImprovementVal = s.ImprovementVal,
                    LandValue = s.LandValue,
                    MarketValue = s.MarketValue,
                    SaleDate = s.SaleDate,
                    Price = s.Price,
                    BookPage = s.BookPage,
                    SaleType = s.SaleType,
                    Owner = s.Owner,
                    AssesedVal = s.AssesedVal,
                    ExemptionAmt = s.ExemptionAmt,
                    TaxableValue = s.TaxableValue,
                    IsOtherPalm = Convert.ToBoolean(s.IsOtherPalm),
                    AdValorem = s.AdValorem,
                    NonAdValorem = s.NonAdValorem,
                    TotalTaxes = s.TotalTaxes,
                    EstMarketValue = s.MarketValue == null ? "" : string.Format(CultureInfo.InvariantCulture, "{0:N0}", (Convert.ToInt32(s.MarketValue.Replace(",", "").Replace("$", "")) * 15 / 100) + Convert.ToInt32(s.MarketValue.Replace(",", "").Replace("$", ""))),
                    OwedOnMortgage = s.OwedOnMortgage,
                    SalesPriceSqFt = string.Format(CultureInfo.InvariantCulture, "{0:N2}", Convert.ToDecimal((string.IsNullOrEmpty(s.Price) ? 0 : Convert.ToDecimal(s.Price.Replace(",", "").Replace("$", "")) / ((string.IsNullOrEmpty(s.totalsqft) || Convert.ToDecimal(s.totalsqft) == 0) ? 1 : Convert.ToDecimal(s.totalsqft))))),
                    AssessedMarketSqFt = string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal((string.IsNullOrEmpty(s.MarketValue) ? 0 : Convert.ToDecimal(s.MarketValue.Replace(",", "").Replace("$", "")) / ((string.IsNullOrEmpty(s.totalsqft) || Convert.ToDecimal(s.totalsqft) == 0) ? 1 : Convert.ToDecimal(s.totalsqft))))),
                    EstOwnerEquity = s.OwedOnMortgage == "FULLY PAID" ? s.OwedOnMortgage : string.Format(CultureInfo.InvariantCulture, "{0:N0}", Convert.ToDecimal((s.MarketValue == null ? 0 : Convert.ToDecimal(s.MarketValue.Replace(",", "").Replace("$", "")) - (s.OwedOnMortgage == null ? 0 : Convert.ToDecimal(s.OwedOnMortgage))))),
                    cntPlaintiffs = Convert.ToBoolean(s.cntPlaintiffs),
                    IsspokioDetails = s.IsSpokioDetails ?? false,
                    issunbizdetailedID=s.issunbizdetailedID,
                    IsPalmLinkedIn=s.IsPalmLinkedIn,
                    IsTwitterPalm=s.IsTwitterPalm,
                    Latitude=s.Latitude,
                    Longitude=s.Longitude,
                    PropertyType=s.propertyType,
                    RedfinPrice=s.RedfinPrice,
                    ZillowPrice=s.ZillowPrice,
                    RedfinPricedetail=s.RedfinPricedetail,
                    ZillowPricedetail=s.ZillowPricedetail,
                    RedfinUrl=s.RedfinUrl,
                    zillowUrl=s.zillowUrl,
                    LoopnetPrice=s.LoopnetPrice,
                    LoopnetUrl=s.LoopnetUrl,
                    NonpaymentStatus=s.NonpaymentStatus,
                    HOARedfinDue=s.HOARedfinDue,
                    Rentzestimate=s.Rentzestimate,
                    Refipayment=s.Refipayment,
                    PropertyOwner=s.PropertyOwner,
                    Redfindays=s.Redfindays,
                    Zillowdays =s.Zillowdays
                    
                }).ToList();

                return _VMPalmBeachLpcases;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }





        public List<SunbizDetail> GetSunbizsearchDetailData()
        {
            try
            {

                List<SunbizDetail> _VMSunbizDetailLpcases = new List<SunbizDetail>();

                _VMSunbizDetailLpcases = db.Sp_GetSunbizDetail_SimaniSearch().AsEnumerable().Select(s => new SunbizDetail
                {
                    SunbizCompanyID = s.SunbizCompanyID,
                    Defendant = s.Defendant,
                    FEIEIN= s.FEIEIN,
                    //DateFiled=s.DateFiled,
                    State = s.State,
                    Status = s.Status,
                    PrincipalAddress = s.PrincipalAddress,
                    MailingAddress = s.MailingAddress,
                    RegisteredagentNameAddress = s.RegisteredagentNameAddress,
                    url = s.url,
                    DocumnentNumber = s.DocumnentNumber,
                    DirectorName=s.DirectorName,
                    LinkedInID=s.LinkedInID,
                    TwitterID=s.TwitterID,


                }).ToList();

                return _VMSunbizDetailLpcases;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public List<SunbizDetail> GetPalmSunbizDetails(int SunbizCompanyID)
        {
            try
            {
                List<SunbizDetail> _VMSpokioList = new List<SunbizDetail>();
               
                _VMSpokioList = db.Sp_GetSunbizDetail_ButtonClick(SunbizCompanyID).AsEnumerable().Select(s => new SunbizDetail
                {
                    SunbizCompanyID = s.SunbizCompanyID,
                    Defendant = s.Defendant,
                    FEIEIN = s.FEIEIN,
                    //DateFiled=s.DateFiled,
                    State = s.State,
                    Status = s.Status,
                    PrincipalAddress = s.PrincipalAddress,
                    MailingAddress = s.MailingAddress,
                    RegisteredagentNameAddress = s.RegisteredagentNameAddress,
                    url = s.url,
                    DocumnentNumber = s.DocumnentNumber,
                }).ToList();
               
                return _VMSpokioList;
            }
            catch
            {
                return null;
            }
        }

        public List<BrowardCasesSunbizDetail> GetBrowardSunbizDetails(int SunbizDetailID)
        {
            try
            {
                List<BrowardCasesSunbizDetail> _VMSpokioList = new List<BrowardCasesSunbizDetail>();

                _VMSpokioList = db.Sp_ShowBroward_SunbizDetails(SunbizDetailID).AsEnumerable().Select(s => new BrowardCasesSunbizDetail
                {
                    SunbizDetailID = s.SunbizDetailID,
                    url = s.url,
                    FEIEIN = s.FEIEIN,
                    //DateFiled=s.DateFiled,
                    State = s.State,
                    Status = s.Status,
                    PrincipalAddress = s.PrincipalAddress,
                    MailingAddress = s.MailingAddress,
                    RegisteredAgentName = s.RegisteredAgentName,
                    CompanyName=s.CompanyName,
                   
                    DirectorName=s.DirectorName,
                    DocumentNumber = s.DocumentNumber,
                    BrowardCaseDetailID =s.BrowardCaseDetailID


                }).ToList();

                return _VMSpokioList;
            }
            catch(Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }
        
        public List<BrowardTaxIsueDetails> GetBrowardTaxIssueDetail(int SearchDefendantID)
        {
            try
            {
                List<BrowardTaxIsueDetails> _VMSpokioList = new List<BrowardTaxIsueDetails>();

                _VMSpokioList = db.Sp_GetAllTaxDetailData(SearchDefendantID).AsEnumerable().Select(s => new BrowardTaxIsueDetails
                {
                    TaxDetailID = s.TaxDetailID,
                    SearchDefendantID = s.SearchDefendantID,
                    SelectedYear = s.SelectedYear,
                   CertificateStatus=s.CertificateStatus,
                    AmountPaidDue = s.AmountPaidDue,
                    Totaltaxdue = s.Totaltaxdue,
                    Advertisednumberr = s.Advertisednumberr,
                    Faceamount = s.Faceamount,
                    Issueddate = s.Issueddate,
                    Expirationdate = s.Expirationdate,
                     Buyer = s.Buyer,
                    Interestrate = s.Interestrate,
                    OwnerName = s.OwnerName,
                    OwnerAddress=s.OwnerAddress,
                    TaxIssueStatus=s.TaxIssueStatus,
                    RecordLatest=s.RecordLatest


                }).ToList();

                return _VMSpokioList;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }


        public List<BrowardTaxIsueDetails> GetBrowardTaxIssueFinancial(int SearchDefendantID)
        {
            try
            {
                List<BrowardTaxIsueDetails> _VMSpokioList = new List<BrowardTaxIsueDetails>();

                _VMSpokioList = db.Sp_GetAllTaxDetailFinancial(SearchDefendantID).AsEnumerable().Select(s => new BrowardTaxIsueDetails
                {
                    TaxDetailID = s.TaxDetailID,
                    SearchDefendantID = s.SearchDefendantID,
                    SelectedYear = s.SelectedYear,
                    CertificateStatus = s.CertificateStatus,
                    AmountPaidDue = s.AmountPaidDue,
                    Totaltaxdue = s.Totaltaxdue,
                    Advertisednumberr = s.Advertisednumberr,
                    Faceamount = s.Faceamount,
                    Issueddate = s.Issueddate,
                    Expirationdate = s.Expirationdate,
                    Buyer = s.Buyer,
                    Interestrate = s.Interestrate,
                    OwnerName = s.OwnerName,
                    OwnerAddress = s.OwnerAddress,
                    TaxIssueStatus = s.TaxIssueStatus,
                    RecordLatest = s.RecordLatest


                }).ToList();

                return _VMSpokioList;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }
        public BrowardTaxIsueDetails GetYearBrowardTaxIssueDetail(int TaxDetailID)
        {
            try
            {
                BrowardTaxIsueDetails _VMSpokioList = new BrowardTaxIsueDetails();

                _VMSpokioList = db.Sp_GetAllTaxDetailDataYearWise(TaxDetailID).AsEnumerable().Select(s => new BrowardTaxIsueDetails
                {
                    TaxDetailID = s.TaxDetailID,
                    SearchDefendantID = s.SearchDefendantID,
                    SelectedYear = s.SelectedYear,
                    CertificateStatus = s.CertificateStatus,
                    AmountPaidDue = s.AmountPaidDue,
                    Totaltaxdue = s.Totaltaxdue,
                    Advertisednumberr = s.Advertisednumberr,
                    Faceamount = s.Faceamount,
                    Issueddate = s.Issueddate,
                    Expirationdate = s.Expirationdate,

                    Buyer = s.Buyer,
                    Interestrate = s.Interestrate,
                    OwnerName = s.OwnerName,
                    OwnerAddress = s.OwnerAddress,
                    TaxIssueStatus = s.TaxIssueStatus,
                    RecordLatest = s.RecordLatest


                }).FirstOrDefault();

                return _VMSpokioList;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }
        public SunbizDetail GetSunbizOwnerViewDetails(int SunbizCompanyID)
        {
            try
            {
                SunbizDetail _VMSpokioList = new SunbizDetail();
                _VMSpokioList = db.Sp_GetSunbizDetail_ButtonClick(SunbizCompanyID).AsEnumerable().Select(s => new SunbizDetail
                {
                    SunbizCompanyID = s.SunbizCompanyID,
                    Defendant = s.Defendant,
                    FEIEIN = s.FEIEIN,
                    //DateFiled=s.DateFiled,
                    State = s.State,
                    Status = s.Status,
                    PrincipalAddress = s.PrincipalAddress,
                    MailingAddress = s.MailingAddress,
                    RegisteredagentNameAddress = s.RegisteredagentNameAddress,
                    url = s.url,
                    DocumnentNumber = s.DocumnentNumber,
                }).FirstOrDefault();



                return _VMSpokioList;
            }
            catch
            {
                return null;
            }
        }

        public BrowardCasesSunbizDetail GetBrowardSunbizDetailsOwner(int SunbizDetailID)
        {
            try
            {
                BrowardCasesSunbizDetail _VMSpokioList = new BrowardCasesSunbizDetail();

                _VMSpokioList = db.Sp_ShowBroward_SunbizDetailsOwnerdetail(SunbizDetailID).AsEnumerable().Select(s => new BrowardCasesSunbizDetail
                {
                    SunbizDetailID = s.SunbizDetailID,
                    url = s.url,
                    FEIEIN = s.FEIEIN,
                    //DateFiled=s.DateFiled,
                    State = s.State,
                    Status = s.Status,
                    PrincipalAddress = s.PrincipalAddress,
                    MailingAddress = s.MailingAddress,
                    RegisteredAgentName = s.RegisteredAgentName,
                    CompanyName = s.CompanyName,

                    DirectorName = s.DirectorName,
                    DocumentNumber = s.DocumentNumber,
                    BrowardCaseDetailID = s.BrowardCaseDetailID


                }).FirstOrDefault();

                return _VMSpokioList;
            }
            catch
            {
                return null;
            }
        }
        
        public List<BrowardCasesLinkedIntable> GetBrowardLinkedInSiteDetails(int OthAddressID)
        {
            try
            {
                List<BrowardCasesLinkedIntable> _VMSpokioList = new List<BrowardCasesLinkedIntable>();

                _VMSpokioList = db.Sp_BrowardCase_LinkedInData(OthAddressID).AsEnumerable().Select(s => new BrowardCasesLinkedIntable
                {
                    BrowardLinkedInID = s.BrowardLinkedInID,
                    OwnerName=s.OwnerName,
                    OwnerCompany=s.OwnerCompany,
                    OwnerLocation=s.OwnerLocation,
                    OwnerContactinfoIink=s.OwnerContactinfoIink,
                    OwnerProfile=s.OwnerProfile,
                    OwnerSchooling=s.OwnerSchooling,
                    OthAddressID=s.OthAddressID

                }).ToList();

                return _VMSpokioList;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public List<BrowardCasesTwittertable> GetBrowardCaseTwitterDetails(int OthAddressID)
        {
            try
            {
                List<BrowardCasesTwittertable> _VMSpokioList = new List<BrowardCasesTwittertable>();

                _VMSpokioList = db.Sp_BrowardCase_TwitterDetail(OthAddressID).AsEnumerable().Select(s => new BrowardCasesTwittertable
                {
                    TwitterBrowardID = s.TwitterBrowardID,
                    TwitterBrowardLink = s.TwitterBrowardLink,
                    
                    OthAddressID = s.OthAddressID

                }).ToList();

                return _VMSpokioList;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public List<PalmBeachCasesTwittertable> GetPalmBeachCaseTwitterData(int OthAddressID)
        {
            try
            {
                List<PalmBeachCasesTwittertable> _VMSpokioList = new List<PalmBeachCasesTwittertable>();

                _VMSpokioList = db.Sp_PalmBeachCases_TwitterDetail(OthAddressID).AsEnumerable().Select(s => new PalmBeachCasesTwittertable
                {
                    TwitterPalmID = s.TwitterPalmID,
                    TwitterPalmLink = s.TwitterPalmLink,

                    OthAddressID = s.OthAddressID

                }).ToList();

                return _VMSpokioList;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }
        public List<PalmBeachCasesLinkedIntable> GetPalmBeachLinkedInSiteDetails(int PalmPCBDetailID)
        {
            try
            {
                List<PalmBeachCasesLinkedIntable> _VMSpokioList = new List<PalmBeachCasesLinkedIntable>();

                _VMSpokioList = db.Sp_PalmBeachCase_LinkedInDetails(PalmPCBDetailID).AsEnumerable().Select(s => new PalmBeachCasesLinkedIntable
                {
                    PalmLinkedInID = s.PalmLinkedInID,
                    OwnerName = s.OwnerName,
                    OwnerCompany = s.OwnerCompany,
                    OwnerLocation = s.OwnerLocation,
                    OwnerContactinfoIink = s.OwnerContactinfoIink,
                    OwnerProfile = s.OwnerProfile,
                    OwnerSchooling = s.OwnerSchooling,
                    PalmPCBDetailedID = s.PalmPCBDetailID

                }).ToList();

                return _VMSpokioList;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }


        public PalmBeachCasesLinkedIntable GetPalmBeachLinkedInSiteProfileData(int PalmPCBDetailID)
        {
            try
            {
                PalmBeachCasesLinkedIntable _VMSpokioList = new PalmBeachCasesLinkedIntable();

                _VMSpokioList = db.Sp_PalmBeachCase_LinkedInDetails(PalmPCBDetailID).AsEnumerable().Select(s => new PalmBeachCasesLinkedIntable
                {
                    PalmLinkedInID = s.PalmLinkedInID,
                    OwnerName = s.OwnerName,
                    OwnerCompany = s.OwnerCompany,
                    OwnerLocation = s.OwnerLocation,
                    OwnerContactinfoIink = s.OwnerContactinfoIink,
                    OwnerProfile = s.OwnerProfile,
                    OwnerSchooling = s.OwnerSchooling,
                    PalmPCBDetailedID = s.PalmPCBDetailID

                }).FirstOrDefault();

                return _VMSpokioList;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public List<tblUser> GetAdminUserPass(int RoleID)
        {
            List<tblUser> _VMSpokioList = new List<tblUser>();
           
            try
            {
              

                _VMSpokioList = db.Sp_GetAdminUserPassword(RoleID).AsEnumerable().Select(s => new tblUser
                {
                  UserId=s.UserId,
                  UserName=s.UserName

                }).ToList();

                return _VMSpokioList;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
           
        }



        public BrowardCasesLinkedIntable GetBrowardLinkedInSiteDetails_OwnerProfile(int OthAddressID)
        {
            try
            {
                BrowardCasesLinkedIntable _VMSpokioList = new BrowardCasesLinkedIntable();

                _VMSpokioList = db.Sp_BrowardCase_LinkedInData_OwnerDetail(OthAddressID).AsEnumerable().Select(s => new BrowardCasesLinkedIntable
                {
                    BrowardLinkedInID = s.BrowardLinkedInID,
                    OwnerName = s.OwnerName,
                    OwnerCompany = s.OwnerCompany,
                    OwnerLocation = s.OwnerLocation,
                    OwnerContactinfoIink = s.OwnerContactinfoIink,
                    OwnerProfile = s.OwnerProfile,
                    OwnerSchooling = s.OwnerSchooling,
                    OthAddressID=s.OthAddressID

                }).FirstOrDefault();

                return _VMSpokioList;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }


        public IndividualLinkedInData GetIndividualLinkedInSiteDetails_OwnerProfile(int OthAddressID)
        {
            try
            {
                IndividualLinkedInData _VMSpokioList = new IndividualLinkedInData();

                _VMSpokioList = db.Sp_IndividualCase_LinkedInData_OwnerDetail(OthAddressID).AsEnumerable().Select(s => new IndividualLinkedInData
                {
                    IndividualSearchID = s.IndividualSearchID,
                    OwnerName = s.OwnerName,
                    OwnerCompany = s.OwnerCompany,
                    OwnerLocation = s.OwnerLocation,
                    OwnerContactinfoIink = s.OwnerContactinfoIink,
                    OwnerProfile = s.OwnerProfile,
                    OwnerSchooling = s.OwnerSchooling,
                    SunbizCompanyID = s.SunbizCompanyID

                }).FirstOrDefault();

                return _VMSpokioList;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public List<IndividualLinkedInData> GetIndividualLinkedInSiteDetails(int SunbizCompanyID)
        {
            try
            {
                List<IndividualLinkedInData> _VMSpokioList = new List<IndividualLinkedInData>();
                
                _VMSpokioList = db.Sp_IndividualCase_LinkedInData(SunbizCompanyID).AsEnumerable().Select(s => new IndividualLinkedInData
                {
                    IndividualSearchID = s.IndividualSearchID,
                    OwnerName = s.OwnerName,
                    OwnerCompany = s.OwnerCompany,
                    OwnerLocation = s.OwnerLocation,
                    OwnerContactinfoIink = s.OwnerContactinfoIink,
                    OwnerProfile = s.OwnerProfile,
                    OwnerSchooling = s.OwnerSchooling,
                    SunbizCompanyID = s.SunbizCompanyID

                }).ToList();

                return _VMSpokioList;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public List<TwitterIndividualData> GetIndividualCaseTwitterDetails(int SunbizCompanyID)
        {
            try
            {
                List<TwitterIndividualData> _VMSpokioList = new List<TwitterIndividualData>();
                
                _VMSpokioList = db.Sp_IndividualCase_TwitterDetail(SunbizCompanyID).AsEnumerable().Select(s => new TwitterIndividualData
                {
                    TwitterIndividualID = s.TwitterIndividualID,
                    TwitterBrowardLink = s.TwitterBrowardLink,

                    SunbizCompanyID = s.SunbizCompanyID

                }).ToList();

                return _VMSpokioList;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        //public List<SunbizDetail> GetSunbizOwnerList(int SunbizCompanyID)
        //{
        //    try
        //    {
        //        List<SunbizDetail> _VMSpokioList = new List<SunbizDetail>();
        //        _VMSpokioList = db.Sp_GetSunbizDetail_ButtonClick().AsEnumerable().Select(s => new SunbizDetail
        //        {
        //            SunbizCompanyID = s.SunbizCompanyID,
        //            Defendant = s.Defendant,
        //            FEIEIN = s.FEIEIN,
        //            //DateFiled=s.DateFiled,
        //            State = s.State,
        //            Status = s.Status,
        //            PrincipalAddress = s.PrincipalAddress,
        //            MailingAddress = s.MailingAddress,
        //            RegisteredagentNameAddress = s.RegisteredagentNameAddress,
        //            url = s.url,
        //            DocumnentNumber = s.DocumnentNumber,

        //        }).ToList();
        //        return _VMSpokioList;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}


        public List<PalmBeachCasesSunbizDetail> GetPalmBeachSunbizDetails(int SunbizDetailID)
        {
            try
            {
                List<PalmBeachCasesSunbizDetail> _VMSpokioList = new List<PalmBeachCasesSunbizDetail>();

                _VMSpokioList = db.Sp_ShowPalmBeach_SunbizDetails(SunbizDetailID).AsEnumerable().Select(s => new PalmBeachCasesSunbizDetail
                {
                    SunbizDetailID = s.SunbizDetailID,
                    url = s.url,
                    FEIEIN = s.FEIEIN,
                    //DateFiled=s.DateFiled,
                    State = s.State,
                    Status = s.Status,
                    PrincipalAddress = s.PrincipalAddress,
                    MailingAddress = s.MailingAddress,
                    RegisteredAgentName = s.RegisteredAgentName,
                    CompanyName = s.CompanyName,

                    DirectorName = s.DirectorName,
                    DocumentNumber = s.DocumentNumber,
                    PalmPCBDetailedID = s.PalmPCBDetailedID


                }).ToList();

                return _VMSpokioList;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }



        public PalmBeachCasesSunbizDetail GetPalmBeachSunbizDetailsOwner(int SunbizDetailID)
        {
            try
            {
                PalmBeachCasesSunbizDetail _VMSpokioList = new PalmBeachCasesSunbizDetail();

                _VMSpokioList = db.Sp_ShowPalBeach_SunbizDetailsOwnerdetail(SunbizDetailID).AsEnumerable().Select(s => new PalmBeachCasesSunbizDetail
                {
                    SunbizDetailID = s.SunbizDetailID,
                    url = s.url,
                    FEIEIN = s.FEIEIN,
                    //DateFiled=s.DateFiled,
                    State = s.State,
                    Status = s.Status,
                    PrincipalAddress = s.PrincipalAddress,
                    MailingAddress = s.MailingAddress,
                    RegisteredAgentName = s.RegisteredAgentName,
                    CompanyName = s.CompanyName,

                    DirectorName = s.DirectorName,
                    DocumentNumber = s.DocumentNumber,
                    PalmPCBDetailedID = s.PalmPCBDetailedID


                }).FirstOrDefault();

                return _VMSpokioList;
            }
            catch
            {
                return null;
            }
        }



        public string UserChangePassword(int Userid, string NewPassword)
        {
            retry:
            try
            {

                db.Sp_ChangePasswordUpdate(Userid, NewPassword);
            }
            catch (Exception ex)
            {
                if (ex.GetType().FullName == "System.Data.EntityException" || ex.GetType().FullName == "System.Data.SqlClient.SqlException")
                {
                    goto retry;
                }
                throw;
            }
            return "";

        }

        public string Addadminuser(string username, string useremail, string password, string phoneno, string county, string zipcode, string address,int RoleID)
        {
            retry:
            try
            {

                db.Sp_Savedetail_Adminuser(username, useremail, password, phoneno, county, zipcode, address, RoleID);
            }
            catch (Exception ex)
            {
                if (ex.GetType().FullName == "System.Data.EntityException" || ex.GetType().FullName == "System.Data.SqlClient.SqlException")
                {
                    goto retry;
                }
                throw;
            }
            return "";

        }

    }




    public class PalmBeachLpcases
    {
        public long PalmBeachLPCaseID { get; set; }
        public string PlantiffName { get; set; }
        public string Name { get; set; }
        public string CrossName { get; set; }
        public string DetailsByUrl { get; set; }
        public Nullable<System.DateTime> CaseDate { get; set; }
        public string CaseType { get; set; }
        public Nullable<int> Book { get; set; }
        public Nullable<int> PageNumber { get; set; }
        public string CFN { get; set; }
        public string Legal { get; set; }
        public Nullable<bool> IsPDFDownloaded { get; set; }
        public Nullable<bool> IsPDFRead { get; set; }
        public Nullable<bool> IsGetDetails { get; set; }
        public Nullable<bool> IsCorrectAddress { get; set; }
        public Nullable<bool> ISCorrectPalmPCB { get; set; }

        public string CorrectPamAddress { get; set; }
        public Nullable<bool> IsPCBDetails { get; set; }
        public string PCNumber { get; set; }

        public Nullable<long> PCBPalmDetailsID { get; set; }
        public string LegalDesc { get; set; }
        public string ImprovementVal { get; set; }
        public string LandValue { get; set; }
        public string MarketValue { get; set; }
        public string SaleDate { get; set; }
        public string Price { get; set; }
        public string BookPage { get; set; }
        public string SaleType { get; set; }
        public string Owner { get; set; }
        public string AssesedVal { get; set; }
        public string ExemptionAmt { get; set; }
        public string TaxableValue { get; set; }
        public string AdValorem { get; set; }
        public string NonAdValorem { get; set; }
        public string TotalTaxes { get; set; }

        public string EstMarketValue { get; set; }
        public string LocationAddress { get; set; }
        public string MailingAddress { get; set; }

        public string OwedOnMortgage { get; set; }
        public string EstOwnerEquity { get; set; }
        public string SalesPriceSqFt { get; set; }
        public string AssessedMarketSqFt { get; set; }
        public bool IsOtherPalm { get; set; }
        public bool cntPlaintiffs { get; set; }
        public bool IsspokioDetails { get; set; }
        public Nullable<int> issunbizdetailedID { get; set; }
        public Nullable<int> IsPalmLinkedIn { get; set; }
        public Nullable<int> IsTwitterPalm { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string PropertyType { get; set; }
        public int? RedfinPrice { get; set; }
        public int? ZillowPrice { get; set; }
        
        public string RedfinPricedetail { get; set; }
        public string ZillowPricedetail { get; set; }
        public string RedfinUrl { get; set; }
        public string zillowUrl { get; set; }
        public string LoopnetPrice { get; set; }
        public string LoopnetUrl { get; set; }
         public string NonpaymentStatus { get; set; }

        public string HOARedfinDue { get; set; }
        public string Rentzestimate { get; set; }
        public string Refipayment { get; set; }
        public string PropertyOwner { get; set; }

        public string Redfindays { get; set; }
        public string Zillowdays { get; set; }
    }
    public class PCBPalmDetail
    {
        public long PCBPalmDetailsID { get; set; }
        public Nullable<long> PalmBeachLPCaseID { get; set; }
        public string PCNumber { get; set; }
        public string OfficialBook { get; set; }
        public string SaleDate { get; set; }
        public string Subdivision { get; set; }
        public string LegalDesc { get; set; }
        public string Units { get; set; }
        public string TotalSqft { get; set; }
        public string Acres { get; set; }
        public string Usecode { get; set; }
        public string Zoning { get; set; }
        public string MailingAddress { get; set; }
        public string LocationAddress { get; set; }
        public List<tblPCBAppraisal> PCBAppraisal { get; set; }
        public List<tblPCBSaleInfo> PCBSaleInfo { get; set; }
        public List<tblPCBTaxAssesed> PCBTaxAssesed { get; set; }
        public List<tblPCBTotalTax> PCBTotalTax { get; set; }

        public string Plaintiff { get; set; }
        public string Defendant { get; set; }
        public string SalePrice { get; set; }
        public string MarketValue { get; set; }
        public string Exemption { get; set; }
        public string Tax { get; set; }
        public string OwedOnMortgage { get; set; }
        public string EstOwnerEquity { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CFN { get; set; }
        public string CaseType { get; set; }
        public string DetailsByUrl { get; set; }
        public string EstMarketValue { get; set; }
        public string PropertyType { get; set; }
    }



    public class SunbizDetail
    {
        public int SunbizCompanyID { get; set; }
        public string Defendant { get; set; }
        public string FEIEIN { get; set; }
        //public string DateFiled { get; set; }
        public string State { get; set; }
        public string Status { get; set; }
        public string PrincipalAddress { get; set; }
        public string MailingAddress { get; set; }
        public string RegisteredagentNameAddress { get; set; }
        public string url { get; set; }
        public string DocumnentNumber { get; set; }
        public string DirectorName { get; set; }

        public int? LinkedInID { get; set; }
        public int? TwitterID { get; set; }
    }



    public class BrowardCasesSunbizDetail
    {
        public int SunbizDetailID { get; set; }
        public string url { get; set; }
        public string FEIEIN { get; set; }


        //public string DateFiled { get; set; }
        public string State { get; set; }
        public string Status { get; set; }
        public string PrincipalAddress { get; set; }
        public string MailingAddress { get; set; }
        public string RegisteredAgentName { get; set; }
        public string CompanyName { get; set; }

        public string DirectorName { get; set; }
        public string DocumentNumber { get; set; }
        public int BrowardCaseDetailID { get; set; }
    }



    public class BrowardCasesLinkedIntable
    {

        public int BrowardLinkedInID { get; set; }
        public string OwnerName { get; set; }
        public string OwnerProfile { get; set; }


        //public string DateFiled { get; set; }
        public string OwnerLocation { get; set; }
        public string OwnerCompany { get; set; }
        public string OwnerSchooling { get; set; }
        public string OwnerContactinfoIink { get; set; }
        public int OthAddressID { get; set; }
      

    }

    public class IndividualLinkedInData
    {
        public int IndividualSearchID { get; set; }
        public string OwnerName { get; set; }
        public string OwnerProfile { get; set; }


        //public string DateFiled { get; set; }
        public string OwnerLocation { get; set; }
        public string OwnerCompany { get; set; }
        public string OwnerSchooling { get; set; }
        public string OwnerContactinfoIink { get; set; }
        public int SunbizCompanyID { get; set; }
    }
    public class PalmBeachCasesSunbizDetail
    {
        public int SunbizDetailID { get; set; }
        public string url { get; set; }
        public string FEIEIN { get; set; }


        //public string DateFiled { get; set; }
        public string State { get; set; }
        public string Status { get; set; }
        public string PrincipalAddress { get; set; }
        public string MailingAddress { get; set; }
        public string RegisteredAgentName { get; set; }
        public string CompanyName { get; set; }

        public string DirectorName { get; set; }
        public string DocumentNumber { get; set; }
        public int PalmPCBDetailedID { get; set; }
    }


    public class PalmBeachCasesLinkedIntable
    {

        public int PalmLinkedInID { get; set; }
        public string OwnerName { get; set; }
        public string OwnerProfile { get; set; }


        //public string DateFiled { get; set; }
        public string OwnerLocation { get; set; }
        public string OwnerCompany { get; set; }
        public string OwnerSchooling { get; set; }
        public string OwnerContactinfoIink { get; set; }
        public int PalmPCBDetailedID { get; set; }


    }


    public class BrowardCasesTwittertable
    {

        public int TwitterBrowardID { get; set; }

        public string TwitterBrowardLink { get; set; }
        public int OthAddressID { get; set; }


    }

    public class TwitterIndividualData
    {
        public int TwitterIndividualID { get; set; }

        public string TwitterBrowardLink { get; set; }
        public int SunbizCompanyID { get; set; }
    }

    public class PalmBeachCasesTwittertable
    {

        public int TwitterPalmID { get; set; }

        public string TwitterPalmLink { get; set; }
        public int OthAddressID { get; set; }


    }



    public class BrowardTaxIsueDetails
    {
        public int TaxDetailID { get; set; }
        public int SearchDefendantID { get; set; }
        public string SelectedYear { get; set; }
        public string AmountPaidDue { get; set; }
        public string CertificateStatus { get; set; }

        //public string DateFiled { get; set; }
        public string Totaltaxdue { get; set; }
        public string Advertisednumberr { get; set; }
        public string Faceamount { get; set; }
        public string Issueddate { get; set; }
        public string Expirationdate { get; set; }
        public string Buyer { get; set; }

        public string Interestrate { get; set; }
        public string OwnerName { get; set; }
        public string OwnerAddress { get; set; }
        public string TaxIssueStatus { get; set; }
        public string RecordLatest { get; set; }
    }
}
