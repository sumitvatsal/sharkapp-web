﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
//using OpenQA.Selenium.Alert;
using SikuliModule;
using System.Windows.Forms;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using System.Data;
using Microsoft.Office.Interop.Excel;
using System.Net;
using System.Text;
using System.IO;
using SharkAppWeb.Models;
using SharkAppWeb.ViewModel;
using System.Collections;
using iTextSharp.text.pdf;
using Ghostscript.NET;
using MODI;
using System.Diagnostics;
using SharkAppWeb.Common;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Globalization;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Configuration;



namespace SharkAppWeb.Controllers
{
    public class HomeController : Controller
    {
        int _PageNumber;
        BrowardclerkEntities db = new BrowardclerkEntities();
        VMCase _vmcase = new VMCase();
        VMPalmCase _vmPalmCase = new VMPalmCase();
       
        VMBulkEmails _vmBulkEmails = new VMBulkEmails();
        IEBrowser browser;
        AutoResetEvent resultEvent = new AutoResetEvent(false);
        ComparePropertyMSAccess _CompPrMSAccess = new ComparePropertyMSAccess();

        public ArrayList myImages = new ArrayList();
        public ArrayList PalmBeachAddress = new ArrayList();
        public ArrayList _CaseNumber = new ArrayList();
        public ArrayList _Casehtml = new ArrayList();
        public ArrayList TextFormate = new ArrayList();
        public ArrayList _OriginalPdf = new ArrayList();
        public ArrayList _CompareFolioToID = new ArrayList();
        public ArrayList _ComparePropertyAddress = new ArrayList();
        public ArrayList _DocResultURL = new ArrayList();

       

        #region Business Search

        [STAThreadAttribute]

        public ActionResult CaseView()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                var filename = Server.MapPath("~/ExcelFiles/TransCapitalRecords.ods");
                Microsoft.Office.Interop.Excel.Application xlApp;
                Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
                Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
                var missing = System.Reflection.Missing.Value;

                xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlWorkBook = xlApp.Workbooks.Open(filename, false, true, missing, missing, missing, true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, '\t', false, false, 0, false, true, 0);
                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                Microsoft.Office.Interop.Excel.Range xlRange = xlWorkSheet.UsedRange;
                Array myValues = (Array)xlRange.Cells.Value2;

                int vertical = myValues.GetLength(0);
                int horizontal = myValues.GetLength(1);

                System.Data.DataTable dt = new System.Data.DataTable();

                // must start with index = 1
                // get header information
                for (int i = 1; i <= horizontal; i++)
                {
                    dt.Columns.Add(new DataColumn(myValues.GetValue(1, i).ToString()));
                }

                // Get the row information
                for (int a = 2; a <= vertical; a++)
                {
                    object[] poop = new object[horizontal];
                    for (int b = 1; b <= horizontal; b++)
                    {
                        poop[b - 1] = myValues.GetValue(a, b);
                    }
                    DataRow row = dt.NewRow();
                    row.ItemArray = poop;
                    dt.Rows.Add(row);
                }
                ViewBag.CaseTable = dt;
                // assign table to default data grid view

                xlWorkBook.Close(true, missing, missing);
                xlApp.Quit();

                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);

                return PartialView("_CaseView", dt);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult ConvertByCaseNumber(string CaseNumber)
        {

            try
            {
                if (System.Web.HttpContext.Current.Session["UserId"] != null)
                {
                    var sourcePath = "C:/PDfFiles/";// Server.MapPath("~/PDfFiles/");
                    var Result = "";
                    if (Session["FolderName"] != null && Session["FolderName"].ToString() != "")
                    {

                        Result = ConvertPdfByCaseNumber(CaseNumber, sourcePath, Result, null);
                    }
                    else
                    {
                        return Json("Session Expired !", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return Json("Error ! please try again.", JsonRequestBehavior.AllowGet);
            }
            return PartialView("_FilterAddress", _vmcase);
            //return Json(_lst.ToList(), JsonRequestBehavior.AllowGet);
        }

        private string ConvertPdfByCaseNumber(string CaseNumber, string sourcePath, string Result, int? SrchDefendantID)
        {
            string Foldername = Session["FolderName"].ToString();
            DateTime _SearchedDate = Convert.ToDateTime(Session["Searcheddate"].ToString());
            int CaseNumberID = Convert.ToInt32(Session["CaseNumberID"].ToString());
            _vmcase.searchPDFs = new List<SearchedPdf>();
            var _SearchPdfCheck = _vmcase.GetsearchPDFResult(CaseNumber, _SearchedDate, CaseNumberID);
            if (_SearchPdfCheck != null && _SearchPdfCheck.Count > 0)
            {
                _vmcase.searchPDFs = _SearchPdfCheck;
            }
            else
            {
                Result = SaveAndReadPdfText(CaseNumber, sourcePath, Result, SrchDefendantID, Foldername);
            }
            return Result;
        }


        private string SaveAndReadPdfDoc(string CaseNumber, string sourcePath, string Result, int? SrchDefendantID, string Foldername)
        {
            if (System.IO.Directory.Exists(sourcePath))
            {
                string[] _Dir = System.IO.Directory.GetDirectories(sourcePath);

                var FilesPath = sourcePath + Foldername;
                bool _result = _Dir.Contains(FilesPath.Trim());
                if (_result == true)
                {
                    string dirName = new DirectoryInfo(FilesPath).Name;
                    if (dirName == Foldername)
                    {
                        string[] files11 = System.IO.Directory.GetFiles(FilesPath);
                        string[] filteredFiles = files11.Where(w => w.Contains(CaseNumber)).ToArray();
                        string directoryPath = "C:\\PdfToImage\\" + Foldername;
                        //string directoryPath = Server.MapPath("~/PdfToImage/" + Foldername + "");
                        if (!System.IO.Directory.Exists(directoryPath))
                        {
                            Directory.CreateDirectory(directoryPath);
                        }

                        foreach (var _file in filteredFiles)
                        {
                            if (System.IO.Path.GetExtension(_file) == ".PDF")
                            {
                                var fileName = System.IO.Path.GetFileName(_file);
                                string FileCaseNo = fileName.Split('_')[0];
                                string SplitCaseNumber = FileCaseNo.Split('-')[0];
                                if (SplitCaseNumber.Trim() == CaseNumber)
                                {

                                    var PdfFilePath = "C:/PDfFiles/" + Foldername + "/" + fileName + "";// Server.MapPath("~/PDfFiles/" + Foldername + "/" + fileName + "");
                                    Result = "Success";
                                    myImages.Clear();
                                    ConvertPdftoImage(PdfFilePath, Foldername);
                                    //string tt = "";
                                    SearchedPdf _searchedPDF = new SearchedPdf();
                                    _searchedPDF = SavePDFResultDoc(CaseNumber, Foldername);

                                    if (_searchedPDF.pdfAddresslist.Count() > 0)
                                    {
                                        _vmcase.searchPDFs.Add(_searchedPDF);
                                    }

                                }
                            }
                            else
                            {
                                Result = "No eSummons Find In This Case";
                            }
                        }
                        AddAddressResultForSave(_vmcase, SrchDefendantID);

                        //SearchedPdf _searchedPDF = SavePDFResult(CaseNumber, Foldername);


                    }
                }
            }
            return Result;
        }
        private string SaveAndReadPdfText(string CaseNumber, string sourcePath, string Result, int? SrchDefendantID, string Foldername)
        {
            if (System.IO.Directory.Exists(sourcePath))
            {
                string[] _Dir = System.IO.Directory.GetDirectories(sourcePath);

                var FilesPath = sourcePath + Foldername;
                bool _result = _Dir.Contains(FilesPath.Trim());
                if (_result == true)
                {
                    string dirName = new DirectoryInfo(FilesPath).Name;
                    if (dirName == Foldername)
                    {
                        string[] files11 = System.IO.Directory.GetFiles(FilesPath);
                        string[] filteredFiles = files11.Where(w => w.Contains(CaseNumber)).ToArray();
                        string directoryPath = "C:\\PdfToImage\\" + Foldername;
                        //string directoryPath = Server.MapPath("~/PdfToImage/" + Foldername + "");
                        if (!System.IO.Directory.Exists(directoryPath))
                        {
                            Directory.CreateDirectory(directoryPath);
                        }

                        foreach (var _file in filteredFiles)
                        {
                            if (System.IO.Path.GetExtension(_file) == ".PDF")
                            {
                                var fileName = System.IO.Path.GetFileName(_file);
                                string FileCaseNo = fileName.Split('_')[0];
                                string SplitCaseNumber = FileCaseNo.Split('-')[0];
                                if (SplitCaseNumber.Trim() == CaseNumber)
                                {
                                    var PdfFilePath = "C:/PDfFiles/" + Foldername + "/" + fileName + "";// Server.MapPath("~/PDfFiles/" + Foldername + "/" + fileName + "");
                                    Result = "Success";
                                    myImages.Clear();
                                    ConvertPdftoImage(PdfFilePath, Foldername);
                                    //string tt = "";
                                    SearchedPdf _searchedPDF = new SearchedPdf();
                                    _searchedPDF = SavePDFResult(CaseNumber, Foldername);

                                    if (_searchedPDF.pdfAddresslist.Count() > 0)
                                    {
                                        _vmcase.searchPDFs.Add(_searchedPDF);
                                    }

                                }
                            }
                            else
                            {
                                Result = "No eSummons Find In This Case";
                            }
                        }
                        AddAddressResultForSave(_vmcase, SrchDefendantID);

                        //SearchedPdf _searchedPDF = SavePDFResult(CaseNumber, Foldername);


                    }
                }
            }
            return Result;
        }



        public ActionResult BCPAAddressByCase()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                return View();
            }
            return RedirectToAction("Login", "Home");
        }
        public ActionResult CaseNumberSearch()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<tblCaseNumberSearch> _tblCaseNumberSearch = db.tblCaseNumberSearches.OrderByDescending(x => x.CaseNumberID).ToList();
                return PartialView("_ResultBYCase", _tblCaseNumberSearch);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult GetSaveCaseAddress(DateTime Searchdate)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                DateTime Searchdate1 = Searchdate.Date;
                List<tblCaseNumberSearch> _tblCaseNumberSearch = db.tblCaseNumberSearches.AsEnumerable().Where(x => x.CaseSearchDate.Value.ToShortDateString() == Searchdate1.ToShortDateString()).OrderByDescending(x => x.CaseNumberID).ToList();
                foreach (var item in _tblCaseNumberSearch)
                {
                    int CaseNumberID = item.CaseNumberID;
                    string Result = "";
                    string sourcePath = "C:/PDfFiles/";
                    string _CaseNumber = GetPdfDetailsFolder(CaseNumberID);
                    Result = ConvertPdfByCaseNumber(_CaseNumber, sourcePath, Result, null);
                }
                List<tblCaseNumberSearch> _CaseNumberSearch = _vmcase.GetCaseNumberHasAddress();
                _vmcase.searchPDFs = new List<SearchedPdf>();

                foreach (var item in _CaseNumberSearch)
                {

                    int CaseNumberID = Convert.ToInt32(item.CaseNumberID);
                    var _SearchPdfCheck = _vmcase.GetPDFResultList(CaseNumberID);

                    if (_SearchPdfCheck != null && _SearchPdfCheck.Count > 0)
                    {
                        foreach (var pdfcheck in _SearchPdfCheck)
                        {
                            _vmcase.searchPDFs.Add(pdfcheck);
                        }

                    }
                }
                // return PartialView("_ResultBYCase", _CaseNumberSearch);
                return PartialView("_FilterAddress", _vmcase);
            }
            return RedirectToAction("Login", "Home");
        }

        private SearchedPdf SavePDFResultDoc(string CaseNumber, string Foldername)
        {
            try
            {
                string _OcrText = "";
                SearchedPdf _searchedPDF = new SearchedPdf();
                _searchedPDF.pdfAddresslist = new List<PDFAddress>();
                foreach (var _Imagename in myImages)
                {

                    string fImagename = "C:\\PdfToImage\\" + Foldername + "\\" + _Imagename;
                    //string fImagename = System.IO.Path.Combine(Server.MapPath("~/PdfToImage/" + Foldername + "/" + _Imagename));
                    string extractText = this.ExtractTextFromImage(fImagename);
                    _OcrText = extractText.Replace(Environment.NewLine, "<br />");
                    TextFormate.Add(_OcrText);

                    string[] numbers = Regex.Split(extractText, @"\D+");

                    //int cntDefendant = 0, cntLocated = 0, cntTowit = 0, cntondefendant = 0, cntCopyFurnishedTo = 0, cntprincipal = 0, cntSERVER = 0, cntaddressis = 0;

                    string _PdfName = _Imagename.ToString();
                    int CaseNumberID = Convert.ToInt32(Session["CaseNumberID"].ToString());
                    _searchedPDF.Pdfname = _PdfName.Split('_')[0];
                    _searchedPDF.SerchedDate = Convert.ToDateTime(Session["Searcheddate"].ToString());
                    _searchedPDF.CaseNumber = CaseNumber;
                    _searchedPDF.CaseNumberID = CaseNumberID;
                    foreach (string value1 in numbers)
                    {
                        Regex rex = new Regex(@"\b[0-9]{5}(?:-[0-9]{4})?\b");

                        if (rex.IsMatch(value1) == true)
                        {
                            PDFAddress _Pdfadd = new PDFAddress();

                            var filtrtr = value1.ToString();
                            var txtfilter = extractText.Split(new string[] { filtrtr }, StringSplitOptions.None);

                            //---------------- Start Get Any Type Address-----------//

                            string mystring = txtfilter[0].ToString();
                            string newAddress = mystring.Substring(Math.Max(0, mystring.Length - 70));
                            _Pdfadd.AddressType = "New Address:";
                            _Pdfadd.Pdfaddress = newAddress + " " + value1;
                            _searchedPDF.pdfAddresslist.Add(_Pdfadd);

                            //----------------End Get Any Type Address-----------//
                        }

                    }

                }

                return _searchedPDF;
            }
            catch
            {
                return null;
            }
        }
        private SearchedPdf SavePDFResult(string CaseNumber, string Foldername)
        {
            try
            {
                string _OcrText = "";
                SearchedPdf _searchedPDF = new SearchedPdf();
                _searchedPDF.pdfAddresslist = new List<PDFAddress>();
                foreach (var _Imagename in myImages)
                {

                    string fImagename = "C:\\PdfToImage\\" + Foldername + "\\" + _Imagename;
                    //string fImagename = System.IO.Path.Combine(Server.MapPath("~/PdfToImage/" + Foldername + "/" + _Imagename));
                    string extractText = this.ExtractTextFromImage(fImagename);
                    _OcrText = extractText.Replace(Environment.NewLine, "<br />");
                    TextFormate.Add(_OcrText);

                    string[] numbers = Regex.Split(extractText, @"\D+");

                    int cntDefendant = 0, cntLocated = 0, cntTowit = 0, cntondefendant = 0, cntCopyFurnishedTo = 0, cntprincipal = 0, cntSERVER = 0, cntaddressis = 0;

                    string _PdfName = _Imagename.ToString();
                    int CaseNumberID = Convert.ToInt32(Session["CaseNumberID"].ToString());
                    _searchedPDF.Pdfname = _PdfName.Split('_')[0];
                    _searchedPDF.SerchedDate = Convert.ToDateTime(Session["Searcheddate"].ToString());
                    _searchedPDF.CaseNumber = CaseNumber;
                    _searchedPDF.CaseNumberID = CaseNumberID;
                    foreach (string value1 in numbers)
                    {
                        Regex rex = new Regex(@"\b[0-9]{5}(?:-[0-9]{4})?\b");

                        if (rex.IsMatch(value1) == true)
                        {
                            PDFAddress _Pdfadd = new PDFAddress();

                            var filtrtr = value1.ToString();
                            var txtfilter = extractText.Split(new string[] { filtrtr }, StringSplitOptions.None);

                            //---------------- Start Get Any Type Address-----------//

                            string mystring = txtfilter[0].ToString();
                            string newAddress = mystring.Substring(Math.Max(0, mystring.Length - 60));
                            _Pdfadd.AddressType = "New Address:";
                            _Pdfadd.Pdfaddress = newAddress + " " + value1;
                            _searchedPDF.pdfAddresslist.Add(_Pdfadd);

                            //----------------End Get Any Type Address-----------//


                            if (txtfilter[0].Contains("TO DEFENDANT:") || txtfilter[0].Contains("TO DEFENDANT."))
                            {
                                if (cntDefendant == 0)
                                {
                                    var DEFENDANT = txtfilter[0].Split(new string[] { "DEFENDANT" }, StringSplitOptions.None);
                                    int _oncnt = DEFENDANT[1].Count();
                                    var defeAdd = "";
                                    if (_oncnt > 50)
                                    {
                                        var concatedword = DEFENDANT[1] + " " + value1;
                                        int index2 = concatedword.LastIndexOf(value1);
                                        if (index2 != -1)
                                        {
                                            var _item = concatedword.Substring(concatedword.Length - (concatedword.Length - 1));
                                            defeAdd = _item;
                                        }
                                    }
                                    else
                                    {
                                        defeAdd = DEFENDANT[1] + " " + value1;
                                    }
                                    _Pdfadd.AddressType = "TO DEFENDANT:";
                                    _Pdfadd.Pdfaddress = defeAdd;
                                    _searchedPDF.pdfAddresslist.Add(_Pdfadd);
                                }
                                cntDefendant++;
                            }
                            if (txtfilter[0].Contains("on Defendant:") || txtfilter[0].Contains("on Defendant."))
                            {
                                if (cntondefendant == 0)
                                {
                                    var Ondefendant = txtfilter[0].Split(new string[] { "on Defendant:" }, StringSplitOptions.None);
                                    int _oncnt = Ondefendant[1].Count();
                                    var OndefendantAdd = "";
                                    if (_oncnt > 50)
                                    {
                                        var concatedword = Ondefendant[1] + " " + value1;
                                        int index2 = concatedword.LastIndexOf(value1);
                                        if (index2 != -1)
                                        {
                                            var _item = concatedword.Substring(concatedword.Length - (concatedword.Length - 1));
                                            OndefendantAdd = _item;
                                        }
                                    }
                                    else
                                    {
                                        OndefendantAdd = Ondefendant[1] + " " + value1;
                                    }
                                    _Pdfadd.AddressType = "On Defendant.";
                                    var _OndefendantAdd = Regex.Replace(OndefendantAdd, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
                                    _Pdfadd.Pdfaddress = _OndefendantAdd;
                                    _searchedPDF.pdfAddresslist.Add(_Pdfadd);
                                }
                                cntondefendant++;
                            }
                            if (txtfilter[0].ToLower().Contains("a/k/a") || txtfilter[0].ToLower().Contains("AIKJA"))
                            {
                                if (cntSERVER == 0)
                                {
                                    string[] Ondefendant;
                                    if (txtfilter[0].ToString().Contains("AIKJA"))
                                    {
                                        Ondefendant = txtfilter[0].Split(new string[] { "AIKJA" }, StringSplitOptions.None);
                                    }
                                    else
                                    {
                                        Ondefendant = txtfilter[0].ToLower().Split(new string[] { "a/k/a" }, StringSplitOptions.None);
                                    }
                                    int _oncnt = Ondefendant[1].Count();
                                    var OndefendantAdd = "";
                                    if (_oncnt > 50)
                                    {
                                        var concatedword = Ondefendant[1] + " " + value1;
                                        int index2 = concatedword.LastIndexOf(value1);
                                        if (index2 != -1)
                                        {
                                            var _item = concatedword.Substring(concatedword.Length - (concatedword.Length - 1));
                                            OndefendantAdd = _item;
                                        }
                                    }
                                    else
                                    {
                                        OndefendantAdd = Ondefendant[1] + " " + value1;
                                    }
                                    _Pdfadd.AddressType = "On Defendant.";
                                    var _OndefendantAdd = Regex.Replace(OndefendantAdd, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
                                    _Pdfadd.Pdfaddress = _OndefendantAdd;
                                    _searchedPDF.pdfAddresslist.Add(_Pdfadd);
                                }
                                cntSERVER++;
                            }
                            if (txtfilter[0].Contains("SERVE:"))
                            {
                                if (cntSERVER == 0)
                                {
                                    var Ondefendant = txtfilter[0].Split(new string[] { "SERVE:" }, StringSplitOptions.None);
                                    int _oncnt = Ondefendant[1].Count();
                                    var OndefendantAdd = "";
                                    if (_oncnt > 50)
                                    {
                                        var concatedword = Ondefendant[1] + " " + value1;
                                        int index2 = concatedword.LastIndexOf(value1);
                                        if (index2 != -1)
                                        {
                                            var _item = concatedword.Substring(concatedword.Length - (concatedword.Length - 1));
                                            OndefendantAdd = _item;
                                        }
                                    }
                                    else
                                    {
                                        OndefendantAdd = Ondefendant[1] + " " + value1;
                                    }
                                    _Pdfadd.AddressType = "On Defendant.";
                                    var _OndefendantAdd = Regex.Replace(OndefendantAdd, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
                                    _Pdfadd.Pdfaddress = _OndefendantAdd;
                                    _searchedPDF.pdfAddresslist.Add(_Pdfadd);
                                }
                                cntSERVER++;
                            }
                            if (txtfilter[0].Contains("address is:"))
                            {
                                if (cntaddressis == 0)
                                {
                                    var Ondefendant = txtfilter[0].Split(new string[] { "address is:" }, StringSplitOptions.None);
                                    int _oncnt = Ondefendant[1].Count();
                                    var OndefendantAdd = "";
                                    if (_oncnt > 50)
                                    {
                                        var concatedword = Ondefendant[1] + " " + value1;
                                        int index2 = concatedword.LastIndexOf(value1);
                                        if (index2 != -1)
                                        {
                                            var _item = concatedword.Substring(concatedword.Length - (concatedword.Length - 1));
                                            OndefendantAdd = _item;
                                        }
                                    }
                                    else
                                    {
                                        OndefendantAdd = Ondefendant[1] + " " + value1;
                                    }
                                    _Pdfadd.AddressType = "On Defendant.";
                                    var _OndefendantAdd = Regex.Replace(OndefendantAdd, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
                                    _Pdfadd.Pdfaddress = _OndefendantAdd;
                                    _searchedPDF.pdfAddresslist.Add(_Pdfadd);
                                }
                                cntaddressis++;
                            }
                            if (txtfilter[0].Contains("located at:"))
                            {
                                if (cntLocated == 0)
                                {
                                    var located = txtfilter[0].Split(new string[] { "located at:" }, StringSplitOptions.None);
                                    int _oncnt = located[1].Count();
                                    var locatedAdd = "";
                                    if (_oncnt > 50)
                                    {
                                        var concatedword = located[1] + " " + value1;
                                        int index2 = concatedword.LastIndexOf(value1);
                                        if (index2 != -1)
                                        {
                                            var _item = concatedword.Substring(concatedword.Length - (concatedword.Length - 1));
                                            locatedAdd = _item;
                                        }
                                    }
                                    else
                                    {
                                        locatedAdd = located[1] + " " + value1;
                                    }
                                    _Pdfadd.AddressType = "Located at:";
                                    _Pdfadd.Pdfaddress = locatedAdd;
                                    _searchedPDF.pdfAddresslist.Add(_Pdfadd);
                                }
                                cntLocated++;
                            }
                            if (txtfilter[0].Contains("to-wit:") || txtfilter[0].Contains("to- wit:") || txtfilter[0].Contains("to wit:") || txtfilter[0].Contains("wit:"))
                            {
                                if (cntTowit == 0)
                                {
                                    var Towit = txtfilter[0].Split(new string[] { "wit:" }, StringSplitOptions.None);
                                    int _oncnt = Towit[1].Count();
                                    var toaitAdd = "";
                                    if (_oncnt > 50)
                                    {
                                        var concatedword = Towit[1] + " " + value1;
                                        int index2 = concatedword.LastIndexOf(value1);
                                        if (index2 != -1)
                                        {
                                            var _item = concatedword.Substring(concatedword.Length - (concatedword.Length - 1));
                                            toaitAdd = _item;
                                        }
                                    }
                                    else
                                    {
                                        toaitAdd = Towit[1] + " " + value1;
                                    }
                                    _Pdfadd.AddressType = "TO-wit:";
                                    _Pdfadd.Pdfaddress = toaitAdd;
                                    _searchedPDF.pdfAddresslist.Add(_Pdfadd);
                                }
                                cntTowit++;
                            }
                            if (txtfilter[0].Contains("COPIES FURNISHED TO:"))
                            {
                                if (cntCopyFurnishedTo == 0)
                                {
                                    var CopyFurnishedTo = txtfilter[0].Split(new string[] { "COPIES FURNISHED TO:" }, StringSplitOptions.None);
                                    int _Copycnt = CopyFurnishedTo[1].Count();
                                    var CopyFurnishedToAdd = "";
                                    if (_Copycnt > 50)
                                    {
                                        var concatedword = CopyFurnishedTo[1] + " " + value1;
                                        int index2 = concatedword.LastIndexOf(value1);
                                        if (index2 != -1)
                                        {
                                            var _item = concatedword.Substring(concatedword.Length - (concatedword.Length - 1));
                                            CopyFurnishedToAdd = _item;
                                        }
                                    }
                                    else
                                    {
                                        CopyFurnishedToAdd = CopyFurnishedTo[1] + " " + value1;
                                    }
                                    Session["pincodeFurnished"] = value1;
                                    _Pdfadd.AddressType = "COPIES FURNISHED TO:";
                                    var _CopyToAdd = Regex.Replace(CopyFurnishedToAdd, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
                                    _Pdfadd.Pdfaddress = _CopyToAdd;
                                    _searchedPDF.pdfAddresslist.Add(_Pdfadd);
                                }
                                else
                                {
                                    var CopyFurnishedTo = txtfilter[0].Split(new string[] { "COPIES FURNISHED TO:" }, StringSplitOptions.None);
                                    int _Copycnt = CopyFurnishedTo[1].Count();
                                    var CopyFurnishedToAdd = "";
                                    if (_Copycnt > 50)
                                    {
                                        string Pincode = Session["pincodeFurnished"].ToString();
                                        var CopyFurnised2 = CopyFurnishedTo[1].Split(new string[] { Pincode }, StringSplitOptions.None);
                                        var concatedword = CopyFurnised2[1] + " " + value1;

                                        int index2 = concatedword.LastIndexOf(value1);
                                        if (index2 != -1)
                                        {
                                            var _item = concatedword.Substring(concatedword.Length - (concatedword.Length - 1));
                                            CopyFurnishedToAdd = _item;
                                        }
                                    }
                                    else
                                    {
                                        CopyFurnishedToAdd = CopyFurnishedTo[1] + " " + value1;
                                    }
                                    _Pdfadd.AddressType = "COPIES FURNISHED TO 2:";
                                    var _CopyToAdd = Regex.Replace(CopyFurnishedToAdd, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
                                    _Pdfadd.Pdfaddress = _CopyToAdd;
                                    _searchedPDF.pdfAddresslist.Add(_Pdfadd);
                                }
                                cntCopyFurnishedTo++;
                            }

                            if (txtfilter[0].Contains("Total Due:"))
                            {
                                if (extractText.ToLower().Contains("principal balance"))
                                {
                                    if (cntprincipal == 0)
                                    {
                                        var principalBal = extractText.Split(new string[] { "Principal Balance" }, StringSplitOptions.None);
                                        var principalBal1 = principalBal[1].Split(new string[] { "Interest." }, StringSplitOptions.None);
                                        var totalDue = "Principal Balance" + principalBal1[0].ToString();
                                        _Pdfadd.AddressType = "Principal Balance:";
                                        var _Principal = Regex.Replace(totalDue, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
                                        var Prstr1 = totalDue.Split('$');
                                        int cnt = Prstr1.Length;
                                        var regex = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                                        int val = 0;
                                        for (int k = 0; k < cnt; k++)
                                        {
                                            string pr = Prstr1[k].ToString();
                                            string principal = "$" + pr.Replace(",", "").ToString();
                                            if (regex.IsMatch(principal))
                                            {
                                                string decimal_places = regex.Match(principal).ToString();
                                                var duesplit = principal.Split(new string[] { "." + decimal_places }, StringSplitOptions.None);
                                                string completedue = duesplit[0].ToString() + "." + decimal_places;
                                                double val1 = Convert.ToDouble(completedue.Replace("$", "").ToString().Trim());
                                                val += Convert.ToInt32(val1);
                                            }
                                        }
                                        string completevalue1 = "TOTAL DUE : $" + val + "-\r\n" + _Principal.ToString();
                                        _Pdfadd.Pdfaddress = completevalue1.ToString();
                                        _searchedPDF.pdfAddresslist.Add(_Pdfadd);
                                    }
                                    cntprincipal++;
                                }
                            }
                        }

                    }

                }

                return _searchedPDF;
            }
            catch
            {
                return null;
            }
        }

        public void AddAddressResultForSave(VMCase _vmcase, int? SrchDefendantID)
        {
            _vmcase.SaveAddressResult(_vmcase, SrchDefendantID);
        }

        public ActionResult SaveAllCaseAddress()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                var sourcePath = "C:/PDfFiles/";
                string Foldername = Session["FolderName"].ToString();
                int CaseNumberID = Convert.ToInt32(Session["CaseNumberID"].ToString());
                DateTime _SearchedDate = Convert.ToDateTime(Session["Searcheddate"].ToString());
                if (System.IO.Directory.Exists(sourcePath))
                {
                    string[] _Dir = System.IO.Directory.GetDirectories(sourcePath);

                    var FilesPath = sourcePath + Foldername;
                    bool _result = _Dir.Contains(FilesPath.Trim());
                    if (_result == true)
                    {
                        string dirName = new DirectoryInfo(FilesPath).Name;
                        if (dirName == Foldername)
                        {
                            string[] files11 = System.IO.Directory.GetFiles(FilesPath);

                            foreach (var _file in files11)
                            {
                                string FileName = new FileInfo(_file).Name;
                                string FileExtensn = new FileInfo(_file).Extension;
                                if (FileExtensn == ".txt")
                                {
                                    string caseNumber1 = FileName.Split('_')[0].Trim();
                                    FilterAndSaveAddress(caseNumber1, Foldername.ToString(), _SearchedDate, CaseNumberID);
                                }
                            }
                        }
                    }
                }
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Login", "Home");
        }


        public void FilterAndSaveAddress(string CaseNumber, string Foldername, DateTime _SearchedDate, int CaseNumberID)
        {
            var sourcePath = "C:/PDfFiles/";// Server.MapPath("~/PDfFiles/");
            //var Result = "";
            _vmcase.searchPDFs = new List<SearchedPdf>();
            var _SearchPdfCheck = _vmcase.GetsearchPDFResult(CaseNumber, _SearchedDate, CaseNumberID);
            if (_SearchPdfCheck != null && _SearchPdfCheck.Count > 0)
            {
                _vmcase.searchPDFs = _SearchPdfCheck;
            }
            else
            {
                if (System.IO.Directory.Exists(sourcePath))
                {
                    string[] _Dir = System.IO.Directory.GetDirectories(sourcePath);

                    var FilesPath = sourcePath + Foldername;
                    bool _result = _Dir.Contains(FilesPath.Trim());
                    if (_result == true)
                    {
                        string dirName = new DirectoryInfo(FilesPath).Name;
                        if (dirName == Foldername)
                        {
                            string[] files11 = System.IO.Directory.GetFiles(FilesPath);
                            string[] filteredFiles = files11.Where(w => w.Contains(CaseNumber)).ToArray();
                            string directoryPath = "C:\\PdfToImage\\" + Foldername;
                            //string directoryPath = Server.MapPath("~/PdfToImage/" + Foldername + "");
                            if (!System.IO.Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(directoryPath);
                            }

                            foreach (var _file in filteredFiles)
                            {
                                if (System.IO.Path.GetExtension(_file) == ".PDF")
                                {
                                    var fileName = System.IO.Path.GetFileName(_file);
                                    string FileCaseNo = fileName.Split('_')[0];
                                    string SplitCaseNumber = FileCaseNo.Split('-')[0];
                                    if (SplitCaseNumber.Trim() == CaseNumber)
                                    {
                                        var PdfFilePath = "C:/PDfFiles/" + Foldername + "/" + fileName + "";// Server.MapPath("~/PDfFiles/" + Foldername + "/" + fileName + "");
                                        //Result = "Success";
                                        ConvertPdftoImage(PdfFilePath, Foldername);
                                    }
                                }
                                else
                                {
                                    //Result = "No eSummons Find In This Case";
                                }
                            }

                            string _OcrText = "";
                            foreach (var _Imagename in myImages)
                            {

                                string fImagename = "C:\\PdfToImage\\" + Foldername + "\\" + _Imagename;
                                // string fImagename = System.IO.Path.Combine(Server.MapPath("~/PdfToImage/" + Foldername + "/" + _Imagename));

                                string extractText = this.ExtractTextFromImage(fImagename);
                                _OcrText = extractText.Replace(Environment.NewLine, "<br />");
                                TextFormate.Add(_OcrText);

                                string[] numbers = Regex.Split(extractText, @"\D+");

                                int cntDefendant = 0, cntLocated = 0, cntTowit = 0, cntondefendant = 0, cntCopyFurnishedTo = 0, cntprincipal = 0; ;

                                SearchedPdf _searchedPDF = new SearchedPdf();
                                _searchedPDF.pdfAddresslist = new List<PDFAddress>();
                                string _PdfName = _Imagename.ToString();
                                _searchedPDF.Pdfname = _PdfName.Split('_')[0];
                                _searchedPDF.SerchedDate = Convert.ToDateTime(Session["Searcheddate"].ToString());
                                _searchedPDF.CaseNumberID = Convert.ToInt32(Session["CaseNumberID"].ToString());

                                _searchedPDF.CaseNumber = CaseNumber;
                                foreach (string value1 in numbers)
                                {
                                    Regex rex = new Regex(@"\b[0-9]{5}(?:-[0-9]{4})?\b");
                                    PDFAddress _Pdfadd = new PDFAddress();
                                    if (rex.IsMatch(value1) == true)
                                    {


                                        var filtrtr = value1.ToString();
                                        var txtfilter = extractText.Split(new string[] { filtrtr }, StringSplitOptions.None);

                                        if (txtfilter[0].Contains("TO DEFENDANT:"))
                                        {
                                            if (cntDefendant == 0)
                                            {
                                                var DEFENDANT = txtfilter[0].Split(new string[] { "TO DEFENDANT:" }, StringSplitOptions.None);
                                                int _oncnt = DEFENDANT[1].Count();
                                                var defeAdd = "";
                                                if (_oncnt > 150)
                                                {
                                                    var concatedword = DEFENDANT[1] + " " + value1;
                                                    int index2 = concatedword.LastIndexOf(value1);
                                                    if (index2 != -1)
                                                    {
                                                        var _item = concatedword.Substring(concatedword.Length - 120);
                                                        defeAdd = _item;
                                                    }
                                                }
                                                else
                                                {
                                                    defeAdd = DEFENDANT[1] + " " + value1;
                                                }
                                                _Pdfadd.AddressType = "TO DEFENDANT:";
                                                _Pdfadd.Pdfaddress = defeAdd;
                                                _searchedPDF.pdfAddresslist.Add(_Pdfadd);
                                            }
                                            cntDefendant++;
                                        }
                                        if (txtfilter[0].Contains("on Defendant:"))
                                        {
                                            if (cntondefendant == 0)
                                            {
                                                var Ondefendant = txtfilter[0].Split(new string[] { "on Defendant:" }, StringSplitOptions.None);
                                                int _oncnt = Ondefendant[1].Count();
                                                var OndefendantAdd = "";
                                                if (_oncnt > 150)
                                                {
                                                    var concatedword = Ondefendant[1] + " " + value1;
                                                    int index2 = concatedword.LastIndexOf(value1);
                                                    if (index2 != -1)
                                                    {
                                                        var _item = concatedword.Substring(concatedword.Length - 120);
                                                        OndefendantAdd = _item;
                                                    }
                                                }
                                                else
                                                {
                                                    OndefendantAdd = Ondefendant[1] + " " + value1;
                                                }
                                                _Pdfadd.AddressType = "On Defendant.";
                                                var _OndefendantAdd = Regex.Replace(OndefendantAdd, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
                                                _Pdfadd.Pdfaddress = _OndefendantAdd;
                                                _searchedPDF.pdfAddresslist.Add(_Pdfadd);
                                            }
                                            cntondefendant++;
                                        }
                                        if (txtfilter[0].Contains("located at:"))
                                        {
                                            if (cntLocated == 0)
                                            {
                                                var located = txtfilter[0].Split(new string[] { "located at:" }, StringSplitOptions.None);
                                                int _oncnt = located[1].Count();
                                                var locatedAdd = "";
                                                if (_oncnt > 150)
                                                {
                                                    var concatedword = located[1] + " " + value1;
                                                    int index2 = concatedword.LastIndexOf(value1);
                                                    if (index2 != -1)
                                                    {
                                                        var _item = concatedword.Substring(concatedword.Length - 120);
                                                        locatedAdd = _item;
                                                    }
                                                }
                                                else
                                                {
                                                    locatedAdd = located[1] + " " + value1;
                                                }
                                                _Pdfadd.AddressType = "Located at:";
                                                _Pdfadd.Pdfaddress = locatedAdd;
                                                _searchedPDF.pdfAddresslist.Add(_Pdfadd);
                                            }
                                            cntLocated++;
                                        }
                                        if (txtfilter[0].Contains("to-wit:") || txtfilter[0].Contains("to- wit:") || txtfilter[0].Contains("to wit:") || txtfilter[0].Contains("wit:"))
                                        {
                                            if (cntTowit == 0)
                                            {
                                                var Towit = txtfilter[0].Split(new string[] { "wit:" }, StringSplitOptions.None);
                                                int _oncnt = Towit[1].Count();
                                                var toaitAdd = "";
                                                if (_oncnt > 150)
                                                {
                                                    var concatedword = Towit[1] + " " + value1;
                                                    int index2 = concatedword.LastIndexOf(value1);
                                                    if (index2 != -1)
                                                    {
                                                        var _item = concatedword.Substring(concatedword.Length - 120);
                                                        toaitAdd = _item;
                                                    }
                                                }
                                                else
                                                {
                                                    toaitAdd = Towit[1] + " " + value1;
                                                }
                                                _Pdfadd.AddressType = "TO-wit:";
                                                _Pdfadd.Pdfaddress = toaitAdd;
                                                _searchedPDF.pdfAddresslist.Add(_Pdfadd);
                                            }
                                            cntTowit++;
                                        }
                                        if (txtfilter[0].Contains("COPIES FURNISHED TO:"))
                                        {
                                            if (cntCopyFurnishedTo == 0)
                                            {
                                                var CopyFurnishedTo = txtfilter[0].Split(new string[] { "COPIES FURNISHED TO:" }, StringSplitOptions.None);
                                                int _Copycnt = CopyFurnishedTo[1].Count();
                                                var CopyFurnishedToAdd = "";
                                                if (_Copycnt > 150)
                                                {
                                                    var concatedword = CopyFurnishedTo[1] + " " + value1;
                                                    int index2 = concatedword.LastIndexOf(value1);
                                                    if (index2 != -1)
                                                    {
                                                        var _item = concatedword.Substring(concatedword.Length - (concatedword.Length - 1));
                                                        CopyFurnishedToAdd = _item;
                                                    }
                                                }
                                                else
                                                {
                                                    CopyFurnishedToAdd = CopyFurnishedTo[1] + " " + value1;
                                                }
                                                Session["pincodeFurnished"] = value1;
                                                _Pdfadd.AddressType = "COPIES FURNISHED TO:";
                                                var _CopyToAdd = Regex.Replace(CopyFurnishedToAdd, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
                                                _Pdfadd.Pdfaddress = _CopyToAdd;
                                                _searchedPDF.pdfAddresslist.Add(_Pdfadd);
                                            }
                                            else
                                            {
                                                var CopyFurnishedTo = txtfilter[0].Split(new string[] { "COPIES FURNISHED TO:" }, StringSplitOptions.None);
                                                int _Copycnt = CopyFurnishedTo[1].Count();
                                                var CopyFurnishedToAdd = "";
                                                if (_Copycnt > 150)
                                                {
                                                    string Pincode = Session["pincodeFurnished"].ToString();
                                                    var CopyFurnised2 = CopyFurnishedTo[1].Split(new string[] { Pincode }, StringSplitOptions.None);
                                                    var concatedword = CopyFurnised2[1] + " " + value1;

                                                    int index2 = concatedword.LastIndexOf(value1);
                                                    if (index2 != -1)
                                                    {
                                                        var _item = concatedword.Substring(concatedword.Length - (concatedword.Length - 1));
                                                        CopyFurnishedToAdd = _item;
                                                    }
                                                }
                                                else
                                                {
                                                    CopyFurnishedToAdd = CopyFurnishedTo[1] + " " + value1;
                                                }
                                                _Pdfadd.AddressType = "COPIES FURNISHED TO 2:";
                                                var _CopyToAdd = Regex.Replace(CopyFurnishedToAdd, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
                                                _Pdfadd.Pdfaddress = _CopyToAdd;
                                                _searchedPDF.pdfAddresslist.Add(_Pdfadd);
                                            }
                                            cntCopyFurnishedTo++;
                                        }
                                    }

                                    else
                                    {
                                        if (extractText.ToLower().Contains("principal balance"))
                                        {
                                            if (cntprincipal == 0)
                                            {
                                                var principalBal = extractText.Split(new string[] { "Principal Balance" }, StringSplitOptions.None);
                                                var principalBal1 = principalBal[1].Split(new string[] { "Interest." }, StringSplitOptions.None);
                                                var totalDue = "Principal Balance" + principalBal1[0].ToString();
                                                _Pdfadd.AddressType = "Principal Balance:";
                                                var _Principal = Regex.Replace(totalDue, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
                                                var Prstr1 = totalDue.Split('$');
                                                int cnt = Prstr1.Length;
                                                var regex = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                                                int val = 0;
                                                for (int k = 0; k < cnt; k++)
                                                {
                                                    string pr = Prstr1[k].ToString();
                                                    string principal = "$" + pr.Replace(",", "").ToString();
                                                    if (regex.IsMatch(principal))
                                                    {
                                                        string decimal_places = regex.Match(principal).ToString();
                                                        var duesplit = principal.Split(new string[] { "." + decimal_places }, StringSplitOptions.None);
                                                        string completedue = duesplit[0].ToString() + "." + decimal_places;
                                                        double val1 = Convert.ToDouble(completedue.Replace("$", "").ToString().Trim());
                                                        val += Convert.ToInt32(val1);
                                                    }
                                                }
                                                string completevalue1 = "TOTAL DUE : $" + val + "-\r\n" + _Principal.ToString();
                                                _Pdfadd.Pdfaddress = completevalue1.ToString();
                                                _searchedPDF.pdfAddresslist.Add(_Pdfadd);
                                            }
                                            cntprincipal++;
                                        }
                                    }
                                }
                                _vmcase.searchPDFs.Add(_searchedPDF);
                            }
                            AddAddressResultForSave(_vmcase, null);
                        }
                    }
                }
            }
        }

        public void CaseSearchitem(string CaseNumber)
        {
            var binary = new FirefoxBinary(Server.MapPath("~/firefox.exe"));
            string path = Server.MapPath("~/firefox.exe");
            //string SearchType = "Bulk";
            FirefoxProfile ffprofile = new FirefoxProfile();
            IWebDriver driver = new FirefoxDriver(ffprofile);
            try
            {
                driver.Manage().Window.Maximize();

                driver.Navigate().GoToUrl("https://www.browardclerk.org/Web2/");
                Thread.Sleep(1000);

                IWebElement EventsDocument = driver.FindElement(By.Id("myTabStandard"));
                var innerHtml1 = EventsDocument.GetAttribute("innerHTML");

                string html = @"<html><body>" + innerHtml1.ToString() + "</body></html>";
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(html);
                var nodes = doc.DocumentNode.SelectNodes("//li/a");

                //var count = 0;
                //var Chk = 0;
                foreach (HtmlNode node in nodes)
                {
                    if (node.InnerText == "Case Number")
                    {
                        var nodepath = node.XPath;
                        var npath = nodepath.Split(new string[] { "body[1]" }, StringSplitOptions.None);
                        var Xpath = "//div[@id='myTabStandard']" + npath[1] + "";
                        //html[1]/body[1]/div[1]/div[1]/ul[1]/li[3]/a[1]

                        driver.FindElement(By.XPath(Xpath)).Click();
                        break;
                    }
                }
                Thread.Sleep(1000);
                driver.FindElement(By.Id("CaseNumber")).SendKeys(CaseNumber);
                Thread.Sleep(1000);
                driver.FindElement(By.Id("CaseNumberSearchResults")).Click();
                //string Search = Server.MapPath("~/Images/Search.png");
                //SikuliAction.Click(Search);
                Thread.Sleep(2000);

                IWebElement _Div = driver.FindElement(By.Id("SearchResultsGrid"));
                var Foldername = CaseNumber;
                string CurrentDate = DateTime.Now.ToString("dd_MM_yyyy_hh_mm");
                DateTime SearchedDate = Convert.ToDateTime(DateTime.Now);
                string Folderpath = "" + Foldername + "_" + CurrentDate + "";
                string directoryPath = "C:/PDfFiles/" + Folderpath + "";// Server.MapPath("~/PDfFiles/" + Folderpath + "");
                Directory.CreateDirectory(directoryPath);
                var innerhtml = _Div.GetAttribute("innerHTML");
                string html1 = @"<html><body>" + innerhtml.ToString() + "</body></html>";
                HtmlAgilityPack.HtmlDocument doc1 = new HtmlAgilityPack.HtmlDocument();
                doc1.LoadHtml(html1);
                var nodes1 = doc1.DocumentNode.SelectNodes("//div[2]/table[1]/tbody/tr/td");
                ArrayList _Tablecolumn = new ArrayList();
                foreach (HtmlNode node in nodes1)
                {
                    _Tablecolumn.Add(node.InnerText);
                }
                string CaseNumber1 = Convert.ToString(_Tablecolumn[0]);
                string CaseStyle = Convert.ToString(_Tablecolumn[1]);
                string CaseType = Convert.ToString(_Tablecolumn[2]);
                DateTime Fillingdate = Convert.ToDateTime(_Tablecolumn[3]);
                string CaseStatus = Convert.ToString(_Tablecolumn[4]);
                //_vmcase.SaveCaseSearchHistory(SearchedDate, CaseNumber1, CaseStyle, CaseType, Fillingdate, CaseStatus, SearchType);
                GetpdfFiles(Folderpath, driver, "1");
                driver.Quit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                driver.Quit();

            }

        }
        public void testrate()
        {
            FirefoxProfile ffprofile = new FirefoxProfile();
            IWebDriver driver = new FirefoxDriver(ffprofile);
            try
            {
                driver.Manage().Window.Maximize();

                string _url = "http://www.freddiemac.com/pmms/pmms30.htm";
                driver.Navigate().GoToUrl(_url);
                Thread.Sleep(1000);

                By xPathdetails = By.XPath("//div[@id='content-main']");

                IWebElement EventsDocumentYearly = driver.FindElement(xPathdetails);
                var innerHtmlYearly = EventsDocumentYearly.GetAttribute("innerHTML");

                string htmlYearly = @"<html><body>" + innerHtmlYearly.ToString() + "</body></html>";
                HtmlAgilityPack.HtmlDocument _docYearly = new HtmlAgilityPack.HtmlDocument();
                _docYearly.LoadHtml(htmlYearly);

                System.Data.DataTable _dt = new System.Data.DataTable();

                _dt.Columns.Add("RateYears", typeof(String));
                _dt.Columns.Add("RtMonth", typeof(String));



                int cntnode = _docYearly.DocumentNode.SelectNodes("//table").Count();

                for (int k = 10; k <= 10; k--)
                {
                    DataRow dr = _dt.NewRow();
                    int _text = 1971;
                    dr["RateYears"] = _text;
                    int cntth = _docYearly.DocumentNode.SelectNodes("//table[" + k + "]/thead/tr/th").Count();
                    int thCnt = 1;
                    foreach (HtmlNode node in _docYearly.DocumentNode.SelectNodes("//table[" + k + "]/tbody[2]/tr/td[]"))
                    {
                        if (thCnt > 1)
                        {
                            if (thCnt <= cntth)
                            {
                                var _text1 = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();

                                dr["RateYears"] = _text1;
                            }
                        }
                        thCnt++;
                        //_dt.Rows.Add(dr);
                    }

                    foreach (HtmlNode node in _docYearly.DocumentNode.SelectNodes("//table[" + k + "]"))
                    {
                    }
                }


            }
            catch (Exception ex)
            {
                string msg = ex.Message;

            }
        }
        public ActionResult Test()
        {
            //GetAmortizationByID(2113);
            //testfolio();
            //testrate();
            //testloan();

            //FirefoxProfile ffprofile = new FirefoxProfile();
            //IWebDriver driver = new FirefoxDriver(ffprofile);
            //try
            //{
            //    driver.Manage().Window.Maximize();

            //    string _url = "http://www.bcpa.net/RecInfo.asp?URL_Folio=484123160050";
            //    driver.Navigate().GoToUrl(_url);
            //    Thread.Sleep(1000);

            //    FetchBCPAHtml(driver, true, 1);
            //}
            //catch { }


            //DataSet model = _CompPrMSAccess.GetCompareableProperty();
            //string imagename = "C:\\PDfFiles\\PDfF1.PDF";
            //if (System.IO.File.Exists(imagename))
            //{
            //    //System.IO.File.Delete(imagename);
            //}
            //CaseSearchitem("CACE17000815");
            //testloan();

            //GetPalmDetailsFrompbcgov();



            //GetPdfFromPlamBeach();

            //SearchPalmBeachRecord();

            //GetPalmListDetailsByCase();

            //ReadPalmPDF();

            //FirefoxProfile ffprofile = new FirefoxProfile();
            //IWebDriver driver = new FirefoxDriver(ffprofile);
            //driver.Manage().Window.Maximize();

            //Int64 PalmBeachLPCaseID = 643;

            //driver.Navigate().GoToUrl("http://pbcgov.com/papa/Asps/GeneralAdvSrch/SearchPage.aspx?f=a");
            //Thread.Sleep(1000);

            //driver.FindElement(By.Id("MainContent_txtOwner")).SendKeys("SIEGEL JERRY");
            //Thread.Sleep(1000);
            //driver.FindElement(By.Id("MainContent_btnAdvSrch")).Click();
            //Thread.Sleep(1000);

            //GetPalmPCBDetails(PalmBeachLPCaseID, driver);
            GetPropertyOwnerDetails();
            return View();

        }
        public void GetPropertyOwnerDetails()
        {
            try
            {
                FirefoxProfile ffprofile = new FirefoxProfile();
                IWebDriver driver = new FirefoxDriver(ffprofile);
                driver.Navigate().GoToUrl("https://www.spokeo.com/login?url=http://www.spokeo.com/");
                Thread.Sleep(1000);
                int OTHAddressID = 815;
                driver.FindElement(By.Id("email_address")).SendKeys("guysimani613@gmail.com");
                driver.FindElement(By.Id("password")).SendKeys("Wbes7777!");
                Thread.Sleep(1000);
                driver.FindElement(By.ClassName("session_button")).Click();
                Thread.Sleep(1000);
                driver.Navigate().GoToUrl("http://www.spokeo.com/user");
                //string mailingAddress = "5211 NE 17th AVE FORT LAUDERDALE FL 33334";
                string mailingAddress = "1805 NE 15th Ave, Fort Lauderdale, FL 33305";
                //string mailingAddress = "686 SW Veronica Ave, Port Saint Lucie, FL 34953";
                driver.FindElement(By.ClassName("autocomplete_input")).Clear();
                driver.FindElement(By.ClassName("autocomplete_input")).SendKeys(mailingAddress);

                Thread.Sleep(1000);
                By xPathHome = By.XPath("//div[@ng_if='showDropdown()']");
                //By xPathHome = By.XPath("//div[@class='autocomplete_results_items']/div[2]/span[@class='ng-binding']"); ng_if='showDropdown()' 
                IWebElement EventsDocHome = driver.FindElement(xPathHome);
                var innerHtmlHome = EventsDocHome.GetAttribute("innerHTML");
                string _htmlHome = @"<html><body>" + innerHtmlHome.ToString() + "</body></html>";
                HtmlAgilityPack.HtmlDocument _docHome = new HtmlAgilityPack.HtmlDocument();
                _docHome.LoadHtml(_htmlHome);

                Thread.Sleep(1000);

                //var nodespan = doc.DocumentNode.SelectNodes("//span[@class='ng-binding']");

                //foreach (HtmlNode node in nodespan)
                //{
                //    if (node.InnerText == "Business Name")
                //    {
                //        var nodepath = node.XPath;
                //        var npath = nodepath.Split(new string[] { "body" }, StringSplitOptions.None);
                //        var Xpath = "//div[@ng_if='showDropdown()']" + npath[1] + "";
                //        driver.FindElement(By.XPath(Xpath)).Click();
                //    }
                //}

                foreach (HtmlNode node in _docHome.DocumentNode.SelectNodes("//span[@class='ng-binding']"))
                {
                    var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    driver.FindElement(By.ClassName("autocomplete_input")).Clear();
                    driver.FindElement(By.ClassName("autocomplete_input")).SendKeys(_text);

                    Thread.Sleep(1000);
                    break;
                }

                Thread.Sleep(1000);

                driver.FindElement(By.ClassName("header_search_button")).Click();
                //"autocomplete_results_items"
                Thread.Sleep(1000);
                bool _AddressNotMatched = driver.PageSource.Contains("block_text_jewel");
                if (_AddressNotMatched != true)
                {
                    IWebElement EventsDocument = driver.FindElement(By.Id("property_owners"));
                    var innerHtml1 = EventsDocument.GetAttribute("innerHTML");
                    string html = @"<html><body>" + innerHtml1.ToString() + "</body></html>";
                    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(html);
                    string Defandent = "Port Of Seattle";
                    //string Defandent = "HANSEN,KYLE H/EKUHN,JOELLE ";
                    ArrayList ownerInfo = new ArrayList();
                    var exist = doc.DocumentNode.SelectNodes("//div[@class='no_results_subtitle']");
                    bool exist1 = doc.DocumentNode.SelectNodes("//div[@class='no_results_subtitle']") != null;
                    var nodes = doc.DocumentNode.SelectNodes("//a[@class='listview_primary_title']");
                    bool nodes1 = doc.DocumentNode.SelectNodes("//a[@class='listview_primary_title']") != null;
                    if (nodes1 == true)
                    {
                        foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//a[@class='listview_primary_title']"))
                        {
                            var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                            var splttext = _text.Split(' ');
                            for (int i = 0; i < splttext.Length; i++)
                            {
                                if (Defandent.ToUpper().ToString().Contains(splttext[i].ToUpper().ToString()))
                                {
                                    string Url = node.Attributes["href"].Value;
                                    string _Url = Regex.Replace(Url, @"\t|\n|\r", "");
                                    string FilterUrl = Regex.Replace(_Url, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", "&").Trim();
                                    var InfoURL = FilterUrl.Split(new string[] { "profile/" }, StringSplitOptions.None);
                                    string OwnerInfoURL = "http://www.spokeo.com/Joelle-Kuhn/Florida/Fort-Lauderdale/p" + InfoURL[1].ToString();

                                    if (!ownerInfo.Contains(OwnerInfoURL))
                                    {
                                        ownerInfo.Add(OwnerInfoURL);
                                    }

                                }
                            }

                        }
                    }
                    foreach (var item in ownerInfo)
                    {
                        string _url = item.ToString();
                        driver.Navigate().GoToUrl(_url);
                        Thread.Sleep(1000);
                        By xPathbirthDate = By.XPath("//span[@itemprop='birthDate']");
                        By xPathtelephone = By.XPath("//span[@itemprop='telephone']");
                        By xPathemail = By.XPath("//span[@itemprop='email']");
                        var UName = driver.FindElement(By.ClassName("autocomplete_input")).GetAttribute("value");
                        //By xPathbirthDate = By.XPath("//div[@id='profile_summary']/div[1]/div[2]/div[3]/div/span[@itemprop='birthDate']");
                        //By xPathtelephone = By.XPath("//div[@id='contact_info']/div[3]/div/div/div/div/div/a/span[@itemprop='telephone']");
                        //By xPathemail = By.XPath("//div[@id='contact_info']/div[4]/div[2]/div/div[1]/div/div/a/a/span[@itemprop='email']");
                        var TxtBirthDate = "";
                        bool _birthDateCheck = driver.PageSource.Contains("itemprop='birthDate'");
                        if (_birthDateCheck == true)
                        {
                            TxtBirthDate = driver.FindElements(xPathbirthDate)[0].Text;
                        }
                        var TxtTelephone = "";
                        bool _telephoneCheck = driver.PageSource.Contains("itemprop='telephone'");
                        if (_telephoneCheck == true)
                        {
                            TxtTelephone = driver.FindElements(xPathtelephone)[0].Text;
                        }
                        bool _EmailCheck = driver.PageSource.Contains("itemprop='email'");
                        var TxtEmail = "";
                        if (_EmailCheck == true)
                        {
                            TxtEmail = driver.FindElements(xPathemail)[0].Text;
                        }
                        var TxtFBLink = "";
                        By xPathbannertitle = By.XPath("//div[@id='court_records']");
                        IWebElement EventsDocbannertitle = driver.FindElement(xPathbannertitle);
                        var innerHtmlbannertitle = EventsDocbannertitle.GetAttribute("innerHTML");
                        string _htmltitle = @"<html><body>" + innerHtmlbannertitle.ToString() + "</body></html>";
                        HtmlAgilityPack.HtmlDocument _docTitle = new HtmlAgilityPack.HtmlDocument();
                        _docTitle.LoadHtml(_htmltitle);
                        bool _NoRecords = false;
                        _NoRecords = IsCourtDetails(_docTitle);
                        System.Data.DataTable _dtCourtDtails = new System.Data.DataTable();
                        if (_NoRecords != true)
                        {
                            //---------------------- Court Details------------------------------//
                            //System.Data.DataTable _dtCourtDtails = new System.Data.DataTable();

                            _dtCourtDtails.Columns.Add("Category", typeof(String));
                            _dtCourtDtails.Columns.Add("OffenseDate", typeof(String));
                            _dtCourtDtails.Columns.Add("OffenseCode", typeof(String));
                            _dtCourtDtails.Columns.Add("OffenseDesc", typeof(String));
                            _dtCourtDtails.Columns.Add("Disposition", typeof(String));
                            _dtCourtDtails.Columns.Add("DispositionDate", typeof(String));
                            _dtCourtDtails.Columns.Add("SourceState", typeof(String));
                            _dtCourtDtails.Columns.Add("SourceName", typeof(String));
                            _dtCourtDtails.Columns.Add("CaseNo", typeof(String));
                            _dtCourtDtails.Columns.Add("ArrestDate", typeof(String));
                            _dtCourtDtails.Columns.Add("Racewhite", typeof(String));

                            var _cnttag = _docTitle.DocumentNode.SelectNodes("//div[@class='court_match_tags']").Count();

                            var _cntnodes = _docTitle.DocumentNode.SelectNodes("//table[@class='court_record_table']").Count();

                            By xPathtCourtDetails = By.XPath("//div[@id='court_records']/div[3]/div[2]/div[2]/div/div[" + (_cnttag + 4) + "]/table");
                            GetCourtDetails(driver, _dtCourtDtails, xPathtCourtDetails);
                            int cnt = 0;
                            for (int p = 2; p <= _cntnodes; p++)
                            {
                                cnt = cnt + 2;
                                By xPathtCourtDetails1 = By.XPath("//div[@id='court_records']/div[3]/div[2]/div[2]/div/div[" + (_cnttag + 5) + "]/div[" + cnt + "]/table");
                                GetCourtDetails(driver, _dtCourtDtails, xPathtCourtDetails1);
                            }

                            bool _Hasrecords = driver.PageSource.Contains("social_profiles");
                            if (_Hasrecords == true)
                            {
                                bool _HasLinks = driver.PageSource.Contains("social_photo_link");
                                Thread.Sleep(1000);
                                By xPathFB = By.XPath("//a[@class='social_photo_link']");
                                TxtFBLink = driver.FindElements(xPathFB)[0].GetAttribute("href");

                            }

                            //string ss = "";

                        }
                        _vmcase.SaveUserSpokioDetails(OTHAddressID, UName, TxtBirthDate, TxtTelephone, TxtEmail, TxtFBLink, _url, _dtCourtDtails);

                    }
                }



            }
            catch (Exception ex)
            {
                string s = ex.ToString();
            }
        }

        private static void GetCourtDetails(IWebDriver driver, System.Data.DataTable _dtCourtDtails, By xPathtCourtDetails1)
        {
            IWebElement EventsDocumentCourt = driver.FindElement(xPathtCourtDetails1);
            var innerHtmlCourt = EventsDocumentCourt.GetAttribute("innerHTML");

            string _html = @"<html><body>" + innerHtmlCourt.ToString() + "</body></html>";
            HtmlAgilityPack.HtmlDocument _doc = new HtmlAgilityPack.HtmlDocument();
            _doc.LoadHtml(_html);
            var _nodes = _doc.DocumentNode.SelectNodes("//tr");
            DataRow drCourt = _dtCourtDtails.NewRow();

            for (int i = 1; i <= _nodes.Count; i++)
            {
                string tdLabel = string.Empty;
                int k = 1;
                foreach (HtmlNode node in _doc.DocumentNode.SelectNodes("//tr[" + i + "]/td"))
                {
                    var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();

                    if (k % 2 != 0)
                    {
                        tdLabel = _text;
                    }

                    if (k % 2 == 0)
                    {
                        switch (tdLabel)
                        {
                            case "Category":
                                drCourt["Category"] = _text;
                                break;
                            case "Offense Date":
                                drCourt["OffenseDate"] = _text;
                                break;
                            case "Offense Code":
                                drCourt["OffenseCode"] = _text;
                                break;
                            case "Offense Desc":
                                drCourt["OffenseDesc"] = _text;
                                break;
                            case "Disposition":
                                drCourt["Disposition"] = _text;
                                break;
                            case "Disposition Date":
                                drCourt["DispositionDate"] = _text;
                                break;
                            case "Source State":
                                drCourt["SourceState"] = _text;
                                break;
                            case "Source Name":
                                drCourt["SourceName"] = _text;
                                break;
                            case "Case No":
                                drCourt["CaseNo"] = _text;
                                break;
                            case "Arrest Date":
                                drCourt["ArrestDate"] = _text;
                                break;
                            case "Race white":
                                drCourt["Racewhite"] = _text;
                                break;

                            default:
                                break;
                        }

                    }
                    k++;
                }

            }
            _dtCourtDtails.Rows.Add(drCourt);
        }

        public Boolean IsCourtDetails(HtmlAgilityPack.HtmlDocument _docTitle)
        {
            bool Result = false;
            try
            {
                foreach (HtmlNode span in _docTitle.DocumentNode.SelectNodes("//span[@class='banner_title']"))
                {
                    Result = true;
                }
            }
            catch
            {
                Result = false;
            }
            return Result;
        }

        private void GetPalmPCBDetails(Int64 PalmBeachLPCaseID, IWebDriver driver)
        {
            try
            {
                if (IsElementPresent(driver, By.Id("MainContent_lblMsg")))
                {
                    //_vmPalmcase.UpdatePalmBeachByNotFound(PalmBeachLPCaseID);
                }
                else
                {
                    string PCN = driver.FindElement(By.Id("MainContent_lblPCN")).Text;
                    string OfficialBook = driver.FindElement(By.Id("MainContent_lblBook")).Text;
                    string SaleDate = driver.FindElement(By.Id("MainContent_lblSaleDate")).Text;
                    string Subdiv = driver.FindElement(By.Id("MainContent_lblSubdiv")).Text;
                    string LegalDesc = driver.FindElement(By.Id("MainContent_lblLegalDesc")).Text;

                    IWebElement EventsDocument = driver.FindElement(By.Id("MainContent_gvSalesInfo"));
                    var innerHtml1 = EventsDocument.GetAttribute("innerHTML");

                    string html = @"<html><body>" + innerHtml1.ToString() + "</body></html>";
                    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(html);
                    var nodes = doc.DocumentNode.SelectNodes("//tr");

                    System.Data.DataTable _dtSaleinfo = new System.Data.DataTable();

                    _dtSaleinfo.Columns.Add("SaleDate", typeof(String));
                    _dtSaleinfo.Columns.Add("Price", typeof(String));
                    _dtSaleinfo.Columns.Add("BookPage", typeof(String));
                    _dtSaleinfo.Columns.Add("SaleType", typeof(String));
                    _dtSaleinfo.Columns.Add("Owner", typeof(String));

                    for (int i = 2; i <= nodes.Count; i++)
                    {
                        DataRow drCaseView = _dtSaleinfo.NewRow();
                        int _cntColumn = 1;
                        var _cntnodes = doc.DocumentNode.SelectNodes("//tr[" + i + "]/td").Count();

                        if (_cntnodes == 1)
                        {
                            break;
                        }

                        if (_cntnodes > 1)
                        {

                            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//tr[" + i + "]/td"))
                            {
                                var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                                switch (_cntColumn)
                                {
                                    case 1:
                                        drCaseView["SaleDate"] = _text;
                                        break;
                                    case 2:
                                        drCaseView["Price"] = _text;
                                        break;
                                    case 3:
                                        string BookPage = Regex.Replace(_text, @"\t|\n|\r", "");
                                        drCaseView["BookPage"] = Regex.Replace(BookPage, @"\s+", "");
                                        break;
                                    case 4:
                                        drCaseView["SaleType"] = _text;
                                        break;
                                    case 5:
                                        drCaseView["Owner"] = _text;
                                        break;
                                }
                                _cntColumn++;
                            }
                            _dtSaleinfo.Rows.Add(drCaseView);
                        }

                    }

                    Thread.Sleep(1000);
                    string Units = driver.FindElement(By.Id("MainContent_lblUnits")).Text;
                    string TotalSqft = driver.FindElement(By.Id("MainContent_lblSqFt")).Text;
                    string Acres = driver.FindElement(By.Id("MainContent_lblAcres")).Text;
                    string Usecode = driver.FindElement(By.Id("MainContent_lblUsecode")).Text;
                    string Zoning = driver.FindElement(By.Id("MainContent_lblZoning")).Text;


                    //Start-----------------------------Appraisals ------------------------------------//////
                    string[] TaxYrArr = new string[3];
                    string[] ImpValArr = new string[3];
                    string[] LandValArr = new string[3];
                    string[] MarketArr = new string[3];

                    TaxYrArr[0] = driver.FindElement(By.Id("MainContent_lblTaxYr1")).Text;
                    TaxYrArr[1] = driver.FindElement(By.Id("MainContent_lblTaxYr2")).Text;
                    TaxYrArr[2] = driver.FindElement(By.Id("MainContent_lblTaxYr3")).Text;

                    ImpValArr[0] = driver.FindElement(By.Id("MainContent_lblImpValue1")).Text;
                    ImpValArr[1] = driver.FindElement(By.Id("MainContent_lblImpValue2")).Text;
                    ImpValArr[2] = driver.FindElement(By.Id("MainContent_lblImpValue3")).Text;

                    LandValArr[0] = driver.FindElement(By.Id("MainContent_lblLandValue1")).Text;
                    LandValArr[1] = driver.FindElement(By.Id("MainContent_lblLandValue2")).Text;
                    LandValArr[2] = driver.FindElement(By.Id("MainContent_lblLandValue3")).Text;

                    MarketArr[0] = driver.FindElement(By.Id("MainContent_lblMarketValue1")).Text;
                    MarketArr[1] = driver.FindElement(By.Id("MainContent_lblMarketValue2")).Text;
                    MarketArr[2] = driver.FindElement(By.Id("MainContent_lblMarketValue3")).Text;

                    System.Data.DataTable _dtAppraisal = new System.Data.DataTable();

                    _dtAppraisal.Columns.Add("TaxYear", typeof(String));
                    _dtAppraisal.Columns.Add("improvementVal", typeof(String));
                    _dtAppraisal.Columns.Add("LandValue", typeof(String));
                    _dtAppraisal.Columns.Add("MarketValue", typeof(String));

                    for (int i = 0; i < 3; i++)
                    {
                        DataRow drApp = _dtAppraisal.NewRow();
                        drApp["TaxYear"] = TaxYrArr[i];
                        drApp["improvementVal"] = ImpValArr[i];
                        drApp["LandValue"] = LandValArr[i];
                        drApp["MarketValue"] = MarketArr[i];
                        _dtAppraisal.Rows.Add(drApp);
                    }
                    //End-----------------------------Appraisals ------------------------------------//////


                    //Start-----------------------------Assesed value ------------------------------------//////
                    string[] TaxYrAssArr = new string[3];
                    string[] AssdValArr = new string[3];
                    string[] ExemptionAmtArr = new string[3];
                    string[] TaxableValueArr = new string[3];

                    TaxYrAssArr[0] = driver.FindElement(By.Id("MainContent_lblAssessedTaxYr1")).Text;
                    TaxYrAssArr[1] = driver.FindElement(By.Id("MainContent_lblAssessedTaxYr2")).Text;
                    TaxYrAssArr[2] = driver.FindElement(By.Id("MainContent_lblAssessedTaxYr3")).Text;

                    AssdValArr[0] = driver.FindElement(By.Id("MainContent_lblAssessedValue1")).Text;
                    AssdValArr[1] = driver.FindElement(By.Id("MainContent_lblAssessedValue2")).Text;
                    AssdValArr[2] = driver.FindElement(By.Id("MainContent_lblAssessedValue3")).Text;

                    ExemptionAmtArr[0] = driver.FindElement(By.Id("MainContent_lblExemptionAmt1")).Text;
                    ExemptionAmtArr[1] = driver.FindElement(By.Id("MainContent_lblExemptionAmt2")).Text;
                    ExemptionAmtArr[2] = driver.FindElement(By.Id("MainContent_lblExemptionAmt3")).Text;

                    TaxableValueArr[0] = driver.FindElement(By.Id("MainContent_lblTaxableValue1")).Text;
                    TaxableValueArr[1] = driver.FindElement(By.Id("MainContent_lblTaxableValue2")).Text;
                    TaxableValueArr[2] = driver.FindElement(By.Id("MainContent_lblTaxableValue3")).Text;

                    System.Data.DataTable _dtAssesed = new System.Data.DataTable();

                    _dtAssesed.Columns.Add("TaxAssdYear", typeof(String));
                    _dtAssesed.Columns.Add("AssesedVal", typeof(String));
                    _dtAssesed.Columns.Add("ExemptionAmt", typeof(String));
                    _dtAssesed.Columns.Add("TaxableValue", typeof(String));

                    for (int i = 0; i < 3; i++)
                    {
                        DataRow drAss = _dtAssesed.NewRow();
                        drAss["TaxAssdYear"] = TaxYrAssArr[i];
                        drAss["AssesedVal"] = AssdValArr[i];
                        drAss["ExemptionAmt"] = ExemptionAmtArr[i];
                        drAss["TaxableValue"] = TaxableValueArr[i];
                        _dtAssesed.Rows.Add(drAss);
                    }
                    //End-----------------------------Assesed value ------------------------------------//////


                    //Start-----------------------------Taxes ------------------------------------//////
                    string[] TaxesTaxArr = new string[3];
                    string[] AdValoremArr = new string[3];
                    string[] NonAdValoremArr = new string[3];
                    string[] TaxesArr = new string[3];

                    TaxesTaxArr[0] = driver.FindElement(By.Id("MainContent_lblTaxesTaxYr1")).Text;
                    TaxesTaxArr[1] = driver.FindElement(By.Id("MainContent_lblTaxesTaxYr2")).Text;
                    TaxesTaxArr[2] = driver.FindElement(By.Id("MainContent_lblTaxesTaxYr3")).Text;

                    AdValoremArr[0] = driver.FindElement(By.Id("MainContent_lblAdValorem1")).Text;
                    AdValoremArr[1] = driver.FindElement(By.Id("MainContent_lblAdValorem2")).Text;
                    AdValoremArr[2] = driver.FindElement(By.Id("MainContent_lblAdValorem3")).Text;

                    NonAdValoremArr[0] = driver.FindElement(By.Id("MainContent_lblNonAdValorem1")).Text;
                    NonAdValoremArr[1] = driver.FindElement(By.Id("MainContent_lblNonAdValorem2")).Text;
                    NonAdValoremArr[2] = driver.FindElement(By.Id("MainContent_lblNonAdValorem3")).Text;

                    TaxesArr[0] = driver.FindElement(By.Id("MainContent_lblTaxes1")).Text;
                    TaxesArr[1] = driver.FindElement(By.Id("MainContent_lblTaxes2")).Text;
                    TaxesArr[2] = driver.FindElement(By.Id("MainContent_lblTaxes3")).Text;

                    System.Data.DataTable _dtTaxes = new System.Data.DataTable();

                    _dtTaxes.Columns.Add("TaxesTax", typeof(String));
                    _dtTaxes.Columns.Add("AdValorem", typeof(String));
                    _dtTaxes.Columns.Add("NonAdValorem", typeof(String));
                    _dtTaxes.Columns.Add("TotalTaxes", typeof(String));

                    for (int i = 0; i < 3; i++)
                    {
                        DataRow drTax = _dtTaxes.NewRow();
                        drTax["TaxesTax"] = TaxesTaxArr[i];
                        drTax["AdValorem"] = AdValoremArr[i];
                        drTax["NonAdValorem"] = NonAdValoremArr[i];
                        drTax["TotalTaxes"] = TaxesArr[i];
                        _dtTaxes.Rows.Add(drTax);
                    }
                    //End-----------------------------Taxes ------------------------------------//////
                    Thread.Sleep(1000);

                    string StrDetails = "http://pbcgov.com/papa/Asps/PropertyDetail/StructuralDetail.aspx?entity_id=" + PCN.Replace("-", "");

                    driver.Navigate().GoToUrl(StrDetails);
                    Thread.Sleep(1000);

                    By xPath = By.XPath("//form[@id='frmPage']/div[3]/div[4]/fieldset/table/tbody/tr[3]/td[1]/table[1]/tbody");
                    IWebElement EventsDocumentDoc = driver.FindElement(xPath);

                    var innerHtmlDoc = EventsDocumentDoc.GetAttribute("innerHTML");

                    string htmlResultdoc = @"<html><body>" + innerHtmlDoc.ToString() + "</body></html>";

                    HtmlAgilityPack.HtmlDocument _docDetails = new HtmlAgilityPack.HtmlDocument();
                    _docDetails.LoadHtml(htmlResultdoc);

                    var cntnodes = _docDetails.DocumentNode.SelectNodes("//tr");

                    System.Data.DataTable _dtStrDetails = new System.Data.DataTable();

                    _dtStrDetails.Columns.Add("Structure", typeof(String));
                    _dtStrDetails.Columns.Add("Details", typeof(String));
                    for (int i = 4; i <= cntnodes.Count; i++)
                    {
                        DataRow drstr = _dtStrDetails.NewRow();
                        int _cntColumn = 1;
                        foreach (HtmlNode node in _docDetails.DocumentNode.SelectNodes("//tr[" + i + "]/td"))
                        {
                            var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                            switch (_cntColumn)
                            {
                                case 2:
                                    drstr["Structure"] = _text;
                                    break;
                                case 3:
                                    drstr["Details"] = _text;
                                    break;
                            }
                            _cntColumn++;
                        }
                        _dtStrDetails.Rows.Add(drstr);
                    }
                    //string Result = "";
                    //-------------SavePalm Cases Details From PCB Site------------------//
                    // _vmPalmcase.SavePalmPbcDetails(PalmBeachLPCaseID, PCN, OfficialBook, SaleDate, Subdiv, LegalDesc, _dtSaleinfo, Units, TotalSqft, Acres, Usecode, Zoning, _dtAppraisal, _dtAssesed, _dtTaxes, _dtStrDetails);
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;

            }
        }
        private bool IsElementPresent(IWebDriver driver, By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }





        public void GetPalmDetailsFrompbcgov()
        {
            try
            {
                FirefoxProfile ffprofile = new FirefoxProfile();
                IWebDriver driver = new FirefoxDriver(ffprofile);
                driver.Manage().Window.Maximize();

                driver.Navigate().GoToUrl("http://pbcgov.com/papa/Asps/GeneralAdvSrch/SearchPage.aspx?f=a");
                Thread.Sleep(1000);
                string StreetNo = "10636";
                string ddlPrefix = "";
                string StreetName = "Grande";
                string ddlSuffix = "Blvd";
                string ddlPostDir = "";
                string UnitNo = "";
                string ddlMuni = "West Palm Beach";
                string Zip = "33412";

                driver.FindElement(By.Id("MainContent_txtStreetNo")).SendKeys(StreetNo);
                driver.FindElement(By.Id("MainContent_ddlPrefix")).SendKeys(ddlPrefix);
                driver.FindElement(By.Id("MainContent_txtStreetName")).SendKeys(StreetName);
                driver.FindElement(By.Id("MainContent_ddlSuffix")).SendKeys(ddlSuffix);
                driver.FindElement(By.Id("MainContent_ddlPostDir")).SendKeys(ddlPostDir);
                driver.FindElement(By.Id("MainContent_txtUnitNo")).SendKeys(UnitNo);
                driver.FindElement(By.Id("MainContent_ddlMuni")).SendKeys(ddlMuni);
                driver.FindElement(By.Id("MainContent_txtZip")).SendKeys(Zip);
                Thread.Sleep(1000);
                driver.FindElement(By.Id("MainContent_btnAdvSrch")).Click();
                Thread.Sleep(1000);
                string PCN = driver.FindElement(By.Id("MainContent_lblPCN")).Text;
                string OfficialBook = driver.FindElement(By.Id("MainContent_lblBook")).Text;
                string SaleDate = driver.FindElement(By.Id("MainContent_lblSaleDate")).Text;
                string Subdiv = driver.FindElement(By.Id("MainContent_lblSubdiv")).Text;
                string LegalDesc = driver.FindElement(By.Id("MainContent_lblLegalDesc")).Text;

                IWebElement EventsDocument = driver.FindElement(By.Id("MainContent_gvSalesInfo"));
                var innerHtml1 = EventsDocument.GetAttribute("innerHTML");

                string html = @"<html><body>" + innerHtml1.ToString() + "</body></html>";
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(html);
                var nodes = doc.DocumentNode.SelectNodes("//tr");

                System.Data.DataTable _dtSaleinfo = new System.Data.DataTable();

                _dtSaleinfo.Columns.Add("SaleDate", typeof(String));
                _dtSaleinfo.Columns.Add("Price", typeof(String));
                _dtSaleinfo.Columns.Add("BookPage", typeof(String));
                _dtSaleinfo.Columns.Add("SaleType", typeof(String));
                _dtSaleinfo.Columns.Add("Owner", typeof(String));

                for (int i = 2; i <= nodes.Count; i++)
                {
                    DataRow drCaseView = _dtSaleinfo.NewRow();
                    int _cntColumn = 1;
                    foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//tr[" + i + "]/td"))
                    {
                        var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                        switch (_cntColumn)
                        {
                            case 1:
                                drCaseView["SaleDate"] = _text;
                                break;
                            case 2:
                                drCaseView["Price"] = _text;
                                break;
                            case 3:
                                string BookPage = Regex.Replace(_text, @"\t|\n|\r", "");
                                drCaseView["BookPage"] = Regex.Replace(BookPage, @"\s+", "");
                                break;
                            case 4:
                                drCaseView["SaleType"] = _text;
                                break;
                            case 5:
                                drCaseView["Owner"] = _text;
                                break;
                        }
                        _cntColumn++;
                    }
                    _dtSaleinfo.Rows.Add(drCaseView);

                }

                Thread.Sleep(1000);
                string Units = driver.FindElement(By.Id("MainContent_lblUnits")).Text;
                string TotalSqft = driver.FindElement(By.Id("MainContent_lblSqFt")).Text;
                string Acres = driver.FindElement(By.Id("MainContent_lblAcres")).Text;
                string Usecode = driver.FindElement(By.Id("MainContent_lblUsecode")).Text;
                string Zoning = driver.FindElement(By.Id("MainContent_lblZoning")).Text;


                //Start-----------------------------Appraisals ------------------------------------//////
                string[] TaxYrArr = new string[3];
                string[] ImpValArr = new string[3];
                string[] LandValArr = new string[3];
                string[] MarketArr = new string[3];

                TaxYrArr[0] = driver.FindElement(By.Id("MainContent_lblTaxYr1")).Text;
                TaxYrArr[1] = driver.FindElement(By.Id("MainContent_lblTaxYr2")).Text;
                TaxYrArr[2] = driver.FindElement(By.Id("MainContent_lblTaxYr3")).Text;

                ImpValArr[0] = driver.FindElement(By.Id("MainContent_lblImpValue1")).Text;
                ImpValArr[1] = driver.FindElement(By.Id("MainContent_lblImpValue2")).Text;
                ImpValArr[2] = driver.FindElement(By.Id("MainContent_lblImpValue3")).Text;

                LandValArr[0] = driver.FindElement(By.Id("MainContent_lblLandValue1")).Text;
                LandValArr[1] = driver.FindElement(By.Id("MainContent_lblLandValue2")).Text;
                LandValArr[2] = driver.FindElement(By.Id("MainContent_lblLandValue3")).Text;

                MarketArr[0] = driver.FindElement(By.Id("MainContent_lblMarketValue1")).Text;
                MarketArr[1] = driver.FindElement(By.Id("MainContent_lblMarketValue2")).Text;
                MarketArr[2] = driver.FindElement(By.Id("MainContent_lblMarketValue3")).Text;

                System.Data.DataTable _dtAppraisal = new System.Data.DataTable();

                _dtAppraisal.Columns.Add("TaxYear", typeof(String));
                _dtAppraisal.Columns.Add("improvementVal", typeof(String));
                _dtAppraisal.Columns.Add("LandValue", typeof(String));
                _dtAppraisal.Columns.Add("MarketValue", typeof(String));

                for (int i = 0; i < 3; i++)
                {
                    DataRow drApp = _dtAppraisal.NewRow();
                    drApp["TaxYear"] = TaxYrArr[i];
                    drApp["improvementVal"] = ImpValArr[i];
                    drApp["LandValue"] = LandValArr[i];
                    drApp["MarketValue"] = MarketArr[i];
                    _dtAppraisal.Rows.Add(drApp);
                }
                //End-----------------------------Appraisals ------------------------------------//////


                //Start-----------------------------Assesed value ------------------------------------//////
                string[] TaxYrAssArr = new string[3];
                string[] AssdValArr = new string[3];
                string[] ExemptionAmtArr = new string[3];
                string[] TaxableValueArr = new string[3];

                TaxYrAssArr[0] = driver.FindElement(By.Id("MainContent_lblAssessedTaxYr1")).Text;
                TaxYrAssArr[1] = driver.FindElement(By.Id("MainContent_lblAssessedTaxYr2")).Text;
                TaxYrAssArr[2] = driver.FindElement(By.Id("MainContent_lblAssessedTaxYr3")).Text;

                AssdValArr[0] = driver.FindElement(By.Id("MainContent_lblAssessedValue1")).Text;
                AssdValArr[1] = driver.FindElement(By.Id("MainContent_lblAssessedValue2")).Text;
                AssdValArr[2] = driver.FindElement(By.Id("MainContent_lblAssessedValue3")).Text;

                ExemptionAmtArr[0] = driver.FindElement(By.Id("MainContent_lblExemptionAmt1")).Text;
                ExemptionAmtArr[1] = driver.FindElement(By.Id("MainContent_lblExemptionAmt2")).Text;
                ExemptionAmtArr[2] = driver.FindElement(By.Id("MainContent_lblExemptionAmt3")).Text;

                TaxableValueArr[0] = driver.FindElement(By.Id("MainContent_lblTaxableValue1")).Text;
                TaxableValueArr[1] = driver.FindElement(By.Id("MainContent_lblTaxableValue2")).Text;
                TaxableValueArr[2] = driver.FindElement(By.Id("MainContent_lblTaxableValue3")).Text;

                System.Data.DataTable _dtAssesed = new System.Data.DataTable();

                _dtAssesed.Columns.Add("TaxAssdYear", typeof(String));
                _dtAssesed.Columns.Add("AssesedVal", typeof(String));
                _dtAssesed.Columns.Add("ExemptionAmt", typeof(String));
                _dtAssesed.Columns.Add("TaxableValue", typeof(String));

                for (int i = 0; i < 3; i++)
                {
                    DataRow drAss = _dtAssesed.NewRow();
                    drAss["TaxAssdYear"] = TaxYrAssArr[i];
                    drAss["AssesedVal"] = AssdValArr[i];
                    drAss["ExemptionAmt"] = ExemptionAmtArr[i];
                    drAss["TaxableValue"] = TaxableValueArr[i];
                    _dtAssesed.Rows.Add(drAss);
                }
                //End-----------------------------Assesed value ------------------------------------//////


                //Start-----------------------------Taxes ------------------------------------//////
                string[] TaxesTaxArr = new string[3];
                string[] AdValoremArr = new string[3];
                string[] NonAdValoremArr = new string[3];
                string[] TaxesArr = new string[3];

                TaxesTaxArr[0] = driver.FindElement(By.Id("MainContent_lblTaxesTaxYr1")).Text;
                TaxesTaxArr[1] = driver.FindElement(By.Id("MainContent_lblTaxesTaxYr2")).Text;
                TaxesTaxArr[2] = driver.FindElement(By.Id("MainContent_lblTaxesTaxYr3")).Text;

                AdValoremArr[0] = driver.FindElement(By.Id("MainContent_lblAdValorem1")).Text;
                AdValoremArr[1] = driver.FindElement(By.Id("MainContent_lblAdValorem2")).Text;
                AdValoremArr[2] = driver.FindElement(By.Id("MainContent_lblAdValorem3")).Text;

                NonAdValoremArr[0] = driver.FindElement(By.Id("MainContent_lblNonAdValorem1")).Text;
                NonAdValoremArr[1] = driver.FindElement(By.Id("MainContent_lblNonAdValorem2")).Text;
                NonAdValoremArr[2] = driver.FindElement(By.Id("MainContent_lblNonAdValorem3")).Text;

                TaxesArr[0] = driver.FindElement(By.Id("MainContent_lblTaxes1")).Text;
                TaxesArr[1] = driver.FindElement(By.Id("MainContent_lblTaxes2")).Text;
                TaxesArr[2] = driver.FindElement(By.Id("MainContent_lblTaxes3")).Text;

                System.Data.DataTable _dtTaxes = new System.Data.DataTable();

                _dtTaxes.Columns.Add("TaxesTax", typeof(String));
                _dtTaxes.Columns.Add("AdValorem", typeof(String));
                _dtTaxes.Columns.Add("NonAdValorem", typeof(String));
                _dtTaxes.Columns.Add("TotalTaxes", typeof(String));

                for (int i = 0; i < 3; i++)
                {
                    DataRow drTax = _dtTaxes.NewRow();
                    drTax["TaxesTax"] = TaxesTaxArr[i];
                    drTax["AdValorem"] = AdValoremArr[i];
                    drTax["NonAdValorem"] = NonAdValoremArr[i];
                    drTax["TotalTaxes"] = TaxesArr[i];
                    _dtTaxes.Rows.Add(drTax);
                }
                //End-----------------------------Taxes ------------------------------------//////
                Int64 PalmBeachLPCaseID = 634;
                _vmcase.SavePalmPbcDetails(PalmBeachLPCaseID, PCN, OfficialBook, SaleDate, Subdiv, LegalDesc, _dtSaleinfo, Units, TotalSqft, Acres, Usecode, Zoning, _dtAppraisal, _dtAssesed, _dtTaxes, null, null);

                Thread.Sleep(1000);

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        private void GetPalmListDetailsByCase()
        {
            FirefoxProfile ffprofile = new FirefoxProfile();
            IWebDriver driver = new FirefoxDriver(ffprofile);
            try
            {
                driver.Manage().Window.Maximize();

                driver.Navigate().GoToUrl("https://applications.mypalmbeachclerk.com/eCaseView/search.aspx");
                Thread.Sleep(1000);
                //driver.FindElement(By.Id("cphBody_ibGuest")).Click();
                //Thread.Sleep(30000);
                //driver.FindElement(By.Id("cphBody_cmdCaptcha")).Click();
                //Thread.Sleep(1000);
                string PalmCaseNumber = "502016CA014421";
                driver.FindElement(By.Id("cphBody_gvSearch_txtParameter_0")).SendKeys(PalmCaseNumber);
                string CourtType = "Circuit Civil";
                driver.FindElement(By.Id("cphBody_gvSearch_cmbParameterPostBack_9")).SendKeys(CourtType);
                driver.FindElement(By.Id("cphBody_gvSearch_cmbParameterPostBack_9")).Click();
                Thread.Sleep(1000);
                driver.FindElement(By.Id("cphBody_cmdSearch")).Click();
                //string CASE = "";

                IWebElement EventsDocument = driver.FindElement(By.Id("cphBody_gvResults"));
                var innerHtml1 = EventsDocument.GetAttribute("innerHTML");

                string html = @"<html><body>" + innerHtml1.ToString() + "</body></html>";
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(html);
                var nodes = doc.DocumentNode.SelectNodes("//tr");
                Thread.Sleep(1000);

                System.Data.DataTable _dtCaseView = new System.Data.DataTable();

                _dtCaseView.Columns.Add("CaseNumber", typeof(String));
                _dtCaseView.Columns.Add("CourtType", typeof(String));
                _dtCaseView.Columns.Add("CaseType", typeof(String));
                _dtCaseView.Columns.Add("ArrestDate", typeof(String));
                _dtCaseView.Columns.Add("FileDate", typeof(String));
                _dtCaseView.Columns.Add("PartyType", typeof(String));
                _dtCaseView.Columns.Add("FullName", typeof(String));
                _dtCaseView.Columns.Add("DOB", typeof(String));
                _dtCaseView.Columns.Add("Status", typeof(String));


                for (int i = 2; i <= nodes.Count; i++)
                {
                    DataRow drCaseView = _dtCaseView.NewRow();
                    int _cntColumn = 1;
                    foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//tr[" + i + "]/td"))
                    {
                        var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                        switch (_cntColumn)
                        {
                            case 1:
                                drCaseView["CaseNumber"] = _text;
                                break;
                            case 2:
                                drCaseView["CourtType"] = _text;
                                break;
                            case 3:
                                drCaseView["CaseType"] = _text;
                                break;
                            case 4:
                                drCaseView["ArrestDate"] = _text;
                                break;
                            case 5:
                                drCaseView["FileDate"] = _text;
                                break;
                            case 6:
                                drCaseView["PartyType"] = _text;
                                break;
                            case 7:
                                drCaseView["FullName"] = _text;
                                break;
                            case 8:
                                drCaseView["DOB"] = _text;
                                break;
                            case 9:
                                drCaseView["Status"] = _text;
                                break;

                        }

                        _cntColumn++;
                    }
                    _dtCaseView.Rows.Add(drCaseView);

                }
                // Save Caseviewlist 
                //_vmcase.SavePalmBeachcasesList(_dtCaseView);

            }
            catch (Exception ex)
            {
                string _ex = ex.ToString();
            }
        }
        public void ReadPalmPDF()
        {
            try
            {
                List<tblPalmBeach> _lstPlam = db.tblPalmBeaches.AsEnumerable().Where(x => x.IsPDFRead == true).ToList();
                if (_lstPlam != null && _lstPlam.Count > 0)
                {
                    foreach (var _Palm in _lstPlam)
                    {
                        int _PalmID = Convert.ToInt32(_Palm.PalmBeachLPCaseID);

                        string PdfFilePath = "C:\\PDfFiles\\PalmBeach\\" + 649 + "-Palm.PDF";
                        string Foldername = 649 + "-PalmBeach";
                        string directoryPath = "C:\\PdfToImage\\" + Foldername;
                        if (!System.IO.Directory.Exists(directoryPath))
                        {
                            Directory.CreateDirectory(directoryPath);
                        }
                        //Get Image From PDF Files In ArrayList 
                        if (System.IO.File.Exists(PdfFilePath))
                        {
                            ConvertPdftoImage(PdfFilePath, Foldername);
                            ReadPamAddressByPDF(649);
                        }
                    }
                }
            }
            catch
            {

            }
        }

        public void ReadPamAddressByPDF(int _PalmID)
        {
            try
            {
                string extractText = "";
                foreach (var _Imagename in myImages)
                {
                    // Read Text From Image 
                    string Foldername = _PalmID + "-PalmBeach";
                    string fImagename = "C:\\PdfToImage\\" + Foldername + "\\" + _Imagename;
                    extractText += this.ExtractTextFromImage(fImagename) + System.Environment.NewLine;
                }
                myImages.Clear();
                string[] numbers = Regex.Split(extractText, @"\D+");
                string _NewCaseNumber = "";
                if (extractText.Contains("XXXX"))
                {
                    var txtfilter = extractText.Split(new string[] { "XXXX" }, StringSplitOptions.None);
                    string mystring = txtfilter[0].ToString();
                    string _CaseNo = mystring.Substring(Math.Max(0, mystring.Length - 15));
                    _NewCaseNumber = Regex.Replace(_CaseNo, @"\W+", "");
                }
                if (extractText.Contains("Division:"))
                {
                    var txtCase = extractText.Split(new string[] { "Division:" }, StringSplitOptions.None);
                    string Filtersplit = txtCase[0].ToString().Split(new string[] { "CASE NUMBER:" }, StringSplitOptions.None)[1].Trim().Replace(" ", "");
                    _NewCaseNumber = Filtersplit.Substring(0, 14);
                }


                foreach (string value1 in numbers)
                {
                    Regex rex = new Regex(@"\b[0-9]{5}(?:-[0-9]{4})?\b");

                    if (rex.IsMatch(value1) == true)
                    {
                        PDFAddress _Pdfadd = new PDFAddress();

                        var filtrtr = value1.ToString();
                        var txtfilter = extractText.Split(new string[] { filtrtr }, StringSplitOptions.None);

                        //---------------- Start Get Any Type Address-----------//

                        string mystring = txtfilter[0].ToString();
                        string newAddress = mystring.Substring(Math.Max(0, mystring.Length - 100));
                        string Address = newAddress + " " + value1;
                        PalmBeachAddress.Add(Address);

                        //----------------End Get Any Type Address-----------//
                    }
                }

                //_vmcase.SavePalmAddress(PalmBeachAddress, _PalmID, PalmCaseNumber);
                PalmBeachAddress.Clear();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        //public void ReadPamAddressByPDF(int _PalmID)
        //{
        //    try
        //    {
        //        string extractText = "";
        //        foreach (var _Imagename in myImages)
        //        {
        //            // Read Text From Image 
        //            string Foldername = _PalmID + "-PalmBeach";
        //            string fImagename = Server.MapPath("~/PdfToImage/" + Foldername + "/" + _Imagename);
        //            //Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\PdfToImage\\" + Foldername + "\\" + _Imagename;
        //            extractText += this.ExtractTextFromImage(fImagename) + System.Environment.NewLine;
        //        }
        //        myImages.Clear();
        //        string[] numbers = Regex.Split(extractText, @"\D+");
        //        var txtCase = extractText.Split(new string[] { "Division:" }, StringSplitOptions.None);
        //        string PalmCaseNumber = txtCase[0].ToString().Split(new string[] { "CASE NUMBER:" }, StringSplitOptions.None)[1].Trim().Replace(" ", "");
        //        string Filtersplit = PalmCaseNumber.Substring(0, 14);
        //        foreach (string value1 in numbers)
        //        {
        //            Regex rex = new Regex(@"\b[0-9]{5}(?:-[0-9]{4})?\b");

        //            if (rex.IsMatch(value1) == true)
        //            {
        //                PDFAddress _Pdfadd = new PDFAddress();

        //                var filtrtr = value1.ToString();
        //                var txtfilter = extractText.Split(new string[] { filtrtr }, StringSplitOptions.None);

        //                //---------------- Start Get Any Type Address-----------//

        //                string mystring = txtfilter[0].ToString();
        //                string newAddress = mystring.Substring(Math.Max(0, mystring.Length - 100));
        //                string Address = newAddress + " " + value1;
        //                PalmBeachAddress.Add(Address);

        //                //----------------End Get Any Type Address-----------//
        //            }
        //        }

        //        //_vmcase.SavePalmAddress(PalmBeachAddress, _PalmID, PalmCaseNumber);
        //        PalmBeachAddress.Clear();

        //    }
        //    catch
        //    {

        //    }
        //}

        public void GetPdfFromPlamBeach()
        {
            try
            {
                List<tblPalmBeach> _lstPlam = db.tblPalmBeaches.AsEnumerable().Where(x => x.IsPDFDownloaded == true).ToList();
                if (_lstPlam != null && _lstPlam.Count > 0)
                {
                    FirefoxProfile ffprofile = new FirefoxProfile();
                    IWebDriver driver = new FirefoxDriver(ffprofile);

                    driver.Manage().Window.Maximize();
                    driver.Navigate().GoToUrl("http://oris.co.palm-beach.fl.us/or_web1/or_sch_1.asp");
                    Thread.Sleep(1000);
                    By xPathEvents1 = By.XPath("//div[@id='window1']/table/tbody/tr/td/table/tbody/tr[1]/td[3]");

                    driver.FindElements(xPathEvents1)[0].FindElement(By.TagName("a")).Click();
                    Thread.Sleep(1000);
                    string DocType = "LP";
                    By xPathEventstnSrch12 = By.XPath("//form[@name='formSearch2']/font[2]/input[1]");
                    By xPathEventstnSrch1 = By.XPath("//form[@name='formSearch2']/input[2]");

                    driver.FindElements(xPathEventstnSrch12)[0].SendKeys(DocType);
                    driver.FindElements(xPathEventstnSrch1)[0].Click();
                    Thread.Sleep(1500);

                    driver.SwitchTo().Alert().Accept();

                    Thread.Sleep(2000);

                    driver.SwitchTo().Window(driver.CurrentWindowHandle);
                    driver.SwitchTo().DefaultContent();
                    foreach (var _palm in _lstPlam)
                    {

                        driver.Navigate().GoToUrl(_palm.DetailsByUrl);
                        Thread.Sleep(1000);

                        string GetImage = Server.MapPath("~/Images/GetImage.png");

                        SikuliAction.Click(GetImage);

                        Thread.Sleep(2000);
                        string ViewPdf = Server.MapPath("~/Images/ViewPdf.png");

                        SikuliAction.Click(ViewPdf);

                        driver.SwitchTo().Alert().Accept();

                        Thread.Sleep(2000);

                        string Download = Server.MapPath("~/Images/Download.png");

                        SikuliAction.Click(Download);

                        Thread.Sleep(2000);
                        SendKeys.SendWait(@"{Enter}");

                        MovePalmBeachPdfs(_palm);

                    }
                    driver.Quit();
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message;

            }
        }

        private static void MovePalmBeachPdfs(tblPalmBeach _palm)
        {
            string directoryPath = "C:\\PDfFiles\\PalmBeach";
            if (!System.IO.Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            string root = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);

            string downloadPath = root + "\\Downloads";

            string[] files11 = System.IO.Directory.GetFiles(downloadPath);
            string[] filteredFiles = files11.Where(w => w.Contains("_img.pdf")).ToArray();
            foreach (var _file in filteredFiles)
            {
                if (System.IO.Path.GetExtension(_file).ToLower() == ".pdf")
                {
                    var fileName = System.IO.Path.GetFileName(_file);
                    string FullFileNmae = _palm.PalmBeachLPCaseID + fileName;
                    //string FullFileNmae = "1-" + fileName;
                    string newFilePath = "C:\\PDfFiles\\PalmBeach\\" + FullFileNmae;
                    string downloadedPath = _file;
                    string oldFilePath = downloadedPath; // Full path of old file
                    //Replace Pdf Form Download To C Drive Folder PDfFiles\\OfficialPDF
                    if (System.IO.File.Exists(downloadedPath))
                    {
                        DirectoryInfo dInfo = new DirectoryInfo(downloadedPath);
                        DirectorySecurity dSecurity = dInfo.GetAccessControl();
                        dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
                        dInfo.SetAccessControl(dSecurity);

                        if (System.IO.File.Exists(newFilePath))
                        {
                            System.IO.File.Delete(newFilePath);
                        }
                        System.IO.File.Copy(oldFilePath, newFilePath);
                        System.IO.File.Delete(oldFilePath);
                    }

                }
            }
        }

        private void SearchPalmBeachRecord()
        {
            try
            {

                FirefoxProfile ffprofile = new FirefoxProfile();
                IWebDriver driver = new FirefoxDriver(ffprofile);

                driver.Manage().Window.Maximize();

                driver.Navigate().GoToUrl("http://oris.co.palm-beach.fl.us/or_web1/or_sch_1.asp");
                Thread.Sleep(1000);
                By xPathEvents1 = By.XPath("//div[@id='window1']/table/tbody/tr/td/table/tbody/tr[1]/td[3]");

                driver.FindElements(xPathEvents1)[0].FindElement(By.TagName("a")).Click();
                Thread.Sleep(1000);
                string DocType = "LP";
                By xPathEventstnSrch12 = By.XPath("//form[@name='formSearch2']/font[2]/input[1]");
                By xPathEventstnSrch1 = By.XPath("//form[@name='formSearch2']/input[2]");

                driver.FindElements(xPathEventstnSrch12)[0].SendKeys(DocType);
                driver.FindElements(xPathEventstnSrch1)[0].Click();
                Thread.Sleep(1500);
                By xPathEventAllResult = By.XPath("//form[@name='formFilter1']/table[4]/tbody");
                By xPathEventResult = By.XPath("//form[@name='formFilter1']/table[3]/tbody/tr[3]/td[2]/font/select");
                By xPathEventSelect = By.XPath("//form[@name='formFilter1']/table[3]/tbody/tr[3]/td[2]/font");
                By xPathEventRecord = By.XPath("//form[@name='formFilter1']/table[3]/tbody/tr[3]/td[2]/font");

                if (isAlertPresent(driver))
                {
                    driver.SwitchTo().Alert();
                    driver.SwitchTo().Alert().Accept();
                    driver.SwitchTo().DefaultContent();
                    //IAlert alert = driver.SwitchTo().Alert();email
                    //alert.Accept();
                }



                var TtlRecords = driver.FindElements(xPathEventRecord)[0].Text;

                string[] textchange = TtlRecords.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                Thread.Sleep(1500);

                System.Data.DataTable _dt = new System.Data.DataTable();
                _dt.Columns.Add("DetailsByUrl", typeof(String));
                _dt.Columns.Add("Name", typeof(String));
                _dt.Columns.Add("CrossName", typeof(String));
                _dt.Columns.Add("Date", typeof(String));
                _dt.Columns.Add("Type", typeof(String));
                _dt.Columns.Add("Book", typeof(String));
                _dt.Columns.Add("Page", typeof(String));
                _dt.Columns.Add("CFN", typeof(String));
                _dt.Columns.Add("Legal", typeof(String));

                for (int k = 1; k < textchange.Length; k++)
                {
                    driver.FindElement(xPathEventResult).Click();
                    if (k > 1)
                    {
                        SelectElement ss = new SelectElement(driver.FindElements(xPathEventSelect)[0].FindElement(By.TagName("select")));

                        foreach (IWebElement element in ss.Options)
                        {
                            if (element.Text == textchange[k].ToString())
                            {
                                element.Click();
                                Thread.Sleep(2000);
                                SendKeys.SendWait(@"{Enter}");
                                break;
                            }
                        }
                    }

                    IWebElement EventsDocumentAllResult = driver.FindElement(xPathEventAllResult);
                    var innerHtmlAllResult = EventsDocumentAllResult.GetAttribute("innerHTML");

                    string htmlAllResult = @"<html><body>" + innerHtmlAllResult.ToString() + "</body></html>";
                    HtmlAgilityPack.HtmlDocument _docAllResult = new HtmlAgilityPack.HtmlDocument();
                    _docAllResult.LoadHtml(htmlAllResult);

                    int _Rowscnt = _docAllResult.DocumentNode.SelectNodes("//tr").Count;

                    for (int i = 2; i < _Rowscnt; i++)
                    {
                        DataRow dr = _dt.NewRow();
                        int _countColumn = 1;

                        foreach (HtmlNode node in _docAllResult.DocumentNode.SelectNodes("//tr[" + i + "]/td[1]/font/a"))
                        {
                            string Url = node.Attributes["href"].Value;//.Replace("\r\n\t","").Replace("\t","");
                            string _Url = Regex.Replace(Url, @"\t|\n|\r", "");
                            string FilterUrl = Regex.Replace(_Url, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", "&").Trim();
                            dr["DetailsByUrl"] = "http://oris.co.palm-beach.fl.us/or_web1/" + FilterUrl;
                            break;
                        }
                        foreach (HtmlNode node in _docAllResult.DocumentNode.SelectNodes("//tr[" + i + "]/td"))
                        {
                            string Text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                            switch (_countColumn)
                            {
                                case 2:
                                    dr["Name"] = Text;
                                    break;
                                case 3:
                                    dr["CrossName"] = Text;
                                    break;
                                case 4:
                                    dr["Date"] = Text;
                                    break;
                                case 5:
                                    dr["Type"] = Text;
                                    break;
                                case 6:
                                    dr["Book"] = Text;
                                    break;
                                case 7:
                                    dr["Page"] = Text;
                                    break;
                                case 8:
                                    dr["CFN"] = Text;
                                    break;
                                case 9:
                                    dr["Legal"] = Text;
                                    break;

                            }

                            _countColumn++;
                        }
                        _dt.Rows.Add(dr);
                    }
                }

                _vmcase.SavePalmBeach(_dt);// Save Palm Records in Table

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }
        public Boolean isAlertPresent(IWebDriver driver)
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            } // try
            catch (Exception e)
            {
                string msg = e.Message;
                return false;
            } // catch
        }

        ArrayList _MultiFolio = new ArrayList();
       

       

        public string IsCorrectValue(string _text)
        {
            string Result = "";
            if (!System.Text.RegularExpressions.Regex.IsMatch(_text, @"[^0-9$,]"))
            {
                Result = _text;
                return Result;
            }
            else
            {
                Result = "0";
                return Result;
            }

        }

        public void TestDemoForBCPA()
        {
            try
            {
                var LpCaseforsearch = _vmcase.GetLpCaseforsearch();

                foreach (var _Lp in LpCaseforsearch)
                {
                    SrchFolioPropertyDetails(_Lp);
                }


            }
            catch
            {

            }

        }

        public void SrchFolioPropertyDetails(SrchDefandant _Lp)
        {
            string Defendant = _Lp.Defendant.ToString();



        }
        public ActionResult Test1()
        {
            var Foldername = "Test";

            var sourcePath1 = "C:/PDfFiles/";
            var CaseNumber = "Case10792";
            if (System.IO.Directory.Exists(sourcePath1))
            {
                string[] _Dir = System.IO.Directory.GetDirectories(sourcePath1);

                foreach (string FilesPath in _Dir)
                {
                    string dirName = new DirectoryInfo(FilesPath).Name;
                    if (dirName == Foldername)
                    {
                        string[] files11 = System.IO.Directory.GetFiles(FilesPath);
                        string[] filteredFiles = files11.Where(w => w.Contains(CaseNumber)).ToArray();

                        string directoryPath = Server.MapPath("~/PdfToImage/" + Foldername + "");
                        Directory.CreateDirectory(directoryPath);

                        foreach (var _file in filteredFiles)
                        {
                            if (System.IO.Path.GetExtension(_file) == ".pdf")
                            {
                                var fileName = System.IO.Path.GetFileName(_file);
                                var PdfFilePath = "C:/PDfFiles/" + Foldername + "/" + fileName + "";// Server.MapPath("~/PDfFiles/" + Foldername + "/" + fileName + "");
                                ConvertPdftoImage(PdfFilePath, Foldername);
                            }
                        }
                        string _OcrText = "";
                        foreach (var _Imagename in myImages)
                        {
                            string fImagename = System.IO.Path.Combine(Server.MapPath("~/PdfToImage/" + Foldername + "/" + _Imagename));
                            string extractText = this.ExtractTextFromImage(fImagename);
                            _OcrText = extractText.Replace(Environment.NewLine, "<br />");
                           // string pattern = @"\$([^\$]*)\$";

                            string[] numbers = Regex.Split(extractText, @"(\w*[^$])");
                            string[] numbers2 = Regex.Split(extractText, @"\$*([^\$]*)\$");

                            foreach (var _splititem in numbers2)
                            {
                                if (_splititem != "")
                                {
                                    string str = _splititem.Substring(0, 10);


                                    string[] numbers22 = Regex.Split(str, "[^\\d.]");
                                    foreach (var _dollr in numbers22)
                                    {
                                        if (_dollr != "")
                                        {
                                            var Currency = "$" + _dollr + "";
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }

            return View();
        }


        private void ConvertPdftoImage(string filePath, string Foldername)
        {

            var _pdfReader = new PdfReader(filePath); //other filestream etc
            //string workFile;
            StringBuilder text = new StringBuilder();
            using (PdfReader reader = new PdfReader(filePath))
            {


                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    string _Imagename = LoadImage(filePath, i, Foldername);
                    myImages.Add(_Imagename);
                }
            }
        }

        public string LoadImage(string InputPDFFile, int PageNumber, string Foldername)
        {

            string outImageName = System.IO.Path.GetFileNameWithoutExtension(InputPDFFile);
            outImageName = outImageName + "_" + PageNumber.ToString() + "_.png";

            //var imagename = Server.MapPath(@"~/PdfToImage/" + Foldername + "/" + outImageName);
            //string directoryPath = "C:/PdfToImage/" + Folderpath + "";// Server.MapPath("~/PDfFiles/" + Folderpath + "");
            //var imagename = "C:/PdfToImage/" + Foldername + "/" + outImageName;
            var imagename = "C:\\PdfToImage\\" + Foldername + "\\" + outImageName;

            if (System.IO.File.Exists(imagename))
            {
                System.IO.File.Delete(imagename);
            }
            GhostscriptPngDevice dev = new GhostscriptPngDevice(GhostscriptPngDeviceType.Png256);
            dev.GraphicsAlphaBits = GhostscriptImageDeviceAlphaBits.V_4;
            dev.TextAlphaBits = GhostscriptImageDeviceAlphaBits.V_4;
            dev.ResolutionXY = new GhostscriptImageDeviceResolution(290, 290);
            dev.InputFiles.Add(InputPDFFile);
            dev.Pdf.FirstPage = PageNumber;
            dev.Pdf.LastPage = PageNumber;
            dev.CustomSwitches.Add("-dDOINTERPOLATE");
            //dev.OutputPath = Server.MapPath(@"~/PdfToImage/" + Foldername + "/" + outImageName);
            dev.OutputPath = "C:\\PdfToImage\\" + Foldername + "\\" + outImageName;
            dev.Process();
            Session["outImageName"] = outImageName;
            return outImageName;
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Unable to release the Object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        public ActionResult Index()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                return View();
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult IndexLogin()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                return View();
            }
            return RedirectToAction("Index", "Home");
        }
        public static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            w.WriteLine("  :");
            w.WriteLine("  :{0}", logMessage);
            w.WriteLine("-------------------------------");
        }

        public ActionResult BeginSearch(string BusinessName, string casecategory, string Fromdate, string Todate)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                var binary = new FirefoxBinary(Server.MapPath("~/firefox.exe"));
                string path = Server.MapPath("~/firefox.exe");

                FirefoxProfile ffprofile = new FirefoxProfile();
                //using (StreamWriter w = System.IO.File.AppendText(Server.MapPath("~/Broward.txt")))
                //{
                //    Log("1 : FirefoxProfile", w);
                //}
                //var options = new InternetExplorerOptions();
                //options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
                //IWebDriver driver = new InternetExplorerDriver(options);

                IWebDriver driver = new FirefoxDriver(ffprofile);

                //string BrowardDesk = Server.MapPath("~/BrowardDesk.exe");

                //Process process = new Process();
                //process.StartInfo.FileName = BrowardDesk;

                //IWebDriver driver = new process.Start();

                //using (Process exeProcess = Process.Start(startInfo))
                //{

                //    ..exeProcess.WaitForExit();
                //}

                // new FirefoxDriver(ffprofile);
                //using (StreamWriter w = System.IO.File.AppendText(Server.MapPath("~/Broward.txt")))
                //{
                //    Log("2 : IWebDriver :" + driver , w);
                //}
                // IWebDriver driver = new FirefoxDriver(ffprofile);

                try
                {
                    //using (StreamWriter w = System.IO.File.AppendText(Server.MapPath("~/Broward.txt")))
                    //{
                    //    Log("3 : Try", w);
                    //}
                    //IWebDriver driver = new ChromeDriver(@"C:\Users\ATS1\Documents\Visual Studio 2013\Projects\SharkAppWeb\SharkAppWeb");
                    driver.Manage().Window.Maximize();

                    driver.Navigate().GoToUrl("https://www.browardclerk.org/Web2/");
                    Thread.Sleep(1000);
                    //string imagePath = Server.MapPath("~/Images/BussinessName.png");

                    //SikuliAction.Click(imagePath);

                    IWebElement EventsDocument = driver.FindElement(By.Id("myTabStandard"));
                    var innerHtml1 = EventsDocument.GetAttribute("innerHTML");

                    string html = @"<html><body>" + innerHtml1.ToString() + "</body></html>";
                    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(html);
                    var nodes = doc.DocumentNode.SelectNodes("//li/a");

                    //var count = 0;
                    //var Chk = 0;
                    foreach (HtmlNode node in nodes)
                    {
                        if (node.InnerText == "Business Name")
                        {
                            var nodepath = node.XPath;
                            var npath = nodepath.Split(new string[] { "body[1]" }, StringSplitOptions.None);
                            var Xpath = "//div[@id='myTabStandard']" + npath[1] + "";
                            //html[1]/body[1]/div[1]/div[1]/ul[1]/li[3]/a[1]

                            driver.FindElement(By.XPath(Xpath)).Click();
                        }
                    }

                    //string DropDwnAll = Server.MapPath("~/Images/DropDwnAll.png");

                    //SikuliAction.Click(DropDwnAll);
                    //string CivilClick = "";
                    string Foldername = "";
                    if (casecategory == "CR,FAM,CV,PR")
                    {
                        Foldername = "Allcategory";
                        driver.FindElement(By.Id("CaseCategoryKeys2")).FindElement(By.CssSelector("option[value='" + casecategory + "']")).Click();
                        //CivilClick = Server.MapPath("~/Images/Allcategory.png");
                    }
                    else if (casecategory == "CV")
                    {
                        Foldername = "Civil";
                        driver.FindElement(By.Id("CaseCategoryKeys2")).FindElement(By.CssSelector("option[value='" + casecategory + "']")).Click();
                        //CivilClick = Server.MapPath("~/Images/CivilClick1.png");
                    }
                    else if (casecategory == "FAM")
                    {
                        Foldername = "Family";
                        driver.FindElement(By.Id("CaseCategoryKeys2")).FindElement(By.CssSelector("option[value='" + casecategory + "']")).Click();
                        //CivilClick = Server.MapPath("~/Images/Family.png");
                    }
                    else if (casecategory == "PR")
                    {
                        Foldername = "Felony";
                        driver.FindElement(By.Id("CaseCategoryKeys2")).FindElement(By.CssSelector("option[value='" + casecategory + "']")).Click();
                        //CivilClick = Server.MapPath("~/Images/Felony.png");
                    }
                    else if (casecategory == "FEL")
                    {
                        Foldername = "Probate";
                        driver.FindElement(By.Id("CaseCategoryKeys2")).FindElement(By.CssSelector("option[value='" + casecategory + "']")).Click();
                        //CivilClick = Server.MapPath("~/Images/Probate.png");
                    }
                    else
                    {
                        Foldername = "TrafficMis";
                        driver.FindElement(By.Id("CaseCategoryKeys2")).FindElement(By.CssSelector("option[value='" + casecategory + "']")).Click();
                        //CivilClick = Server.MapPath("~/Images/TrafficMis.png");
                    }
                    //SikuliAction.Click(CivilClick);

                    //string TextTransCapital = Server.MapPath("~/Images/BussinessTrans.png");


                    //SikuliAction.Click(TextTransCapital);

                    string pasteString = BusinessName.ToString();

                    //driver.FindElement(By.Id("BusiName")).SendKeys("TransCapital");
                    driver.FindElement(By.Id("BusiName")).SendKeys(BusinessName);

                    if (Fromdate != null && Fromdate != "")
                    {
                        driver.FindElement(By.Id("filingDateOnOrAfterB")).SendKeys(Fromdate);
                        driver.FindElement(By.Id("filingDateOnOrBeforeB")).SendKeys(Todate);
                    }

                    string Search = Server.MapPath("~/Images/Search.png");
                    Thread.Sleep(30000);
                    driver.FindElement(By.Id("BusinessSearchResults")).Click();

                    // SikuliAction.Click(Search);
                    Thread.Sleep(5000);
                    IWebElement _Div = driver.FindElement(By.Id("SearchResultsGrid"));

                    IWebElement _PageText = _Div.FindElement(By.ClassName("k-pager-last"));
                    _PageNumber = Convert.ToInt32(_PageText.GetAttribute("data-page").ToString());

                    IWebElement elements = driver.FindElement(By.TagName("tbody"));

                    string CurrentDate = DateTime.Now.ToString("dd_MM_yyyy_hh_mm");
                    DateTime SearchedDate = Convert.ToDateTime(DateTime.Now);
                    string Folderpath = "" + Foldername + "_" + CurrentDate + "";
                    string directoryPath = "C:/PDfFiles/" + Folderpath + "";// Server.MapPath("~/PDfFiles/" + Folderpath + "");
                    Directory.CreateDirectory(directoryPath);
                    _vmcase.SaveSeacrhHistory(Convert.ToDateTime(Todate), Convert.ToDateTime(Fromdate), Foldername, SearchedDate, pasteString);
                    //_Div.FindElement(By.ClassName("k-pager-last")).Click();
                    GetpdfFiles(Folderpath, driver, "1");

                    for (int i = 1; i < _PageNumber; i++)
                    {
                        int num = Convert.ToInt32(i + 1);
                        Thread.Sleep(1000);
                        var currentWindow1 = driver.CurrentWindowHandle;
                        var availableWindows1 = new List<string>(driver.WindowHandles);
                        foreach (string w in availableWindows1)
                        {
                            IWebDriver popup = null;
                            if (w != currentWindow1)
                            {
                                driver.SwitchTo().Window(w);
                                popup = driver.SwitchTo().Window(w);
                                popup.Close();
                                driver.SwitchTo().Window(currentWindow1);
                                driver.FindElement(By.ClassName("k-i-arrow-e")).Click();
                            }
                        }

                        GetpdfFiles(Folderpath, driver, num.ToString());

                        if (_PageNumber == Convert.ToInt32(num))
                        {
                            foreach (string w in availableWindows1)
                            {
                                IWebDriver popup = null;
                                if (w != currentWindow1)
                                {
                                    driver.SwitchTo().Window(w);
                                    popup = driver.SwitchTo().Window(w);
                                    popup.Close();
                                    driver.SwitchTo().Window(currentWindow1);
                                    driver.FindElement(By.ClassName("k-pager-first")).Click();
                                    popup = driver.SwitchTo().Window(currentWindow1);
                                    popup.Close();
                                    break;
                                }
                            }
                        }
                    }

                    var currentWindow2 = driver.CurrentWindowHandle;
                    var availableWindows2 = new List<string>(driver.WindowHandles);
                    foreach (string w in availableWindows2)
                    {
                        IWebDriver popup2 = null;
                        if (w != currentWindow2)
                        {
                            driver.SwitchTo().Window(w);
                            popup2 = driver.SwitchTo().Window(w);
                            popup2.Close();
                        }
                    }
                    driver.Quit();
                    var result = "Success";
                    //return RedirectToAction("CaseView","Home");
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    //using (StreamWriter w = System.IO.File.AppendText(Server.MapPath("~/Broward.txt")))
                    //{
                    //    Log("4 : catch " + ex.Message.ToString(), w);
                    //}

                    var currentWindow3 = driver.CurrentWindowHandle;
                    var availableWindows3 = new List<string>(driver.WindowHandles);
                    foreach (string w in availableWindows3)
                    {
                        IWebDriver popup3 = null;
                        if (w != currentWindow3)
                        {
                            driver.SwitchTo().Window(w);
                            popup3 = driver.SwitchTo().Window(w);
                            popup3.Close();
                        }
                    }
                    driver.Quit();
                    return Json("Please try Again", JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");
        }


        public ActionResult BusinessSearchHistory()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                //var sourcePath = "C:/PDfFiles/";// Server.MapPath("~/PDfFiles/");
                //var Result = "";

                List<SearchHistory> _searchhistorytable = db.SearchHistories.OrderByDescending(x => x.SearchID).ToList();

                //if (System.IO.Directory.Exists(sourcePath))
                //{
                //    string[] _Dir = System.IO.Directory.GetDirectories(sourcePath);

                //    foreach (string FilesPath in _Dir)
                //    {
                //        string dirName = new DirectoryInfo(FilesPath).Name;

                //        string[] files11 = System.IO.Directory.GetFiles(FilesPath);
                //    }
                //}

                return PartialView("_SrchListbyDate", _searchhistorytable);
            }
            return RedirectToAction("Login", "Home");
        }

        public string GetPdfDetailsBussiness(int SearchID)
        {
            SearchHistory _SrchDate = new SearchHistory();
            var getData = db.SearchHistories.Where(x => x.SearchID == SearchID).FirstOrDefault();
            string SrchedType = getData.SearchCourtType;
            DateTime _dt = Convert.ToDateTime(getData.SearchedDate);

            string srcdt = _dt.ToString("dd/MM/yyyy hh:mm");
            Session["Searcheddate"] = _dt;
            ViewBag.Seracheddate = _dt;
            string[] SrchedDate = srcdt.Split('/');
            //var sourcePath = "C:/PDfFiles/";// Server.MapPath("~/PDfFiles/");
            var Foldername = SrchedType + "_" + SrchedDate[0] + "_" + SrchedDate[1] + "_" + SrchedDate[2].Split(' ')[0] + "_" + SrchedDate[2].Split(' ')[1].Split(':')[0] + "_" + SrchedDate[2].Split(' ')[1].Split(':')[1] + "";
            //var Result = "";
            //Session["FolderName"] = Foldername;
            return Foldername;
        }
        public ActionResult SearchedDetails(int SearchID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                SearchHistory _SrchDate = new SearchHistory();
                var getData = db.SearchHistories.Where(x => x.SearchID == SearchID).FirstOrDefault();
                string SrchedType = getData.SearchCourtType;
                DateTime _dt = Convert.ToDateTime(getData.SearchedDate);

                string srcdt = _dt.ToString("dd/MM/yyyy hh:mm");
                Session["Searcheddate"] = _dt;
                ViewBag.Seracheddate = _dt;
                string[] SrchedDate = srcdt.Split('/');
                var sourcePath = "C:/PDfFiles/";// Server.MapPath("~/PDfFiles/");
                var Foldername = SrchedType + "_" + SrchedDate[0] + "_" + SrchedDate[1] + "_" + SrchedDate[2].Split(' ')[0] + "_" + SrchedDate[2].Split(' ')[1].Split(':')[0] + "_" + SrchedDate[2].Split(' ')[1].Split(':')[1] + "";
                //var Result = "";
                Session["FolderName"] = Foldername;
                if (System.IO.Directory.Exists(sourcePath))
                {
                    string[] _Dir = System.IO.Directory.GetDirectories(sourcePath);

                    var FilesPath = sourcePath + Foldername;
                    bool _result = _Dir.Contains(FilesPath.Trim());
                    if (_result == true)
                    {
                        string dirName = new DirectoryInfo(FilesPath).Name;
                        if (dirName == Foldername)
                        {
                            string[] files11 = System.IO.Directory.GetFiles(FilesPath);

                            foreach (var _file in files11)
                            {
                                string FileName = new FileInfo(_file).Name;
                                string FileExtensn = new FileInfo(_file).Extension;
                                if (FileExtensn == ".txt")
                                {
                                    string caseNumber1 = FileName.Split('_')[0].Trim();
                                    _CaseNumber.Add(caseNumber1);
                                }
                                else
                                {
                                    string caseNumber = FileName.Split('-')[0].Trim();
                                    _CaseNumber.Add(caseNumber);
                                }
                            }
                        }
                    }
                }

                var filename = Server.MapPath("~/ExcelFiles/TransCapitalRecords.ods");
                Microsoft.Office.Interop.Excel.Application xlApp;
                Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
                Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
                var missing = System.Reflection.Missing.Value;

                xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlWorkBook = xlApp.Workbooks.Open(filename, false, true, missing, missing, missing, true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, '\t', false, false, 0, false, true, 0);
                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                Microsoft.Office.Interop.Excel.Range xlRange = xlWorkSheet.UsedRange;
                Array myValues = (Array)xlRange.Cells.Value2;

                int vertical = myValues.GetLength(0);
                int horizontal = myValues.GetLength(1);

                System.Data.DataTable dt = new System.Data.DataTable();
                // must start with index = 1
                // get header information
                for (int i = 1; i <= horizontal; i++)
                {
                    dt.Columns.Add(new DataColumn(myValues.GetValue(1, i).ToString()));
                }
                // Get the row information
                for (int a = 2; a <= vertical; a++)
                {
                    object[] poop = new object[horizontal];
                    for (int b = 1; b <= horizontal; b++)
                    {
                        //poop[b - 1] = myValues.GetValue(a, b);
                        string _value = myValues.GetValue(a, b).ToString();
                        var FilterHtml = Regex.Replace(_value, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                        poop[b - 1] = FilterHtml;
                    }
                    DataRow row = dt.NewRow();
                    string popitem = poop[0].ToString();
                    bool _result = _CaseNumber.Contains(popitem.Trim());
                    if (_result == true)
                    {
                        row.ItemArray = poop;
                        dt.Rows.Add(row);
                    }
                }
                ViewBag.CaseTable = dt;
                // assign table to default data grid view

                xlWorkBook.Close(true, missing, missing);
                xlApp.Quit();

                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);

                return View(dt);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ViewOriginalPdf(string CaseNumber)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                var sourcePath = "C:/PDfFiles/";// Server.MapPath("~/PDfFiles/");
                var Result = "";
                string Foldername = Session["FolderName"].ToString();
                if (System.IO.Directory.Exists(sourcePath))
                {
                    string[] _Dir = System.IO.Directory.GetDirectories(sourcePath);

                    var FilesPath = sourcePath + Foldername;
                    bool _result = _Dir.Contains(FilesPath.Trim());
                    if (_result == true)
                    {
                        string dirName = new DirectoryInfo(FilesPath).Name;
                        if (dirName == Foldername)
                        {
                            string[] files11 = System.IO.Directory.GetFiles(FilesPath);
                            string[] filteredFiles = files11.Where(w => w.Contains(CaseNumber)).ToArray();
                            foreach (var _file in filteredFiles)
                            {
                                if (System.IO.Path.GetExtension(_file) == ".PDF")
                                {
                                    var fileName = System.IO.Path.GetFileName(_file);
                                    string FileCaseNo = fileName.Split('_')[0];
                                    //var _htmlpdfFile = "<a href='javascript:PdfView('ggg')' onclick='PdfView(" + FileCaseNo + ")' id='anchoriginalpdf'>" + FileCaseNo + "</a></br>";
                                    //var _htmlpdfFile = "<a href='javascript:PdfView(ggg)' id='anchoriginalpdf'>" + FileCaseNo + "</a></br>";
                                    var _htmlpdfFile = "<a class='anchoriginalpdf' data-q_id='" + FileCaseNo + "' id='anchoriginalpdf' href='/Home/ViewPdfInTab?CaseNumber=" + FileCaseNo + "'>" + FileCaseNo + "</a></br>";
                                    _OriginalPdf.Add(_htmlpdfFile);
                                }
                            }

                        }
                    }
                }

                if (_OriginalPdf.Count > 0)
                {
                    return Json(_OriginalPdf, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Result = "No PDF files for case number: " + CaseNumber + "";
                    return Json(Result, JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public FileResult ViewPdfInTab(string CaseNumber)
        {

            var sourcePath = "C:/PDfFiles/";// Server.MapPath("~/PDfFiles/");
            //var Result = "";
            string Foldername = Session["FolderName"].ToString();
            if (System.IO.Directory.Exists(sourcePath))
            {
                string[] _Dir = System.IO.Directory.GetDirectories(sourcePath);
                var FilesPath = sourcePath + Foldername;
                bool _result = _Dir.Contains(FilesPath.Trim());
                if (_result == true)
                {
                    string dirName = new DirectoryInfo(FilesPath).Name;
                    if (dirName == Foldername)
                    {
                        string[] files11 = System.IO.Directory.GetFiles(FilesPath);
                        string[] filteredFiles = files11.Where(w => w.Contains(CaseNumber)).ToArray();
                        foreach (var _file in filteredFiles)
                        {

                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + CaseNumber + ".pdf");
                            Response.TransmitFile(_file);
                            Response.End();
                        }
                    }
                }
            }

            return null; //Json(Result, JsonRequestBehavior.AllowGet);

        }


        public ActionResult RefreshCaseList()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                var binary = new FirefoxBinary(@"C:\Users\ATS1\Documents\Visual Studio 2013\Projects\SharkAppWeb\SharkAppWeb\firefox.exe");
                //string path = @"C:\Users\ATS1\Documents\Visual Studio 2013\Projects\SharkAppWeb\SharkAppWeb\firefox.exe";
                FirefoxProfile ffprofile = new FirefoxProfile();
                IWebDriver driver = new FirefoxDriver(ffprofile);
                //IWebDriver driver = new ChromeDriver(@"C:\Users\ATS1\Documents\Visual Studio 2013\Projects\SharkAppWeb\SharkAppWeb");
                driver.Manage().Window.Maximize();

                driver.Navigate().GoToUrl("https://www.browardclerk.org/Web2/");
                Thread.Sleep(1000);
                //string imagePath = Server.MapPath("~/Images/BussinessName.png");

                //SikuliAction.Click(imagePath);

                IWebElement EventsDocumenttab = driver.FindElement(By.Id("myTabStandard"));
                var innerHtmltab = EventsDocumenttab.GetAttribute("innerHTML");

                string htmltab = @"<html><body>" + innerHtmltab.ToString() + "</body></html>";
                HtmlAgilityPack.HtmlDocument doctab = new HtmlAgilityPack.HtmlDocument();
                doctab.LoadHtml(htmltab);
                var nodes = doctab.DocumentNode.SelectNodes("//li/a");

                //var count = 0;
                //var Chk = 0;
                foreach (HtmlNode node in nodes)
                {
                    if (node.InnerText == "Business Name")
                    {
                        var nodepath = node.XPath;
                        var npath = nodepath.Split(new string[] { "body[1]" }, StringSplitOptions.None);
                        var Xpath = "//div[@id='myTabStandard']" + npath[1] + "";
                        //html[1]/body[1]/div[1]/div[1]/ul[1]/li[2]/a[1]
                        driver.FindElement(By.XPath(Xpath)).Click();
                    }
                }

                string DropDwnAll = Server.MapPath("~/Images/DropDwnAll.png");
                SikuliAction.Click(DropDwnAll);

                string CivilClick = Server.MapPath("~/Images/CivilClick1.png");
                SikuliAction.Click(CivilClick);

                string TextTransCapital = Server.MapPath("~/Images/BussinessTrans.png");


                SikuliAction.Click(TextTransCapital);

                //string pasteString = "TransCapital";

                driver.FindElement(By.Id("BusiName")).SendKeys("TransCapital");

                string Search = Server.MapPath("~/Images/Search.png");
                SikuliAction.Click(Search);
                Thread.Sleep(1000);
                IWebElement _Div = driver.FindElement(By.Id("SearchResultsGrid"));

                IWebElement _PageText = _Div.FindElement(By.ClassName("k-pager-last"));
                _PageNumber = Convert.ToInt32(_PageText.GetAttribute("data-page").ToString());

                IWebElement elements = driver.FindElement(By.TagName("tbody"));
                WebHtmlElement(elements, 1);

                for (int i = 1; i < _PageNumber; i++)
                {
                    driver.FindElement(By.ClassName("k-i-arrow-e")).Click();
                    IWebElement elements1 = driver.FindElement(By.TagName("tbody"));
                    WebHtmlElement(elements1, i + 1);

                }
                //return Json("Success",JsonRequestBehavior.AllowGet)
                return RedirectToAction("CaseView", "Home");
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ViewDetails(string CaseNumber, int? CtnID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                var sourcePath = "C:/PDfFiles/";// Server.MapPath("~/PDfFiles/");
                //var Result = "";
                string Foldername = Session["FolderName"].ToString();
                if (System.IO.Directory.Exists(sourcePath))
                {
                    string[] _Dir = System.IO.Directory.GetDirectories(sourcePath);
                    var FilesPath = sourcePath + Foldername;
                    bool _result = _Dir.Contains(FilesPath.Trim());
                    if (_result == true)
                    {
                        string dirName = new DirectoryInfo(FilesPath).Name;
                        if (dirName == Foldername)
                        {
                            var txtFilePath = "C:/PDfFiles/" + Foldername + "/" + CaseNumber + "_html.txt";// Server.MapPath("~/PDfFiles/" + Foldername + "/" + CaseNumber + "_html.txt");
                            string htmltxt = System.IO.File.Exists(txtFilePath) ? System.IO.File.ReadAllText(txtFilePath) : string.Empty;

                            string html = @"<html><body>" + htmltxt.ToString() + "</body></html>";
                            HtmlAgilityPack.HtmlDocument doc1 = new HtmlAgilityPack.HtmlDocument();
                            doc1.LoadHtml(html);
                            //tblParty


                            var Casenumber = "";
                            if (doc1.DocumentNode.SelectNodes("//div[contains(@class,'well')]") != null)
                            {
                                foreach (HtmlNode node in doc1.DocumentNode.SelectNodes("//div[contains(@class,'well')]"))
                                {

                                    Casenumber = node.SelectSingleNode(".//span[@id='liCN']").InnerText;
                                    _vmcase.StateRptnumber = node.SelectSingleNode(".//span[@id='liSRN']").InnerText;
                                    _vmcase.CourtType = node.SelectSingleNode(".//span[@id='liCourtType']").InnerText;
                                    var _CaseType = node.SelectSingleNode(".//span[@id='liCaseType']").InnerText;
                                    var noHtml = Regex.Replace(_CaseType, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                                    _vmcase.CaseType = noHtml;
                                    _vmcase.FilingDate = Convert.ToDateTime(node.SelectSingleNode(".//span[@id='liCRFilingDate']").InnerText);
                                    _vmcase.CaseStatusT = node.SelectSingleNode(".//span[@id='liCRCaseStatus']").InnerText;
                                    _vmcase.CourtLocation = node.SelectSingleNode(".//span[@id='liCRCourtLocation']").InnerText;
                                    _vmcase.JudgeIdName = node.SelectSingleNode(".//span[@id='liCRJudge']").InnerText;
                                    _vmcase.MagistrateIdName = node.SelectSingleNode(".//span[@id='liCRMagistrate']").InnerText;
                                    _vmcase.CaseNumber = Casenumber;
                                    _vmcase.SearchedDate = Convert.ToDateTime(Session["Searcheddate"].ToString());
                                }

                            }
                            int trCount = 0;
                            //var Plantiff = "";
                            //var AttornyAddress = "";
                            var node22 = doc1.DocumentNode.SelectNodes("//tbody[contains(@id,'PartyDetailsRow')]//tr");
                            _vmcase.defandantList = new List<Defandant>();
                            if (doc1.DocumentNode.SelectNodes("//tbody[contains(@id,'PartyDetailsRow')]//tr") != null)
                            {
                                foreach (HtmlNode node1 in doc1.DocumentNode.SelectNodes("//tbody[contains(@id,'PartyDetailsRow')]//tr"))
                                {
                                    if (trCount == 0)
                                    {

                                        var htmlitem1 = node1.InnerHtml;
                                        var tdcount = 0;
                                        foreach (HtmlNode col in node1.ChildNodes)
                                        {
                                            if (tdcount == 1)
                                            {
                                                _vmcase.Plantiff = col.InnerText;
                                            }
                                            else if (tdcount == 3)
                                            {
                                                var noHtml = Regex.Replace(col.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                                                _vmcase.AttornyAddress = noHtml;
                                            }
                                            tdcount++;

                                        }
                                    }
                                    else
                                    {

                                        Defandant _def = new Defandant();
                                        var htmlitem1 = node1.InnerHtml;
                                        var tdcnt = 0;
                                        var _Defendant = "";
                                        foreach (HtmlNode col in node1.ChildNodes)
                                        {
                                            if (tdcnt == 1)
                                            {
                                                if (_Defendant == "Defendant")
                                                {
                                                    _def.CaseNumber = Casenumber;

                                                    var noHtml = Regex.Replace(col.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                                                    _def.PartyName = noHtml;
                                                    _vmcase.defandantList.Add(_def);
                                                }
                                            }
                                            if (col.InnerText == "Defendant")
                                            {
                                                _Defendant = col.InnerText;
                                            }
                                            else
                                            {
                                                _Defendant = "";
                                            }
                                            //else if (tdcnt == 3)
                                            //{
                                            //    var noHtml = Regex.Replace(col.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                                            //    _vmcase.AttornyAddress = noHtml;
                                            //}

                                            tdcnt++;
                                        }
                                    }
                                    trCount++;
                                }
                            }

                            if (CtnID != null && CtnID > 0)
                            {
                                var trCountCT = 0;
                                _vmcase.CitationList = new List<Citation>();
                                if (doc1.DocumentNode.SelectNodes("//table[@id='tblCitation']/tbody/tr") != null)
                                {
                                    foreach (HtmlNode node2 in doc1.DocumentNode.SelectNodes("//table[@id='tblCitation']/tbody/tr"))
                                    {
                                        Citation _Citn = new Citation();
                                        var tdcntCt = 0;
                                        foreach (HtmlNode col2 in node2.ChildNodes)
                                        {
                                            if (tdcntCt == 0)
                                            {
                                                _Citn.OffenseDate = Convert.ToDateTime(col2.InnerText);
                                            }
                                            else if (tdcntCt == 2)
                                            {
                                                _Citn.CitaionNumber = col2.InnerText;
                                            }
                                            _vmcase.CitationList.Add(_Citn);
                                            tdcntCt++;
                                        }
                                        trCountCT++;
                                    }
                                }

                            }
                            //AddCaseDetils(_vmcase); // Save All Fields In database
                        }
                    }
                }
                //return Json("Success", JsonRequestBehavior.AllowGet);
                return PartialView("_CaseDetails", _vmcase);
            }
            return RedirectToAction("Login", "Home");
        }


        public ActionResult SaveCasesCitaionDetails(int? tabid)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                // Server.MapPath("~/PDfFiles/");
                //var Result = "";
                string Foldername = "";
                if (tabid == 3)
                {
                    List<tblCaseNumberSearch> _tblCaseNumberSearch = db.tblCaseNumberSearches.OrderByDescending(x => x.CaseNumberID).ToList();
                    foreach (var SearchId in _tblCaseNumberSearch)
                    {
                        var CaseNumber = SearchId.CaseNumber;
                        var CaseNumber1 = GetPdfDetailsFolder(SearchId.CaseNumberID);
                        Foldername = Session["FolderName"].ToString();
                        SaveByCaseAndCitation(Foldername, CaseNumber.Trim());
                        Session["FolderName"] = "";
                    }
                }
                if (tabid == 4)
                {
                    List<tblCitationNumberHistory> _tblCitationNumberHistory = db.tblCitationNumberHistories.OrderByDescending(x => x.CitationNumberID).ToList();
                    foreach (var SearchId in _tblCitationNumberHistory)
                    {
                        var CaseNumber = SearchId.CaseNumber;
                        var CaseNumber2 = GetPdfDetailsInCitation(SearchId.CitationNumberID);
                        Foldername = Session["FolderName"].ToString();
                        SaveByCaseAndCitation(Foldername, CaseNumber.Trim());
                        Session["FolderName"] = "";
                    }
                }


                return Json("Success", JsonRequestBehavior.AllowGet);
                //return PartialView("_CaseDetails", _vmcase);
            }
            return RedirectToAction("Login", "Home");
        }


        public ActionResult SaveCasesCitaionAddress(int? tabid)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                string Foldername = "";
                if (tabid == 3)
                {
                    List<tblCaseNumberSearch> _tblCaseNumberSearch = db.tblCaseNumberSearches.OrderByDescending(x => x.CaseNumberID).ToList();
                    foreach (var SearchId in _tblCaseNumberSearch)
                    {
                        var CaseNumber = SearchId.CaseNumber;
                        var CaseNumber1 = GetPdfDetailsFolder(SearchId.CaseNumberID);
                        Foldername = Session["FolderName"].ToString();
                        DateTime _SearchDate = Convert.ToDateTime(SearchId.CaseSearchDate);
                        DateTime _SearchedDate = DateTime.ParseExact(_SearchDate.ToString("MM/dd/yyyy hh:mm:ss tt"), "MM/dd/yyyy hh:mm:ss tt", null);
                        FilterAndSaveAddress(CaseNumber.Trim(), Foldername.ToString(), _SearchedDate, SearchId.CaseNumberID);
                        Session["FolderName"] = "";
                    }
                }
                if (tabid == 4)
                {
                    List<tblCitationNumberHistory> _tblCitationNumberHistory = db.tblCitationNumberHistories.OrderByDescending(x => x.CitationNumberID).ToList();
                    foreach (var SearchId in _tblCitationNumberHistory)
                    {
                        var CaseNumber = SearchId.CaseNumber;
                        var CaseNumber2 = GetPdfDetailsInCitation(SearchId.CitationNumberID);
                        Foldername = Session["FolderName"].ToString();
                        int CitationNumberID = SearchId.CitationNumberID;
                        DateTime _SearchDate = Convert.ToDateTime(SearchId.CitationNumberDate);
                        DateTime _SearchedDate = DateTime.ParseExact(_SearchDate.ToString("MM/dd/yyyy hh:mm:ss tt"), "MM/dd/yyyy hh:mm:ss tt", null);
                        FilterAndSaveAddress(CaseNumber.Trim(), Foldername.ToString(), _SearchedDate, CitationNumberID);

                        Session["FolderName"] = "";
                    }
                }


                return Json("Success", JsonRequestBehavior.AllowGet);
                //return PartialView("_CaseDetails", _vmcase);
            }
            return RedirectToAction("Login", "Home");
        }


        public void SaveByCaseAndCitation(string Foldername, string CaseNumber)
        {
            var sourcePath = "C:/PDfFiles/";
            if (System.IO.Directory.Exists(sourcePath))
            {
                string[] _Dir = System.IO.Directory.GetDirectories(sourcePath);
                var FilesPath = sourcePath + Foldername;
                bool _result = _Dir.Contains(FilesPath.Trim());
                if (_result == true)
                {
                    string dirName = new DirectoryInfo(FilesPath).Name;
                    if (dirName == Foldername)
                    {
                        var txtFilePath = "C:/PDfFiles/" + Foldername + "/" + CaseNumber + "_html.txt";

                        //Server.MapPath("~/PDfFiles/" + Foldername + "/" + CaseNumber + "_html.txt");
                        string htmltxt = System.IO.File.Exists(txtFilePath) ? System.IO.File.ReadAllText(txtFilePath) : string.Empty;

                        string html = @"<html><body>" + htmltxt.ToString() + "</body></html>";
                        HtmlAgilityPack.HtmlDocument doc1 = new HtmlAgilityPack.HtmlDocument();
                        doc1.LoadHtml(html);
                        //tblParty
                        var Casenumber = "";
                        if (doc1.DocumentNode.SelectNodes("//div[contains(@class,'well')]") != null)
                        {
                            foreach (HtmlNode node in doc1.DocumentNode.SelectNodes("//div[contains(@class,'well')]"))
                            {
                                Casenumber = node.SelectSingleNode(".//span[@id='liCN']").InnerText;
                                _vmcase.StateRptnumber = node.SelectSingleNode(".//span[@id='liSRN']").InnerText;
                                _vmcase.CourtType = node.SelectSingleNode(".//span[@id='liCourtType']").InnerText;
                                var _CaseType = node.SelectSingleNode(".//span[@id='liCaseType']").InnerText;
                                var noHtml = Regex.Replace(_CaseType, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                                _vmcase.CaseType = noHtml;
                                _vmcase.FilingDate = Convert.ToDateTime(node.SelectSingleNode(".//span[@id='liCRFilingDate']").InnerText);
                                _vmcase.CaseStatusT = node.SelectSingleNode(".//span[@id='liCRCaseStatus']").InnerText;
                                _vmcase.CourtLocation = node.SelectSingleNode(".//span[@id='liCRCourtLocation']").InnerText;
                                _vmcase.JudgeIdName = node.SelectSingleNode(".//span[@id='liCRJudge']").InnerText;
                                _vmcase.MagistrateIdName = node.SelectSingleNode(".//span[@id='liCRMagistrate']").InnerText;
                                _vmcase.CaseNumber = Casenumber;
                                _vmcase.SearchedDate = Convert.ToDateTime(Session["Searcheddate"].ToString());
                            }
                        }
                        int trCount = 0;
                        //var Plantiff = "";
                        //var AttornyAddress = "";
                        var node22 = doc1.DocumentNode.SelectNodes("//tbody[contains(@id,'PartyDetailsRow')]//tr");
                        _vmcase.defandantList = new List<Defandant>();
                        if (doc1.DocumentNode.SelectNodes("//tbody[contains(@id,'PartyDetailsRow')]//tr") != null)
                        {
                            foreach (HtmlNode node1 in doc1.DocumentNode.SelectNodes("//tbody[contains(@id,'PartyDetailsRow')]//tr"))
                            {
                                if (trCount == 0)
                                {
                                    var htmlitem1 = node1.InnerHtml;
                                    var tdcount = 0;
                                    foreach (HtmlNode col in node1.ChildNodes)
                                    {
                                        if (tdcount == 1)
                                        {
                                            _vmcase.Plantiff = col.InnerText;
                                        }
                                        else if (tdcount == 3)
                                        {
                                            var noHtml = Regex.Replace(col.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                                            _vmcase.AttornyAddress = noHtml;
                                        }
                                        tdcount++;
                                    }
                                }
                                else
                                {
                                    Defandant _def = new Defandant();
                                    var htmlitem1 = node1.InnerHtml;
                                    var tdcnt = 0;
                                    var _Defendant = "";
                                    foreach (HtmlNode col in node1.ChildNodes)
                                    {
                                        if (tdcnt == 1)
                                        {
                                            if (_Defendant == "Defendant")
                                            {
                                                _def.CaseNumber = Casenumber;

                                                var noHtml = Regex.Replace(col.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                                                _def.PartyName = noHtml;
                                                _vmcase.defandantList.Add(_def);
                                            }
                                        }
                                        if (col.InnerText == "Defendant")
                                        {
                                            _Defendant = col.InnerText;
                                        }
                                        else
                                        {
                                            _Defendant = "";
                                        }
                                        //else if (tdcnt == 3)
                                        //{
                                        //    var noHtml = Regex.Replace(col.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                                        //    _vmcase.AttornyAddress = noHtml;
                                        //}
                                        tdcnt++;
                                    }
                                }
                                trCount++;
                            }
                        }
                        var trCountCT = 0;
                        _vmcase.CitationList = new List<Citation>();
                        if (doc1.DocumentNode.SelectNodes("//table[@id='tblCitation']/tbody/tr") != null)
                        {
                            foreach (HtmlNode node2 in doc1.DocumentNode.SelectNodes("//table[@id='tblCitation']/tbody/tr"))
                            {
                                Citation _Citn = new Citation();
                                var tdcntCt = 0;
                                foreach (HtmlNode col2 in node2.ChildNodes)
                                {
                                    if (tdcntCt == 0)
                                    {
                                        _Citn.OffenseDate = Convert.ToDateTime(col2.InnerText);
                                    }
                                    else if (tdcntCt == 2)
                                    {
                                        _Citn.CitaionNumber = col2.InnerText;
                                    }
                                    _vmcase.CitationList.Add(_Citn);
                                    tdcntCt++;
                                }
                                trCountCT++;
                            }
                        }
                        // Save All Fields In database
                        AddCaseDetils(_vmcase);
                    }
                }
            }
        }
        public ActionResult SaveCasesDetails(string CaseNumber)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                var sourcePath = "C:/PDfFiles/";// Server.MapPath("~/PDfFiles/");            
                var Foldername = Session["FolderName"].ToString();
                if (System.IO.Directory.Exists(sourcePath))
                {
                    string[] _Dir = System.IO.Directory.GetDirectories(sourcePath);
                    var FilesPath = sourcePath + Foldername;
                    bool _result = _Dir.Contains(FilesPath.Trim());
                    if (_result == true)
                    {
                        string dirName = new DirectoryInfo(FilesPath).Name;
                        if (dirName == Foldername)
                        {
                            var _filepath = "C:/PDfFiles/" + Foldername;
                            string[] _Files = System.IO.Directory.GetFiles(_filepath);
                            foreach (var txtFilePath in _Files)
                            {


                                //var txtFilePath1 = "C:/PDfFiles/" + Foldername + "/" + CaseNumber + "_html.txt";

                                // Server.MapPath("~/PDfFiles/" + Foldername + "/" + CaseNumber + "_html.txt");
                                string htmltxt = System.IO.File.Exists(txtFilePath) ? System.IO.File.ReadAllText(txtFilePath) : string.Empty;

                                string html = @"<html><body>" + htmltxt.ToString() + "</body></html>";
                                HtmlAgilityPack.HtmlDocument doc1 = new HtmlAgilityPack.HtmlDocument();
                                doc1.LoadHtml(html);
                                //tblParty


                                var Casenumber = "";
                                if (doc1.DocumentNode.SelectNodes("//div[contains(@class,'well')]") != null)
                                {
                                    foreach (HtmlNode node in doc1.DocumentNode.SelectNodes("//div[contains(@class,'well')]"))
                                    {

                                        Casenumber = node.SelectSingleNode(".//span[@id='liCN']").InnerText;
                                        _vmcase.StateRptnumber = node.SelectSingleNode(".//span[@id='liSRN']").InnerText;
                                        _vmcase.CourtType = node.SelectSingleNode(".//span[@id='liCourtType']").InnerText;
                                        var _CaseType = node.SelectSingleNode(".//span[@id='liCaseType']").InnerText;
                                        var noHtml = Regex.Replace(_CaseType, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                                        _vmcase.CaseType = noHtml;
                                        _vmcase.FilingDate = Convert.ToDateTime(node.SelectSingleNode(".//span[@id='liCRFilingDate']").InnerText);
                                        _vmcase.CaseStatusT = node.SelectSingleNode(".//span[@id='liCRCaseStatus']").InnerText;
                                        _vmcase.CourtLocation = node.SelectSingleNode(".//span[@id='liCRCourtLocation']").InnerText;
                                        _vmcase.JudgeIdName = node.SelectSingleNode(".//span[@id='liCRJudge']").InnerText;
                                        _vmcase.MagistrateIdName = node.SelectSingleNode(".//span[@id='liCRMagistrate']").InnerText;
                                        _vmcase.CaseNumber = Casenumber;
                                        _vmcase.SearchedDate = Convert.ToDateTime(Session["Searcheddate"].ToString());
                                    }

                                }
                                int trCount = 0;
                                //var Plantiff = "";
                                //var AttornyAddress = "";
                                var node22 = doc1.DocumentNode.SelectNodes("//tbody[contains(@id,'PartyDetailsRow')]//tr");
                                _vmcase.defandantList = new List<Defandant>();
                                if (doc1.DocumentNode.SelectNodes("//tbody[contains(@id,'PartyDetailsRow')]//tr") != null)
                                {
                                    foreach (HtmlNode node1 in doc1.DocumentNode.SelectNodes("//tbody[contains(@id,'PartyDetailsRow')]//tr"))
                                    {
                                        if (trCount == 0)
                                        {

                                            var htmlitem1 = node1.InnerHtml;
                                            var tdcount = 0;
                                            foreach (HtmlNode col in node1.ChildNodes)
                                            {
                                                if (tdcount == 1)
                                                {
                                                    _vmcase.Plantiff = col.InnerText;
                                                }
                                                else if (tdcount == 3)
                                                {
                                                    var noHtml = Regex.Replace(col.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                                                    _vmcase.AttornyAddress = noHtml;
                                                }
                                                tdcount++;

                                            }
                                        }
                                        else
                                        {

                                            Defandant _def = new Defandant();
                                            var htmlitem1 = node1.InnerHtml;
                                            var tdcnt = 0;
                                            var _Defendant = "";
                                            foreach (HtmlNode col in node1.ChildNodes)
                                            {
                                                if (tdcnt == 1)
                                                {
                                                    if (_Defendant == "Defendant")
                                                    {
                                                        _def.CaseNumber = Casenumber;

                                                        var noHtml = Regex.Replace(col.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                                                        _def.PartyName = noHtml;
                                                        _vmcase.defandantList.Add(_def);
                                                    }
                                                }
                                                if (col.InnerText == "Defendant")
                                                {
                                                    _Defendant = col.InnerText;
                                                }
                                                else
                                                {
                                                    _Defendant = "";
                                                }
                                                //else if (tdcnt == 3)
                                                //{
                                                //    var noHtml = Regex.Replace(col.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                                                //    _vmcase.AttornyAddress = noHtml;
                                                //}

                                                tdcnt++;
                                            }
                                        }
                                        trCount++;
                                    }
                                }
                                var trCountCT = 0;
                                _vmcase.CitationList = new List<Citation>();
                                if (doc1.DocumentNode.SelectNodes("//table[@id='tblCitation']/tbody/tr") != null)
                                {
                                    foreach (HtmlNode node2 in doc1.DocumentNode.SelectNodes("//table[@id='tblCitation']/tbody/tr"))
                                    {
                                        Citation _Citn = new Citation();
                                        var tdcntCt = 0;
                                        foreach (HtmlNode col2 in node2.ChildNodes)
                                        {
                                            if (tdcntCt == 0)
                                            {
                                                _Citn.OffenseDate = Convert.ToDateTime(col2.InnerText);
                                            }
                                            else if (tdcntCt == 2)
                                            {
                                                _Citn.CitaionNumber = col2.InnerText;
                                            }
                                            _vmcase.CitationList.Add(_Citn);
                                            tdcntCt++;
                                        }
                                        trCountCT++;
                                    }
                                }
                                AddCaseDetils(_vmcase);
                            }// Save All Fields In database
                        }
                    }
                }
                return Json("Success", JsonRequestBehavior.AllowGet);
                //return PartialView("_CaseDetails", _vmcase);
            }
            return RedirectToAction("Login", "Home");
        }
        //public ActionResult SaveCasesDetails(string CaseNumber, int? CtnID, int? SearchId, int? tabid)

        //{
        //    var sourcePath = "C:/PDfFiles/";// Server.MapPath("~/PDfFiles/");
        //    var Result = "";
        //    string Foldername = "";
        //    if (SearchId != null)
        //    {
        //        if (tabid == 3)
        //        {

        //            var CaseNumber1 = GetPdfDetailsFolder(SearchId);

        //            Foldername = Session["FolderName"].ToString();
        //        }
        //        else if (tabid == 4)
        //        {

        //            var CaseNumber2 = GetPdfDetailsInCitation(SearchId);

        //            Foldername = Session["FolderName"].ToString();
        //        }

        //    }
        //    else
        //    {
        //        Foldername = Session["FolderName"].ToString();
        //    }
        //    if (System.IO.Directory.Exists(sourcePath))
        //    {
        //        string[] _Dir = System.IO.Directory.GetDirectories(sourcePath);

        //        foreach (string FilesPath in _Dir)
        //        {
        //            string dirName = new DirectoryInfo(FilesPath).Name;
        //            if (dirName == Foldername)
        //            {
        //                var _filepath = "C:/PDfFiles/" + Foldername;
        //                string[] _Files = System.IO.Directory.GetFiles(_filepath);
        //                foreach (var txtFilePath in _Files)
        //               {


        //                    //var txtFilePath1 = "C:/PDfFiles/" + Foldername + "/" + CaseNumber + "_html.txt";

        //                    // Server.MapPath("~/PDfFiles/" + Foldername + "/" + CaseNumber + "_html.txt");
        //                    string htmltxt = System.IO.File.Exists(txtFilePath) ? System.IO.File.ReadAllText(txtFilePath) : string.Empty;

        //                    string html = @"<html><body>" + htmltxt.ToString() + "</body></html>";
        //                    HtmlAgilityPack.HtmlDocument doc1 = new HtmlAgilityPack.HtmlDocument();
        //                    doc1.LoadHtml(html);
        //                    //tblParty


        //                    var Casenumber = "";
        //                    if (doc1.DocumentNode.SelectNodes("//div[contains(@class,'well')]") != null)
        //                    {
        //                        foreach (HtmlNode node in doc1.DocumentNode.SelectNodes("//div[contains(@class,'well')]"))
        //                        {

        //                            Casenumber = node.SelectSingleNode(".//span[@id='liCN']").InnerText;
        //                            _vmcase.StateRptnumber = node.SelectSingleNode(".//span[@id='liSRN']").InnerText;
        //                            _vmcase.CourtType = node.SelectSingleNode(".//span[@id='liCourtType']").InnerText;
        //                            var _CaseType = node.SelectSingleNode(".//span[@id='liCaseType']").InnerText;
        //                            var noHtml = Regex.Replace(_CaseType, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
        //                            _vmcase.CaseType = noHtml;
        //                            _vmcase.FilingDate = Convert.ToDateTime(node.SelectSingleNode(".//span[@id='liCRFilingDate']").InnerText);
        //                            _vmcase.CaseStatusT = node.SelectSingleNode(".//span[@id='liCRCaseStatus']").InnerText;
        //                            _vmcase.CourtLocation = node.SelectSingleNode(".//span[@id='liCRCourtLocation']").InnerText;
        //                            _vmcase.JudgeIdName = node.SelectSingleNode(".//span[@id='liCRJudge']").InnerText;
        //                            _vmcase.MagistrateIdName = node.SelectSingleNode(".//span[@id='liCRMagistrate']").InnerText;
        //                            _vmcase.CaseNumber = Casenumber;
        //                            _vmcase.SearchedDate = Convert.ToDateTime(Session["Searcheddate"].ToString());
        //                        }

        //                    }
        //                    int trCount = 0;
        //                    var Plantiff = "";
        //                    var AttornyAddress = "";
        //                    var node22 = doc1.DocumentNode.SelectNodes("//tbody[contains(@id,'PartyDetailsRow')]//tr");
        //                    _vmcase.defandantList = new List<Defandant>();
        //                    if (doc1.DocumentNode.SelectNodes("//tbody[contains(@id,'PartyDetailsRow')]//tr") != null)
        //                    {
        //                        foreach (HtmlNode node1 in doc1.DocumentNode.SelectNodes("//tbody[contains(@id,'PartyDetailsRow')]//tr"))
        //                        {
        //                            if (trCount == 0)
        //                            {

        //                                var htmlitem1 = node1.InnerHtml;
        //                                var tdcount = 0;
        //                                foreach (HtmlNode col in node1.ChildNodes)
        //                                {
        //                                    if (tdcount == 1)
        //                                    {
        //                                        _vmcase.Plantiff = col.InnerText;
        //                                    }
        //                                    else if (tdcount == 3)
        //                                    {
        //                                        var noHtml = Regex.Replace(col.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
        //                                        _vmcase.AttornyAddress = noHtml;
        //                                    }
        //                                    tdcount++;

        //                                }
        //                            }
        //                            else
        //                            {

        //                                Defandant _def = new Defandant();
        //                                var htmlitem1 = node1.InnerHtml;
        //                                var tdcnt = 0;
        //                                var _Defendant = "";
        //                                foreach (HtmlNode col in node1.ChildNodes)
        //                                {
        //                                    if (tdcnt == 1)
        //                                    {
        //                                        if (_Defendant == "Defendant")
        //                                        {
        //                                            _def.CaseNumber = Casenumber;

        //                                            var noHtml = Regex.Replace(col.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
        //                                            _def.PartyName = noHtml;
        //                                            _vmcase.defandantList.Add(_def);
        //                                        }
        //                                    }
        //                                    if (col.InnerText == "Defendant")
        //                                    {
        //                                        _Defendant = col.InnerText;
        //                                    }
        //                                    else
        //                                    {
        //                                        _Defendant = "";
        //                                    }
        //                                    //else if (tdcnt == 3)
        //                                    //{
        //                                    //    var noHtml = Regex.Replace(col.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
        //                                    //    _vmcase.AttornyAddress = noHtml;
        //                                    //}

        //                                    tdcnt++;
        //                                }
        //                            }
        //                            trCount++;
        //                        }
        //                    }

        //                    if (CtnID != null && CtnID > 0)
        //                    {
        //                        var trCountCT = 0;
        //                        _vmcase.CitationList = new List<Citation>();
        //                        if (doc1.DocumentNode.SelectNodes("//table[@id='tblCitation']/tbody/tr") != null)
        //                        {
        //                            foreach (HtmlNode node2 in doc1.DocumentNode.SelectNodes("//table[@id='tblCitation']/tbody/tr"))
        //                            {
        //                                Citation _Citn = new Citation();
        //                                var tdcntCt = 0;
        //                                foreach (HtmlNode col2 in node2.ChildNodes)
        //                                {
        //                                    if (tdcntCt == 0)
        //                                    {
        //                                        _Citn.OffenseDate = Convert.ToDateTime(col2.InnerText);
        //                                    }
        //                                    else if (tdcntCt == 2)
        //                                    {
        //                                        _Citn.CitaionNumber = col2.InnerText;
        //                                    }
        //                                    _vmcase.CitationList.Add(_Citn);
        //                                    tdcntCt++;
        //                                }
        //                                trCountCT++;
        //                            }
        //                        }

        //                    }
        //                    AddCaseDetils(_vmcase);
        //                // Save All Fields In database
        //            }
        //        }
        //    }
        //    return Json("Success", JsonRequestBehavior.AllowGet);
        //    //return PartialView("_CaseDetails", _vmcase);
        //}
        //public 


        private void AddCaseDetils(VMCase _vmcase)
        {
            VMCase _VMCase = new VMCase();
            _VMCase.SaveCaseDetials(_vmcase);

        }

        public void GetpdfFiles(string Folderpath, IWebDriver driver, string pagenum)
        {

            try
            {
                By xPath = By.XPath("//div/table/tbody/tr/td[1]//a");

                IReadOnlyCollection<IWebElement> linkCollection = driver.FindElements(xPath);
                for (int i = 0; i < linkCollection.Count; i++)
                {
                    Thread.Sleep(1000);
                    if (pagenum != "1")
                    {
                        driver.FindElement(By.ClassName("k-textbox")).Clear();
                        driver.FindElement(By.ClassName("k-textbox")).SendKeys(pagenum.ToString());
                        driver.FindElement(By.ClassName("k-textbox")).SendKeys(OpenQA.Selenium.Keys.Enter);
                    }

                    //wait for the elements to be exist
                    new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementExists(xPath));

                    //Click on the elements by index
                    var Casenum = driver.FindElements(xPath)[i].Text;
                    driver.FindElements(xPath)[i].Click();
                    Thread.Sleep(1000);
                    IWebElement _pagehtml = driver.FindElement(By.Id("contentToPrint"));
                    var _html = _pagehtml.GetAttribute("innerHTML");
                    string _htmlfilepath = "C:/PDfFiles/" + Folderpath + "/" + Casenum + "_html.txt";// Server.MapPath("~/PDfFiles/" + Folderpath + "/" + Casenum + "_html.txt"); // Full path of new file
                    using (StreamWriter outputFile = new StreamWriter(_htmlfilepath, true))
                    {
                        outputFile.WriteLine(_html);
                    }

                    IWebElement EventsDocument = driver.FindElement(By.Id("accordionEvent"));
                    var innerHtml1 = EventsDocument.GetAttribute("innerHTML");

                    string html = @"<html><body>" + innerHtml1.ToString() + "</body></html>";
                    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(html);
                    var nodes = doc.DocumentNode.SelectNodes("//td[2]");

                    var count = 0;
                    var Chk = 0;
                    if (nodes != null)
                    {
                        foreach (HtmlNode node in nodes)
                        {


                            if (node.InnerText == "eSummons Issuance")
                            {
                                By xPathEvents = By.XPath("//table[@id='tblEvents']/tbody/tr/td[4]");

                                IReadOnlyCollection<IWebElement> linkxPathEvents1 = EventsDocument.FindElements(xPathEvents);

                                new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(ExpectedConditions.ElementExists(xPathEvents));


                                driver.FindElements(xPathEvents)[count].FindElement(By.TagName("a")).Click();
                                Thread.Sleep(1000);
                                var currentWindow = driver.CurrentWindowHandle;
                                var availableWindows = new List<string>(driver.WindowHandles);

                                if (availableWindows != null)
                                {

                                    foreach (string w in availableWindows)
                                    {
                                        IWebDriver popup = null;
                                        if (w != currentWindow)
                                        {
                                            Chk++;
                                            driver.SwitchTo().Window(w);
                                            if (driver.Title == driver.Title)
                                            {
                                                Thread.Sleep(4000);
                                                By xPathEvets1 = By.XPath("//div[@id='rdToolBar']/div/div/div/ul/li/a[@title='View Entire Document']");

                                                bool _Error = driver.PageSource.Contains("rdToolBar");
                                                if (_Error == true)
                                                {
                                                    driver.FindElements(xPathEvets1)[0].Click();
                                                    Thread.Sleep(1000);
                                                }
                                                By xPathEventsfrm = By.XPath("//iframe[@id='RAD_SPLITTER_PANE_EXT_CONTENT_pdfview']");
                                                var _frameUrl = driver.FindElements(xPathEventsfrm)[0].GetAttribute("src");
                                                string frameUrl = "https://www.browardclerk.org/" + _frameUrl;

                                                FirefoxProfile profile = new FirefoxProfile();
                                                profile.SetPreference("browser.download.folderList", 2);
                                                profile.SetPreference("browser.download.dir", "C:\\Windows\\temp");
                                                profile.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");
                                                profile.SetPreference("pdfjs.disabled", true);

                                                IWebDriver driver1 = new FirefoxDriver(profile);

                                                driver1.Navigate().GoToUrl(_frameUrl.ToString());

                                                Thread.Sleep(1000);
                                                driver1.FindElement(By.Id("download")).SendKeys(OpenQA.Selenium.Keys.Enter);
                                                Thread.Sleep(1000);

                                                for (int p = 1; p <= 6; p++)
                                                {
                                                    int OkButton = IsCorrectOKButton(p);
                                                    if (OkButton == 2)
                                                    {
                                                        break;
                                                    }
                                                }

                                                System.Threading.Thread.Sleep(1000);

                                                //string Download = Server.MapPath("~/Images/Download.png");
                                                //Screen _screen = Screen.AllScreens[0];
                                                //Thread.Sleep(1000);
                                                //SikuliAction.Click(Download);

                                                //SendKeys.SendWait(@"{Enter}");
                                                ////Thread.Sleep(2000);
                                                //string OkButton = Server.MapPath("~/Images/OkButton.png");
                                                //SikuliAction.Click(OkButton);

                                                string root = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                                                string downloadPath = root + "\\Downloads\\MC_Docket.PDF";
                                                string CaseTitle = driver.Title;
                                                string CaseNumber = CaseTitle.Split('-')[0];

                                                string oldFilePath = downloadPath; // Full path of old file

                                                string newFilePath = "C:/PDfFiles/" + Folderpath + "/" + CaseNumber + "-" + Chk + "_MC_Docket.PDF";// Server.MapPath("~/PDfFiles/" + Folderpath + "/" + CaseNumber + "-" + Chk + "_MC_Docket.PDF"); // Full path of new file

                                                if (System.IO.File.Exists(downloadPath))
                                                {
                                                    if (System.IO.File.Exists(newFilePath))
                                                    {
                                                        System.IO.File.Delete(newFilePath);
                                                    }
                                                    System.IO.File.Copy(oldFilePath, newFilePath);
                                                    System.IO.File.Delete(oldFilePath);

                                                }
                                                Thread.Sleep(500);
                                                popup = driver.SwitchTo().Window(w);
                                                popup.Close();
                                                Thread.Sleep(500);
                                                driver.SwitchTo().Window(currentWindow);

                                            }
                                            else
                                            {
                                                driver.SwitchTo().Window(currentWindow);

                                            }

                                        }

                                    }
                                }

                            }
                            count++;
                        }
                    }
                    driver.Navigate().Back();
                    driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(10));


                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        public int IsCorrectOKButton(int i)
        {
            try
            {
                string BtnOK = Server.MapPath("~/Images/BtnOK-" + i + ".png");
                SikuliAction.Click(BtnOK);
                return 2;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return 1;
            }

        }

        private string ExtractTextFromImage(string filePath)
        {
            MODI.Document modiDocument = new MODI.Document();
            modiDocument.Create(filePath);
            modiDocument.OCR(MiLANGUAGES.miLANG_ENGLISH);
            MODI.Image modiImage = (modiDocument.Images[0] as MODI.Image);
            string extractedText = modiImage.Layout.Text;
            modiDocument.Close();
            return extractedText;
        }


        public void WebHtmlElement(IWebElement elements, int PageCountCheck)
        {
            try
            {
                var innerHtml = elements.GetAttribute("innerHTML");
                string html = @"<html><body>" + innerHtml.ToString() + "</body></html>";
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(html);
                var nodes = doc.DocumentNode.SelectNodes("//td");

                System.Data.DataTable table = new System.Data.DataTable();
                table.TableName = "TransCapital " + PageCountCheck + "";
                DataRow dr = null;

                DataColumn column = new DataColumn();
                DataColumn column2 = new DataColumn();
                DataColumn column3 = new DataColumn();
                DataColumn column4 = new DataColumn();
                DataColumn column5 = new DataColumn();

                column.ColumnName = "Case Number";
                table.Columns.Add(column);
                column2.ColumnName = "Case Style";
                table.Columns.Add(column2);
                column3.ColumnName = "Case Type";
                table.Columns.Add(column3);
                column4.ColumnName = "Filing Date";
                table.Columns.Add(column4);
                column5.ColumnName = "Case Status";
                table.Columns.Add(column5);

                int cnt = 0;
                foreach (HtmlNode node in nodes)
                {
                    if (cnt > 4)
                        cnt = 0;

                    if (cnt == 0)
                    {
                        dr = table.NewRow();
                        dr[cnt] = node.InnerText;
                    }
                    else
                        dr[cnt] = node.InnerText;

                    if (cnt == 4)
                        table.Rows.Add(dr);

                    cnt++;
                }

                DatasetTables(table, PageCountCheck);

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        DataSet _dstables = new DataSet();
        public void DatasetTables(System.Data.DataTable dtDataTable1, int PageCntCheck)
        {

            try
            {

                _dstables.Tables.Add(dtDataTable1);

                if (_PageNumber == PageCntCheck)
                {
                    DSToExcel(_dstables);
                }


            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void DSToExcel(System.Data.DataSet dsDataSet)
        {

            Sheets xlSheets = null;
            Worksheet xlWorksheet = null;
            Microsoft.Office.Interop.Excel.Application ExcelApp = new Microsoft.Office.Interop.Excel.Application();
            Workbook xlWorkBook = ExcelApp.Workbooks.Add(Microsoft.Office.Interop.Excel.XlWBATemplate.xlWBATWorksheet);
            try
            {

                for (int i = 1; i > 0; i--)
                {

                    //Create Excel sheet
                    xlSheets = ExcelApp.Sheets;
                    xlWorksheet = (Worksheet)xlSheets.Add(xlSheets[1], Type.Missing, Type.Missing, Type.Missing);
                    xlWorksheet.Name = "TransCapital";

                    Range cells = xlWorkBook.Worksheets[1].Cells;
                    // set each cell's format to Text
                    cells.NumberFormat = "@";
                    // reset horizontal alignment to the right
                    cells.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                    cells.VerticalAlignment = XlVAlign.xlVAlignTop;
                    //cells.WrapText = true;

                    int _Cnt = 2;
                    for (int d = 0; d < dsDataSet.Tables.Count; d++)
                    {
                        System.Data.DataTable dtDataTable1 = new System.Data.DataTable();
                        dtDataTable1 = dsDataSet.Tables[d];
                        if (d == 0)
                        {
                            for (int j = 1; j < dtDataTable1.Columns.Count + 1; j++)
                            {
                                ExcelApp.Cells[i, j] = dtDataTable1.Columns[j - 1].ColumnName;
                                ExcelApp.Cells[1, j].Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green);
                                ExcelApp.Cells[i, j].Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.WhiteSmoke);

                            }
                        }
                        // for the data of the excel
                        for (int k = 0; k < dtDataTable1.Rows.Count; k++)
                        {
                            for (int l = 0; l < dtDataTable1.Columns.Count; l++)
                            {
                                //ExcelApp.Cells[d+k + 2, l + 1] = dtDataTable1.Rows[k].ItemArray[l].ToString();
                                ExcelApp.Cells[_Cnt, l + 1] = dtDataTable1.Rows[k].ItemArray[l].ToString();
                                //ExcelApp.Cells.WrapText = true;

                                ExcelApp.Cells.VerticalAlignment = XlVAlign.xlVAlignTop;
                                ExcelApp.Cells.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                            }
                            _Cnt++;
                        }
                    }
                    ExcelApp.Columns.AutoFit();
                }
                ((Worksheet)ExcelApp.ActiveWorkbook.Sheets[ExcelApp.ActiveWorkbook.Sheets.Count]).Delete();
                ExcelApp.Visible = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                xlSheets = null;
                xlWorksheet = null;
                xlWorkBook = null;
            }
        }

        public ActionResult ChekSaveCaseDetails(string SearchedDate, int Count)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                DateTime _SearchedDate = Convert.ToDateTime(SearchedDate);
                List<tblCaseDetail> _tblCaseDetail = db.tblCaseDetails.Where(x => x.SearchedDate == _SearchedDate).ToList();
                if (_tblCaseDetail.Count == Count)
                {
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else if (_tblCaseDetail.Count < Count)
                {
                    return Json("Save Again", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult DeleteCasesHistory(int SearchID, int tabid)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                if (tabid == 1)
                {
                    var _SearchHistories = db.SearchHistories.Where(w => w.SearchID == SearchID).FirstOrDefault();
                    if (_SearchHistories != null)
                    {
                        string foldernmae = GetPdfDetailsBussiness(SearchID);
                        db.SearchHistories.Remove(_SearchHistories);
                        db.SaveChanges();
                        //Delete Folder from Directory
                        string newFilePath = "C:/PDfFiles/" + foldernmae + "";
                        if (System.IO.Directory.Exists(newFilePath))
                        {
                            string[] files11 = System.IO.Directory.GetFiles(newFilePath);
                            foreach (string files11Path in files11)
                            {
                                System.IO.File.Delete(files11Path);
                            }
                            System.IO.Directory.Delete(newFilePath);
                        }
                        DateTime _SearchedDate = Convert.ToDateTime(Session["Searcheddate"]);
                        DateTime _SearchDate = DateTime.ParseExact(_SearchedDate.ToString("MM/dd/yyyy hh:mm:ss tt"), "MM/dd/yyyy hh:mm:ss tt", null);
                        db.sp_DeleteCaseDetails(_SearchDate);

                        Session["Searcheddate"] = "";
                        Session["FolderName"] = "";
                    }
                }
                else if (tabid == 2)
                {
                    var _SearchHistories = db.tblCaseNumberSearches.Where(w => w.CaseNumberID == SearchID).FirstOrDefault();
                    if (_SearchHistories != null)
                    {
                        string SrchedType = GetPdfDetailsFolder(SearchID);

                        db.tblCaseNumberSearches.Remove(_SearchHistories);
                        db.SaveChanges();
                        var foldernmae = Session["FolderName"].ToString();
                        string newFilePath = "C:/PDfFiles/" + foldernmae + "";// Server.MapPath("~/PDfFiles/" + Folderpath + "/" + CaseNumber + "-" + Chk + "_MC_Docket.PDF"); // Full path of new file                     
                        //Delete Folder from Directory
                        if (System.IO.Directory.Exists(newFilePath))
                        {
                            string[] files11 = System.IO.Directory.GetFiles(newFilePath);
                            foreach (string files11Path in files11)
                            {
                                System.IO.File.Delete(files11Path);
                            }
                            System.IO.Directory.Delete(newFilePath);
                        }
                        DateTime _SearchedDate = Convert.ToDateTime(Session["Searcheddate"]);
                        DateTime _SearchDate = DateTime.ParseExact(_SearchedDate.ToString("MM/dd/yyyy hh:mm:ss tt"), "MM/dd/yyyy hh:mm:ss tt", null);
                        db.sp_DeleteCaseDetails(_SearchDate);

                        Session["Searcheddate"] = "";
                        Session["FolderName"] = "";
                    }
                }
                else if (tabid == 3)
                {
                    var _SearchHistories = db.tblCitationNumberHistories.Where(w => w.CitationNumberID == SearchID).FirstOrDefault();
                    if (_SearchHistories != null)
                    {
                        string SrchedType = GetPdfDetailsInCitation(SearchID);
                        db.tblCitationNumberHistories.Remove(_SearchHistories);
                        db.SaveChanges();

                        var foldernmae = Session["FolderName"].ToString();
                        string newFilePath = "C:/PDfFiles/" + foldernmae + "";// Server.MapPath("~/PDfFiles/" + Folderpath + "/" + CaseNumber + "-" + Chk + "_MC_Docket.PDF"); // Full path of new file                     
                        //Delete Folder from Directory
                        if (System.IO.Directory.Exists(newFilePath))
                        {
                            string[] files11 = System.IO.Directory.GetFiles(newFilePath);
                            foreach (string files11Path in files11)
                            {
                                System.IO.File.Delete(files11Path);
                            }
                            System.IO.Directory.Delete(newFilePath);
                        }
                        DateTime _SearchedDate = Convert.ToDateTime(Session["Searcheddate"]);
                        DateTime _SearchDate = DateTime.ParseExact(_SearchedDate.ToString("MM/dd/yyyy hh:mm:ss tt"), "MM/dd/yyyy hh:mm:ss tt", null);
                        db.sp_DeleteCaseDetails(_SearchDate);

                        Session["Searcheddate"] = "";
                        Session["FolderName"] = "";
                    }
                }
                else if (tabid == 4)
                {
                    var _SearchHistories = db.tblPartySearchHistories.Where(w => w.PartySearchID == SearchID).FirstOrDefault();
                    if (_SearchHistories != null)
                    {
                        string foldernmae = GetPdfDetailsParty(SearchID);
                        db.tblPartySearchHistories.Remove(_SearchHistories);
                        db.SaveChanges();

                        string newFilePath = "C:/PDfFiles/" + foldernmae + "";// Server.MapPath("~/PDfFiles/" + Folderpath + "/" + CaseNumber + "-" + Chk + "_MC_Docket.PDF"); // Full path of new file                     
                        //Delete Folder from Directory
                        if (System.IO.Directory.Exists(newFilePath))
                        {
                            string[] files11 = System.IO.Directory.GetFiles(newFilePath);
                            foreach (string files11Path in files11)
                            {
                                System.IO.File.Delete(files11Path);
                            }
                            System.IO.Directory.Delete(newFilePath);
                        }
                        DateTime _SearchedDate = Convert.ToDateTime(Session["Searcheddate"]);
                        DateTime _SearchDate = DateTime.ParseExact(_SearchedDate.ToString("MM/dd/yyyy hh:mm:ss tt"), "MM/dd/yyyy hh:mm:ss tt", null);
                        db.sp_DeleteCaseDetails(_SearchDate);

                        Session["Searcheddate"] = "";
                        Session["FolderName"] = "";
                    }
                }

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Login", "Home");
        }


        public static bool IsOdd(int value)
        {
            return value % 2 != 0;
        }


        public ActionResult ExportToExcel(string SearchedDate)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                string Result = "";
                try
                {
                    DateTime _SearchedDate = Convert.ToDateTime(SearchedDate);
                    List<VMCase> _VMCase = _vmcase.GetCaseDetails(_SearchedDate);


                    int count = 1;
                    if (_VMCase.Count > 0)
                    {
                        CreateExcelDoc excell_app = new CreateExcelDoc();
                        CreateExcelFormat(excell_app, ref Result, _VMCase, ref count);
                    }
                    else
                    {
                        Result = "NoRecord";
                    }
                }

                catch (Exception ex)
                {
                    string msg = ex.Message;
                    return Json("Error ! Please Try Again", JsonRequestBehavior.AllowGet);
                }
                return Json(Result, JsonRequestBehavior.AllowGet);
                //return PartialView("_ImPortExcel", _VMCase);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ExportToExcelByCaseCitaion(int? _Rows)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                string Result = "";
                try
                {
                    List<VMCase> _VMCase = _vmcase.GetCaseCitaionRowsDetails(_Rows);
                    int count = 1;
                    if (_VMCase.Count > 0)
                    {
                        CreateExcelDoc excell_app = new CreateExcelDoc();
                        CreateExcelFormat(excell_app, ref Result, _VMCase, ref count);
                    }
                    else
                    {
                        Result = "NoRecord";
                    }
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    return Json("Error ! Please Try Again", JsonRequestBehavior.AllowGet);
                }
                return Json(Result, JsonRequestBehavior.AllowGet);
                //return PartialView("_ImPortExcel", _VMCase);
            }
            return RedirectToAction("Login", "Home");
        }
        private static void CreateExcelFormat(CreateExcelDoc excell_app, ref string Result, List<VMCase> _VMCase, ref int count)
        {


            int _CountOdd = 0, _IncOdd = 0, _CountEven = 0, _IncEven = 0; ;
            foreach (var item in _VMCase)
            {
                if (IsOdd(count))
                {
                    _IncOdd = _CountOdd == 0 ? _IncOdd : (_IncOdd + 32);
                    //creates the main header 1st Side Sheet
                    excell_app.createTitle(_IncOdd + 3, 2, "" + Convert.ToDateTime(item.SearchedDate).ToString("MMMM-dd-yyyy") + "", "B" + (_IncOdd + 3) + "", "B" + (_IncOdd + 3) + "", 6, "WHITE", true, 40, "");
                    excell_app.createHeaders(_IncOdd + 4, 2, "Case Number", "B" + (_IncOdd + 4) + "", "B" + (_IncOdd + 4) + "", 0, "LightCyan", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 5, 2, "Date Complaint", "B" + (_IncOdd + 5) + "", "B" + (_IncOdd + 5) + "", 0, "LightCyan", true, 40, "n");

                    excell_app.createHeaders(_IncOdd + 6, 2, "Plaintiff", "B" + (_IncOdd + 6) + "", "B" + (_IncOdd + 6) + "", 0, "LightCyan", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 7, 2, "Defandant", "B" + (_IncOdd + 7) + "", "B" + (_IncOdd + 7) + "", 0, "LightCyan", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 8, 2, "Defandant", "B" + (_IncOdd + 8) + "", "B" + (_IncOdd + 8) + "", 0, "LightCyan", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 9, 2, "Defandant", "B" + (_IncOdd + 9) + "", "B" + (_IncOdd + 9) + "", 0, "LightCyan", true, 40, "n");

                    excell_app.createHeaders(_IncOdd + 11, 2, "Not Value", "B" + (_IncOdd + 11) + "", "B" + (_IncOdd + 11) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 11, 3, "Assessed Value", "C" + (_IncOdd + 11) + "", "C" + (_IncOdd + 11) + "", 0, "WHITE", true, 20, "n");
                    excell_app.createHeaders(_IncOdd + 14, 2, "Judgment Amount", "B" + (_IncOdd + 14) + "", "B" + (_IncOdd + 14) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 14, 3, "Property Address", "C" + (_IncOdd + 14) + "", "C" + (_IncOdd + 14) + "", 0, "WHITE", true, 20, "n");
                    excell_app.createHeaders(_IncOdd + 17, 2, "Attorneys", "B" + (_IncOdd + 17) + "", "B" + (_IncOdd + 17) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 17, 3, "Default", "C" + (_IncOdd + 17) + "", "C" + (_IncOdd + 17) + "", 0, "WHITE", true, 20, "n");
                    excell_app.createHeaders(_IncOdd + 20, 2, "Consent", "B" + (_IncOdd + 20) + "", "B" + (_IncOdd + 20) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 20, 3, "Settled Date", "C" + (_IncOdd + 20) + "", "C" + (_IncOdd + 20) + "", 0, "WHITE", true, 20, "n");
                    excell_app.createHeaders(_IncOdd + 23, 2, "Judgment Date", "B" + (_IncOdd + 23) + "", "B" + (_IncOdd + 23) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 23, 3, "COT Date", "C" + (_IncOdd + 23) + "", "C" + (_IncOdd + 23) + "", 0, "WHITE", true, 20, "n");
                    excell_app.createHeaders(_IncOdd + 26, 2, "Closed/Open", "B" + (_IncOdd + 26) + "", "B" + (_IncOdd + 26) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 26, 3, "Bankruptcy Stay", "C" + (_IncOdd + 26) + "", "C" + (_IncOdd + 26) + "", 0, "WHITE", true, 20, "n");
                    excell_app.createHeaders(_IncOdd + 29, 2, "Lifted? Date", "B" + (_IncOdd + 29) + "", "B" + (_IncOdd + 29) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 29, 3, "Highest Bidder", "C" + (_IncOdd + 29) + "", "C" + (_IncOdd + 29) + "", 0, "WHITE", true, 20, "n");
                    excell_app.createHeaders(_IncOdd + 32, 2, "Bid Amount", "B" + (_IncOdd + 32) + "", "B" + (_IncOdd + 32) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 32, 3, "", "C" + (_IncOdd + 32) + "", "C" + (_IncOdd + 32) + "", 0, "WHITE", true, 20, "n");

                    //Space 
                    excell_app.createHeaders(_IncOdd + 10, 2, "", "B" + (_IncOdd + 10) + "", "C" + (_IncOdd + 10) + "", 2, "LightCoral", true, 60, "");
                    excell_app.createHeaders(_IncOdd + 13, 2, "", "B" + (_IncOdd + 13) + "", "C" + (_IncOdd + 13) + "", 2, "LightCoral", true, 60, "");
                    excell_app.createHeaders(_IncOdd + 16, 2, "", "B" + (_IncOdd + 16) + "", "C" + (_IncOdd + 16) + "", 2, "LightCoral", true, 60, "");
                    excell_app.createHeaders(_IncOdd + 19, 2, "", "B" + (_IncOdd + 19) + "", "C" + (_IncOdd + 19) + "", 2, "LightCoral", true, 60, "");
                    excell_app.createHeaders(_IncOdd + 22, 2, "", "B" + (_IncOdd + 22) + "", "C" + (_IncOdd + 22) + "", 2, "LightCoral", true, 60, "");
                    excell_app.createHeaders(_IncOdd + 25, 2, "", "B" + (_IncOdd + 25) + "", "C" + (_IncOdd + 25) + "", 2, "LightCoral", true, 60, "");
                    excell_app.createHeaders(_IncOdd + 28, 2, "", "B" + (_IncOdd + 28) + "", "C" + (_IncOdd + 28) + "", 2, "LightCoral", true, 60, "");
                    excell_app.createHeaders(_IncOdd + 31, 2, "", "B" + (_IncOdd + 31) + "", "C" + (_IncOdd + 31) + "", 2, "LightCoral", true, 60, "");
                    excell_app.createHeaders(_IncOdd + 34, 2, "", "B" + (_IncOdd + 34) + "", "C" + (_IncOdd + 34) + "", 2, "LightCoral", true, 60, "");

                    //Add cell data
                    excell_app.addData(_IncOdd + 4, 3, "" + item.CaseNumber + "", "C" + (_IncOdd + 4) + "", "C" + (_IncOdd + 4) + "", "", 20, "LightCyan");
                    excell_app.addData(_IncOdd + 5, 3, "", "C" + (_IncOdd + 5) + "", "C" + (_IncOdd + 5) + "", "", 20, "LightCyan");
                    excell_app.addData(_IncOdd + 6, 3, "" + item.Plantiff + "", "C" + (_IncOdd + 6) + "", "C" + (_IncOdd + 6) + "", "", 20, "LightCyan");
                    //Bind List of defandent
                    int _ContDeff = 1;
                    if (item.defandantList != null)
                    {
                        for (int i = 1; i <= 3; i++)
                        {
                            int _CountDe = item.defandantList.ToList().Count();
                            var _deff = _CountDe >= i ? item.defandantList[i - 1] : null;
                            switch (_ContDeff)
                            {
                                case 1:
                                    if (_deff == null)
                                    {
                                        excell_app.addData(_IncOdd + 7, 3, "", "C" + (_IncOdd + 7) + "", "C" + (_IncOdd + 7) + "", "", 20, "LightCyan");
                                    }
                                    else
                                    {
                                        if (_deff.PartyName != "" && _deff.PartyName != null)
                                            excell_app.addData(_IncOdd + 7, 3, "" + _deff.PartyName + "", "C" + (_IncOdd + 7) + "", "C" + (_IncOdd + 7) + "", "", 20, "LightCyan");
                                        else
                                            excell_app.addData(_IncOdd + 7, 3, "", "C" + (_IncOdd + 7) + "", "C" + (_IncOdd + 7) + "", "", 20, "LightCyan");
                                    }
                                    break;
                                case 2:
                                    if (_deff == null)
                                    {
                                        excell_app.addData(_IncOdd + 8, 3, "", "C" + (_IncOdd + 8) + "", "C" + (_IncOdd + 8) + "", "", 20, "LightCyan");
                                    }
                                    else
                                    {
                                        if (_deff.PartyName != "" && _deff.PartyName != null)
                                            excell_app.addData(_IncOdd + 8, 3, "" + _deff.PartyName + "", "C" + (_IncOdd + 8) + "", "C" + (_IncOdd + 8) + "", "", 20, "LightCyan");
                                        else
                                            excell_app.addData(_IncOdd + 8, 3, "", "C" + (_IncOdd + 8) + "", "C" + (_IncOdd + 8) + "", "", 20, "LightCyan");
                                    }
                                    break;
                                case 3:
                                    if (_deff == null)
                                    {
                                        excell_app.addData(_IncOdd + 9, 3, "", "C" + (_IncOdd + 9) + "", "C" + (_IncOdd + 9) + "", "", 20, "LightCyan");
                                    }
                                    else
                                    {
                                        if (_deff.PartyName != "" && _deff.PartyName != null)
                                            excell_app.addData(_IncOdd + 9, 3, "" + _deff.PartyName + "", "C" + (_IncOdd + 9) + "", "C" + (_IncOdd + 9) + "", "", 20, "LightCyan");
                                        else
                                            excell_app.addData(_IncOdd + 9, 3, "", "C" + (_IncOdd + 9) + "", "C" + (_IncOdd + 9) + "", "", 20, "LightCyan");
                                    }
                                    break;
                            }

                            _ContDeff++;
                        }
                    }
                    else
                    {
                        excell_app.addData(_IncOdd + 7, 3, "", "C" + (_IncOdd + 7) + "", "C" + (_IncOdd + 7) + "", "", 20, "LightCyan");
                        excell_app.addData(_IncOdd + 8, 3, "", "C" + (_IncOdd + 8) + "", "C" + (_IncOdd + 8) + "", "", 20, "LightCyan");
                        excell_app.addData(_IncOdd + 9, 3, "", "C" + (_IncOdd + 9) + "", "C" + (_IncOdd + 9) + "", "", 20, "LightCyan");
                    }

                    excell_app.addData(_IncOdd + 12, 2, "XXXXXX", "B" + (_IncOdd + 12) + "", "B" + (_IncOdd + 12) + "", "", 40, "");
                    excell_app.addData(_IncOdd + 12, 3, "XXXXXX", "C" + (_IncOdd + 12) + "", "C" + (_IncOdd + 12) + "", "", 20, "");

                    excell_app.addData(_IncOdd + 15, 2, "XXXXXX", "B" + (_IncOdd + 15) + "", "B" + (_IncOdd + 15) + "", "", 40, "");
                    excell_app.addData(_IncOdd + 15, 3, "XXXXXX", "C" + (_IncOdd + 15) + "", "C" + (_IncOdd + 15) + "", "", 20, "");

                    excell_app.addData(_IncOdd + 18, 2, "" + item.AttornyAddress + "", "B" + (_IncOdd + 18) + "", "B" + (_IncOdd + 18) + "", "", 40, "");
                    excell_app.addData(_IncOdd + 18, 3, "XXXXXX", "C" + (_IncOdd + 18) + "", "C" + (_IncOdd + 18) + "", "", 20, "");

                    excell_app.addData(_IncOdd + 21, 2, "XXXXXX", "B" + (_IncOdd + 21) + "", "B" + (_IncOdd + 21) + "", "", 40, "");
                    excell_app.addData(_IncOdd + 21, 3, "XXXXXX", "C" + (_IncOdd + 21) + "", "C" + (_IncOdd + 21) + "", "", 20, "");

                    excell_app.addData(_IncOdd + 24, 2, "XXXXXX", "B" + (_IncOdd + 24) + "", "B" + (_IncOdd + 24) + "", "", 40, "");
                    excell_app.addData(_IncOdd + 24, 3, "XXXXXX", "C" + (_IncOdd + 24) + "", "C" + (_IncOdd + 24) + "", "", 20, "");

                    excell_app.addData(_IncOdd + 27, 2, "XXXXXX", "B" + (_IncOdd + 27) + "", "B" + (_IncOdd + 27) + "", "", 40, "");
                    excell_app.addData(_IncOdd + 27, 3, "XXXXXX", "C" + (_IncOdd + 27) + "", "C" + (_IncOdd + 27) + "", "", 20, "");

                    excell_app.addData(_IncOdd + 30, 2, "XXXXXX", "B" + (_IncOdd + 30) + "", "B" + (_IncOdd + 30) + "", "", 40, "");
                    excell_app.addData(_IncOdd + 30, 3, "XXXXXX", "C" + (_IncOdd + 30) + "", "C" + (_IncOdd + 30) + "", "", 20, "");

                    excell_app.addData(_IncOdd + 33, 2, "XXXXXX", "B" + (_IncOdd + 33) + "", "B" + (_IncOdd + 33) + "", "", 40, "");
                    excell_app.addData(_IncOdd + 33, 3, "", "C" + (_IncOdd + 33) + "", "C" + (_IncOdd + 33) + "", "", 20, "");

                    _CountOdd++;

                }
                else
                {
                    _IncEven = _CountEven == 0 ? _IncEven : (_IncEven + 32);

                    //2nd Side sheet
                    excell_app.createTitle(_IncEven + 3, 5, "", "E" + (_IncEven + 3) + "", "F" + (_IncEven + 3) + "", 6, "WHITE", true, 20, "");
                    excell_app.createHeaders(_IncEven + 4, 5, "Case Number", "E" + (_IncEven + 4) + "", "E" + (_IncEven + 4) + "", 0, "LemonChiffon", true, 40, "n");
                    excell_app.createHeaders(_IncEven + 5, 5, "Date Complaint", "E" + (_IncEven + 5) + "", "E" + (_IncEven + 5) + "", 0, "LemonChiffon", true, 40, "n");

                    excell_app.createHeaders(_IncEven + 6, 5, "Plaintiff", "E" + (_IncEven + 6) + "", "E" + (_IncEven + 6) + "", 0, "LemonChiffon", true, 40, "n");
                    excell_app.createHeaders(_IncEven + 7, 5, "Defandant", "E" + (_IncEven + 7) + "", "E" + (_IncEven + 7) + "", 0, "LemonChiffon", true, 40, "n");
                    excell_app.createHeaders(_IncEven + 8, 5, "Defandant", "E" + (_IncEven + 8) + "", "E" + (_IncEven + 8) + "", 0, "LemonChiffon", true, 40, "n");
                    excell_app.createHeaders(_IncEven + 9, 5, "Defandant", "E" + (_IncEven + 9) + "", "E" + (_IncEven + 9) + "", 0, "LemonChiffon", true, 40, "n");

                    excell_app.createHeaders(_IncEven + 11, 5, "Not Value", "E" + (_IncEven + 11) + "", "E" + (_IncEven + 11) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncEven + 11, 6, "Assessed Value", "F" + (_IncEven + 11) + "", "F" + (_IncEven + 11) + "", 0, "WHITE", true, 20, "n");
                    excell_app.createHeaders(_IncEven + 14, 5, "Judgment Amount", "E" + (_IncEven + 14) + "", "E" + (_IncEven + 14) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncEven + 14, 6, "Property Address", "F" + (_IncEven + 14) + "", "F" + (_IncEven + 14) + "", 0, "WHITE", true, 20, "n");
                    excell_app.createHeaders(_IncEven + 17, 5, "Attorneys", "E" + (_IncEven + 17) + "", "E" + (_IncEven + 17) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncEven + 17, 6, "Default", "F" + (_IncEven + 17) + "", "F" + (_IncEven + 17) + "", 0, "WHITE", true, 20, "n");
                    excell_app.createHeaders(_IncEven + 20, 5, "Consent", "E" + (_IncEven + 20) + "", "E" + (_IncEven + 20) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncEven + 20, 6, "Settled Date", "F" + (_IncEven + 20) + "", "F" + (_IncEven + 20) + "", 0, "WHITE", true, 20, "n");
                    excell_app.createHeaders(_IncEven + 23, 5, "Judgment Date", "E" + (_IncEven + 23) + "", "E" + (_IncEven + 23) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncEven + 23, 6, "COT Date", "F" + (_IncEven + 23) + "", "F" + (_IncEven + 23) + "", 0, "WHITE", true, 20, "n");
                    excell_app.createHeaders(_IncEven + 26, 5, "Closed/Open", "E" + (_IncEven + 26) + "", "E" + (_IncEven + 26) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncEven + 26, 6, "Bankruptcy Stay", "F" + (_IncEven + 26) + "", "F" + (_IncEven + 26) + "", 0, "WHITE", true, 20, "n");
                    excell_app.createHeaders(_IncEven + 29, 5, "Lifted? Date", "E" + (_IncEven + 29) + "", "E" + (_IncEven + 29) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncEven + 29, 6, "Highest Bidder", "F" + (_IncEven + 29) + "", "F" + (_IncEven + 29) + "", 0, "WHITE", true, 20, "n");
                    excell_app.createHeaders(_IncEven + 32, 5, "Bid Amount", "E" + (_IncEven + 32) + "", "E" + (_IncEven + 32) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncEven + 32, 6, "", "F" + (_IncEven + 32) + "", "F" + (_IncEven + 32) + "", 0, "WHITE", true, 20, "n");

                    //Space 
                    excell_app.createHeaders(_IncEven + 10, 5, "", "E" + (_IncEven + 10) + "", "F" + (_IncEven + 10) + "", 2, "LightCoral", true, 60, "");
                    excell_app.createHeaders(_IncEven + 13, 5, "", "E" + (_IncEven + 13) + "", "F" + (_IncEven + 13) + "", 2, "LightCoral", true, 60, "");
                    excell_app.createHeaders(_IncEven + 16, 5, "", "E" + (_IncEven + 16) + "", "F" + (_IncEven + 16) + "", 2, "LightCoral", true, 60, "");
                    excell_app.createHeaders(_IncEven + 19, 5, "", "E" + (_IncEven + 19) + "", "F" + (_IncEven + 19) + "", 2, "LightCoral", true, 60, "");
                    excell_app.createHeaders(_IncEven + 22, 5, "", "E" + (_IncEven + 22) + "", "F" + (_IncEven + 22) + "", 2, "LightCoral", true, 60, "");
                    excell_app.createHeaders(_IncEven + 25, 5, "", "E" + (_IncEven + 25) + "", "F" + (_IncEven + 25) + "", 2, "LightCoral", true, 60, "");
                    excell_app.createHeaders(_IncEven + 28, 5, "", "E" + (_IncEven + 28) + "", "F" + (_IncEven + 28) + "", 2, "LightCoral", true, 60, "");
                    excell_app.createHeaders(_IncEven + 31, 5, "", "E" + (_IncEven + 31) + "", "F" + (_IncEven + 31) + "", 2, "LightCoral", true, 60, "");
                    excell_app.createHeaders(_IncEven + 34, 5, "", "E" + (_IncEven + 34) + "", "F" + (_IncEven + 34) + "", 2, "LightCoral", true, 60, "");

                    //Add cell data
                    excell_app.addData(_IncEven + 4, 6, "" + item.CaseNumber + "", "F" + (_IncEven + 4) + "", "F" + (_IncEven + 4) + "", "", 20, "LemonChiffon");
                    excell_app.addData(_IncEven + 5, 6, "", "F" + (_IncEven + 5) + "", "F" + (_IncEven + 5) + "", "", 20, "LemonChiffon");
                    excell_app.addData(_IncEven + 6, 6, "" + item.Plantiff + "", "F" + (_IncEven + 6) + "", "F" + (_IncEven + 6) + "", "", 20, "LemonChiffon");

                    excell_app.addData(_IncEven + 7, 6, "XXXXXX", "F" + (_IncEven + 7) + "", "F" + (_IncEven + 7) + "", "", 20, "LemonChiffon");
                    excell_app.addData(_IncEven + 8, 6, "XXXXXX", "F" + (_IncEven + 8) + "", "F" + (_IncEven + 8) + "", "", 20, "LemonChiffon");
                    excell_app.addData(_IncEven + 9, 6, "XXXXXX", "F" + (_IncEven + 9) + "", "F" + (_IncEven + 9) + "", "", 20, "LemonChiffon");

                    //Bind List of defandent
                    int _ContDeff = 1;
                    if (item.defandantList != null)
                    {
                        for (int i = 1; i <= 3; i++)
                        {
                            int _CountDe = item.defandantList.ToList().Count();
                            var _deff = _CountDe >= i ? item.defandantList[i - 1] : null;
                            switch (_ContDeff)
                            {
                                case 1:
                                    if (_deff == null)
                                    {
                                        excell_app.addData(_IncEven + 7, 6, "", "F" + (_IncEven + 7) + "", "F" + (_IncEven + 7) + "", "", 20, "LemonChiffon");
                                    }
                                    else
                                    {
                                        if (_deff.PartyName != "" && _deff.PartyName != null)
                                            excell_app.addData(_IncEven + 7, 6, "" + _deff.PartyName + "", "F" + (_IncEven + 7) + "", "F" + (_IncEven + 7) + "", "", 20, "LemonChiffon");
                                        else
                                            excell_app.addData(_IncEven + 7, 6, "", "F" + (_IncEven + 7) + "", "F" + (_IncEven + 7) + "", "", 20, "LemonChiffon");
                                    }
                                    break;
                                case 2:
                                    if (_deff == null)
                                    {
                                        excell_app.addData(_IncEven + 8, 6, "", "F" + (_IncEven + 8) + "", "F" + (_IncEven + 8) + "", "", 20, "LemonChiffon");
                                    }
                                    else
                                    {
                                        if (_deff.PartyName != "" && _deff.PartyName != null)
                                            excell_app.addData(_IncEven + 8, 6, "" + _deff.PartyName + "", "F" + (_IncEven + 8) + "", "F" + (_IncEven + 8) + "", "", 20, "LemonChiffon");
                                        else
                                            excell_app.addData(_IncEven + 8, 6, "", "F" + (_IncEven + 8) + "", "F" + (_IncEven + 8) + "", "", 20, "LemonChiffon");
                                    }
                                    break;
                                case 3:
                                    if (_deff == null)
                                    {
                                        excell_app.addData(_IncEven + 9, 6, "", "F" + (_IncEven + 9) + "", "F" + (_IncEven + 9) + "", "", 20, "LemonChiffon");
                                    }
                                    else
                                    {
                                        if (_deff.PartyName != "" && _deff.PartyName != null)
                                            excell_app.addData(_IncEven + 9, 6, "" + _deff.PartyName + "", "F" + (_IncEven + 9) + "", "F" + (_IncEven + 9) + "", "", 20, "LemonChiffon");
                                        else
                                            excell_app.addData(_IncEven + 9, 6, "", "F" + (_IncEven + 9) + "", "F" + (_IncEven + 9) + "", "", 20, "LemonChiffon");
                                    }
                                    break;
                            }

                            _ContDeff++;
                        }
                    }
                    else
                    {
                        excell_app.addData(_IncEven + 7, 6, "", "F" + (_IncEven + 7) + "", "F" + (_IncEven + 7) + "", "", 20, "LemonChiffon");
                        excell_app.addData(_IncEven + 8, 6, "", "F" + (_IncEven + 8) + "", "F" + (_IncEven + 8) + "", "", 20, "LemonChiffon");
                        excell_app.addData(_IncEven + 9, 6, "", "F" + (_IncEven + 9) + "", "F" + (_IncEven + 9) + "", "", 20, "LemonChiffon");

                    }

                    excell_app.addData(_IncEven + 12, 5, "XXXXXX", "E" + (_IncEven + 12) + "", "E" + (_IncEven + 12) + "", "", 40, "");
                    excell_app.addData(_IncEven + 12, 6, "XXXXXX", "F" + (_IncEven + 12) + "", "F" + (_IncEven + 12) + "", "", 20, "");

                    excell_app.addData(_IncEven + 15, 5, "XXXXXX", "E" + (_IncEven + 15) + "", "E" + (_IncEven + 15) + "", "", 40, "");
                    excell_app.addData(_IncEven + 15, 6, "XXXXXX", "F" + (_IncEven + 15) + "", "F" + (_IncEven + 15) + "", "", 20, "");

                    excell_app.addData(_IncEven + 18, 5, "" + item.AttornyAddress + "", "E" + (_IncEven + 18) + "", "E" + (_IncEven + 18) + "", "", 40, "");
                    excell_app.addData(_IncEven + 18, 6, "XXXXXX", "F" + (_IncEven + 18) + "", "F" + (_IncEven + 18) + "", "", 20, "");

                    excell_app.addData(_IncEven + 21, 5, "XXXXXX", "E" + (_IncEven + 21) + "", "E" + (_IncEven + 21) + "", "", 40, "");
                    excell_app.addData(_IncEven + 21, 6, "XXXXXX", "F" + (_IncEven + 21) + "", "F" + (_IncEven + 21) + "", "", 20, "");

                    excell_app.addData(_IncEven + 24, 5, "XXXXXX", "E" + (_IncEven + 24) + "", "E" + (_IncEven + 24) + "", "", 40, "");
                    excell_app.addData(_IncEven + 24, 6, "XXXXXX", "F" + (_IncEven + 24) + "", "F" + (_IncEven + 24) + "", "", 20, "");

                    excell_app.addData(_IncEven + 27, 5, "XXXXXX", "E" + (_IncEven + 27) + "", "E" + (_IncEven + 27) + "", "", 40, "");
                    excell_app.addData(_IncEven + 27, 6, "XXXXXX", "F" + (_IncEven + 27) + "", "F" + (_IncEven + 27) + "", "", 20, "");

                    excell_app.addData(_IncEven + 30, 5, "XXXXXX", "E" + (_IncEven + 30) + "", "E" + (_IncEven + 30) + "", "", 40, "");
                    excell_app.addData(_IncEven + 30, 6, "XXXXXX", "F" + (_IncEven + 30) + "", "F" + (_IncEven + 30) + "", "", 20, "");

                    excell_app.addData(_IncEven + 33, 5, "XXXXXX", "E" + (_IncEven + 33) + "", "E" + (_IncEven + 33) + "", "", 40, "");
                    excell_app.addData(_IncEven + 33, 6, "", "F" + (_IncEven + 33) + "", "F" + (_IncEven + 33) + "", "", 20, "");

                    _CountEven++;

                }

                count++;
            }
            Result = "Success";
        }


        #endregion

        #region Party Search

        public ActionResult PartySearch(string FirstName, string LastName, string MiddleName, string casecategory, string Fromdate, string Todate)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                var binary = new FirefoxBinary(Server.MapPath("~/firefox.exe"));
                string path = Server.MapPath("~/firefox.exe");
                FirefoxProfile ffprofile = new FirefoxProfile();
                int Noitem = 0;
                IWebDriver driver = new FirefoxDriver(ffprofile);
                try
                {
                    driver.Manage().Window.Maximize();
                    driver.Navigate().GoToUrl("https://www.browardclerk.org/Web2/");
                    Thread.Sleep(1000);
                    IWebElement EventsDocument = driver.FindElement(By.Id("myTabStandard"));
                    var innerHtml1 = EventsDocument.GetAttribute("innerHTML");

                    string html = @"<html><body>" + innerHtml1.ToString() + "</body></html>";
                    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(html);
                    var nodes = doc.DocumentNode.SelectNodes("//li/a");

                    //var count = 0;
                    //var Chk = 0;
                    if (nodes != null)
                    {
                        foreach (HtmlNode node in nodes)
                        {
                            if (node.InnerText == "Party Name")
                            {
                                var nodepath = node.XPath;
                                var npath = nodepath.Split(new string[] { "body[1]" }, StringSplitOptions.None);
                                var Xpath = "//div[@id='myTabStandard']" + npath[1] + "";
                                driver.FindElement(By.XPath(Xpath)).Click();
                            }
                        }
                    }
                    //string CivilClick = "";
                    string Foldername = "";


                    if (casecategory == "CR,FAM,CV,PR")
                    {
                        Foldername = "Allcategory";
                        driver.FindElement(By.Id("CaseCategoryKeys")).FindElement(By.CssSelector("option[value='" + casecategory + "']")).Click();
                    }
                    else if (casecategory == "CV")
                    {
                        Foldername = "Civil";
                        driver.FindElement(By.Id("CaseCategoryKeys")).FindElement(By.CssSelector("option[value='" + casecategory + "']")).Click();
                    }
                    else if (casecategory == "FAM")
                    {
                        Foldername = "Family";
                        driver.FindElement(By.Id("CaseCategoryKeys")).FindElement(By.CssSelector("option[value='" + casecategory + "']")).Click();
                    }
                    else if (casecategory == "PR")
                    {
                        Foldername = "Felony";
                        driver.FindElement(By.Id("CaseCategoryKeys")).FindElement(By.CssSelector("option[value='" + casecategory + "']")).Click();
                    }
                    else if (casecategory == "FEL")
                    {
                        Foldername = "Probate";
                        driver.FindElement(By.Id("CaseCategoryKeys")).FindElement(By.CssSelector("option[value='" + casecategory + "']")).Click();
                    }
                    else
                    {
                        Foldername = "TrafficMis";
                        driver.FindElement(By.Id("CaseCategoryKeys")).FindElement(By.CssSelector("option[value='" + casecategory + "']")).Click();
                    }

                    driver.FindElement(By.Id("lastName")).SendKeys(LastName);

                    driver.FindElement(By.Id("firstName")).SendKeys(FirstName);

                    if (MiddleName != null && MiddleName != "")
                    {
                        driver.FindElement(By.Id("middleName")).SendKeys(MiddleName);
                    }

                    if (Fromdate != null && Fromdate != "")
                    {
                        driver.FindElement(By.Id("filingDateOnOrAfterP")).SendKeys(Fromdate);
                        driver.FindElement(By.Id("filingDateOnOrBeforeP")).SendKeys(Todate);
                    }
                    Thread.Sleep(500);
                    driver.FindElement(By.Id("PersonSearchResults")).Click();
                    Thread.Sleep(1000);

                    IWebElement _Div = driver.FindElement(By.Id("SearchResultsGrid"));


                    IWebElement _PageText = _Div.FindElement(By.ClassName("k-pager-last"));
                    int _PageNumber = Convert.ToInt32(_PageText.GetAttribute("data-page").ToString());

                    if (_PageNumber == 0)
                    {
                        driver.Quit();
                        Noitem = 1;

                    }
                    IWebElement elements = driver.FindElement(By.TagName("tbody"));

                    string CurrentDate = DateTime.Now.ToString("dd_MM_yyyy_hh_mm");
                    DateTime SearchedDate = Convert.ToDateTime(DateTime.Now);
                    string Folderpath = "" + Foldername + "_" + CurrentDate + "";
                    string directoryPath = "C:/PDfFiles/" + Folderpath + "";// Server.MapPath("~/PDfFiles/" + Folderpath + "");
                    Directory.CreateDirectory(directoryPath);
                    _vmcase.SavePartySeacrhHistory(FirstName, LastName, (string.IsNullOrEmpty(Todate) ? (DateTime?)null : Convert.ToDateTime(Todate)), (string.IsNullOrEmpty(Fromdate) ? (DateTime?)null : Convert.ToDateTime(Fromdate)), Foldername, SearchedDate);

                    GetpdfFiles(Folderpath, driver, "1");

                    for (int i = 1; i < _PageNumber; i++)
                    {
                        int num = Convert.ToInt32(i + 1);
                        Thread.Sleep(1000);
                        var currentWindow1 = driver.CurrentWindowHandle;
                        var availableWindows1 = new List<string>(driver.WindowHandles);
                        if (availableWindows1 != null)
                        {
                            foreach (string w in availableWindows1)
                            {
                                IWebDriver popup = null;
                                if (w != currentWindow1)
                                {
                                    driver.SwitchTo().Window(w);
                                    popup = driver.SwitchTo().Window(w);
                                    popup.Close();
                                    driver.SwitchTo().Window(currentWindow1);
                                    driver.FindElement(By.ClassName("k-i-arrow-e")).Click();
                                }
                            }
                        }
                        GetpdfFiles(Folderpath, driver, num.ToString());

                        if (_PageNumber == Convert.ToInt32(num))
                        {
                            if (availableWindows1 != null)
                            {
                                foreach (string w in availableWindows1)
                                {
                                    IWebDriver popup = null;
                                    if (w != currentWindow1)
                                    {
                                        driver.SwitchTo().Window(w);
                                        popup = driver.SwitchTo().Window(w);
                                        popup.Close();
                                        driver.SwitchTo().Window(currentWindow1);
                                        driver.FindElement(By.ClassName("k-pager-first")).Click();
                                        popup = driver.SwitchTo().Window(currentWindow1);
                                        popup.Close();
                                        break;
                                    }
                                }
                            }
                        }

                    }

                    var currentWindow2 = driver.CurrentWindowHandle;
                    var availableWindows2 = new List<string>(driver.WindowHandles);
                    foreach (string w in availableWindows2)
                    {
                        IWebDriver popup2 = null;
                        if (w != currentWindow2)
                        {
                            driver.SwitchTo().Window(w);
                            popup2 = driver.SwitchTo().Window(w);
                            popup2.Close();
                        }
                    }
                    driver.Quit();

                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    if (Noitem == 1)
                    {
                        return Json("No Items Found.", JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        var currentWindow3 = driver.CurrentWindowHandle;
                        var availableWindows3 = new List<string>(driver.WindowHandles);
                        foreach (string w in availableWindows3)
                        {
                            IWebDriver popup3 = null;
                            if (w != currentWindow3)
                            {
                                driver.SwitchTo().Window(w);
                                popup3 = driver.SwitchTo().Window(w);
                                popup3.Close();
                            }
                        }
                        driver.Quit();
                        return Json("Error ! Please Try Again", JsonRequestBehavior.AllowGet);
                    }

                }
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PartySearchHistory()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<tblPartySearchHistory> _tblPartySearch = db.tblPartySearchHistories.OrderByDescending(x => x.PartySearchID).ToList();
                return PartialView("_PartySearchHistory", _tblPartySearch);
            }
            return RedirectToAction("Login", "Home");
        }

        public string GetPdfDetailsParty(int? PartySearchID)
        {
            tblPartySearchHistory _SrchDate = new tblPartySearchHistory();
            var getData = db.tblPartySearchHistories.Where(x => x.PartySearchID == PartySearchID).FirstOrDefault();
            string SrchedType = getData.PartyCourtType;
            DateTime _dt = Convert.ToDateTime(getData.PartySearchDate);

            string srcdt = _dt.ToString("dd/MM/yyyy hh:mm");
            Session["Searcheddate"] = _dt;
            ViewBag.PartySearchDate = _dt;
            string[] SrchedDate = srcdt.Split('/');
            //var sourcePath = "C:/PDfFiles/";// Server.MapPath("~/PDfFiles/");
            var Foldername = SrchedType + "_" + SrchedDate[0] + "_" + SrchedDate[1] + "_" + SrchedDate[2].Split(' ')[0] + "_" + SrchedDate[2].Split(' ')[1].Split(':')[0] + "_" + SrchedDate[2].Split(' ')[1].Split(':')[1] + "";
            //var Result = "";
            return Foldername;

        }

        public ActionResult PartyDetails(int PartySearchID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                tblPartySearchHistory _SrchDate = new tblPartySearchHistory();
                var getData = db.tblPartySearchHistories.Where(x => x.PartySearchID == PartySearchID).FirstOrDefault();
                string SrchedType = getData.PartyCourtType;
                DateTime _dt = Convert.ToDateTime(getData.PartySearchDate);

                string srcdt = _dt.ToString("dd/MM/yyyy hh:mm");
                Session["Searcheddate"] = _dt;
                ViewBag.PartySearchDate = _dt;
                string[] SrchedDate = srcdt.Split('/');
                var sourcePath = "C:/PDfFiles/";// Server.MapPath("~/PDfFiles/");
                var Foldername = SrchedType + "_" + SrchedDate[0] + "_" + SrchedDate[1] + "_" + SrchedDate[2].Split(' ')[0] + "_" + SrchedDate[2].Split(' ')[1].Split(':')[0] + "_" + SrchedDate[2].Split(' ')[1].Split(':')[1] + "";
                //var Result = "";
                Session["FolderName"] = Foldername;
                if (System.IO.Directory.Exists(sourcePath))
                {
                    string[] _Dir = System.IO.Directory.GetDirectories(sourcePath);

                    foreach (string FilesPath in _Dir)
                    {
                        string dirName = new DirectoryInfo(FilesPath).Name;
                        if (dirName == Foldername)
                        {
                            string[] files11 = System.IO.Directory.GetFiles(FilesPath);

                            foreach (var _file in files11)
                            {
                                string FileName = new FileInfo(_file).Name;
                                string FileExtensn = new FileInfo(_file).Extension;
                                if (FileExtensn == ".txt")
                                {
                                    string caseNumber1 = FileName.Split('_')[0].Trim();
                                    _CaseNumber.Add(caseNumber1);
                                }
                                else
                                {
                                    string caseNumber = FileName.Split('-')[0].Trim();
                                    _CaseNumber.Add(caseNumber);
                                }
                            }
                        }
                    }
                }

                var filename = Server.MapPath("~/ExcelFiles/TransCapitalRecords.ods");
                Microsoft.Office.Interop.Excel.Application xlApp;
                Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
                Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
                var missing = System.Reflection.Missing.Value;

                xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlWorkBook = xlApp.Workbooks.Open(filename, false, true, missing, missing, missing, true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, '\t', false, false, 0, false, true, 0);
                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                Microsoft.Office.Interop.Excel.Range xlRange = xlWorkSheet.UsedRange;
                Array myValues = (Array)xlRange.Cells.Value2;

                int vertical = myValues.GetLength(0);
                int horizontal = myValues.GetLength(1);

                System.Data.DataTable dt = new System.Data.DataTable();
                // must start with index = 1
                // get header information
                for (int i = 1; i <= horizontal; i++)
                {
                    dt.Columns.Add(new DataColumn(myValues.GetValue(1, i).ToString()));
                }
                // Get the row information
                for (int a = 2; a <= vertical; a++)
                {
                    object[] poop = new object[horizontal];
                    for (int b = 1; b <= horizontal; b++)
                    {
                        string _value = myValues.GetValue(a, b).ToString();
                        var FilterHtml = Regex.Replace(_value, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                        poop[b - 1] = FilterHtml;
                    }
                    DataRow row = dt.NewRow();
                    string popitem = poop[0].ToString();
                    bool _result = _CaseNumber.Contains(popitem.Trim());
                    if (_result == true)
                    {
                        row.ItemArray = poop;
                        dt.Rows.Add(row);
                    }
                }
                ViewBag.CaseTable = dt;
                // assign table to default data grid view

                xlWorkBook.Close(true, missing, missing);
                xlApp.Quit();

                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);

                return View(dt);
            }
            return RedirectToAction("Login", "Home");
        }

        #endregion

        #region Case Search

        public ActionResult CaseSearch(string CaseNumber)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                var binary = new FirefoxBinary(Server.MapPath("~/firefox.exe"));
                string path = Server.MapPath("~/firefox.exe");
                string SearchType = "Bulk";
                FirefoxProfile ffprofile = new FirefoxProfile();
                IWebDriver driver = new FirefoxDriver(ffprofile);
                try
                {
                    driver.Manage().Window.Maximize();

                    driver.Navigate().GoToUrl("https://www.browardclerk.org/Web2/");
                    Thread.Sleep(1000);

                    IWebElement EventsDocument = driver.FindElement(By.Id("myTabStandard"));
                    var innerHtml1 = EventsDocument.GetAttribute("innerHTML");

                    string html = @"<html><body>" + innerHtml1.ToString() + "</body></html>";
                    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(html);
                    var nodes = doc.DocumentNode.SelectNodes("//li/a");

                    //var count = 0;
                    //var Chk = 0;
                    foreach (HtmlNode node in nodes)
                    {
                        if (node.InnerText == "Case Number")
                        {
                            var nodepath = node.XPath;
                            var npath = nodepath.Split(new string[] { "body[1]" }, StringSplitOptions.None);
                            var Xpath = "//div[@id='myTabStandard']" + npath[1] + "";
                            //html[1]/body[1]/div[1]/div[1]/ul[1]/li[3]/a[1]

                            driver.FindElement(By.XPath(Xpath)).Click();
                        }
                    }

                    driver.FindElement(By.Id("CaseNumber")).SendKeys(CaseNumber);
                    Thread.Sleep(1000);
                    driver.FindElement(By.Id("CaseNumberSearchResults")).Click();
                    //string Search = Server.MapPath("~/Images/Search.png");
                    //SikuliAction.Click(Search);
                    Thread.Sleep(1000);

                    IWebElement _Div = driver.FindElement(By.Id("SearchResultsGrid"));
                    var Foldername = CaseNumber;
                    string CurrentDate = DateTime.Now.ToString("dd_MM_yyyy_hh_mm");
                    DateTime SearchedDate = Convert.ToDateTime(DateTime.Now);
                    string Folderpath = "" + Foldername + "_" + CurrentDate + "";
                    string directoryPath = "C:/PDfFiles/" + Folderpath + "";// Server.MapPath("~/PDfFiles/" + Folderpath + "");
                    Directory.CreateDirectory(directoryPath);
                    var innerhtml = _Div.GetAttribute("innerHTML");
                    string html1 = @"<html><body>" + innerhtml.ToString() + "</body></html>";
                    HtmlAgilityPack.HtmlDocument doc1 = new HtmlAgilityPack.HtmlDocument();
                    doc1.LoadHtml(html1);
                    var nodes1 = doc1.DocumentNode.SelectNodes("//div[2]/table[1]/tbody/tr/td");
                    ArrayList _Tablecolumn = new ArrayList();
                    foreach (HtmlNode node in nodes1)
                    {
                        _Tablecolumn.Add(node.InnerText);
                    }
                    string CaseNumber1 = Convert.ToString(_Tablecolumn[0]);
                    string CaseStyle = Convert.ToString(_Tablecolumn[1]);
                    string CaseType = Convert.ToString(_Tablecolumn[2]);
                    DateTime Fillingdate = Convert.ToDateTime(_Tablecolumn[3]);
                    string CaseStatus = Convert.ToString(_Tablecolumn[4]);
                    _vmcase.SaveCaseSearchHistory(SearchedDate, CaseNumber1, CaseStyle, CaseType, Fillingdate, CaseStatus, SearchType);
                    GetpdfFiles(Folderpath, driver, "1");
                    driver.Quit();
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    driver.Quit();
                    return Json("Error ! Please Try Again", JsonRequestBehavior.AllowGet);
                }
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult CaseNumberHistory()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<tblCaseNumberSearch> _tblCaseNumberSearch = db.tblCaseNumberSearches.OrderByDescending(x => x.CaseNumberID).ToList();
                return PartialView("_CaseSearchResult", _tblCaseNumberSearch);
            }
            return RedirectToAction("Login", "Home");
        }
        public ActionResult CaseSearchDetails(int CaseNumberID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                string SrchedType = GetPdfDetailsFolder(CaseNumberID);
                return RedirectToAction("ViewDetails", "Home", new { CaseNumber = SrchedType });
            }
            return RedirectToAction("Login", "Home");
        }

        private string GetPdfDetailsFolder(int? CaseNumberID)
        {
            SearchHistory _SrchDate = new SearchHistory();
            var getData = db.tblCaseNumberSearches.Where(x => x.CaseNumberID == CaseNumberID).FirstOrDefault();
            string SrchedType = getData.CaseNumber.Trim();
            DateTime _dt = Convert.ToDateTime(getData.CaseSearchDate);
            Session["Searcheddate"] = _dt;
            Session["CaseNumberID"] = getData.CaseNumberID;
            string srcdt = _dt.ToString("dd/MM/yyyy hh:mm");
            string[] SrchedDate = srcdt.Split('/');
            var Foldername = SrchedType + "_" + SrchedDate[0] + "_" + SrchedDate[1] + "_" + SrchedDate[2].Split(' ')[0] + "_" + SrchedDate[2].Split(' ')[1].Split(':')[0] + "_" + SrchedDate[2].Split(' ')[1].Split(':')[1] + "";
            Session["FolderName"] = Foldername;
            return SrchedType;
        }

        public ActionResult GetpdfFromCaseHistory(int CaseNumberID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                string SrchedType = GetPdfDetailsFolder(CaseNumberID);
                return RedirectToAction("ViewOriginalPdf", "Home", new { CaseNumber = SrchedType });
            }
            return RedirectToAction("Login", "Home");
        }
        public ActionResult GetAddressByCase(int CaseNumberID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                string _CaseNumber = GetPdfDetailsFolder(CaseNumberID);
                return RedirectToAction("ConvertByCaseNumber", "Home", new { CaseNumber = _CaseNumber });
            }
            return RedirectToAction("Login", "Home");
        }

        #endregion

        #region Citation Search

        public ActionResult CitationSearch(string CitationNumber)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                var binary = new FirefoxBinary(Server.MapPath("~/firefox.exe"));
                string path = Server.MapPath("~/firefox.exe");

                FirefoxProfile ffprofile = new FirefoxProfile();
                IWebDriver driver = new FirefoxDriver(ffprofile);
                try
                {
                    driver.Manage().Window.Maximize();

                    driver.Navigate().GoToUrl("https://www.browardclerk.org/Web2/");
                    Thread.Sleep(1000);

                    IWebElement EventsDocument = driver.FindElement(By.Id("myTabStandard"));
                    var innerHtml1 = EventsDocument.GetAttribute("innerHTML");

                    string html = @"<html><body>" + innerHtml1.ToString() + "</body></html>";
                    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(html);
                    var nodes = doc.DocumentNode.SelectNodes("//li/a");

                    //var count = 0;
                    //var Chk = 0;
                    foreach (HtmlNode node in nodes)
                    {
                        if (node.InnerText == "Citation Number")
                        {
                            var nodepath = node.XPath;
                            var npath = nodepath.Split(new string[] { "body[1]" }, StringSplitOptions.None);
                            var Xpath = "//div[@id='myTabStandard']" + npath[1] + "";
                            //html[1]/body[1]/div[1]/div[1]/ul[1]/li[3]/a[1]

                            driver.FindElement(By.XPath(Xpath)).Click();
                        }
                    }

                    driver.FindElement(By.Id("CitNumber")).SendKeys(CitationNumber);
                    Thread.Sleep(1000);
                    driver.FindElement(By.Id("CitationSearchResults")).Click();
                    //string Search = Server.MapPath("~/Images/Search.png");
                    //SikuliAction.Click(Search);
                    Thread.Sleep(1000);

                    IWebElement _Div = driver.FindElement(By.Id("SearchResultsGrid"));
                    var Foldername = CitationNumber;
                    string CurrentDate = DateTime.Now.ToString("dd_MM_yyyy_hh_mm");
                    DateTime SearchedDate = Convert.ToDateTime(DateTime.Now);
                    string Folderpath = "" + Foldername + "_" + CurrentDate + "";
                    string directoryPath = "C:/PDfFiles/" + Folderpath + "";// Server.MapPath("~/PDfFiles/" + Folderpath + "");
                    Directory.CreateDirectory(directoryPath);
                    var innerhtml = _Div.GetAttribute("innerHTML");
                    string html1 = @"<html><body>" + innerhtml.ToString() + "</body></html>";
                    HtmlAgilityPack.HtmlDocument doc1 = new HtmlAgilityPack.HtmlDocument();
                    doc1.LoadHtml(html1);
                    var nodes1 = doc1.DocumentNode.SelectNodes("//div[2]/table[1]/tbody/tr/td");
                    ArrayList _Tablecolumn = new ArrayList();
                    foreach (HtmlNode node in nodes1)
                    {
                        _Tablecolumn.Add(node.InnerText);
                    }
                    string CaseNumber1 = Convert.ToString(_Tablecolumn[0]);
                    string CaseStyle = Convert.ToString(_Tablecolumn[1]);
                    string CaseType = Convert.ToString(_Tablecolumn[2]);
                    DateTime Fillingdate = Convert.ToDateTime(_Tablecolumn[3]);
                    string CaseStatus = Convert.ToString(_Tablecolumn[4]);
                    _vmcase.SaveCitationSeacrhHistory(SearchedDate, CitationNumber, CaseNumber1.Trim(), CaseStyle, CaseType, Fillingdate, CaseStatus);
                    GetpdfFiles(Folderpath, driver, "1");
                    driver.Quit();
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    driver.Quit();
                    return Json("Error ! Please Try Again", JsonRequestBehavior.AllowGet);
                }
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Login", "Home");
        }


        public ActionResult CitationNumberHistory()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<tblCitationNumberHistory> _tblCitationNumberSearch = db.tblCitationNumberHistories.OrderByDescending(x => x.CitationNumberID).ToList();
                return PartialView("_CitationNumberHistory", _tblCitationNumberSearch);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult CitationSearchDetails(int CitationNumberID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                string SrchedType = GetPdfDetailsInCitation(CitationNumberID);
                return RedirectToAction("ViewDetails", "Home", new { CaseNumber = SrchedType, CtnID = CitationNumberID });
            }
            return RedirectToAction("Login", "Home");
        }


        private string GetPdfDetailsInCitation(int? CitationNumberID)
        {
            SearchHistory _SrchDate = new SearchHistory();
            var getData = db.tblCitationNumberHistories.Where(x => x.CitationNumberID == CitationNumberID).FirstOrDefault();
            string SrchedTypeCTNumber = getData.CitationNumber.Trim();
            string SrchedTypeCaseNumber = getData.CaseNumber.Trim();
            DateTime _dt = Convert.ToDateTime(getData.CitationNumberDate);
            Session["Searcheddate"] = _dt;
            string srcdt = _dt.ToString("dd/MM/yyyy hh:mm");
            string[] SrchedDate = srcdt.Split('/');
            //var sourcePath = "C:/PDfFiles/";// Server.MapPath("~/PDfFiles/");
            var Foldername = SrchedTypeCTNumber + "_" + SrchedDate[0] + "_" + SrchedDate[1] + "_" + SrchedDate[2].Split(' ')[0] + "_" + SrchedDate[2].Split(' ')[1].Split(':')[0] + "_" + SrchedDate[2].Split(' ')[1].Split(':')[1] + "";
            Session["FolderName"] = Foldername;
            return SrchedTypeCaseNumber;
        }

        public ActionResult GetpdfFromCitationHistory(int CitationNumberID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                string SrchedType = GetPdfDetailsInCitation(CitationNumberID);
                return RedirectToAction("ViewOriginalPdf", "Home", new { CaseNumber = SrchedType });
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult GetaddressBycitaion(int CitationNumberID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                string _Casenumber = GetPdfDetailsInCitation(CitationNumberID);
                return RedirectToAction("ConvertByCaseNumber", "Home", new { CaseNumber = _Casenumber });
            }
            return RedirectToAction("Login", "Home");
        }
        #endregion

        #region Daily Defendent Search

        public ActionResult BrowardDocReport()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                GetTabsSubTabs();
                return View();
            }
            return RedirectToAction("Login", "Home");
        }

        private void GetTabsSubTabs()
        {
            TabsCountedLp _TabsCountedLp = _vmcase.GetCountTabsLpCase();

            System.Data.DataTable _SubTabCnt = _vmcase.GetSubTabsCount();

            ViewData["SubTabCnt"] = _SubTabCnt;
            if (_TabsCountedLp != null)
            {
                ViewBag.Residential = _TabsCountedLp.Residential;
                ViewBag.Commercial = _TabsCountedLp.Commercial;
                ViewBag.Industrial = _TabsCountedLp.Industrial;
                ViewBag.Agricultural = _TabsCountedLp.Agricultural;
                ViewBag.Institutional = _TabsCountedLp.Institutional;
                ViewBag.Government = _TabsCountedLp.Government;
                ViewBag.Miscellaneous = _TabsCountedLp.Miscellaneous;
                ViewBag.CentrallyAssessed = _TabsCountedLp.CentrallyAssessed;
                ViewBag.NonAgricultural = _TabsCountedLp.NonAgricultural;
                ViewBag.ALLLEADS = _TabsCountedLp.ALLLEADS;
                ViewBag.MultiProperty = _TabsCountedLp.MultiProperty;
                ViewBag.SetSubjtProperty = _TabsCountedLp.SetSubjtProperty;
                ViewBag.NotFound = _TabsCountedLp.NotFound;
                ViewBag.totalMiscellaneous = _TabsCountedLp.totalMiscellaneous;
            }
            List<UseCodeCategory> _PropertyType = _vmcase.GetPropertyTypeTab();
            ViewBag.PropertyType = _PropertyType;
        }

        private void PageWiseDocResult(HtmlAgilityPack.HtmlDocument _docResult, string[] _FilteredItem, int k)
        {
            int _countColumn = 1;
            foreach (HtmlNode node in _docResult.DocumentNode.SelectNodes("//tr[" + k + "]/td[4]"))
            {
                var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                foreach (var item in _FilteredItem)
                {
                    if (_text.ToLower().Contains(item))
                    {
                        var _resultUrl = node.InnerHtml;

                        string htmlResultr = @"<html><body>" + _resultUrl.ToString() + "</body></html>";
                        HtmlAgilityPack.HtmlDocument _docR = new HtmlAgilityPack.HtmlDocument();
                        _docR.LoadHtml(htmlResultr);
                        foreach (HtmlNode node1 in _docR.DocumentNode.SelectNodes("//a"))
                        {
                            HtmlAttribute att = node1.Attributes["href"];
                            string _htmlurl = node1.SelectSingleNode("//a").Attributes["href"].Value;
                            string _completeUrl = "https://officialrecords.broward.org/oncoreV2/" + _htmlurl + "";
                            string _FilteredUrl = Regex.Replace(_completeUrl, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", "&").Trim();
                            _DocResultURL.Add(_FilteredUrl);
                        }
                        break;
                    }
                }

            }
            _countColumn++;
        }

        public ActionResult ShowReport(int UseCodeCategoryID, string SubTabId, int? CntRecord)
        {
             if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<SrchDefandant> _tblSrchDefandant = new List<SrchDefandant>();

                //_tblSrchDefandant = GetSrchDefandentLpCases(UseCodeCategoryID, SubTabId, CntRecord, _tblSrchDefandant);

                //TempData["showDocReportLp"] = _tblSrchDefandant;

                return PartialView("_ShowdocReport", _tblSrchDefandant);
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public ActionResult LoadBrowardData(int UseCodeCategoryID, string SubTabId, int? CntRecord )
        {

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            using (BrowardclerkEntities dc = new BrowardclerkEntities())
            {
                IQueryable<SrchDefandant> _tblSrchDefandant;

                if (TempData["showDocReportLp"] != null)
                {
                    _tblSrchDefandant = ((List<SrchDefandant>)TempData["showDocReportLp"]).AsQueryable();
                }
                else
                {
                    if (SubTabId == "Tab01")
                    {
                        _tblSrchDefandant = _vmcase.GetSaerchDefendent(UseCodeCategoryID, null, CntRecord, null).OrderByDescending(x => x.SrchDefendantID).AsQueryable();
                    }
                    else
                    {
                        _tblSrchDefandant = _vmcase.GetSaerchDefendent(UseCodeCategoryID, null, CntRecord, SubTabId).OrderByDescending(x => x.SrchDefendantID).AsQueryable();
                    }
                }
                //SORT
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    bool desc = sortColumnDir == "asc";
                    _tblSrchDefandant = Extenstions.OrderBy(_tblSrchDefandant, sortColumn, desc);
                }
                if(pageSize==-1)
                {
                    pageSize = _tblSrchDefandant.Count();
                }
                recordsTotal = _tblSrchDefandant.Count();
                var data = _tblSrchDefandant.Skip(skip).Take(pageSize).ToList();
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);
            }
        }

        private List<SrchDefandant> GetSrchDefandentLpCases(int UseCodeCategoryID, string SubTabId, int? CntRecord, List<SrchDefandant> _tblSrchDefandant)
        {
            Session["SubTabId"] = SubTabId;
            if (UseCodeCategoryID != 0)
            {
                if (SubTabId != null && SubTabId != "")
                {
                    if (SubTabId == "Tab01")
                    {
                        _tblSrchDefandant = _vmcase.GetSaerchDefendent(UseCodeCategoryID, null, CntRecord, null).OrderByDescending(x => x.OTHAddressID).ToList();
                    }
                    else
                    {
                        _tblSrchDefandant = _vmcase.GetSaerchDefendent(UseCodeCategoryID, null, CntRecord, SubTabId).OrderByDescending(x => x.OTHAddressID).ToList();
                    }
                }

            }
            return _tblSrchDefandant;
        }

       
        public ActionResult GetContactInfoForm(int OTHAddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                ContactInfo _cntInfo = new ContactInfo();
                try
                {
                    _cntInfo = _vmcase.GetContactInfo(OTHAddressID);

                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                }
                return PartialView("_ContactInfo", _cntInfo);
            }
            return RedirectToAction("Login", "Home");
        }
        public ActionResult SaveContactInfo(int OTHAddressID, string Mobile, string Email, string Landline, string Landline1, string Mobile2, string Email1, string Email2, string Email3, string NOTESBOX)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    _vmcase.SaveAndUppdateContactInfo(OTHAddressID, Mobile, Email, Landline, Landline1, Mobile2, Email1, Email2, Email3, NOTESBOX);
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult GetPalmContactInfoForm(long? PCBPalmDetailsID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                ContactInfo _cntInfo = new ContactInfo();
                try
                {
                    _cntInfo = _vmPalmCase.GetPalmContactInfo(PCBPalmDetailsID);

                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                }
                return PartialView("_PalmContactInfo", _cntInfo);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult SavePalmContactInfo(long PCBPalmDetailsID, string Mobile, string Email, string Landline, string Landline1, string Mobile2, string Email1, string Email2, string Email3, string NOTESBOX)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    _vmPalmCase.SaveAndUpdatePalmContactInfo(PCBPalmDetailsID, Mobile, Email, Landline, Landline1, Mobile2, Email1, Email2, Email3, NOTESBOX);
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult SaveEstOwnerEquity(int UseCodeCategoryID, string SubTabId, int? CntRecord)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<SrchDefandant> _tblSrchDefandant = new List<SrchDefandant>();
                try
                {
                    List<SrchDefandant> _tblSrch = GetSrchDefandentLpCases(UseCodeCategoryID, SubTabId, CntRecord, _tblSrchDefandant);

                    GetSaveEstOwnerEquity(_tblSrch);

                    _tblSrchDefandant = GetSrchDefandentLpCases(UseCodeCategoryID, SubTabId, CntRecord, _tblSrchDefandant);

                    //return PartialView("_ShowdocReport", _tblSrchDefandant);
                    return PartialView("_BrowardTabs", _tblSrchDefandant);
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    return null;
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult FilterbyAssesedvalue(int UseCodeCategoryID,string SubTabId, int? AssesedFrom, int? AssesedTo, decimal? LandFactorFrom, decimal? LandFactorTo, int? Zipcode, decimal? UnitsMin,decimal? UnitsMax, decimal? BedroomMin,decimal? BedroomMax, decimal? BathroomMin,decimal? BathroomMax,string LocationAddresses, string MailingAddresses, string FolioIDs, string Plaintiffs, string Defendants,string RecordDateFroms,string RecordDateTos,string SaleDateFroms,string SaleDateTos,int? SalePriceFrom,int? SalePriceTo,decimal? EstimatedMortFrom,decimal? EstimatedMortTo,decimal? EstOwnerFrom,decimal? EstOwnerTo,int FilterStatuss,int TaxIssueStatuss,string HOAfeemins,string HOAfeemaxs)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<SrchDefandant> _tblSrchDefandant = new List<SrchDefandant>();
                DateTime? RecordDateFrom = null;
                DateTime? RecordDateTo = null;
                DateTime? SaleDateFrom = null;
                DateTime? SaleDateTo = null;
                string FilterStatus = "";
                string TaxIssueStatus = "";
                string HOAfeemin = "";
                string HOAfeemax = "";
                if (!string.IsNullOrEmpty(RecordDateFroms))
                {
                    RecordDateFroms = RecordDateFroms.Split('/')[2] + "-" + RecordDateFroms.Split('/')[0] + "-" + RecordDateFroms.Split('/')[1];
                    RecordDateFrom = Convert.ToDateTime(RecordDateFroms).Date;
                }

                if (!string.IsNullOrEmpty(RecordDateTos))
                {
                    RecordDateTos = RecordDateTos.Split('/')[2] + "-" + RecordDateTos.Split('/')[0] + "-" + RecordDateTos.Split('/')[1];
                    RecordDateTo = Convert.ToDateTime(RecordDateTos).Date;
                }

                if (!string.IsNullOrEmpty(SaleDateFroms))
                if (!string.IsNullOrEmpty(SaleDateFroms))
                {
                    SaleDateFroms = SaleDateFroms.Split('/')[2] + "-" + SaleDateFroms.Split('/')[0] + "-" + SaleDateFroms.Split('/')[1];
                    SaleDateFrom = Convert.ToDateTime(SaleDateFroms).Date;
                }
                if (!string.IsNullOrEmpty(SaleDateTos))
                {
                    SaleDateTos = SaleDateTos.Split('/')[2] + "-" + SaleDateTos.Split('/')[0] + "-" + SaleDateTos.Split('/')[1];
                    SaleDateTo = Convert.ToDateTime(SaleDateTos).Date;
                }

                if (FilterStatuss == 1)
                {
                    FilterStatus = "1,2";
                }
                else if (FilterStatuss == 2)
                {
                    FilterStatus = " ";
                }
                else
                {
                    FilterStatus = "1,2, ";
                }

                if (TaxIssueStatuss == 1)
                {
                    TaxIssueStatus = "TAX ISSUE";
                }
                else if (TaxIssueStatuss == 2)
                {
                    TaxIssueStatus = "NO ISSUE–CY Paid";
                }
                else if (TaxIssueStatuss == 3)
                {
                    TaxIssueStatus = "NO ISSUE–CY Not Paid";
                }
                else if (TaxIssueStatuss == 4)
                {
                    TaxIssueStatus = "NO ISSUE(Past)–CY Paid";
                }
                else if (TaxIssueStatuss == 5)
                {
                    TaxIssueStatus = "NO ISSUE(Past)–CY Not Paid";
                }
                else if(TaxIssueStatuss == 0)
                {
                    TaxIssueStatus = "TAX ISSUE,NO ISSUE–CY Paid,NO ISSUE–CY Not Paid,NO ISSUE(Past)–CY Paid,NO ISSUE(Past)–CY Not Paid";
                }
                if (HOAfeemins == ""){
                    HOAfeemin = null;

                }
                else
                {
                    HOAfeemin = HOAfeemins;
                }

                if (HOAfeemaxs == "")
                {
                    HOAfeemax = null;

                }
                else
                {
                    HOAfeemax = HOAfeemaxs;
                }
                string MailingAddress;
                string LocationAddress;
                string FolioID;
                string Plaintiff;
                string Defendant;
                
                if (LocationAddresses == "")
                {
                    LocationAddress = null;
                }
                else
                {
                    LocationAddress = LocationAddresses;
                }
                if (MailingAddresses == "")
                {
                    MailingAddress = null;
                }
                else
                {
                    MailingAddress = MailingAddresses;
                }
                if (FolioIDs == "")
                {
                    FolioID = null;

                }
                else
                {
                    FolioID = FolioIDs;
                }
                if (Defendants == "")
                {
                    Defendant = null;
                }
                else
                {
                    Defendant = Defendants;
                }
                if (Plaintiffs == "")
                {
                    Plaintiff = null;
                }
                else
                {
                    Plaintiff= Plaintiffs;
                }
                try
                {
                    if (SubTabId == "Tab01")
                    {
                        _tblSrchDefandant = _vmcase.DefandantFilterByAssesd(UseCodeCategoryID, null, AssesedFrom, AssesedTo, LandFactorFrom, LandFactorTo, Zipcode, UnitsMin, UnitsMax, BedroomMin, BedroomMax, BathroomMin, BathroomMax, LocationAddress, MailingAddress, FolioID, Plaintiff, Defendant,RecordDateFrom,RecordDateTo,SaleDateFrom,SaleDateTo,SalePriceFrom,SalePriceTo,EstimatedMortFrom,EstimatedMortTo,EstOwnerFrom,EstOwnerTo, FilterStatus,TaxIssueStatus, HOAfeemin, HOAfeemax).OrderByDescending(x => x.SrchDefendantID).ToList();
                    }
                    else
                    {
                        _tblSrchDefandant = _vmcase.DefandantFilterByAssesd(UseCodeCategoryID, SubTabId, AssesedFrom, AssesedTo, LandFactorFrom, LandFactorTo, Zipcode, UnitsMin, UnitsMax, BedroomMin, BedroomMax, BathroomMin, BathroomMax, LocationAddress, MailingAddress, FolioID, Plaintiff, Defendant, RecordDateFrom, RecordDateTo, SaleDateFrom, SaleDateTo, SalePriceFrom, SalePriceTo, EstimatedMortFrom, EstimatedMortTo, EstOwnerFrom, EstOwnerTo, FilterStatus,TaxIssueStatus, HOAfeemin, HOAfeemax).OrderByDescending(x => x.SrchDefendantID).ToList();
                    }
                    TempData["showDocReportLp"] = _tblSrchDefandant;
                }
                catch
                {

                }
                GetTabsSubTabs();
                return PartialView("_BrowardTabs", _tblSrchDefandant);
            }
            return RedirectToAction("Login", "Home");
        }


        public ActionResult FiltersINPalmBeach(int UseCodeCategoryID, string SubTabId, int? AssesedFrom, int? AssesedTo, decimal? LandFactorFrom, decimal?LandFactorTo, int? Zipcode, decimal? UnitsMin, decimal? UnitsMax, decimal? BedroomMin, decimal? BedroomMax, decimal? BathroomMin, decimal? BathroomMax,string LocationAddress, string MailingAddress, string Plaintiff, string Defendant,string RecordDateFroms, string RecordDateTos, DateTime? SaleDateFrom, DateTime? SaleDateTo, int? SalePriceFrom, int? SalePriceTo, decimal? EstimatedMortFrom, decimal? EstimatedMortTo, decimal? EstOwnerFrom, decimal? EstOwnerTo,int HOAFilterStatuss,string HOAfeemins,string HOAfeemaxs)
        
            {
            DateTime? RecordDateFrom = null;
            DateTime? RecordDateTo = null;
            string HOAFilterStatus = "";
            string HOAfeemin = "";
            string HOAfeemax = "";
            try
            {
                if (!string.IsNullOrEmpty(RecordDateFroms))
                {
                    RecordDateFroms = RecordDateFroms.Split('/')[2] + "-" + RecordDateFroms.Split('/')[0] + "-" + RecordDateFroms.Split('/')[1];
                    RecordDateFrom = Convert.ToDateTime(RecordDateFroms);
                    //DateTime sss = RecordDateFrom.ToString("yyyy-MM-dd","");
                }
                if (!string.IsNullOrEmpty(RecordDateTos))
                {
                    RecordDateTos = RecordDateTos.Split('/')[2] + "-" + RecordDateTos.Split('/')[0] + "-" + RecordDateTos.Split('/')[1];
                    RecordDateTo = Convert.ToDateTime(RecordDateTos).Date;
                }

                if (HOAFilterStatuss == 1) {
                    HOAFilterStatus = "1,2";
                }
                else if(HOAFilterStatuss==2)
                {
                    HOAFilterStatus = " ";
                }
                else
                {
                    HOAFilterStatus = "1,2, ";
                }
                if (HOAfeemins == "")
                {
                    HOAfeemin = null;
                }
                else
                {
                    HOAfeemin = HOAfeemins;
                }
                if (HOAfeemaxs == "")
                {
                    HOAfeemax = null;
                }
                else
                {
                    HOAfeemax = HOAfeemaxs;
                }
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<PalmBeachLpcases> _PalmBeachLpcases = new List<PalmBeachLpcases>();


                try
                {
                    if(SubTabId=="Tab01")
                    {
                        SubTabId = null;
                    }
                    _PalmBeachLpcases = _vmPalmCase.GetLPFiltersINPalm(UseCodeCategoryID, SubTabId, AssesedFrom, AssesedTo, LandFactorFrom, LandFactorTo, Zipcode, UnitsMin, UnitsMax, BedroomMin, BedroomMax, BathroomMin, BathroomMax,LocationAddress,MailingAddress,Plaintiff, Defendant,RecordDateFrom, RecordDateTo, SaleDateFrom, SaleDateTo, SalePriceFrom, SalePriceTo, EstimatedMortFrom, EstimatedMortTo, EstOwnerFrom, EstOwnerTo, HOAFilterStatus,HOAfeemin,HOAfeemax).OrderByDescending(x => x.PalmBeachLPCaseID).ToList();
                    TempData["showDocReportPalm"] = _PalmBeachLpcases;
                }
                catch
                {

                }
                GetPalmBeachTabs();
                return PartialView("_PalmBeachTabs", _PalmBeachLpcases);
            }
            }
            catch (Exception ex)
            {
               
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult GetBCPADetails(int SrchDefendantID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                int AddressID = db.tblOtherProperties.Where(x => x.SrchDefendantID == SrchDefendantID && x.IsOTHCorrectProperty == true).Select(x => x.OTHAddressID).FirstOrDefault();
                var _VMPropertyFolioDetails = _vmcase.OtherPropertiesDetails(AddressID);
                return PartialView("_ShowOtherPropertyDetails", _VMPropertyFolioDetails);
            }
            return RedirectToAction("Login", "Home");
        }
        public ActionResult GetBCPAPropertyDetails(int SrchDefendantID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
               // List<tblOtherProperty> test = db.tblOtherProperties.Where(x => x.SrchDefendantID == SrchDefendantID).ToList();
                int AddressID = db.tblOtherProperties.Where(x => x.SrchDefendantID == SrchDefendantID && x.IsOTHCorrectProperty == true).Select(x => x.OTHAddressID).FirstOrDefault();
                var _VMPropertyFolioDetails = _vmcase.OtherPropertiesDetails(AddressID);
                return PartialView("_ShowOTHPropertyDetails", _VMPropertyFolioDetails);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult GetSpokioDetails(int OTHAddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<SpokioDetail> _VMGetSpokioOwnerList = _vmcase.GetSpokioOwnerList(OTHAddressID);

                return PartialView("_ShowSpokioOwnerList", _VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult SpokeoOwnerProfileDetail(long spokioDetailsID, int othAddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                SpokioDetail vmSpokioOwnerDetail = _vmcase.GetSpokioOwnerDetails(spokioDetailsID, othAddressID);
                return View(vmSpokioOwnerDetail);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult GetSpokioCourtList(int SpokioDetailsID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<tblCourtDetail> _VMGetCourtList = _vmcase.GetCourtList(SpokioDetailsID);

                return PartialView("_ShowSpokioCourtList", _VMGetCourtList);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult GetPalmSpokioDetails(int PCBPalmDetailsID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<SpokioDetail> _VMGetSpokioOwnerList = _vmPalmCase.GetSpokioOwnerList(PCBPalmDetailsID);

                return PartialView("_ShowPalmSpokioOwnerList", _VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult SpokeoPalmOwnerProfileDetail(long spokioDetailsID, int PCBPalmDetailsID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                SpokioDetail vmSpokioOwnerDetail = _vmPalmCase.GetPalmSpokioOwnerDetails(spokioDetailsID, PCBPalmDetailsID);
                return View(vmSpokioOwnerDetail);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult GetPalmSpokioCourtList(int SpokioDetailsID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<tblPalmCourtDetail> _VMGetCourtList = _vmPalmCase.GetCourtList(SpokioDetailsID);

                return PartialView("_ShowPalmSpokioCourtList", _VMGetCourtList);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult RefreshSetAsDefaultLP()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    string Result = SetDefaultPropertyLpCase();
                    return Json(Result, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    return Json("Some Error Exist ! Please Try Again.", JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public string SetDefaultPropertyLpCase()
        {
            string Result = "";
            try
            {
                var LpCaseforsearch = _vmcase.GetLpCasesearchProperty().ToList();
                if (LpCaseforsearch != null && LpCaseforsearch.Count > 0)
                {
                    int Cnt = 0;
                    foreach (var _Lp in LpCaseforsearch)
                    {
                        int _SrchDefendantID = Convert.ToInt32(_Lp.SrchDefendantID.ToString());
                        List<OtherProperty> _OtherProperty = _vmcase.GetOtherPropertyList(_SrchDefendantID).Where(x => x.IsOTHCorrectProperty != true).ToList();
                        if (_OtherProperty != null && _OtherProperty.Count > 0)
                        {
                            if (_OtherProperty.Count == 1)
                            {
                                foreach (var _OTHPr in _OtherProperty)
                                {
                                    _vmcase.SaveLpSetAsDefault(_SrchDefendantID, _OTHPr.OTHAddressID, _OTHPr.OTHSiteAddressSrch);
                                    Cnt++;

                                }
                            }
                        }
                    }
                    if (LpCaseforsearch.Count == Cnt)
                    {
                        Result = "All Pending Lp Cases For Default Property Successfully Set";
                    }
                    else
                    {
                        var LpCaseforsrch = _vmcase.GetLpCasesearchProperty().ToList();
                        int pendingcase = LpCaseforsrch.Count;
                        Result = "Total Pending Cases For Set As Default Property : " + pendingcase + " Cases.";
                    }

                }
                else
                {
                    Result = "There is no pending cases For Set Ad Default Property.";
                }
                return Result;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                Result = "Error";
                return Result;
            }
        }

        public ActionResult DeleteSelectedLpCase(string[] RowLpCaseId)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    _vmcase.DeleteSelectedLPCases(RowLpCaseId);
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public ActionResult DeleteSelectedPalmLpCase(string[] RowLpCaseId)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    _vmPalmCase.DeletePalmLPCases(RowLpCaseId);
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");
        }



        public ActionResult ShowMapSelectedLpCase()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                return View();
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public ActionResult LoadBrowardMapData()
        {

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            using (BrowardclerkEntities dc = new BrowardclerkEntities())
            {
                string[] showDocReportLp1 = (string[])Session["MapDocReport"];
                IQueryable<SrchDefandant> _tblSrchDefandant;
                var OTHAddressID = Session["OTHAddressID"];
                if (OTHAddressID != null)
                {
                    _tblSrchDefandant = _vmcase.GetMapRecordBySrchID(Convert.ToInt32(OTHAddressID)).AsQueryable();
                    Session["OTHAddressID"] = null;
                }
                else
                {
                    _tblSrchDefandant = _vmcase.GetLpDefandentMapRecord(showDocReportLp1).AsQueryable();

                }

                //SORT
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    bool desc = sortColumnDir == "asc";
                    _tblSrchDefandant = Extenstions.OrderBy(_tblSrchDefandant, sortColumn, desc);
                }
                if (pageSize == -1)
                {
                    pageSize = _tblSrchDefandant.Count();
                }
                recordsTotal = _tblSrchDefandant.Count();
                var data = _tblSrchDefandant.Skip(skip).Take(pageSize).ToList();
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);
            }
        }


      

        public ActionResult ShowMapOthProperty(int OTHAddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                Session["OTHAddressID"] = OTHAddressID;
                return RedirectToAction("ShowMapSelectedLpCase");
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ShowMapBySrchDefendantID(int SrchDefendantID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                int OTHAddressID = db.tblOtherProperties.Where(x => x.SrchDefendantID == SrchDefendantID && x.IsOTHCorrectProperty == true).Select(x => x.OTHAddressID).FirstOrDefault();
                Session["OTHAddressID"] = OTHAddressID;
                return RedirectToAction("ShowMapSelectedLpCase");
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ShowMapPalmCasesByID(Int64 PCBPalmDetailsID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                Session["PCBPalmDetailsID"] = PCBPalmDetailsID;
               // List<PCBPalmDetail> _PCBPalmDetail = _vmPalmCase.GetPCBPalmForMap(PCBPalmDetailsID);
                return RedirectToAction("ShowMapPalmCases");
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult SelectedLpCaseAjax(string[] showDocReportLp, string[] RowLpCaseId)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                //List<SrchDefandant> model = new List<SrchDefandant>();
                try
                {
                    Session["MapDocReport"] = showDocReportLp;
                    //model = showDocReportLp.Where(w => RowLpCaseId.Contains(w.SrchDefendantID.ToString())).ToList();
                    return Json("Success", JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }

            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult SelectedPalmCaseAjax(string[] showDocReportLp)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    Session["PalmMapDocReport"] = showDocReportLp;
                    return Json("Success", JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }

            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult GetFolioByLpSrchDefendantID(int SrchDefendantID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                var _PropertyFolio = _vmcase.GetFolioBySrchDefendantID(SrchDefendantID);
                int AddressId = Convert.ToInt32(_PropertyFolio.ToString());
                return Json(AddressId, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult GetAddressListFromLp(int SrchDefendantID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    _vmcase.searchPDFs = new List<SearchedPdf>();
                    var _tblCaseSrch = db.tblCaseNumberSearches.Where(x => x.SrchDefendantID == SrchDefendantID).OrderByDescending(x => x.CaseSearchDate).FirstOrDefault();
                    if (_tblCaseSrch != null)
                    {
                        int CaseNumberID = Convert.ToInt32(_tblCaseSrch.CaseNumberID);
                        string Result = "";
                        string sourcePath = "C:/PDfFiles/";
                        string _CaseNumber = GetPdfDetailsFolder(CaseNumberID);

                        //Result = ConvertPdfByCaseNumber(_CaseNumber, sourcePath, Result, SrchDefendantID);

                        string Foldername = Session["FolderName"].ToString();
                        DateTime _SearchedDate = Convert.ToDateTime(Session["Searcheddate"].ToString());
                        //int CaseNumberID = Convert.ToInt32(Session["CaseNumberID"].ToString());

                        var _SearchPdfCheck = _vmcase.GetPDFResultRecord(SrchDefendantID);
                        if (_SearchPdfCheck != null && _SearchPdfCheck.Count > 0)
                        {
                            foreach (var item in _SearchPdfCheck)
                            {
                                if (item.tblNewAllAddress != null && item.tblNewAllAddress.Count > 0)
                                {
                                    _vmcase.searchPDFs = _SearchPdfCheck;
                                }
                                else
                                {
                                    Result = SaveAndReadPdfDoc(_CaseNumber, sourcePath, Result, SrchDefendantID, Foldername);
                                    var _SearchPdf = _vmcase.GetPDFResultRecord(SrchDefendantID);
                                    _vmcase.searchPDFs = _SearchPdf;
                                    _vmcase.UpdatedLpAddressListSaved(SrchDefendantID);
                                    
                                }
                            }
                        }
                        else
                        {
                            Result = SaveAndReadPdfDoc(_CaseNumber, sourcePath, Result, SrchDefendantID, Foldername);
                            var _SearchPdf = _vmcase.GetPDFResultRecord(SrchDefendantID);
                            _vmcase.searchPDFs = _SearchPdf;
                        }

                    }
                    return PartialView("_SelectAddress", _vmcase);


                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    return Json("Error", JsonRequestBehavior.AllowGet);

                }
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult SaveSelectedAddress(int SrchDefendantID, string SelectedAddress, tblSelectedAddress _obj)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                _vmcase.SaveAddressInLPList(SrchDefendantID, _obj);

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public ActionResult DeleteDefandantlist(int SrchDefendantID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    string response = _vmcase.deletedefandant(SrchDefendantID);
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);

                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public ActionResult DeletePalBeachCase(int palmBeachLPCaseID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    string response =_vmPalmCase.DeletePalmBeachCase(palmBeachLPCaseID);
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);

                }
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ChangePropertyType(int SrchDefendantID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    string response = _vmcase.ConvertPropertyType(SrchDefendantID);
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);

                }
            }
            return RedirectToAction("Login", "Home");
        }

        #endregion

        #region Property Search

        public ActionResult BrowardProperty()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                return View();
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult SearchByProperty1(string PropertyAddress)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                PropertyAddress = "3860 N 51 AVENUE, HOLLYWOOD";
                var Result = "";
                var binary = new FirefoxBinary(Server.MapPath("~/firefox.exe"));
                string path = Server.MapPath("~/firefox.exe");
                bool Compareable = true;
                FirefoxProfile ffprofile = new FirefoxProfile();
                IWebDriver driver = new FirefoxDriver(ffprofile);
                try
                {
                    driver.Manage().Window.Maximize();

                    driver.Navigate().GoToUrl("http://www.bcpa.net/RecAddr.asp");
                    Thread.Sleep(1000);
                    string[] _address = PropertyAddress.Split(' ');
                    string _Streetnumber = _address[0].Replace(",", "");
                    string _StreetDirection = _address[1].Replace(",", "");
                    string _StreetName = _address[2].Replace(",", "");
                    string _StreetType = _address[3].Replace(",", "");
                    string _StreetCity = _address[4].Replace(",", "");
                    //string _Streetnumber = "3860";
                    //string _StreetDirection = "N";
                    //string _StreetName = "51";
                    //string _StreetType = "AVENUE";
                    //string _StreetCity = "HOLLYWOOD";
                    driver.FindElement(By.Name("Situs_Street_Number")).SendKeys(_Streetnumber);
                    driver.FindElement(By.Name("Situs_Street_Direction")).SendKeys(_StreetDirection);
                    driver.FindElement(By.Name("Situs_Street_Name")).SendKeys(_StreetName);
                    driver.FindElement(By.Name("Situs_Street_Type")).SendKeys(_StreetType);
                    driver.FindElement(By.Name("Situs_City")).SendKeys(_StreetCity);
                    //string Search = Server.MapPath("~/Images/BtnSearch.png");
                    // SikuliAction.Click(Search);
                    By xPathSearch = By.XPath("//form[@name='homeind']/table/tbody/tr/td[1]/table[2]/tbody/tr/td/a[2]");

                    driver.FindElements(xPathSearch)[0].Click();
                    Thread.Sleep(1000);

                    By xPathdetails = By.XPath("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[1]/tbody/tr/td[1]/table/tbody");
                    By xPathdetailsval = By.XPath("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[1]/tbody/tr/td[3]/table/tbody");
                    By xPathdLegalDesc = By.XPath("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[3]/tbody");
                    By xPathEvents = By.XPath("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[9]/tbody/tr/td[2]/table/tbody");
                    //By xPathEvents1 = By.XPath("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[5]/tbody");
                    By xPathdYearly = By.XPath("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[5]/tbody");
                    By xPathsaleHistory = By.XPath("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[9]/tbody/tr/td[1]/table/tbody");
                    string LandFactor = "";
                    string AdvBldgSF = "";
                    string UnitsBEDBath = "";
                    string _LegalDesc = "";
                    string _FolioID = "";
                    string _Milage = "";
                    string _use = "";
                    string _SiteAddress = "";
                    string _PropertOwner = "";
                    string _mailingAddress = "";
                    string FolioSiteLink = "";
                    //-----------------------Legl description --------------------------

                    IWebElement EventsDocumentYearly = driver.FindElement(xPathdYearly);
                    var innerHtmlYearly = EventsDocumentYearly.GetAttribute("innerHTML");

                    string htmlYearly = @"<html><body>" + innerHtmlYearly.ToString() + "</body></html>";
                    HtmlAgilityPack.HtmlDocument _docYearly = new HtmlAgilityPack.HtmlDocument();
                    _docYearly.LoadHtml(htmlYearly);

                    System.Data.DataTable _dt = new System.Data.DataTable();

                    _dt.Columns.Add("Year", typeof(int));
                    _dt.Columns.Add("Land", typeof(String));
                    _dt.Columns.Add("Building", typeof(String));
                    _dt.Columns.Add("MarketValue", typeof(String));
                    _dt.Columns.Add("AssessedValue", typeof(String));
                    _dt.Columns.Add("AssessedTax", typeof(String));

                    for (int k = 3; k <= 5; k++)
                    {
                        DataRow dr = _dt.NewRow();
                        int _countColumn = 1;
                        foreach (HtmlNode node in _docYearly.DocumentNode.SelectNodes("//tr[" + k + "]/td"))
                        {
                            var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                            switch (_countColumn)
                            {
                                case 1:
                                    dr["Year"] = _text;
                                    break;
                                case 2:
                                    dr["Land"] = _text;
                                    break;
                                case 3:
                                    dr["Building"] = _text;
                                    break;
                                case 4:
                                    dr["MarketValue"] = _text;
                                    break;
                                case 5:
                                    dr["AssessedValue"] = _text;
                                    break;
                                case 6:
                                    dr["AssessedTax"] = _text;
                                    break;
                            }

                            if (_countColumn >= 6)
                            {
                                _dt.Rows.Add(dr);
                                _countColumn = 0;
                            }
                            else
                                _countColumn++;
                        }
                    }
                    //====----------------SaleHistory--------------===//
                    IWebElement EventsDocumentsaleHistory = driver.FindElement(xPathsaleHistory);
                    var innerHtmlsaleHistory = EventsDocumentsaleHistory.GetAttribute("innerHTML");

                    string htmlsaleHistory = @"<html><body>" + innerHtmlsaleHistory.ToString() + "</body></html>";
                    HtmlAgilityPack.HtmlDocument _docsaleHistory = new HtmlAgilityPack.HtmlDocument();
                    _docsaleHistory.LoadHtml(htmlsaleHistory);

                    System.Data.DataTable _dtSaleHistory = new System.Data.DataTable();

                    _dtSaleHistory.Columns.Add("SaleDate", typeof(DateTime));
                    _dtSaleHistory.Columns.Add("SaleType", typeof(String));
                    _dtSaleHistory.Columns.Add("SalePrice", typeof(String));
                    _dtSaleHistory.Columns.Add("SaleBookPage", typeof(String));

                    for (int p = 3; p <= 5; p++)
                    {
                        DataRow drSaleHistory = _dtSaleHistory.NewRow();
                        int _cntsaleColumn = 1;


                        foreach (HtmlNode node in _docsaleHistory.DocumentNode.SelectNodes("//tr[" + p + "]/td"))
                        {
                            var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                            switch (_cntsaleColumn)
                            {
                                case 1:
                                    drSaleHistory["SaleDate"] = _text;
                                    break;
                                case 2:
                                    drSaleHistory["SaleType"] = _text;
                                    break;
                                case 3:
                                    drSaleHistory["SalePrice"] = _text;
                                    break;
                                case 4:
                                    drSaleHistory["SaleBookPage"] = _text;
                                    break;

                            }
                            if (_cntsaleColumn >= 4)
                            {
                                _dtSaleHistory.Rows.Add(drSaleHistory);
                                _cntsaleColumn = 0;
                            }
                            else
                                _cntsaleColumn++;
                        }
                    }

                    //-----------------------Legl description --------------------------
                    IWebElement EventsDocumentLegal = driver.FindElement(xPathdLegalDesc);
                    var innerHtmlLegal = EventsDocumentLegal.GetAttribute("innerHTML");

                    string htmlLegal = @"<html><body>" + innerHtmlLegal.ToString() + "</body></html>";
                    HtmlAgilityPack.HtmlDocument _doclegal = new HtmlAgilityPack.HtmlDocument();
                    _doclegal.LoadHtml(htmlLegal);


                    foreach (HtmlNode node in _doclegal.DocumentNode.SelectNodes("//tr[1]/td[2]"))
                    {
                        _LegalDesc = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    }
                    //-----------------------FolioId --------------------------
                    IWebElement EventsDocumentID = driver.FindElement(xPathdetailsval);
                    var innerHtmlID = EventsDocumentID.GetAttribute("innerHTML");

                    string htmlID = @"<html><body>" + innerHtmlID.ToString() + "</body></html>";
                    HtmlAgilityPack.HtmlDocument _docID = new HtmlAgilityPack.HtmlDocument();
                    _docID.LoadHtml(htmlID);


                    foreach (HtmlNode node in _docID.DocumentNode.SelectNodes("//tr[1]/td[2]"))
                    {
                        _FolioID = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    }
                    foreach (HtmlNode node in _docID.DocumentNode.SelectNodes("//tr[2]/td[2]"))
                    {
                        _Milage = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    }
                    foreach (HtmlNode node in _docID.DocumentNode.SelectNodes("//tr[3]/td[2]"))
                    {
                        _use = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    }

                    //-----------------------Property details Address --------------------------
                    IWebElement EventsDocumentDetails = driver.FindElement(xPathdetails);
                    var innerHtmlDetials = EventsDocumentDetails.GetAttribute("innerHTML");

                    string htmldetails = @"<html><body>" + innerHtmlDetials.ToString() + "</body></html>";
                    HtmlAgilityPack.HtmlDocument _docDetails = new HtmlAgilityPack.HtmlDocument();
                    _docDetails.LoadHtml(htmldetails);


                    foreach (HtmlNode node in _docDetails.DocumentNode.SelectNodes("//tr[1]/td[2]"))
                    {
                        string _SiteFilter = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                        _SiteAddress = _SiteFilter.Split('\r')[0];

                    }
                    foreach (HtmlNode node in _docDetails.DocumentNode.SelectNodes("//tr[2]/td[2]"))
                    {
                        _PropertOwner = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();

                    }
                    foreach (HtmlNode node in _docDetails.DocumentNode.SelectNodes("//tr[3]/td[2]"))
                    {
                        _mailingAddress = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();

                    }
                    //-----------------------Land factors --------------------------
                    IWebElement EventsDocument = driver.FindElement(xPathEvents);
                    var innerHtml = EventsDocument.GetAttribute("innerHTML");

                    string html = @"<html><body>" + innerHtml.ToString() + "</body></html>";
                    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(html);

                    foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//tr[3]/td[2]"))
                    {
                        LandFactor = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();

                    }

                    foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//tr[7]/td[2]"))
                    {
                        AdvBldgSF = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();

                    }
                    foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//tr[8]/td[2]"))
                    {
                        UnitsBEDBath = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();

                    }
                    //--------------Insert InTo database-----------------

                    FolioSiteLink = driver.Url;

                    Result = _vmcase.InsertAssessedDetails(null, null, _SiteAddress, _PropertOwner, _mailingAddress, _FolioID, _Milage, _use, _LegalDesc, LandFactor, AdvBldgSF, UnitsBEDBath, FolioSiteLink, _dt, Compareable, _dtSaleHistory);
                    driver.Quit();
                }
                catch (Exception ex)
                {
                    driver.Quit();
                    Result = ex.ToString();
                }

                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ShowPropertyList()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                //List<tblSrchFolioID> _tblSrchFolioID = db.tblSrchFolioIDs.Where(x => x.Compareable == true).OrderByDescending(x => x.AddressID).ToList();
                List<PropertyFolio> _PropertyFolio = _vmcase.GetFolioPropertyList();
                return PartialView("_PropertyList", _PropertyFolio);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult OtherProperties(int SrchDefendantID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<OtherProperty> _PropertyFolio = _vmcase.GetOtherPropertyList(SrchDefendantID);
                return View(_PropertyFolio);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ShowOtherPalmLP(int PalmBeachLPCaseID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<PalmBeachLpcases> _PalmBeachLp = _vmPalmCase.GetOtherPalmList(PalmBeachLPCaseID);
                return PartialView("_ShowOtherPalmLPList", _PalmBeachLp);
            }
            return RedirectToAction("Login", "Home");
        }

        

        public ActionResult ShowOtherPlaintiffs(int PalmBeachLPCaseID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<PalmBeachLpcases> _PalmBeachLp = _vmPalmCase.GetOtherPalintiffLP(PalmBeachLPCaseID);
                return PartialView("_ShowOtherPlaintiffsLP", _PalmBeachLp);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ShowOtherDefendantPlaintiffs(int SrchDefendantID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<SrchDefandant> _PalmBeachLp =_vmcase.GetOtherDefendantPalintiff(SrchDefendantID);
                return PartialView("_ShowOtherDefendPlaintiffs", _PalmBeachLp);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ShowOtherSetablePalmLP(int PalmBeachLPCaseID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<PalmBeachLpcases> _PalmBeachLp = _vmPalmCase.GetOtherPalmList(PalmBeachLPCaseID);
                return PartialView("_OtherPalmSetAsDefault", _PalmBeachLp);
            }
            return RedirectToAction("Login", "Home");
        }



        public ActionResult SetDefaultPalmProperty(Int64 PCBPalmDetailsID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    _vmPalmCase.SetDefaultPalmPBC(PCBPalmDetailsID);

                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");

        }

        public ActionResult ShowOtherPropertyList(int SrchDefendantID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<OtherProperty> _PropertyFolio = _vmcase.GetOtherPropertyList(SrchDefendantID);
                return PartialView("_ShowOtherProperties", _PropertyFolio);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult GetOTHAddressByLpSrchDefendantID(int SrchDefendantID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<OtherProperty> _PropertyFolio = _vmcase.GetOtherPropertyList(SrchDefendantID);
                return PartialView("_OtherPropertyAddress", _PropertyFolio);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult SaveCorrectPropertyAddress(int SrchDefendantID, int OthAddressID, string SiteAddress)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                string Result = "";
                Result = _vmcase.SaveCorrectPropertyAddress(SrchDefendantID, OthAddressID, SiteAddress);
                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public ActionResult GetPriviousYearsDetails(int AddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<AssesedValueslist> _VMAssesedValueslist = _vmcase.PriviousYearsDetails(AddressID);

                return PartialView("_PriviousYearsDetails", _VMAssesedValueslist);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult GetOtherPriviousYearsDetails(int AddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<OtherAssessedValue> _VMAssesedValueslist = _vmcase.OtherPriviousYearsDetails(AddressID);

                return PartialView("_OTHPriviousYearsDetails", _VMAssesedValueslist);
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public ActionResult GetPropertiesDetails(int AddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                var _VMPropertyFolioDetails = _vmcase.PropertiesDetails(AddressID);

                return PartialView("_ShowPropertyDetails", _VMPropertyFolioDetails);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult GetOtherPropertiesDetails(int AddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                var _VMPropertyFolioDetails = _vmcase.OtherPropertiesDetails(AddressID);

                return PartialView("_ShowOtherPropertyDetails", _VMPropertyFolioDetails);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult SavecomparedData(int SubJectAddressID, string[] Rowhtml)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    bool Compareable = false;
                    string response = "";

                    _vmcase.SaveBCPARecords(SubJectAddressID, Rowhtml, Compareable, null);

                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult SaveOthercomparedData(int SubJectAddressID, string[] Rowhtml)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    string response = "";
                    _vmcase.SaveOtherBCPARecords(SubJectAddressID, Rowhtml);
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public ActionResult DeleteOtherProperty(int AddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    string response = _vmcase.DeleteOtherProperty(AddressID);
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public ActionResult DeleteProperty(int AddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    string response = _vmcase.DeleteProperty(AddressID);
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult GetFolioForm(int AddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<CompareFolioID> _model = _vmcase.GetFolioFormFields(AddressID);
                return PartialView("_GetFolioForm", _model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ShowComparison(int AddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<DataRow> _model = _vmcase.GetFolioFormRows(AddressID);
                ViewBag.AddressID = AddressID;
                return PartialView("_ViewComparison", _model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ShowOtherComparison(int AddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<DataRow> _model = _vmcase.GetOtherFolioFormRows(AddressID);
                ViewBag.AddressID = AddressID;
                return PartialView("_ViewOtherComparison", _model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ViewComparision(int AddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<DataRow> _model = _vmcase.GetFolioFormRows(AddressID);
                ViewBag.AddressID = AddressID;
                return View(_model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult OtherComparision(int AddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<DataRow> _model = _vmcase.GetOtherFolioFormRows(AddressID);
                ViewBag.AddressID = AddressID;
                return View(_model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ViewAdjustment(int SubJectAddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<DataRow> _model = _vmcase.GetSubjectProperty(SubJectAddressID);
                ViewBag.AddressID = SubJectAddressID;
                return View(_model);
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public ActionResult CalculateAdjustMent(int SubJectAddressID, int? AdvBldgSF, int? SalePrice, int? AssedMarketSqFt, int? Units, int? BedRoom, int? BathRoom, int? Milage, int? Use)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<DataRow> _model = _vmcase.GetCalculateAdjustProperty(SubJectAddressID, AdvBldgSF, SalePrice, AssedMarketSqFt, Units, BedRoom, BathRoom, Milage, Use);

                return PartialView("_ViewAdustment", _model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ViewOtherAdjustment(int SubJectAddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<DataRow> _model = _vmcase.GetOtherSubjectProperty(SubJectAddressID);
                ViewBag.AddressID = SubJectAddressID;
                return View(_model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult CalculateOtherAdjustMent(int SubJectAddressID, int? AdvBldgSF, int? SalePrice, int? AssedMarketSqFt, int? Units, int? BedRoom, int? BathRoom, int? Milage, int? Use)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<DataRow> _model = _vmcase.GetOtherCalculateAdjustProperty(SubJectAddressID, AdvBldgSF, SalePrice, AssedMarketSqFt, Units, BedRoom, BathRoom, Milage, Use);

                return PartialView("_ViewAdustment", _model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult MsComparisonBP(int AddressID, int count)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                ComparePropertyMSAccess _CompPrMSAccess = new ComparePropertyMSAccess();

                var _VMProperty = _vmcase.PropertiesDetails(AddressID);
                int SrchDefendantID = Convert.ToInt32(_VMProperty.SrchDefendantID);
                DataSet model = _CompPrMSAccess.GetCopareableProperty(SrchDefendantID, _VMProperty.SiteAddressSrch, _VMProperty.MailingAddress, _VMProperty.LandFactor, count, 0);


                return PartialView("_ComparisionBrProperty", model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult MsOtherComparisonBP(int AddressID, int count)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    var _VMProperty = _vmcase.OtherPropertiesDetails(AddressID);
                    int SrchDefendantID = Convert.ToInt32(_VMProperty.SrchDefendantID);
                    DataSet model = _CompPrMSAccess.GetCopareableProperty(SrchDefendantID, _VMProperty.OTHSiteAddressSrch, _VMProperty.OTHMailingAddress, _VMProperty.OTHLandFactor, count, 1);


                    return PartialView("_ComparisionBrProperty", model);
                }
                catch (Exception ex)
                {
                    return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult SaveMatchedProperty(int SubJectAddressID, string[] Rowhtml)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    //string response = "";
                    bool Compareable = true;
                    int? ChkBcpa = 1;
                    _vmcase.SaveBCPARecords(SubJectAddressID, Rowhtml, Compareable, ChkBcpa);

                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult MatchedPropertySetDefault(int SubJectAddressID, int OTHAddressID, string _SiteAddress)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    _vmcase.SaveDefaultProperty(SubJectAddressID, OTHAddressID, _SiteAddress);

                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ListMatchedProperty(int SrchDefendantID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<SrchDefandant> model = _vmcase.GetOtherPropertyMatched(SrchDefendantID);
                var CaseNumber = (from _model1 in model
                                  select new { _model1.CaseNumber }).Distinct().OrderBy(i => i.CaseNumber).ToList();
                ViewBag.CaseNumber = CaseNumber[0].CaseNumber.ToString();

                return PartialView("_MatchForDefaultProperty", model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PlantiffMatchFromBCPA(int SrchDefendantID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                string Plaintiff = db.tblSrchDefandants.AsEnumerable().Where(x => x.SrchDefendantID == SrchDefendantID).Select(x => x.Plaintiff).FirstOrDefault();
                DataSet model = _CompPrMSAccess.GetMatchedPlantiffInBCPA(SrchDefendantID, Plaintiff);
                ViewBag.Plaintiff = Plaintiff;

                return PartialView("_MatchedFromBCPA", model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult DeletePropertyFolio(int CmpFolioTOId)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    string response = _vmcase.DeleteCmpProperty(CmpFolioTOId);
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult DeleteOtherPropertyFolio(int CmpFolioTOId)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                try
                {
                    string response = _vmcase.DeleteOtherCmpProperty(CmpFolioTOId);
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult SaveSearchCmpFolio(string[] txtFolioId, string[] txtPRFolioId, string[] txtPropertyAddress, System.Web.Mvc.FormCollection _Frm)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                string Result = "";
                int AddressID = Convert.ToInt32(_Frm["hdnAddressToID"].ToString());
                int index = 0;
                DateTime? CompareDate = DateTime.Now;
                bool Compareable = false;
                var objCmpFolioToID = (from c in db.tblCompareProperties
                                       select c).Where(c => c.CmpFolioByID == AddressID).ToList();
                foreach (var _CmpFID in objCmpFolioToID)
                {
                    string _lstFolioProp = db.tblSrchFolioIDs.Where(s => s.AddressID == _CmpFID.CmpFloioToID).Select(s => s.FolioId).FirstOrDefault();

                    _CompareFolioToID.Add(_lstFolioProp.Replace(" ", ""));
                }
                int RadioVal = Convert.ToInt32(_Frm["hdnRadioValue"].ToString());
                if (RadioVal == 2)
                {
                    foreach (var _CmpPrAdrs in objCmpFolioToID)
                    {
                        string _lstFolioProp = db.tblSrchFolioIDs.Where(s => s.AddressID == _CmpPrAdrs.CmpFloioToID).Select(s => s.SiteAddressSrch).FirstOrDefault();

                        _ComparePropertyAddress.Add(_lstFolioProp);
                    }

                    foreach (string i in txtPropertyAddress)
                    {
                        int _count = txtPropertyAddress.Count();
                        if (index < _count)
                        {
                            var _PropertyAddress = txtPropertyAddress[index].ToString();
                            if (_PropertyAddress != "" && _PropertyAddress != null)
                            {
                                var _filterAddress = _PropertyAddress.Replace(",", " ");
                                if (_filterAddress != "" && _filterAddress != null)
                                {
                                    var _filterAddress1 = _filterAddress.Replace("  ", " ");
                                    if (!_ComparePropertyAddress.Contains(_filterAddress1))
                                    {
                                        string Address = _PropertyAddress;
                                        string _url = "http://www.bcpa.net/RecAddr.asp";
                                        searchAndSaveFolioID(_url, AddressID, CompareDate, Compareable, RadioVal, Address);
                                        Result = "Success";
                                    }
                                }
                                //Result = "Compared";
                            }

                        }
                        index++;
                    }
                }
                else
                {
                    foreach (string i in txtFolioId)
                    {
                        var FolioID = txtFolioId[index].ToString();
                        if (FolioID != "" && FolioID != null)
                        {
                            if (!_CompareFolioToID.Contains(FolioID))
                            {
                                string _url = "http://www.bcpa.net/RecInfo.asp?URL_Folio=" + FolioID;
                                searchAndSaveFolioID(_url, AddressID, CompareDate, Compareable, 1, null);
                                Result = "Success";
                            }
                        }
                        index++;
                    }
                }

                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult SearchByFolioID(string FolioID, int Radiovalue)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                string Result = "";
                int AddressID = 0;
                if (Radiovalue == 1)
                {
                    string _url = "http://www.bcpa.net/RecInfo.asp?URL_Folio=" + FolioID;

                    //searchAndSaveFolioID(_url, AddressID, null, true);
                    searchAndSaveFolioID(_url, AddressID, null, true, Radiovalue, null);
                }
                else
                {
                    //string Address = "3860 N 51 AVENUE, HOLLYWOOD";
                    string Address = FolioID;
                    string _url = "http://www.bcpa.net/RecAddr.asp";
                    searchAndSaveFolioID(_url, AddressID, null, true, Radiovalue, Address);
                }
                Result = Session["Success"].ToString();
                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Login", "Home");
        }

        public void searchAndSaveFolioID(string _url, int? AddressID, DateTime? CompareDate, bool Compareable, int radioVal, string Address)
        {

            //string _url = "http://www.bcpa.net/RecInfo.asp?URL_Folio=514206081320";
            //browser = new IEBrowser(true, resultEvent, _url);

            browser = new IEBrowser(true, resultEvent, _url, radioVal, Address);
            if (radioVal == 1)
            {
                Thread.Sleep(30000);
            }
            else
            {
                Thread.Sleep(40000);
            }
            string html = browser.HtmlResult;
            SaveFolioProperty(html, _url, AddressID, CompareDate, Compareable, radioVal, Address);
        }

        public ActionResult GetandSaveMortgage(int AddressID, string MortgageCFN)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                int radioVal = 3;
                string Address = "";
                string _url = "https://officialrecords.broward.org/oncorev2/details.aspx?CFN=" + MortgageCFN;
                browser = new IEBrowser(true, resultEvent, _url, radioVal, Address);
                Thread.Sleep(20000);
                string html = browser.HtmlResult;

                return Json("", JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Login", "Home");
        }

        public void SaveFolioProperty(string html, string _url, int? AddressID, DateTime? CompareDate, bool Compareable, int radioVal, string Address)
        {
            string LandFactor = "";
            string AdvBldgSF = "";
            string UnitsBEDBath = "";
            string _LegalDesc = "";
            string _FolioID = "";
            string _Milage = "";
            string _use = "";
            string _SiteAddress = "";
            string _PropertOwner = "";
            string _mailingAddress = "";
            string FolioSiteLink = "";
            string Result = "";
            //-----------------------Legl description --------------------------


            string _html = @"<html><body>" + html.ToString() + "</body></html>";
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(_html);

            System.Data.DataTable _dt = new System.Data.DataTable();

            _dt.Columns.Add("Year", typeof(int));
            _dt.Columns.Add("Land", typeof(String));
            _dt.Columns.Add("Building", typeof(String));
            _dt.Columns.Add("MarketValue", typeof(String));
            _dt.Columns.Add("AssessedValue", typeof(String));
            _dt.Columns.Add("AssessedTax", typeof(String));

            System.Data.DataTable _dtSaleHistory = new System.Data.DataTable();

            _dtSaleHistory.Columns.Add("SaleDate", typeof(DateTime));
            _dtSaleHistory.Columns.Add("SaleType", typeof(String));
            _dtSaleHistory.Columns.Add("SalePrice", typeof(String));
            _dtSaleHistory.Columns.Add("SaleBookPage", typeof(String));

            //-------------------Incomplete Address--------------------------------------//
            if (html == "Incomplete Address")
            {
                string _Addrs = Address.Replace(",", " ");
                string _Address = _Addrs.Replace("  ", " ");
                _SiteAddress = "Incomplete Address !" + _Address.Trim();
                _FolioID = "Incomplete-Address";
                Result = _vmcase.InsertAssessedDetails(AddressID, CompareDate, _SiteAddress, _PropertOwner, _mailingAddress, _FolioID, _Milage, _use, _LegalDesc, LandFactor, AdvBldgSF, UnitsBEDBath, FolioSiteLink, _dt, Compareable, _dtSaleHistory);

            }
            else
            {
                //HtmlNode checknode = doc.DocumentNode.SelectSingleNode("//*[@name=\"homeind\"]");
                //if (checknode.Name == "form")
                //{
                //    Result="Not Found !";
                //}
                //else { 
                for (int k = 3; k <= 5; k++)
                {
                    DataRow dr = _dt.NewRow();
                    int _countColumn = 1;
                    foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[5]/tbody/tr[" + k + "]/td"))
                    {
                        var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                        switch (_countColumn)
                        {
                            case 1:
                                dr["Year"] = _text;
                                break;
                            case 2:
                                dr["Land"] = _text;
                                break;
                            case 3:
                                dr["Building"] = _text;
                                break;
                            case 4:
                                dr["MarketValue"] = _text;
                                break;
                            case 5:
                                dr["AssessedValue"] = _text;
                                break;
                            case 6:
                                dr["AssessedTax"] = _text;
                                break;
                        }

                        if (_countColumn >= 6)
                        {
                            _dt.Rows.Add(dr);
                            _countColumn = 0;
                        }
                        else
                            _countColumn++;
                    }
                }

                //====----------------SaleHistory--------------===//
                int cntnode = doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[9]/tbody/tr/td[1]/table/tbody/tr").Count();
                for (int p = 3; p <= cntnode; p++)
                {
                    DataRow drSaleHistory = _dtSaleHistory.NewRow();
                    int _cntsaleColumn = 1;

                    foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[9]/tbody/tr/td[1]/table/tbody/tr[" + p + "]/td"))
                    {
                        var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                        if (!string.IsNullOrEmpty(_text))
                        {
                            switch (_cntsaleColumn)
                            {
                                case 1:
                                    drSaleHistory["SaleDate"] = _text;
                                    break;
                                case 2:
                                    drSaleHistory["SaleType"] = _text;
                                    break;
                                case 3:
                                    drSaleHistory["SalePrice"] = _text;
                                    break;
                                case 4:
                                    drSaleHistory["SaleBookPage"] = _text;
                                    break;

                            }
                        }
                        if (_cntsaleColumn >= 4)
                        {
                            _dtSaleHistory.Rows.Add(drSaleHistory);
                            _cntsaleColumn = 0;
                        }
                        else
                            _cntsaleColumn++;
                    }
                }

                //-----------------------Legl description --------------------------

                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[3]/tbody/tr[1]/td[2]"))
                {
                    _LegalDesc = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    break;
                }

                //-----------------------FolioId --------------------------

                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[1]/tbody/tr/td[3]/table/tbody/tr[1]/td[2]"))
                {
                    _FolioID = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    break;
                }
                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[1]/tbody/tr/td[3]/table/tbody/tr[2]/td[2]"))
                {
                    _Milage = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    break;
                }
                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[1]/tbody/tr/td[3]/table/tbody/tr[3]/td[2]"))
                {
                    _use = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    break;
                }

                //-----------------------Property details Address --------------------------

                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[1]/tbody/tr/td[1]/table/tbody/tr[1]/td[2]"))
                {
                    string _SiteFilter = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    string FilterAddress = _SiteFilter.Split('\r')[0];
                    _SiteAddress = FilterAddress.Replace(",", "");
                    break;
                }
                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[1]/tbody/tr/td[1]/table/tbody/tr[2]/td[2]"))
                {
                    _PropertOwner = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    break;
                }
                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[1]/tbody/tr/td[1]/table/tbody/tr[3]/td[2]"))
                {
                    _mailingAddress = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    break;
                }
                //-----------------------Land factors --------------------------

                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[9]/tbody/tr/td[2]/table/tbody/tr[3]/td[2]"))
                {
                    LandFactor = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    break;
                }
                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[9]/tbody/tr/td[2]/table/tbody/tr[7]/td[2]"))
                {
                    AdvBldgSF = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    break;
                }
                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[9]/tbody/tr/td[2]/table/tbody/tr[8]/td[2]"))
                {
                    UnitsBEDBath = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    break;
                }
                //--------------Insert InTo database-----------------
                if (radioVal == 2)
                {
                    string _PropertyUrl = _FolioID.Replace(" ", "");
                    FolioSiteLink = "http://www.bcpa.net/RecInfo.asp?URL_Folio=" + _PropertyUrl;
                }
                else
                {
                    FolioSiteLink = _url;
                }


                Result = _vmcase.InsertAssessedDetails(AddressID, CompareDate, _SiteAddress, _PropertOwner, _mailingAddress, _FolioID, _Milage, _use, _LegalDesc, LandFactor, AdvBldgSF, UnitsBEDBath, FolioSiteLink, _dt, Compareable, _dtSaleHistory);

            }
            //}
            Session["Success"] = Result;
        }

        #endregion

        #region Palm Beach Search

        public ActionResult PalmBeachCases()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                //int cnt = db.sp_GetPalmBeachLpcases(-1).AsEnumerable().ToList().Count();
                //ViewBag.CntpalmBeach = cnt;

                GetPalmBeachTabs();

                return View();
            }
            return RedirectToAction("Login", "Home");
        }

        private void GetPalmBeachTabs()
        {
            TabsCountedLp _TabsCountedLp = _vmPalmCase.GetCountTabsLpCase();

            System.Data.DataTable _SubTabCnt = _vmPalmCase.GetSubTabsCount();

            ViewData["PalmSubTabCnt"] = _SubTabCnt;
            if (_TabsCountedLp != null)
            {
                ViewBag.Residential = _TabsCountedLp.Residential;
                ViewBag.Commercial = _TabsCountedLp.Commercial;
                ViewBag.Industrial = _TabsCountedLp.Industrial;
                ViewBag.Miscellaneous = _TabsCountedLp.Miscellaneous;
                ViewBag.ALLLEADS = _TabsCountedLp.ALLLEADS;
                ViewBag.MultiProperty = _TabsCountedLp.MultiProperty;
                ViewBag.SetSubjtProperty = _TabsCountedLp.SetSubjtProperty;
                ViewBag.NotFound = _TabsCountedLp.NotFound;
            }
            List<UseCodeCategory> _PropertyType = _vmPalmCase.GetPropertyTypeTab();
            ViewBag.PropertyType = _PropertyType;
        }

        public ActionResult ShowPalmBeachCases(int? PageSize)
        { if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
            List<PalmBeachLpcases> _PalmBeachLpcases = _vmPalmCase.GetPalmBeachCases(PageSize);
            return PartialView("_ShowPalmBeachCases", _PalmBeachLpcases); }
            return RedirectToAction("Login", "Home");
        }


        public ActionResult ShowPalmReport(int UseCodeCategoryID, string SubTabId, int? CntRecord)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<PalmBeachLpcases> _tblSrchDefandant = new List<PalmBeachLpcases>();

               // _tblSrchDefandant = GetSearchPalmBeachCases(UseCodeCategoryID, SubTabId, CntRecord, _tblSrchDefandant);

               // TempData["showDocReportPalm"] = _tblSrchDefandant;

                return PartialView("_ShowPalmBeachCases", _tblSrchDefandant);
            }
            return RedirectToAction("Login", "Home");
            
        }

       // [HttpPost]
        public ActionResult LoadPalmBeachData(int UseCodeCategoryID, string SubTabId, int? CntRecord, bool groupByCFN)
        {

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            using (BrowardclerkEntities dc = new BrowardclerkEntities())
            {
                IQueryable<PalmBeachLpcases> _tblSrchDefandant;

                if (TempData["showDocReportPalm"] != null)
                {
                    _tblSrchDefandant = ((List<PalmBeachLpcases>)TempData["showDocReportPalm"]).AsQueryable();
                }
                else
                {
                    if (SubTabId == "Tab01")
                    {
                        _tblSrchDefandant = _vmPalmCase.GetSerachPalmBeachCases(UseCodeCategoryID, null, CntRecord, null, groupByCFN).OrderByDescending(x => x.PCBPalmDetailsID).AsQueryable();
                    }
                    else
                    {
                        _tblSrchDefandant = _vmPalmCase.GetSerachPalmBeachCases(UseCodeCategoryID, null, CntRecord, SubTabId, groupByCFN).OrderByDescending(x => x.PCBPalmDetailsID).AsQueryable();
                    }
                }
                //SORT
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    bool desc = sortColumnDir == "asc";
                    _tblSrchDefandant = Extenstions.OrderBy(_tblSrchDefandant, sortColumn, desc);
                }
                if (pageSize == -1)
                {
                    pageSize = _tblSrchDefandant.Count();
                }
                recordsTotal = _tblSrchDefandant.Count();
                var data = _tblSrchDefandant.Skip(skip).Take(pageSize).ToList();
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);
            }
        }


        //private List<PalmBeachLpcases> GetSearchPalmBeachCases(int UseCodeCategoryID, string SubTabId, int? CntRecord, List<PalmBeachLpcases> _tblSrchDefandant)
        //{
        //    Session["PalmSubTabId"] = SubTabId;
        //    if (UseCodeCategoryID != null)
        //    {
        //        if (SubTabId != null && SubTabId != "")
        //        {
        //            if (SubTabId == "Tab01")
        //            {
        //                _tblSrchDefandant = _vmPalmCase.GetSerachPalmBeachCases(UseCodeCategoryID, null, CntRecord, null).OrderByDescending(x => x.PalmBeachLPCaseID).ToList();
        //            }
        //            else
        //            {
        //                _tblSrchDefandant = _vmPalmCase.GetSerachPalmBeachCases(UseCodeCategoryID, null, CntRecord, SubTabId).OrderByDescending(x => x.PalmBeachLPCaseID).ToList();
        //            }
        //        }

        //    }
        //    return _tblSrchDefandant;
        //}

        public ActionResult ShowMapPalmCases()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                return View();
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public ActionResult LoadPalmBeachMapData()
        {

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            using (BrowardclerkEntities dc = new BrowardclerkEntities())
            {
                string[] showDocReportLp1 = (string[])Session["PalmMapDocReport"];
                IQueryable<PCBPalmDetail> showDocReportLp;
                var PCBPalmDetailsID = Session["PCBPalmDetailsID"] == null ? 0 : Convert.ToInt64(Session["PCBPalmDetailsID"]);
                if (PCBPalmDetailsID!=0)
                {
                    showDocReportLp = _vmPalmCase.GetPCBPalmForMap(PCBPalmDetailsID).AsQueryable();
                    Session["PCBPalmDetailsID"] = null;
                }
                else
                {
                    showDocReportLp = _vmPalmCase.GetPCBPalmMapRecord(showDocReportLp1).AsQueryable();

                }

                //SORT
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    bool desc = sortColumnDir == "asc";
                    showDocReportLp = Extenstions.OrderBy(showDocReportLp, sortColumn, desc);
                }
                if (pageSize == -1)
                {
                    pageSize = showDocReportLp.Count();
                }
                recordsTotal = showDocReportLp.Count();
                var data = showDocReportLp.Skip(skip).Take(pageSize).ToList();
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult ShowPCBDetails(Int64 PCBPalmDetailsID)
        { if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
            List<PCBPalmDetail> _PCBPalmDetail = _vmPalmCase.GetPCBPalmDetail(PCBPalmDetailsID);
            return PartialView("_ShowPalmPCBDetails", _PCBPalmDetail); }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult LoanAmortization()
        { if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
            List<LoanAmortiZd> _lst = new List<LoanAmortiZd>();
            if (Session["lstLoanAmortiZd"] != null)
            {
                _lst = (List<LoanAmortiZd>)Session["lstLoanAmortiZd"];

                ViewBag.Anualrate = Session["CurrentRate"];
                ViewBag.LoanPrincipleAmount = Session["LoanPrincipleAmount"];
                ViewBag.LoanStartDate = Session["LoanStartDate"];
                ViewBag.LoanPeriod = Session["LoanPeriod"];
               ViewBag.Downpayment = Session["Downpayment"];
                string currentMonthYear = DateTime.Now.ToString("MMM") + "-" + DateTime.Now.Year.ToString();
                ViewBag.CurrentMonthYear = currentMonthYear;
                int index = _lst.FindIndex(a => a.Month == currentMonthYear);

                ViewBag.CurrentDTPage = index / 10;
                //TempData["LoanSummary"] = LoanSummary(_lst);
                //LoanSummary(_lst);
            }



            return View(_lst);
             }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ShowLoanAmortization()
        { if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
            List<LoanAmortiZd> _lst = new List<LoanAmortiZd>();
            _lst = testloan();
            return PartialView("_LoanAmortization", _lst); }
            return RedirectToAction("Login", "Home");

        }

        public ActionResult LoanSummary()
        { if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
            try
            {
                List<LoanAmortiZd> _lst = new List<LoanAmortiZd>();
                if (Session["lstLoanAmortiZd"] != null)
                {
                    _lst = (List<LoanAmortiZd>)Session["lstLoanAmortiZd"];
                }

                CreateExcelDoc excell_app = new CreateExcelDoc();
                DateTime _dt = DateTime.Now;

                if (_lst != null)
                {
                    string ReviewDate = _dt.Date.ToShortDateString();
                    DateTime last_date = new DateTime(_dt.Year, _dt.Month, DateTime.DaysInMonth(_dt.Year, _dt.Month));
                    string MonthEndDate = last_date.ToShortDateString();
                    string LoanStartDate = "";
                    if (Session["LoanStartDate"] != null)
                    {
                        DateTime LoanSDate = Convert.ToDateTime(Session["LoanStartDate"].ToString());
                        LoanStartDate = LoanSDate.ToShortDateString();
                    }

                    decimal TotalLoanRepayments = 0;
                    decimal TotalInterestPaid = 0;
                    decimal TotalCapitalRepayment = 0;
                    decimal OutstandingCapitalBalance = 0;
                    string OutstandingCapital = "";

                    decimal PTotalLoanRepayments = 0;
                    decimal PTotalInterestPaid = 0;
                    decimal PTotalCapitalRepayment = 0;

                    decimal NTotalLoanRepayments = 0;
                    decimal NTotalInterestPaid = 0;
                    decimal NTotalCapitalRepayment = 0;

                    decimal LTotalLoanRepayments = 0;
                    decimal LTotalInterestPaid = 0;
                    decimal LTotalCapitalRepayment = 0;
                    int MonthChk = 0;
                    foreach (var item in _lst)
                    {
                        DateTime _date = Convert.ToDateTime("1-" + item.Month);

                        if (MonthChk == 0)
                        {
                            if (_date.Year <= _dt.Year)
                            {
                                TotalLoanRepayments += Convert.ToDecimal(item.LoanRepayment);
                                TotalInterestPaid += Convert.ToDecimal(item.InterestCharged);
                                TotalCapitalRepayment += Convert.ToDecimal(item.CapitalRepaid);
                            }
                            if (_date.Month == _dt.Month && _date.Year == _dt.Year)
                            {
                                OutstandingCapital = item.CapitalOutstanding;
                                MonthChk = 1;
                            }
                        }
                        if (_date.Year == (_dt.Year - 1))
                        {
                            PTotalLoanRepayments += Convert.ToDecimal(item.LoanRepayment);
                            PTotalInterestPaid += Convert.ToDecimal(item.InterestCharged);
                            PTotalCapitalRepayment += Convert.ToDecimal(item.CapitalRepaid);
                        }
                        if (_date.Year == (_dt.Year + 1))
                        {
                            NTotalLoanRepayments += Convert.ToDecimal(item.LoanRepayment);
                            NTotalInterestPaid += Convert.ToDecimal(item.InterestCharged);
                            NTotalCapitalRepayment += Convert.ToDecimal(item.CapitalRepaid);
                        }

                        LTotalLoanRepayments += Convert.ToDecimal(item.LoanRepayment);
                        LTotalInterestPaid += Convert.ToDecimal(item.InterestCharged);
                        LTotalCapitalRepayment += Convert.ToDecimal(item.CapitalRepaid);

                    }
                    OutstandingCapitalBalance = (LTotalCapitalRepayment - TotalCapitalRepayment);

                    int _IncOdd = 0;
                    // ======================Insert Excel Header Title ============================// 
                    excell_app.createTitle(_IncOdd + 1, 1, "Loan Summary", "A" + (_IncOdd + 1) + "", "A" + (_IncOdd + 1) + "", 6, "WHITE", true, 40, "");
                    excell_app.createHeaders(_IncOdd + 3, 1, "Review Date", "A" + (_IncOdd + 3) + "", "A" + (_IncOdd + 3) + "", 0, "WHITE", true, 40, "n");

                    excell_app.createHeaders(_IncOdd + 5, 1, "Month End Date", "A" + (_IncOdd + 5) + "", "A" + (_IncOdd + 5) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 6, 1, "Month Satrt date", "A" + (_IncOdd + 6) + "", "A" + (_IncOdd + 6) + "", 0, "WHITE", true, 40, "n");

                    excell_app.createHeaders(_IncOdd + 8, 1, "To Date", "A" + (_IncOdd + 8) + "", "A" + (_IncOdd + 8) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 9, 1, "Total Loan Repayments", "A" + (_IncOdd + 9) + "", "A" + (_IncOdd + 9) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 10, 1, "Total Interest Paid", "A" + (_IncOdd + 10) + "", "A" + (_IncOdd + 10) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 11, 1, "Total Capital Repayment", "A" + (_IncOdd + 11) + "", "A" + (_IncOdd + 11) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 12, 1, "Outstanding Capital Balance", "A" + (_IncOdd + 12) + "", "A" + (_IncOdd + 12) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 13, 1, "Outstanding Capital %", "A" + (_IncOdd + 13) + "", "A" + (_IncOdd + 13) + "", 0, "WHITE", true, 40, "n");

                    excell_app.createHeaders(_IncOdd + 15, 1, "Previous 12 Months", "A" + (_IncOdd + 15) + "", "A" + (_IncOdd + 15) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 16, 1, "Total Loan Repayments", "A" + (_IncOdd + 16) + "", "A" + (_IncOdd + 16) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 17, 1, "Total Interest Paid", "A" + (_IncOdd + 17) + "", "A" + (_IncOdd + 17) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 18, 1, "Total Capital Repayment", "A" + (_IncOdd + 18) + "", "A" + (_IncOdd + 18) + "", 0, "WHITE", true, 40, "n");

                    excell_app.createHeaders(_IncOdd + 20, 1, "Next 12 Months", "A" + (_IncOdd + 20) + "", "A" + (_IncOdd + 20) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 21, 1, "Total Loan Repayments", "A" + (_IncOdd + 21) + "", "A" + (_IncOdd + 21) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 22, 1, "Total Interest Paid", "A" + (_IncOdd + 22) + "", "A" + (_IncOdd + 22) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 23, 1, "Total Capital Repayment", "A" + (_IncOdd + 23) + "", "A" + (_IncOdd + 23) + "", 0, "WHITE", true, 40, "n");

                    excell_app.createHeaders(_IncOdd + 25, 1, "Loan Period", "A" + (_IncOdd + 25) + "", "A" + (_IncOdd + 25) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 26, 1, "Total Loan Repayments", "A" + (_IncOdd + 26) + "", "A" + (_IncOdd + 26) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 27, 1, "Total Interest Paid", "A" + (_IncOdd + 27) + "", "A" + (_IncOdd + 27) + "", 0, "WHITE", true, 40, "n");
                    excell_app.createHeaders(_IncOdd + 28, 1, "Total Capital Repayment %", "A" + (_IncOdd + 28) + "", "A" + (_IncOdd + 28) + "", 0, "WHITE", true, 40, "n");

                    // ======================Insert Excel data ============================// 
                    excell_app.addDateData(_IncOdd + 3, 2, "" + ReviewDate + "", "B" + (_IncOdd + 3) + "", "B" + (_IncOdd + 3) + "", "", 20, "");

                    excell_app.addDateData(_IncOdd + 5, 2, "" + MonthEndDate + "", "B" + (_IncOdd + 5) + "", "B" + (_IncOdd + 5) + "", "", 20, "");
                    excell_app.addDateData(_IncOdd + 6, 2, "" + LoanStartDate.ToString() + "", "B" + (_IncOdd + 6) + "", "B" + (_IncOdd + 6) + "", "", 20, "");

                    excell_app.addData(_IncOdd + 9, 2, "" + TotalLoanRepayments + "", "B" + (_IncOdd + 9) + "", "B" + (_IncOdd + 9) + "", "", 20, "");
                    excell_app.addData(_IncOdd + 10, 2, "" + TotalInterestPaid + "", "B" + (_IncOdd + 10) + "", "B" + (_IncOdd + 10) + "", "", 20, "");
                    excell_app.addData(_IncOdd + 11, 2, "" + TotalCapitalRepayment + "", "B" + (_IncOdd + 11) + "", "B" + (_IncOdd + 11) + "", "", 20, "");
                    excell_app.addData(_IncOdd + 12, 2, "" + OutstandingCapitalBalance + "", "B" + (_IncOdd + 12) + "", "B" + (_IncOdd + 12) + "", "", 20, "");
                    excell_app.addPercentageData(_IncOdd + 13, 2, "" + OutstandingCapital + "", "B" + (_IncOdd + 13) + "", "B" + (_IncOdd + 13) + "", "", 20, "");

                    excell_app.addData(_IncOdd + 16, 2, "" + PTotalLoanRepayments + "", "B" + (_IncOdd + 16) + "", "B" + (_IncOdd + 16) + "", "", 20, "");
                    excell_app.addData(_IncOdd + 17, 2, "" + PTotalInterestPaid + "", "B" + (_IncOdd + 17) + "", "B" + (_IncOdd + 17) + "", "", 20, "");
                    excell_app.addData(_IncOdd + 18, 2, "" + PTotalCapitalRepayment + "", "B" + (_IncOdd + 18) + "", "B" + (_IncOdd + 18) + "", "", 20, "");

                    excell_app.addData(_IncOdd + 21, 2, "" + NTotalLoanRepayments + "", "B" + (_IncOdd + 21) + "", "B" + (_IncOdd + 21) + "", "", 20, "");
                    excell_app.addData(_IncOdd + 22, 2, "" + NTotalInterestPaid + "", "B" + (_IncOdd + 22) + "", "B" + (_IncOdd + 22) + "", "", 20, "");
                    excell_app.addData(_IncOdd + 23, 2, "" + NTotalCapitalRepayment + "", "B" + (_IncOdd + 23) + "", "B" + (_IncOdd + 23) + "", "", 20, "");

                    excell_app.addData(_IncOdd + 26, 2, "" + LTotalLoanRepayments + "", "B" + (_IncOdd + 26) + "", "B" + (_IncOdd + 26) + "", "", 20, "");
                    excell_app.addData(_IncOdd + 27, 2, "" + LTotalInterestPaid + "", "B" + (_IncOdd + 27) + "", "B" + (_IncOdd + 27) + "", "", 20, "");
                    excell_app.addData(_IncOdd + 28, 2, "" + LTotalCapitalRepayment + "", "B" + (_IncOdd + 28) + "", "B" + (_IncOdd + 28) + "", "", 20, "");

                }
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ShotLoanAmortizationLst(int OTHAddressID)
        { if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
            List<LoanAmortiZd> _lst = new List<LoanAmortiZd>();
            // _lst = testloan();
            _lst = GetAmortizationByID(OTHAddressID);
            //TempData["lstLoanAmortiZd"] = _lst;
            Session["lstLoanAmortiZd"] = _lst;
            //List<LoanAmortiZd> _LoanAmortiZation = CalculateLoanAmortiZation(LoanPrincipleAmount, AnnualInterestRate, LoanPeriod, OriginalRepaymentAmount, LoanStartDate);
            // return PartialView("_LoanAmortization");
            return RedirectToAction("LoanAmortization");}
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PalmLoanAmortizationLst(int PCBPalmDetailsID)
        { if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
            List<LoanAmortiZd> _lst = new List<LoanAmortiZd>();

            _lst = GetPalmAmortizationByID(PCBPalmDetailsID);

            Session["lstLoanAmortiZd"] = _lst;

            return RedirectToAction("LoanAmortization");}
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PalmSaveEstOwnerEquity(int? PageSize)
        {

            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
            List<PalmBeachLpcases> _PalmBeachLpcases = new List<PalmBeachLpcases>();
            _PalmBeachLpcases = _vmPalmCase.GetPalmBeachCases(PageSize);

            try
            {
                foreach (var item in _PalmBeachLpcases)
                {
                    //if (item.OwedOnMortgage == null)
                    //{
                    long PCBPalmDetailsID = Convert.ToInt64(item.PCBPalmDetailsID);

                    //GetandSaveOwndMortgate(OTHAddressID);
                    PalmLpSaveOwndMortgate(PCBPalmDetailsID);
                    //}

                }
                _PalmBeachLpcases = _vmPalmCase.GetPalmBeachCases(PageSize);
                    return PartialView("_PalmBeachTabs", _PalmBeachLpcases);
                }
            catch
            {

            }
                return PartialView("_PalmBeachTabs", _PalmBeachLpcases);
            }
            return RedirectToAction("Login", "Home");
        }

        private void PalmLpSaveOwndMortgate(long PCBPalmDetailsID)
        {
            List<LoanAmortiZd> _lst = new List<LoanAmortiZd>();

            tblPCBSaleInfo _SaleInfo = new tblPCBSaleInfo();
            try
            {

                _SaleInfo = db.tblPCBSaleInfoes.AsEnumerable().Where(x => x.PCBPalmDetailsID == PCBPalmDetailsID).Take(1).FirstOrDefault();

                // "103,000.00 ";
                if (_SaleInfo != null)
                {
                    string LoanPrincipleAmount = "";
                    decimal LaonAmount = Convert.ToDecimal(_SaleInfo.Price.Replace(",", "").Replace("$", ""));

                    decimal DownLaonAmount = Convert.ToDecimal(LaonAmount * 10 / 100);

                    LoanPrincipleAmount = Convert.ToString(LaonAmount - DownLaonAmount);

                    int LoanPeriod1 = 360;

                    //string OriginalRepaymentAmount = "903.90";
                    string LoanStartDate = _SaleInfo.SaleDate.ToString(); // "1/15/1999";
                    string RepaymentType = "End";  //"Beginning"; //"End";
                    Session["LoanStartDate"] = LoanStartDate;
                    DateTime _Currntdate = DateTime.Now;
                    //int _LoanPeriod = Convert.ToInt32(TLoanPeriod.ToString());
                    //int LoanPeriod1 = Convert.ToInt32(_LoanPeriod);
                    //int Type = 1;
                    DateTime _loandate = Convert.ToDateTime(LoanStartDate.ToString());

                    int loanMonth = _loandate.Month;
                    tblFixedRate _FixedRate = db.tblFixedRates.AsEnumerable().Where(x => Convert.ToInt32(x.RateYears) == _loandate.Year).FirstOrDefault();
                    ArrayList ArrMonth = new ArrayList();
                    ArrMonth.Add(_FixedRate.RtJan);
                    ArrMonth.Add(_FixedRate.RtFeb);
                    ArrMonth.Add(_FixedRate.RtMarch);
                    ArrMonth.Add(_FixedRate.RtApril);
                    ArrMonth.Add(_FixedRate.RtMay);
                    ArrMonth.Add(_FixedRate.RtJune);
                    ArrMonth.Add(_FixedRate.RtJuly);
                    ArrMonth.Add(_FixedRate.RTAugst);
                    ArrMonth.Add(_FixedRate.RtSept);
                    ArrMonth.Add(_FixedRate.RtOct);
                    ArrMonth.Add(_FixedRate.RtNov);
                    ArrMonth.Add(_FixedRate.RtDec);

                    string Intrestrate = "";   //"9.50";

                    for (int p = 0; p < 12; p++)
                    {
                        if (ArrMonth[p] == null)
                        {
                            Intrestrate = ArrMonth[p - 1].ToString();
                            break;
                        }
                        if ((loanMonth - 1) == p)
                        {
                            Intrestrate = ArrMonth[p].ToString() == "na" ? "10" : ArrMonth[p].ToString();
                            break;
                        }
                    }

                    for (int i = 0; i < LoanPeriod1; i++)
                    {
                        decimal InterestRate = Convert.ToDecimal(Intrestrate == "na" ? "10" : Intrestrate);
                        LoanAmortiZd _Loan = new LoanAmortiZd();
                        string _LoanPAmnt = LoanPrincipleAmount.Replace(",", "").Replace("$", "");
                        decimal _LoanPAmntt = Convert.ToDecimal(_LoanPAmnt.ToString());

                        string _month = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(_loandate.Month);
                        string _loanStartdate = _month + "-" + _loandate.Year;
                        _loandate = Convert.ToDateTime(_loandate.AddMonths(1));
                        decimal _InterestCharged = 0;
                        decimal _capitalRapid;
                        decimal _ClosingBalance;
                        decimal capitaloutstanding;
                        if (i == 0)
                        {
                            //double pmt = PMT(9.50, 360, 103000.00);
                            decimal pmt1;
                            int chkloanindex = (LoanPeriod1 + 1) - (i + 1);
                            if (chkloanindex == 0)
                            {
                                pmt1 = 0;
                            }
                            else
                            {
                                if (RepaymentType == "Beginning")
                                {
                                    double pmt = PMT(Convert.ToDouble(InterestRate), chkloanindex, Convert.ToDouble(_LoanPAmntt), 1);

                                    pmt1 = Decimal.Round(Convert.ToDecimal(pmt), 2);
                                    _InterestCharged = 0;
                                }
                                else
                                {
                                    double pmt = PMT(Convert.ToDouble(InterestRate), chkloanindex, Convert.ToDouble(_LoanPAmntt), 0);
                                    pmt1 = Decimal.Round(Convert.ToDecimal(pmt), 2);
                                    _InterestCharged = Decimal.Round(Convert.ToDecimal(_LoanPAmntt * (InterestRate / 100) / 12), 2); //(_LoanPAmntt * InterestRate / 12);
                                }

                            }
                            _capitalRapid = (pmt1 - _InterestCharged);

                            _ClosingBalance = (_LoanPAmntt - _capitalRapid);

                            capitaloutstanding = Math.Round((Convert.ToDecimal(_ClosingBalance) / Convert.ToDecimal(_LoanPAmntt)), 1);

                            _Loan.Month = _loanStartdate;
                            _Loan.RepaymentNumber = i + 1;
                            _Loan.OpeningBalance = _LoanPAmntt;
                            _Loan.LoanRepayment = pmt1;
                            _Loan.InterestCharged = _InterestCharged;
                            _Loan.CapitalRepaid = _capitalRapid;
                            _Loan.ClosingBalance = _ClosingBalance;
                            _Loan.CapitalOutstanding = (capitaloutstanding * 100) + "%";
                            _Loan.InterestRate = InterestRate;
                        }
                        else
                        {
                            decimal _LoanPAmntt1;
                            decimal pmt2;
                            decimal _InterestCharged1 = 0;
                            decimal _capitalRapid1;
                            decimal _ClosingBalance1;
                            decimal capitaloutstanding1;
                            decimal LoanRepay = Convert.ToDecimal(_lst[i - 1].ClosingBalance);
                            decimal _ChkloanRe = Decimal.Round(LoanRepay, 0);
                            if (_ChkloanRe > 1)
                            {
                                _LoanPAmntt1 = LoanRepay;
                            }
                            else
                            {
                                _LoanPAmntt1 = 0;
                                break;
                            }

                            int chkLaonprd = LoanPeriod1 + 1 - (i + 1);
                            if (chkLaonprd == 0)
                            {
                                pmt2 = 0;
                            }
                            else
                            {
                                double pmt = PMT(Convert.ToDouble(InterestRate), chkLaonprd, Convert.ToDouble(_LoanPAmntt1), 0);
                                pmt2 = Decimal.Round(Convert.ToDecimal(pmt), 2, 0);

                            }
                            _InterestCharged1 = Decimal.Round(Convert.ToDecimal(_LoanPAmntt1 * (InterestRate / 100) / 12), 2);

                            _capitalRapid1 = (pmt2 - _InterestCharged1);

                            _ClosingBalance1 = (_LoanPAmntt1 - _capitalRapid1);

                            capitaloutstanding1 = (Convert.ToDecimal(_ClosingBalance1) / Convert.ToDecimal(_LoanPAmntt1));

                            _Loan.Month = _loanStartdate;
                            _Loan.RepaymentNumber = i + 1;
                            _Loan.OpeningBalance = _LoanPAmntt1;
                            _Loan.LoanRepayment = pmt2;
                            _Loan.InterestCharged = _InterestCharged1;
                            _Loan.CapitalRepaid = _capitalRapid1;
                            _Loan.ClosingBalance = _ClosingBalance1;
                            _Loan.CapitalOutstanding = String.Format("{0:0.0}", (capitaloutstanding1 * 100)) + "%";
                            _Loan.InterestRate = InterestRate;
                            if (_Currntdate.Year == _loandate.Year && (_Currntdate.Month + 1) == (_loandate.Month - 1))
                            {
                                break;
                            }
                        }
                        _lst.Add(_Loan);

                    }

                    string _monthDate = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(_Currntdate.Month);
                    string Matchdate = _monthDate + "-" + _Currntdate.Year;

                    var itemset = _lst.Where(w => w.Month == Matchdate).FirstOrDefault();
                    string OwedOnMortgage = "";
                    if (itemset != null)
                    {
                        OwedOnMortgage = itemset.ClosingBalance.ToString();
                    }
                    else
                    {
                        OwedOnMortgage = "FULLY PAID";
                    }

                    //---------------------Update Table TblpcbpalmDetails By  OwedOnMortgage-------------//
                    _vmcase.SaveOwedOnMortgageInPalm(PCBPalmDetailsID, Matchdate, OwedOnMortgage);

                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }

        }

        public void GetSaveEstOwnerEquity(List<SrchDefandant> _tblSrch)
        {

            try
            {
                foreach (var item in _tblSrch)
                {
                    //if (item.OwedOnMortgage == null)
                    // {
                    int OTHAddressID = Convert.ToInt32(item.OTHAddressID);

                    GetandSaveOwndMortgate(OTHAddressID);
                    // }

                }

            }
            catch
            {

            }
        }

        private void GetandSaveOwndMortgate(int OTHAddressID)
        {
            List<LoanAmortiZd> _lst = new List<LoanAmortiZd>();
            OtherProperty _OthPropty = new OtherProperty();
            try
            {
                int SubTabId = Convert.ToInt32(Session["SubTabId"].ToString() == null ? "1" : Session["SubTabId"].ToString());
                _OthPropty = _vmcase.OtherPropertiesDetails(OTHAddressID);

                // "103,000.00 ";
                if (_OthPropty != null)
                {
                    string LoanPrincipleAmount = "";
                    decimal LaonAmount = Convert.ToDecimal(_OthPropty.SalePrice.Replace(",", "").Replace("$", ""));
                    decimal DownLaonAmount = Convert.ToDecimal(LaonAmount * 10 / 100);

                    LoanPrincipleAmount = Convert.ToString(LaonAmount - DownLaonAmount);

                    int LoanPeriod1 = 360;
                    //string OriginalRepaymentAmount = "903.90";
                    string LoanStartDate = _OthPropty.OthSaleDate.ToString(); // "1/15/1999";
                    string RepaymentType = "End";  //"Beginning"; //"End";
                    Session["LoanStartDate"] = LoanStartDate;
                    DateTime _Currntdate = DateTime.Now;
                    //int _LoanPeriod = Convert.ToInt32(TLoanPeriod.ToString());
                    //int LoanPeriod1 = Convert.ToInt32(_LoanPeriod);
                    //int Type = 1;
                    DateTime _loandate = Convert.ToDateTime(LoanStartDate.ToString());

                    int loanMonth = _loandate.Month;

                    tblFixedRate _FixedRate = db.tblFixedRates.AsEnumerable().Where(x => Convert.ToInt32(x.RateYears) == _loandate.Year).FirstOrDefault();
                    ArrayList ArrMonth = new ArrayList();
                    ArrMonth.Add(_FixedRate.RtJan);
                    ArrMonth.Add(_FixedRate.RtFeb);
                    ArrMonth.Add(_FixedRate.RtMarch);
                    ArrMonth.Add(_FixedRate.RtApril);
                    ArrMonth.Add(_FixedRate.RtMay);
                    ArrMonth.Add(_FixedRate.RtJune);
                    ArrMonth.Add(_FixedRate.RtJuly);
                    ArrMonth.Add(_FixedRate.RTAugst);
                    ArrMonth.Add(_FixedRate.RtSept);
                    ArrMonth.Add(_FixedRate.RtOct);
                    ArrMonth.Add(_FixedRate.RtNov);
                    ArrMonth.Add(_FixedRate.RtDec);

                    string Intrestrate = "";   //"9.50";

                    for (int p = 0; p < 12; p++)
                    {
                        if (ArrMonth[p] == null)
                        {
                            Intrestrate = ArrMonth[p - 1].ToString();
                            break;
                        }
                        if ((loanMonth - 1) == p)
                        {
                            Intrestrate = ArrMonth[p].ToString() == "na" ? "10" : ArrMonth[p].ToString();
                            break;
                        }
                    }

                    for (int i = 0; i < LoanPeriod1; i++)
                    {
                        decimal InterestRate = Convert.ToDecimal(Intrestrate == "na" ? "10" : Intrestrate);

                        LoanAmortiZd _Loan = new LoanAmortiZd();
                        string _LoanPAmnt = LoanPrincipleAmount;
                        decimal _LoanPAmntt = Convert.ToDecimal(_LoanPAmnt.ToString());

                        string _month = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(_loandate.Month);
                        string _loanStartdate = _month + "-" + _loandate.Year;
                        _loandate = Convert.ToDateTime(_loandate.AddMonths(1));
                        decimal _InterestCharged = 0;
                        decimal _capitalRapid;
                        decimal _ClosingBalance;
                        decimal capitaloutstanding;
                        if (i == 0)
                        {
                            //double pmt = PMT(9.50, 360, 103000.00);
                            decimal pmt1;
                            int chkloanindex = (LoanPeriod1 + 1) - (i + 1);
                            if (chkloanindex == 0)
                            {
                                pmt1 = 0;
                            }
                            else
                            {
                                if (RepaymentType == "Beginning")
                                {
                                    double pmt = PMT(Convert.ToDouble(InterestRate), chkloanindex, Convert.ToDouble(_LoanPAmntt), 1);

                                    pmt1 = Decimal.Round(Convert.ToDecimal(pmt), 2);
                                    _InterestCharged = 0;
                                }
                                else
                                {
                                    double pmt = PMT(Convert.ToDouble(InterestRate), chkloanindex, Convert.ToDouble(_LoanPAmntt), 0);
                                    pmt1 = Decimal.Round(Convert.ToDecimal(pmt), 2);
                                    _InterestCharged = Decimal.Round(Convert.ToDecimal(_LoanPAmntt * (InterestRate / 100) / 12), 2); //(_LoanPAmntt * InterestRate / 12);
                                }

                            }
                            _capitalRapid = (pmt1 - _InterestCharged);

                            _ClosingBalance = (_LoanPAmntt - _capitalRapid);

                            capitaloutstanding = Math.Round((Convert.ToDecimal(_ClosingBalance) / Convert.ToDecimal(_LoanPAmntt)), 1);

                            _Loan.Month = _loanStartdate;
                            _Loan.RepaymentNumber = i + 1;
                            _Loan.OpeningBalance = _LoanPAmntt;
                            _Loan.LoanRepayment = pmt1;
                            _Loan.InterestCharged = _InterestCharged;
                            _Loan.CapitalRepaid = _capitalRapid;
                            _Loan.ClosingBalance = _ClosingBalance;
                            _Loan.CapitalOutstanding = (capitaloutstanding * 100) + "%";
                            _Loan.InterestRate = InterestRate;
                        }
                        else
                        {
                            decimal _LoanPAmntt1;
                            decimal pmt2;
                            decimal _InterestCharged1 = 0;
                            decimal _capitalRapid1;
                            decimal _ClosingBalance1;
                            decimal capitaloutstanding1;
                            decimal LoanRepay = Convert.ToDecimal(_lst[i - 1].ClosingBalance);
                            decimal _ChkloanRe = Decimal.Round(LoanRepay, 0);
                            if (_ChkloanRe > 1)
                            {
                                _LoanPAmntt1 = LoanRepay;
                            }
                            else
                            {
                                _LoanPAmntt1 = 0;
                                break;
                            }

                            int chkLaonprd = LoanPeriod1 + 1 - (i + 1);
                            if (chkLaonprd == 0)
                            {
                                pmt2 = 0;
                            }
                            else
                            {
                                double pmt = PMT(Convert.ToDouble(InterestRate), chkLaonprd, Convert.ToDouble(_LoanPAmntt1), 0);
                                pmt2 = Decimal.Round(Convert.ToDecimal(pmt), 2, 0);

                            }
                            _InterestCharged1 = Decimal.Round(Convert.ToDecimal(_LoanPAmntt1 * (InterestRate / 100) / 12), 2);

                            _capitalRapid1 = (pmt2 - _InterestCharged1);

                            _ClosingBalance1 = (_LoanPAmntt1 - _capitalRapid1);

                            capitaloutstanding1 = (Convert.ToDecimal(_ClosingBalance1) / Convert.ToDecimal(_LoanPAmntt1));

                            _Loan.Month = _loanStartdate;
                            _Loan.RepaymentNumber = i + 1;
                            _Loan.OpeningBalance = _LoanPAmntt1;
                            _Loan.LoanRepayment = pmt2;
                            _Loan.InterestCharged = _InterestCharged1;
                            _Loan.CapitalRepaid = _capitalRapid1;
                            _Loan.ClosingBalance = _ClosingBalance1;
                            _Loan.CapitalOutstanding = String.Format("{0:0.0}", (capitaloutstanding1 * 100)) + "%";
                            _Loan.InterestRate = InterestRate;

                            if (_Currntdate.Year == _loandate.Year && (_Currntdate.Month + 1) == (_loandate.Month - 1))
                            {
                                break;
                            }
                        }
                        _lst.Add(_Loan);

                    }

                    string _monthDate = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(_Currntdate.Month);
                    string Matchdate = _monthDate + "-" + _Currntdate.Year;

                    var itemset = _lst.Where(w => w.Month == Matchdate).FirstOrDefault();
                    string OwedOnMortgage = "";
                    if (itemset != null)
                    {
                        OwedOnMortgage = itemset.ClosingBalance.ToString();
                    }
                    else
                    {
                        OwedOnMortgage = "FULLY PAID";
                    }

                    //---------------------Update Table TblOthProperty By  OwedOnMortgage-------------//
                    _vmcase.SaveOwedOnMortgageInOTH(OTHAddressID, Matchdate, OwedOnMortgage);
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }

        }

        //-------------------------------Calculate Amortization By Monthly Intrest Rate Applied---------------------------//
        //private void GetandSaveOwndMortgate(int OTHAddressID)
        //{
        //    List<LoanAmortiZd> _lst = new List<LoanAmortiZd>();
        //    OtherProperty _OthPropty = new OtherProperty();
        //    try
        //    {
        //        int SubTabId = Convert.ToInt32(Session["SubTabId"].ToString() == null ? "1" : Session["SubTabId"].ToString());
        //        _OthPropty = _vmcase.OtherPropertiesDetails(OTHAddressID);

        //        // "103,000.00 ";
        //        if (_OthPropty != null)
        //        {
        //            string LoanPrincipleAmount = _OthPropty.SalePrice;

        //            int LoanPeriod1 = 360;
        //            string OriginalRepaymentAmount = "903.90";
        //            string LoanStartDate = _OthPropty.OthSaleDate.ToString(); // "1/15/1999";
        //            string RepaymentType = "End";  //"Beginning"; //"End";
        //            Session["LoanStartDate"] = LoanStartDate;
        //            DateTime _Currntdate = DateTime.Now;
        //            //int _LoanPeriod = Convert.ToInt32(TLoanPeriod.ToString());
        //            //int LoanPeriod1 = Convert.ToInt32(_LoanPeriod);
        //            int Type = 1;
        //            DateTime _loandate = Convert.ToDateTime(LoanStartDate.ToString());


        //            List<tblFixedRate> _FixedRate = new List<tblFixedRate>();

        //            _FixedRate = db.tblFixedRates.AsEnumerable().Where(x => Convert.ToInt32(x.RateYears) >= _loandate.Year).ToList();

        //            tblFixedRate test = db.tblFixedRates.AsEnumerable().OrderByDescending(o => o.RateYears).Take(1).FirstOrDefault();
        //            ArrayList ArrChk = new ArrayList();
        //            ArrChk.Add(test.RtJan);
        //            ArrChk.Add(test.RtFeb);
        //            ArrChk.Add(test.RtMarch);
        //            ArrChk.Add(test.RtApril);
        //            ArrChk.Add(test.RtMay);
        //            ArrChk.Add(test.RtJune);
        //            ArrChk.Add(test.RtJuly);
        //            ArrChk.Add(test.RTAugst);
        //            ArrChk.Add(test.RtSept);
        //            ArrChk.Add(test.RtOct);
        //            ArrChk.Add(test.RtNov);
        //            ArrChk.Add(test.RtDec);
        //            int ChkMonth = 0;
        //            foreach (var chk in ArrChk)
        //            {
        //                if (chk == null)
        //                {
        //                    break;
        //                }
        //                ChkMonth++;
        //            }

        //            DateTime end = DateTime.Now;
        //            var diffMonths = (1 + end.Year * 12) - (_loandate.Month + _loandate.Year * 12);
        //            int _TotalMonth = (Convert.ToInt32(diffMonths) + ChkMonth);
        //            int PendingMonth = (LoanPeriod1 - _TotalMonth);

        //            int i = 0;
        //            int CntCheckBalance = 0;
        //            string CurrentRate = "";

        //            foreach (var item in _FixedRate)
        //            {
        //                int loanMonth = 1;
        //                if (i == 0)
        //                {
        //                    loanMonth = _loandate.Month;
        //                }

        //                if (CntCheckBalance == 1)
        //                {
        //                    break;
        //                }
        //                string AnnualInterestRate = "9.50";
        //                ArrayList ArrMonth = new ArrayList();

        //                ArrMonth.Add(item.RtJan);
        //                ArrMonth.Add(item.RtFeb);
        //                ArrMonth.Add(item.RtMarch);
        //                ArrMonth.Add(item.RtApril);
        //                ArrMonth.Add(item.RtMay);
        //                ArrMonth.Add(item.RtJune);
        //                ArrMonth.Add(item.RtJuly);
        //                ArrMonth.Add(item.RTAugst);
        //                ArrMonth.Add(item.RtSept);
        //                ArrMonth.Add(item.RtOct);
        //                ArrMonth.Add(item.RtNov);
        //                ArrMonth.Add(item.RtDec);

        //                for (int p = 0; p < 12; p++)
        //                {

        //                    if (_lst.Count > 1)
        //                    {
        //                        decimal LoanClosingBalance = Convert.ToDecimal(_lst[i - 1].ClosingBalance);
        //                        if (LoanClosingBalance <= 0)
        //                        {
        //                            CntCheckBalance = 1;
        //                            break;
        //                        }
        //                    }

        //                    if (ArrMonth[p] == null)
        //                    {
        //                        CurrentRate = ArrMonth[p - 1].ToString();

        //                        GetPendingAmortized(_lst, i, CurrentRate, _loandate, LoanPeriod1, PendingMonth);
        //                        break;
        //                    }
        //                    if ((loanMonth - 1) <= p)
        //                    {
        //                        decimal InterestRate = Convert.ToDecimal(ArrMonth[p] == "na" ? "10" : ArrMonth[p]);

        //                        LoanAmortiZd _Loan = new LoanAmortiZd();
        //                        string _LoanPAmnt = LoanPrincipleAmount.Replace(",", "").Replace("$", "");
        //                        decimal _LoanPAmntt = Convert.ToDecimal(_LoanPAmnt.ToString());

        //                        string _month = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(_loandate.Month);
        //                        string _loanStartdate = _month + "-" + _loandate.Year;
        //                        _loandate = Convert.ToDateTime(_loandate.AddMonths(1));
        //                        decimal _InterestCharged = 0;
        //                        decimal _capitalRapid;
        //                        decimal _ClosingBalance;
        //                        decimal capitaloutstanding;
        //                        if (i == 0)
        //                        {
        //                            //double pmt = PMT(9.50, 360, 103000.00);
        //                            decimal pmt1;
        //                            int chkloanindex = (LoanPeriod1 + 1) - (i + 1);
        //                            if (chkloanindex == 0)
        //                            {
        //                                pmt1 = 0;
        //                            }
        //                            else
        //                            {
        //                                if (RepaymentType == "Beginning")
        //                                {
        //                                    double pmt = PMT(Convert.ToDouble(InterestRate), chkloanindex, Convert.ToDouble(_LoanPAmntt), 1);

        //                                    pmt1 = Decimal.Round(Convert.ToDecimal(pmt), 2);
        //                                    _InterestCharged = 0;
        //                                }
        //                                else
        //                                {
        //                                    double pmt = PMT(Convert.ToDouble(InterestRate), chkloanindex, Convert.ToDouble(_LoanPAmntt), 0);
        //                                    pmt1 = Decimal.Round(Convert.ToDecimal(pmt), 2);
        //                                    _InterestCharged = Decimal.Round(Convert.ToDecimal(_LoanPAmntt * (InterestRate / 100) / 12), 2); //(_LoanPAmntt * InterestRate / 12);
        //                                }

        //                            }
        //                            _capitalRapid = (pmt1 - _InterestCharged);

        //                            _ClosingBalance = (_LoanPAmntt - _capitalRapid);

        //                            capitaloutstanding = Math.Round((Convert.ToDecimal(_ClosingBalance) / Convert.ToDecimal(_LoanPAmntt)), 1);

        //                            _Loan.Month = _loanStartdate;
        //                            _Loan.RepaymentNumber = i + 1;
        //                            _Loan.OpeningBalance = _LoanPAmntt;
        //                            _Loan.LoanRepayment = pmt1;
        //                            _Loan.InterestCharged = _InterestCharged;
        //                            _Loan.CapitalRepaid = _capitalRapid;
        //                            _Loan.ClosingBalance = _ClosingBalance;
        //                            _Loan.CapitalOutstanding = (capitaloutstanding * 100) + "%";
        //                            _Loan.InterestRate = InterestRate;
        //                        }
        //                        else
        //                        {
        //                            decimal _LoanPAmntt1;
        //                            decimal pmt2;
        //                            decimal _InterestCharged1 = 0;
        //                            decimal _capitalRapid1;
        //                            decimal _ClosingBalance1;
        //                            decimal capitaloutstanding1;
        //                            decimal LoanRepay = Convert.ToDecimal(_lst[i - 1].ClosingBalance);
        //                            decimal _ChkloanRe = Decimal.Round(LoanRepay, 0);
        //                            if (_ChkloanRe > 0)
        //                            {
        //                                _LoanPAmntt1 = LoanRepay;
        //                            }
        //                            else
        //                            {
        //                                _LoanPAmntt1 = 0;
        //                            }

        //                            int chkLaonprd = LoanPeriod1 + 1 - (i + 1);
        //                            if (chkLaonprd == 0)
        //                            {
        //                                pmt2 = 0;
        //                            }
        //                            else
        //                            {
        //                                double pmt = PMT(Convert.ToDouble(InterestRate), chkLaonprd, Convert.ToDouble(_LoanPAmntt1), 0);
        //                                pmt2 = Decimal.Round(Convert.ToDecimal(pmt), 2, 0);

        //                            }
        //                            _InterestCharged1 = Decimal.Round(Convert.ToDecimal(_LoanPAmntt1 * (InterestRate / 100) / 12), 2);

        //                            _capitalRapid1 = (pmt2 - _InterestCharged1);

        //                            _ClosingBalance1 = (_LoanPAmntt1 - _capitalRapid1);

        //                            capitaloutstanding1 = (Convert.ToDecimal(_ClosingBalance1) / Convert.ToDecimal(_LoanPAmntt1));

        //                            _Loan.Month = _loanStartdate;
        //                            _Loan.RepaymentNumber = i + 1;
        //                            _Loan.OpeningBalance = _LoanPAmntt1;
        //                            _Loan.LoanRepayment = pmt2;
        //                            _Loan.InterestCharged = _InterestCharged1;
        //                            _Loan.CapitalRepaid = _capitalRapid1;
        //                            _Loan.ClosingBalance = _ClosingBalance1;
        //                            _Loan.CapitalOutstanding = String.Format("{0:0.0}", (capitaloutstanding1 * 100)) + "%";
        //                            _Loan.InterestRate = InterestRate;
        //                            if (_Currntdate.Year == _loandate.Year && _Currntdate.Month == _loandate.Month - 1)
        //                            {
        //                                break;
        //                            }
        //                        }
        //                        _lst.Add(_Loan);
        //                        i++;
        //                    }
        //                }
        //            }


        //            string _monthDate = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(_Currntdate.Month);
        //            string Matchdate = _monthDate + "-" + _Currntdate.Year;

        //            var itemset = _lst.Where(w => w.Month == Matchdate).FirstOrDefault();
        //            string OwedOnMortgage = "";
        //            if (itemset != null)
        //            {
        //                OwedOnMortgage = itemset.ClosingBalance.ToString();
        //            }
        //            else
        //            {
        //                OwedOnMortgage = "FULLY PAID";
        //            }



        //            //---------------------Update Table TblOthProperty By  OwedOnMortgage-------------//
        //            _vmcase.SaveOwedOnMortgageInOTH(OTHAddressID, Matchdate, OwedOnMortgage);
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //}

        public List<LoanAmortiZd> GetPendingAmortized(List<LoanAmortiZd> _lst, int i, string CurrentRate, DateTime _loandate, int LoanPeriod1, int PendingMonth)
        {
            try
            {
                DateTime Currentdate = DateTime.Now;
                decimal InterestRate = Convert.ToDecimal(CurrentRate == "na" ? "10" : CurrentRate);

                for (int p = 1; p <= PendingMonth; p++)
                {
                    LoanAmortiZd _Loan = new LoanAmortiZd();

                    string _month = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(_loandate.Month);
                    string _loanStartdate = _month + "-" + _loandate.Year;
                    _loandate = Convert.ToDateTime(_loandate.AddMonths(1));

                    decimal _LoanPAmntt1;
                    decimal pmt2;
                    decimal _InterestCharged1 = 0;
                    decimal _capitalRapid1;
                    decimal _ClosingBalance1;
                    decimal capitaloutstanding1;
                    decimal LoanRepay = Convert.ToDecimal(_lst[i - 1].ClosingBalance);
                    decimal _ChkloanRe = Decimal.Round(LoanRepay, 0);
                    if (_ChkloanRe > 0)
                    {
                        _LoanPAmntt1 = LoanRepay;
                    }
                    else
                    {
                        _LoanPAmntt1 = 0;
                    }

                    int chkLaonprd = LoanPeriod1 + 1 - (i + 1);
                    if (chkLaonprd == 0)
                    {
                        pmt2 = 0;
                    }
                    else
                    {
                        double pmt = PMT(Convert.ToDouble(InterestRate), chkLaonprd, Convert.ToDouble(_LoanPAmntt1), 0);
                        pmt2 = Decimal.Round(Convert.ToDecimal(pmt), 2, 0);

                    }
                    _InterestCharged1 = Decimal.Round(Convert.ToDecimal(_LoanPAmntt1 * (InterestRate / 100) / 12), 2);

                    _capitalRapid1 = (pmt2 - _InterestCharged1);

                    _ClosingBalance1 = (_LoanPAmntt1 - _capitalRapid1);

                    capitaloutstanding1 = (Convert.ToDecimal(_ClosingBalance1) / Convert.ToDecimal(_LoanPAmntt1));

                    _Loan.Month = _loanStartdate;
                    _Loan.RepaymentNumber = i + 1;
                    _Loan.OpeningBalance = _LoanPAmntt1;
                    _Loan.LoanRepayment = pmt2;
                    _Loan.InterestCharged = _InterestCharged1;
                    _Loan.CapitalRepaid = _capitalRapid1;
                    _Loan.ClosingBalance = _ClosingBalance1;
                    _Loan.CapitalOutstanding = String.Format("{0:0.0}", (capitaloutstanding1 * 100)) + "%";
                    _Loan.InterestRate = InterestRate;
                    _lst.Add(_Loan);

                    if (Currentdate.Month == (_loandate.Month - 1) && Currentdate.Year == (_loandate.Year))
                    {
                        break;
                    }

                    i++;
                }
                return _lst;
            }

            catch
            {
                return _lst;
            }
        }

        public List<LoanAmortiZd> GetPalmAmortizationByID(int PCBPalmDetailsID)
        {
            try
            {
                List<LoanAmortiZd> _lst = new List<LoanAmortiZd>();
                tblPCBSaleInfo _SaleInfo = new tblPCBSaleInfo();

               // _SaleInfo = db.tblPCBSaleInfoes.AsEnumerable().Where(x => x.PCBPalmDetailsID == PCBPalmDetailsID).Take(1).FirstOrDefault();

                _SaleInfo = _vmcase.OtherPalmPropertiesDetails(PCBPalmDetailsID);
                string LoanPrincipleAmount = "";
                int division = 0;
                if (_SaleInfo.UseCodeCategpryID == 1)
                {
                    division = 10;
                }
                else if (_SaleInfo.UseCodeCategpryID == 1)
                {
                    division = 30;
                }
                else
                {
                    division = 10;
                }
                //decimal LaonAmount = Convert.ToDecimal(_SaleInfo.Price.Replace(",", "").Replace("$", ""));
                string Downpayment = "";
                var LaonAmount = Convert.ToDecimal(_SaleInfo.Price.Replace("$", ""));
                var DownLaonAmount = Convert.ToDecimal(LaonAmount * division / 100);

                LoanPrincipleAmount = String.Format("{0:C}", (LaonAmount - DownLaonAmount));
                Downpayment = String.Format("{0:C}", (DownLaonAmount));
                //string add = Convert.ToString(_SaleInfo.Price);
                //string ten = "10";
                //StringBuilder sb = new StringBuilder(ten * add);

               
                
                string LoanPeriod = "360.00";
                //string OriginalRepaymentAmount = "903.90";
                string LoanStartDate = _SaleInfo.SaleDate.ToString(); // "1/15/1999";
                string RepaymentType = "End";  //"Beginning"; //"End";
                Session["LoanStartDate"] = LoanStartDate;
                DateTime _Currntdate = DateTime.Now;
                decimal _LoanPeriod = Convert.ToDecimal(LoanPeriod.ToString());
                int LoanPeriod1 = Convert.ToInt32(_LoanPeriod);
                //int Type = 1;
                DateTime _loandate = Convert.ToDateTime(LoanStartDate.ToString());

                //string CurrentRate = "";

                Session["LoanPrincipleAmount"] = LoanPrincipleAmount;
                Session["LoanStartDate"] = LoanStartDate;
                Session["LoanPeriod"] = LoanPeriod1;
                Session["Downpayment"] = Downpayment;
                int loanMonth = _loandate.Month;

                tblFixedRate _FixedRate = db.tblFixedRates.AsEnumerable().Where(x => Convert.ToInt32(x.RateYears) == _loandate.Year).FirstOrDefault();
                if (_FixedRate == null)
                {

                }
                else
                {
                    ArrayList ArrMonth = new ArrayList();
                    ArrMonth.Add(_FixedRate.RtJan);
                    ArrMonth.Add(_FixedRate.RtFeb);
                    ArrMonth.Add(_FixedRate.RtMarch);
                    ArrMonth.Add(_FixedRate.RtApril);
                    ArrMonth.Add(_FixedRate.RtMay);
                    ArrMonth.Add(_FixedRate.RtJune);
                    ArrMonth.Add(_FixedRate.RtJuly);
                    ArrMonth.Add(_FixedRate.RTAugst);
                    ArrMonth.Add(_FixedRate.RtSept);
                    ArrMonth.Add(_FixedRate.RtOct);
                    ArrMonth.Add(_FixedRate.RtNov);
                    ArrMonth.Add(_FixedRate.RtDec);

                    string Intrestrate = "";   //"9.50";
                    string CurrentRate = "";
                    for (int p = 0; p < 12; p++)
                    {
                        if (ArrMonth[p] == null)
                        {
                            Intrestrate = ArrMonth[p - 1].ToString();
                            CurrentRate = ArrMonth[p - 1].ToString();

                            Session["CurrentRate"] = CurrentRate;
                            break;
                        }
                        if ((loanMonth - 1) == p)
                        {
                            Intrestrate = ArrMonth[p].ToString() == "na" ? "10" : ArrMonth[p].ToString();
                            Session["CurrentRate"] = Intrestrate;
                            break;
                        }
                    }

                    for (int i = 0; i < LoanPeriod1; i++)
                    {
                        decimal InterestRate = Convert.ToDecimal(Intrestrate == "na" ? "10" : Intrestrate);


                        LoanAmortiZd _Loan = new LoanAmortiZd();
                        string _LoanPAmnt = LoanPrincipleAmount.Replace(",", "").Replace("$", "");
                        decimal _LoanPAmntt = Convert.ToDecimal(_LoanPAmnt.ToString());

                        string _month = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(_loandate.Month);
                        string _loanStartdate = _month + "-" + _loandate.Year;
                        _loandate = Convert.ToDateTime(_loandate.AddMonths(1));
                        decimal _InterestCharged = 0;
                        decimal _capitalRapid;
                        decimal _ClosingBalance;
                        decimal capitaloutstanding;
                        if (i == 0)
                        {
                            //double pmt = PMT(9.50, 360, 103000.00);
                            decimal pmt1;
                            int chkloanindex = (LoanPeriod1 + 1) - (i + 1);
                            if (chkloanindex == 0)
                            {
                                pmt1 = 0;
                            }
                            else
                            {
                                if (RepaymentType == "Beginning")
                                {
                                    double pmt = PMT(Convert.ToDouble(InterestRate), chkloanindex, Convert.ToDouble(_LoanPAmntt), 1);

                                    pmt1 = Decimal.Round(Convert.ToDecimal(pmt), 2);
                                    _InterestCharged = 0;
                                }
                                else
                                {
                                    double pmt = PMT(Convert.ToDouble(InterestRate), chkloanindex, Convert.ToDouble(_LoanPAmntt), 0);
                                    pmt1 = Decimal.Round(Convert.ToDecimal(pmt), 2);
                                    _InterestCharged = Decimal.Round(Convert.ToDecimal(_LoanPAmntt * (InterestRate / 100) / 12), 2); //(_LoanPAmntt * InterestRate / 12);
                                }

                            }
                            _capitalRapid = (pmt1 - _InterestCharged);

                            _ClosingBalance = (_LoanPAmntt - _capitalRapid);

                            capitaloutstanding = Math.Round((Convert.ToDecimal(_ClosingBalance) / Convert.ToDecimal(_LoanPAmntt)), 1);

                            _Loan.Month = _loanStartdate;
                            _Loan.RepaymentNumber = i + 1;
                            _Loan.OpeningBalance = _LoanPAmntt;
                            _Loan.OpeningBalanceNew = String.Format("{0:C}", (_LoanPAmntt));
                            _Loan.LoanRepayment = pmt1;
                            _Loan.LoanRepaymentNew = String.Format("{0:C}", (pmt1));
                            _Loan.InterestCharged = _InterestCharged;
                            _Loan.InterestChargedNew = String.Format("{0:C}", (_InterestCharged));
                            _Loan.CapitalRepaid = _capitalRapid;
                            _Loan.CapitalRepaidNew = String.Format("{0:C}", (_capitalRapid));
                            _Loan.ClosingBalance = _ClosingBalance;
                            _Loan.ClosingBalanceNew = String.Format("{0:C}", (_ClosingBalance));
                            _Loan.CapitalOutstanding = String.Format("{0:C}", (capitaloutstanding * 100)) + "%";
                            _Loan.InterestRate = InterestRate;
                            _Loan.InterestRateNew = String.Format("{0:C}", (InterestRate)) + "%";
                        }
                        else
                        {
                            decimal _LoanPAmntt1;
                            decimal pmt2;
                            decimal _InterestCharged1 = 0;
                            decimal _capitalRapid1;
                            decimal _ClosingBalance1;
                            decimal capitaloutstanding1;
                            decimal LoanRepay = Convert.ToDecimal(_lst[i - 1].ClosingBalance);
                            decimal _ChkloanRe = Decimal.Round(LoanRepay, 0);
                            if (_ChkloanRe > 1)
                            {
                                _LoanPAmntt1 = LoanRepay;
                            }
                            else
                            {
                                _LoanPAmntt1 = 0;
                                break;
                            }

                            int chkLaonprd = LoanPeriod1 + 1 - (i + 1);
                            if (chkLaonprd == 0)
                            {
                                pmt2 = 0;
                            }
                            else
                            {
                                double pmt = PMT(Convert.ToDouble(InterestRate), chkLaonprd, Convert.ToDouble(_LoanPAmntt1), 0);
                                pmt2 = Decimal.Round(Convert.ToDecimal(pmt), 2, 0);

                            }
                            _InterestCharged1 = Decimal.Round(Convert.ToDecimal(_LoanPAmntt1 * (InterestRate / 100) / 12), 2);

                            _capitalRapid1 = (pmt2 - _InterestCharged1);

                            _ClosingBalance1 = (_LoanPAmntt1 - _capitalRapid1);

                            capitaloutstanding1 = (Convert.ToDecimal(_ClosingBalance1) / Convert.ToDecimal(_LoanPAmntt1));

                            _Loan.Month = _loanStartdate;
                            _Loan.RepaymentNumber = i + 1;
                            _Loan.OpeningBalance = _LoanPAmntt1;
                            _Loan.OpeningBalanceNew = String.Format("{0:C}", (_LoanPAmntt1));
                            _Loan.LoanRepayment = pmt2;
                            _Loan.LoanRepaymentNew = String.Format("{0:C}", (pmt2));
                            _Loan.InterestCharged = _InterestCharged1;
                            _Loan.InterestChargedNew = String.Format("{0:C}", (_InterestCharged1));
                            _Loan.CapitalRepaid = _capitalRapid1;
                            _Loan.CapitalRepaidNew = String.Format("{0:C}", (_capitalRapid1));
                            _Loan.ClosingBalance = _ClosingBalance1;
                            _Loan.ClosingBalanceNew = String.Format("{0:C}", (_ClosingBalance1));
                            _Loan.CapitalOutstanding = String.Format("{0:C}", (capitaloutstanding1 * 100)) + "%";

                            _Loan.InterestRate = InterestRate;
                            _Loan.InterestRateNew = String.Format("{0:C}", (InterestRate)) + "%";

                            if (_Currntdate.Year == _loandate.Year && _Currntdate.Month == _loandate.Month)
                            {
                                //string tt = "";
                            }
                        }
                        _lst.Add(_Loan);

                    }
                }
                return _lst;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }


        }

        public List<LoanAmortiZd> CalculateLoanAmortiZation(string LoanPrincipleAmount, string AnnualInterestRate, string LoanPeriod, string OriginalRepaymentAmount, DateTime LoanStartDate)
        {
            try
            {
                List<LoanAmortiZd> _lst = new List<LoanAmortiZd>();

                //string LoanPrincipleAmount = "103,000.00 ";
                //string AnnualInterestRate = "9.50";
                string _Intrest = AnnualInterestRate;
                decimal InterestRate = Convert.ToDecimal(_Intrest);
                //string LoanPeriod = " 360.00";
                //string OriginalRepaymentAmount = "903.90";
                //string LoanStartDate = "1/15/1999";
                string RepaymentType = "End";  //"Beginning"; //"End";

                DateTime _Currntdate = DateTime.Now;
                decimal _LoanPeriod = Convert.ToDecimal(LoanPeriod.ToString());
                int LoanPeriod1 = Convert.ToInt32(_LoanPeriod);
                //int Type = 1;
                DateTime _loandate = Convert.ToDateTime(LoanStartDate.ToString());
                for (int i = 0; i < LoanPeriod1; i++)
                {
                    LoanAmortiZd _Loan = new LoanAmortiZd();
                    string _LoanPAmnt = LoanPrincipleAmount.Replace(",", "").Replace("$", ""); 
                    decimal _LoanPAmntt = Convert.ToDecimal(_LoanPAmnt.ToString());

                    string _month = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(_loandate.Month);
                    string _loanStartdate = _month + "-" + _loandate.Year;
                    _loandate = Convert.ToDateTime(_loandate.AddMonths(1));
                    decimal _InterestCharged = 0;
                    decimal _capitalRapid;
                    decimal _ClosingBalance;
                    decimal capitaloutstanding;
                    if (i == 0)
                    {
                        //double pmt = PMT(9.50, 360, 103000.00);
                        decimal pmt1;
                        int chkloanindex = (LoanPeriod1 + 1) - (i + 1);
                        if (chkloanindex == 0)
                        {
                            pmt1 = 0;
                        }
                        else
                        {
                            if (RepaymentType == "Beginning")
                            {
                                double pmt = PMT(Convert.ToDouble(InterestRate), chkloanindex, Convert.ToDouble(_LoanPAmntt), 1);

                                pmt1 = Decimal.Round(Convert.ToDecimal(pmt), 2);
                                _InterestCharged = 0;
                            }
                            else
                            {
                                double pmt = PMT(Convert.ToDouble(InterestRate), chkloanindex, Convert.ToDouble(_LoanPAmntt), 0);
                                pmt1 = Decimal.Round(Convert.ToDecimal(pmt), 2);
                                _InterestCharged = Decimal.Round(Convert.ToDecimal(_LoanPAmntt * (InterestRate / 100) / 12), 2); //(_LoanPAmntt * InterestRate / 12);
                            }

                        }
                        _capitalRapid = (pmt1 - _InterestCharged);

                        _ClosingBalance = (_LoanPAmntt - _capitalRapid);

                        capitaloutstanding = Math.Round((Convert.ToDecimal(_ClosingBalance) / Convert.ToDecimal(_LoanPAmntt)), 1);

                        _Loan.Month = _loanStartdate;
                        _Loan.RepaymentNumber = i + 1;
                        _Loan.OpeningBalance = _LoanPAmntt;
                        _Loan.OpeningBalanceNew = String.Format("{0:C}", (_LoanPAmntt));
                        _Loan.LoanRepayment = pmt1;
                        _Loan.LoanRepaymentNew = String.Format("{0:C}", (pmt1));
                        _Loan.InterestCharged = _InterestCharged;
                        _Loan.InterestChargedNew = String.Format("{0:C}", (_InterestCharged));
                        _Loan.CapitalRepaid = _capitalRapid;
                        _Loan.CapitalRepaidNew = String.Format("{0:C}", (_capitalRapid));
                        _Loan.ClosingBalance = _ClosingBalance;
                        _Loan.ClosingBalanceNew = String.Format("{0:C}", (_ClosingBalance));
                        _Loan.CapitalOutstanding = String.Format("{0:C}", (capitaloutstanding * 100)) + "%";
                        _Loan.InterestRate = InterestRate;
                        _Loan.InterestRateNew = String.Format("{0:C}", (InterestRate)) + "%";
                    }
                    else
                    {
                        decimal _LoanPAmntt1;
                        decimal pmt2;
                        decimal _InterestCharged1 = 0;
                        decimal _capitalRapid1;
                        decimal _ClosingBalance1;
                        decimal capitaloutstanding1;
                        decimal LoanRepay = Convert.ToDecimal(_lst[i - 1].ClosingBalance);
                        decimal _ChkloanRe = Decimal.Round(LoanRepay, 0);
                        if (_ChkloanRe > 0)
                        {
                            _LoanPAmntt1 = LoanRepay;
                        }
                        else
                        {
                            _LoanPAmntt1 = 0;
                        }

                        int chkLaonprd = LoanPeriod1 + 1 - (i + 1);
                        if (chkLaonprd == 0)
                        {
                            pmt2 = 0;
                        }
                        else
                        {
                            double pmt = PMT(Convert.ToDouble(InterestRate), chkLaonprd, Convert.ToDouble(_LoanPAmntt1), 0);
                            pmt2 = Decimal.Round(Convert.ToDecimal(pmt), 2, 0);

                        }
                        _InterestCharged1 = Decimal.Round(Convert.ToDecimal(_LoanPAmntt1 * (InterestRate / 100) / 12), 2);

                        _capitalRapid1 = (pmt2 - _InterestCharged1);

                        _ClosingBalance1 = (_LoanPAmntt1 - _capitalRapid1);

                        capitaloutstanding1 = (Convert.ToDecimal(_ClosingBalance1) / Convert.ToDecimal(_LoanPAmntt));

                        _Loan.Month = _loanStartdate;
                        _Loan.RepaymentNumber = i + 1;
                        _Loan.OpeningBalance = _LoanPAmntt1;
                        _Loan.OpeningBalanceNew = String.Format("{0:C}", (_LoanPAmntt1));
                        _Loan.LoanRepayment = pmt2;
                        _Loan.LoanRepaymentNew = String.Format("{0:C}", (pmt2));
                        _Loan.InterestCharged = _InterestCharged1;
                        _Loan.InterestChargedNew = String.Format("{0:C}", (_InterestCharged1));
                        _Loan.CapitalRepaid = _capitalRapid1;
                        _Loan.CapitalRepaidNew = String.Format("{0:C}", (_capitalRapid1));
                        _Loan.ClosingBalance = _ClosingBalance1;
                        _Loan.ClosingBalanceNew = String.Format("{0:C}", (_ClosingBalance1));
                        _Loan.CapitalOutstanding = String.Format("{0:C}", (capitaloutstanding1 * 100)) + "%";

                        _Loan.InterestRate = InterestRate;
                        _Loan.InterestRateNew = String.Format("{0:C}", (InterestRate)) + "%";

                    }
                    _lst.Add(_Loan);

                }
                //ExportListedToExcel(_lst);
                return _lst;
            }
            catch
            {
                return null;
            }
        }

        public List<LoanAmortiZd> GetAmortizationByID(int OTHAddressID)
        {
            try
            {
                List<LoanAmortiZd> _lst = new List<LoanAmortiZd>();
                OtherProperty _OthPropty = new OtherProperty();
                //int SubTabId = Convert.ToInt32(Session["SubTabId"].ToString() == null ? "1" : Session["SubTabId"].ToString());
                _OthPropty = _vmcase.OtherPropertiesDetails(OTHAddressID);
                int division = 0;
                if (_OthPropty.UseCodeCategoryID == 1)
                {
                    division = 10;
                }
                else if (_OthPropty.UseCodeCategoryID == 2)
                {
                    division = 30;
                }
                else
                {
                    division = 10;
                }
                string LoanPrincipleAmount = "";
                string Downpayment = "";
                decimal LaonAmount = Convert.ToDecimal(_OthPropty.SalePrice.Replace(",", "").Replace("$", ""));
                decimal DownLaonAmount = Convert.ToDecimal(LaonAmount * division / 100);

                LoanPrincipleAmount = String.Format("{0:C}", (LaonAmount - DownLaonAmount));
                Downpayment = String.Format("{0:C}", (DownLaonAmount));
                string LoanPeriod = "360.00";
                //string OriginalRepaymentAmount = "903.90";
                string LoanStartDate = _OthPropty.OthSaleDate.ToString(); // "1/15/1999";
                string RepaymentType = "End";  //"Beginning"; //"End";
                Session["LoanStartDate"] = LoanStartDate;
                Session["Downpayment"] = Downpayment;
                DateTime _Currntdate = DateTime.Now;
                decimal _LoanPeriod = Convert.ToDecimal(LoanPeriod.ToString());
                int LoanPeriod1 = Convert.ToInt32(_LoanPeriod);
                //int Type = 1;
                Session["LoanPrincipleAmount"] = LoanPrincipleAmount;
                Session["LoanStartDate"] = LoanStartDate;
                Session["LoanPeriod"] = LoanPeriod1;
                DateTime _loandate = Convert.ToDateTime(LoanStartDate.ToString());

                tblFixedRate _FixedRate = db.tblFixedRates.AsEnumerable().Where(x => Convert.ToInt32(x.RateYears) == _loandate.Year).FirstOrDefault();

                //int CntCheckBalance = 0;
                //string CurrentRate = "";

                if (_FixedRate == null)
                {

                }
                else
                {
                    

                    int loanMonth = _loandate.Month;

                    ArrayList ArrMonth = new ArrayList();

                    ArrMonth.Add(_FixedRate.RtJan);
                    ArrMonth.Add(_FixedRate.RtFeb);
                    ArrMonth.Add(_FixedRate.RtMarch);
                    ArrMonth.Add(_FixedRate.RtApril);
                    ArrMonth.Add(_FixedRate.RtMay);
                    ArrMonth.Add(_FixedRate.RtJune);
                    ArrMonth.Add(_FixedRate.RtJuly);
                    ArrMonth.Add(_FixedRate.RTAugst);
                    ArrMonth.Add(_FixedRate.RtSept);
                    ArrMonth.Add(_FixedRate.RtOct);
                    ArrMonth.Add(_FixedRate.RtNov);
                    ArrMonth.Add(_FixedRate.RtDec);

                    string Intrestrate = "";
                    string CurrentRate = "";
                    //"9.50";



                    for (int p = 0; p < 12; p++)
                    {
                        if (ArrMonth[p] == null)
                        {
                            Intrestrate = ArrMonth[p - 1].ToString();
                            CurrentRate = ArrMonth[p - 1].ToString();

                            Session["CurrentRate"] = CurrentRate;
                            break;
                        }
                        if ((loanMonth - 1) == p)
                        {
                            Intrestrate = ArrMonth[p].ToString() == "na" ? "10" : ArrMonth[p].ToString();
                            Session["CurrentRate"] = Intrestrate;
                            break;
                        }


                    }



                    for (int i = 0; i < LoanPeriod1; i++)
                    {
                        LoanAmortiZd _Loan = new LoanAmortiZd();
                        string _LoanPAmnt = LoanPrincipleAmount.Replace("$", "").Replace(",", "");
                        decimal _LoanPAmntt = Convert.ToDecimal(_LoanPAmnt.ToString());

                        string _month = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(_loandate.Month);
                        string _loanStartdate = _month + "-" + _loandate.Year;
                        _loandate = Convert.ToDateTime(_loandate.AddMonths(1));
                        decimal _InterestCharged = 0;

                        decimal InterestRate = Convert.ToDecimal(Intrestrate == "na" ? "10" : Intrestrate);

                        decimal _capitalRapid;
                        decimal _ClosingBalance;

                        decimal capitaloutstanding;

                        if (i == 0)
                        {
                            //double pmt = PMT(9.50, 360, 103000.00);
                            decimal pmt1;
                            int chkloanindex = (LoanPeriod1 + 1) - (i + 1);
                            if (chkloanindex == 0)
                            {
                                pmt1 = 0;
                            }
                            else
                            {
                                if (RepaymentType == "Beginning")
                                {
                                    double pmt = PMT(Convert.ToDouble(InterestRate), chkloanindex, Convert.ToDouble(_LoanPAmntt), 1);

                                    pmt1 = Decimal.Round(Convert.ToDecimal(pmt), 2);
                                    _InterestCharged = 0;
                                }
                                else
                                {
                                    double pmt = PMT(Convert.ToDouble(InterestRate), chkloanindex, Convert.ToDouble(_LoanPAmntt), 0);
                                    pmt1 = Decimal.Round(Convert.ToDecimal(pmt), 2);
                                    _InterestCharged = Decimal.Round(Convert.ToDecimal(_LoanPAmntt * (InterestRate / 100) / 12), 2); //(_LoanPAmntt * InterestRate / 12);
                                }

                            }
                            _capitalRapid = (pmt1 - _InterestCharged);

                            _ClosingBalance = (_LoanPAmntt - _capitalRapid);

                            capitaloutstanding = Math.Round((Convert.ToDecimal(_ClosingBalance) / Convert.ToDecimal(_LoanPAmntt)), 1);

                            _Loan.Month = _loanStartdate;
                            _Loan.RepaymentNumber = i + 1;
                            _Loan.OpeningBalance = _LoanPAmntt;
                            _Loan.OpeningBalanceNew = String.Format("{0:C}", (_LoanPAmntt));
                            _Loan.LoanRepayment = pmt1;
                            _Loan.LoanRepaymentNew = String.Format("{0:C}", (pmt1));
                            _Loan.InterestCharged = _InterestCharged;
                            _Loan.InterestChargedNew = String.Format("{0:C}", (_InterestCharged));
                            _Loan.CapitalRepaid = _capitalRapid;
                            _Loan.CapitalRepaidNew = String.Format("{0:C}", (_capitalRapid));
                            _Loan.ClosingBalance = _ClosingBalance;
                            _Loan.ClosingBalanceNew = String.Format("{0:C}", (_ClosingBalance));
                            _Loan.CapitalOutstanding = String.Format("{0:C}", (capitaloutstanding * 100)) + "%";
                            _Loan.InterestRate = InterestRate;
                            _Loan.InterestRateNew = String.Format("{0:C}", (InterestRate)) + "%";
                        }
                        else
                        {
                            decimal _LoanPAmntt1;
                            decimal pmt2;
                            decimal _InterestCharged1 = 0;
                            decimal _capitalRapid1;
                            decimal _ClosingBalance1;
                            decimal capitaloutstanding1;
                            decimal LoanRepay = Convert.ToDecimal(_lst[i - 1].ClosingBalance);
                            decimal _ChkloanRe = Decimal.Round(LoanRepay, 0);
                            if (_ChkloanRe > 1)
                            {
                                _LoanPAmntt1 = LoanRepay;
                            }
                            else
                            {
                                _LoanPAmntt1 = 0;
                                break;
                            }

                            int chkLaonprd = LoanPeriod1 + 1 - (i + 1);
                            if (chkLaonprd == 0)
                            {
                                pmt2 = 0;
                            }
                            else
                            {
                                double pmt = PMT(Convert.ToDouble(InterestRate), chkLaonprd, Convert.ToDouble(_LoanPAmntt1), 0);
                                pmt2 = Decimal.Round(Convert.ToDecimal(pmt), 2, 0);

                            }
                            _InterestCharged1 = Decimal.Round(Convert.ToDecimal(_LoanPAmntt1 * (InterestRate / 100) / 12), 2);

                            _capitalRapid1 = (pmt2 - _InterestCharged1);

                            _ClosingBalance1 = (_LoanPAmntt1 - _capitalRapid1);

                            capitaloutstanding1 = (Convert.ToDecimal(_ClosingBalance1) / Convert.ToDecimal(_LoanPAmntt1));

                            _Loan.Month = _loanStartdate;
                            _Loan.RepaymentNumber = i + 1;
                            _Loan.OpeningBalance = _LoanPAmntt1;
                            _Loan.OpeningBalanceNew = String.Format("{0:C}", (_LoanPAmntt1));
                            _Loan.LoanRepayment = pmt2;
                            _Loan.LoanRepaymentNew = String.Format("{0:C}", (pmt2));
                            _Loan.InterestCharged = _InterestCharged1;
                            _Loan.InterestChargedNew = String.Format("{0:C}", (_InterestCharged1));
                            _Loan.CapitalRepaid = _capitalRapid1;
                            _Loan.CapitalRepaidNew = String.Format("{0:C}", (_capitalRapid1));
                            _Loan.ClosingBalance = _ClosingBalance1;
                            _Loan.ClosingBalanceNew = String.Format("{0:C}", (_ClosingBalance1));
                            _Loan.CapitalOutstanding = String.Format("{0:C}", (capitaloutstanding1 * 100)) + "%";

                            _Loan.InterestRate = InterestRate;
                            _Loan.InterestRateNew = String.Format("{0:C}", (InterestRate)) + "%";

                            if (_Currntdate.Year == _loandate.Year && _Currntdate.Month == _loandate.Month)
                            {
                                //string tt = "";
                            }
                        }
                        _lst.Add(_Loan);

                    }

                   
                }
                return _lst;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }


        }

        //---------------------- Varied the intrest rate bY monthly base----------------------------------//
        public List<LoanAmortiZd> GetAmortizationByID1(int OTHAddressID)
        {
            try
            {
                List<LoanAmortiZd> _lst = new List<LoanAmortiZd>();
                OtherProperty _OthPropty = new OtherProperty();
                int SubTabId = Convert.ToInt32(Session["SubTabId"].ToString() == null ? "1" : Session["SubTabId"].ToString());
                _OthPropty = _vmcase.OtherPropertiesDetails(OTHAddressID);


                string LoanPrincipleAmount = _OthPropty.SalePrice;  // "103,000.00 ";

                string LoanPeriod = "360.00";
                //string OriginalRepaymentAmount = "903.90";
                string LoanStartDate = _OthPropty.OthSaleDate.ToString(); // "1/15/1999";
                string RepaymentType = "End";  //"Beginning"; //"End";
                Session["LoanStartDate"] = LoanStartDate;
                DateTime _Currntdate = DateTime.Now;
                decimal _LoanPeriod = Convert.ToDecimal(LoanPeriod.ToString());
                int LoanPeriod1 = Convert.ToInt32(_LoanPeriod);
                //int Type = 1;
                DateTime _loandate = Convert.ToDateTime(LoanStartDate.ToString());


                List<tblFixedRate> _FixedRate = new List<tblFixedRate>();

                _FixedRate = db.tblFixedRates.AsEnumerable().Where(x => Convert.ToInt32(x.RateYears) >= _loandate.Year).ToList();

                tblFixedRate test = db.tblFixedRates.AsEnumerable().OrderByDescending(o => o.RateYears).Take(1).FirstOrDefault();
                ArrayList ArrChk = new ArrayList();
                ArrChk.Add(test.RtJan);
                ArrChk.Add(test.RtFeb);
                ArrChk.Add(test.RtMarch);
                ArrChk.Add(test.RtApril);
                ArrChk.Add(test.RtMay);
                ArrChk.Add(test.RtJune);
                ArrChk.Add(test.RtJuly);
                ArrChk.Add(test.RTAugst);
                ArrChk.Add(test.RtSept);
                ArrChk.Add(test.RtOct);
                ArrChk.Add(test.RtNov);
                ArrChk.Add(test.RtDec);
                int ChkMonth = 0;
                foreach (var chk in ArrChk)
                {
                    if (chk == null)
                    {
                        break;
                    }
                    ChkMonth++;

                }

                //DateTime end = DateTime.Now;
                //var diffMonths = (1 + end.Year * 12) - (_loandate.Month + _loandate.Year * 12);

                //int LoanPeriod1 = (Convert.ToInt32(diffMonths) + ChkMonth + 120);

                DateTime end = DateTime.Now;
                var diffMonths = (1 + end.Year * 12) - (_loandate.Month + _loandate.Year * 12);
                int _TotalMonth = (Convert.ToInt32(diffMonths) + ChkMonth);
                int PendingMonth = (LoanPeriod1 - _TotalMonth);

                int i = 0;
                int CntCheckBalance = 0;
                string CurrentRate = "";

                Session["LoanPrincipleAmount"] = LoanPrincipleAmount;
                Session["LoanStartDate"] = LoanStartDate;
                Session["LoanPeriod"] = LoanPeriod1;


                foreach (var item in _FixedRate)
                {
                    int loanMonth = 1;
                    if (i == 0)
                    {
                        loanMonth = _loandate.Month;
                    }

                    if (CntCheckBalance == 1)
                    {
                        break;
                    }
                    //string AnnualInterestRate = "9.50";
                    ArrayList ArrMonth = new ArrayList();

                    ArrMonth.Add(item.RtJan);
                    ArrMonth.Add(item.RtFeb);
                    ArrMonth.Add(item.RtMarch);
                    ArrMonth.Add(item.RtApril);
                    ArrMonth.Add(item.RtMay);
                    ArrMonth.Add(item.RtJune);
                    ArrMonth.Add(item.RtJuly);
                    ArrMonth.Add(item.RTAugst);
                    ArrMonth.Add(item.RtSept);
                    ArrMonth.Add(item.RtOct);
                    ArrMonth.Add(item.RtNov);
                    ArrMonth.Add(item.RtDec);

                    for (int p = 0; p < 12; p++)
                    {

                        if (_lst.Count > 1)
                        {
                            decimal LoanClosingBalance = Convert.ToDecimal(_lst[i - 1].ClosingBalance);
                            if (LoanClosingBalance <= 0)
                            {
                                CntCheckBalance = 1;
                                break;
                            }
                        }

                        if (ArrMonth[p] == null)
                        {
                            CurrentRate = ArrMonth[p - 1].ToString();

                            Session["CurrentRate"] = CurrentRate;
                            GetAmortizationMore(_lst, i, CurrentRate, _loandate, LoanPeriod1, PendingMonth);
                            break;
                        }
                        if ((loanMonth - 1) <= p)
                        {
                            decimal InterestRate = Convert.ToDecimal(ArrMonth[p].ToString() == "na" ? "10" : ArrMonth[p]);

                            LoanAmortiZd _Loan = new LoanAmortiZd();
                            string _LoanPAmnt = LoanPrincipleAmount.Replace(",", "").Replace("$", "");
                            decimal _LoanPAmntt = Convert.ToDecimal(_LoanPAmnt.ToString());

                            string _month = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(_loandate.Month);
                            string _loanStartdate = _month + "-" + _loandate.Year;
                            _loandate = Convert.ToDateTime(_loandate.AddMonths(1));
                            decimal _InterestCharged = 0;
                            decimal _capitalRapid;
                            decimal _ClosingBalance;
                            decimal capitaloutstanding;
                            if (i == 0)
                            {
                                //double pmt = PMT(9.50, 360, 103000.00);
                                decimal pmt1;
                                int chkloanindex = (LoanPeriod1 + 1) - (i + 1);
                                if (chkloanindex == 0)
                                {
                                    pmt1 = 0;
                                }
                                else
                                {
                                    if (RepaymentType == "Beginning")
                                    {
                                        double pmt = PMT(Convert.ToDouble(InterestRate), chkloanindex, Convert.ToDouble(_LoanPAmntt), 1);

                                        pmt1 = Decimal.Round(Convert.ToDecimal(pmt), 2);
                                        _InterestCharged = 0;
                                    }
                                    else
                                    {
                                        double pmt = PMT(Convert.ToDouble(InterestRate), chkloanindex, Convert.ToDouble(_LoanPAmntt), 0);
                                        pmt1 = Decimal.Round(Convert.ToDecimal(pmt), 2);
                                        _InterestCharged = Decimal.Round(Convert.ToDecimal(_LoanPAmntt * (InterestRate / 100) / 12), 2); //(_LoanPAmntt * InterestRate / 12);
                                    }

                                }
                                _capitalRapid = (pmt1 - _InterestCharged);

                                _ClosingBalance = (_LoanPAmntt - _capitalRapid);

                                capitaloutstanding = Math.Round((Convert.ToDecimal(_ClosingBalance) / Convert.ToDecimal(_LoanPAmntt)), 1);

                                _Loan.Month = _loanStartdate;
                                _Loan.RepaymentNumber = i + 1;
                                _Loan.OpeningBalance = _LoanPAmntt;
                                _Loan.LoanRepayment = pmt1;
                                _Loan.InterestCharged = _InterestCharged;
                                _Loan.CapitalRepaid = _capitalRapid;
                                _Loan.ClosingBalance = _ClosingBalance;
                                _Loan.CapitalOutstanding = (capitaloutstanding * 100) + "%";
                                _Loan.InterestRate = InterestRate;
                            }
                            else
                            {
                                decimal _LoanPAmntt1;
                                decimal pmt2;
                                decimal _InterestCharged1 = 0;
                                decimal _capitalRapid1;
                                decimal _ClosingBalance1;
                                decimal capitaloutstanding1;
                                decimal LoanRepay = Convert.ToDecimal(_lst[i - 1].ClosingBalance);
                                decimal _ChkloanRe = Decimal.Round(LoanRepay, 0);
                                if (_ChkloanRe > 0)
                                {
                                    _LoanPAmntt1 = LoanRepay;
                                }
                                else
                                {
                                    _LoanPAmntt1 = 0;
                                }

                                int chkLaonprd = LoanPeriod1 + 1 - (i + 1);
                                if (chkLaonprd == 0)
                                {
                                    pmt2 = 0;
                                }
                                else
                                {
                                    double pmt = PMT(Convert.ToDouble(InterestRate), chkLaonprd, Convert.ToDouble(_LoanPAmntt1), 0);
                                    pmt2 = Decimal.Round(Convert.ToDecimal(pmt), 2, 0);

                                }
                                _InterestCharged1 = Decimal.Round(Convert.ToDecimal(_LoanPAmntt1 * (InterestRate / 100) / 12), 2);

                                _capitalRapid1 = (pmt2 - _InterestCharged1);

                                _ClosingBalance1 = (_LoanPAmntt1 - _capitalRapid1);

                                capitaloutstanding1 = (Convert.ToDecimal(_ClosingBalance1) / Convert.ToDecimal(_LoanPAmntt1));

                                _Loan.Month = _loanStartdate;
                                _Loan.RepaymentNumber = i + 1;
                                _Loan.OpeningBalance = _LoanPAmntt1;
                                _Loan.LoanRepayment = pmt2;
                                _Loan.InterestCharged = _InterestCharged1;
                                _Loan.CapitalRepaid = _capitalRapid1;
                                _Loan.ClosingBalance = _ClosingBalance1;
                                _Loan.CapitalOutstanding = String.Format("{0:0.0}", (capitaloutstanding1 * 100)) + "%";
                                _Loan.InterestRate = InterestRate;
                                if (_Currntdate.Year == _loandate.Year && _Currntdate.Month == _loandate.Month)
                                {
                                    //string tt = "";
                                }
                            }
                            _lst.Add(_Loan);
                            i++;
                        }
                    }
                }

                return _lst;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }


        }

        public List<LoanAmortiZd> GetAmortizationMore(List<LoanAmortiZd> _lst, int i, string CurrentRate, DateTime _loandate, int LoanPeriod1, int PendingMonth)
        {
            try
            {
                decimal InterestRate = Convert.ToDecimal(CurrentRate == "na" ? "10" : CurrentRate);

                for (int p = 1; p <= PendingMonth; p++)
                {
                    LoanAmortiZd _Loan = new LoanAmortiZd();

                    string _month = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(_loandate.Month);
                    string _loanStartdate = _month + "-" + _loandate.Year;
                    _loandate = Convert.ToDateTime(_loandate.AddMonths(1));

                    decimal _LoanPAmntt1;
                    decimal pmt2;
                    decimal _InterestCharged1 = 0;
                    decimal _capitalRapid1;
                    decimal _ClosingBalance1;
                    decimal capitaloutstanding1;
                    decimal LoanRepay = Convert.ToDecimal(_lst[i - 1].ClosingBalance);
                    decimal _ChkloanRe = Decimal.Round(LoanRepay, 0);
                    if (_ChkloanRe > 0)
                    {
                        _LoanPAmntt1 = LoanRepay;
                    }
                    else
                    {
                        _LoanPAmntt1 = 0;
                    }

                    int chkLaonprd = LoanPeriod1 + 1 - (i + 1);
                    if (chkLaonprd == 0)
                    {
                        pmt2 = 0;
                    }
                    else
                    {
                        double pmt = PMT(Convert.ToDouble(InterestRate), chkLaonprd, Convert.ToDouble(_LoanPAmntt1), 0);
                        pmt2 = Decimal.Round(Convert.ToDecimal(pmt), 2, 0);

                    }
                    _InterestCharged1 = Decimal.Round(Convert.ToDecimal(_LoanPAmntt1 * (InterestRate / 100) / 12), 2);

                    _capitalRapid1 = (pmt2 - _InterestCharged1);

                    _ClosingBalance1 = (_LoanPAmntt1 - _capitalRapid1);

                    capitaloutstanding1 = (Convert.ToDecimal(_ClosingBalance1) / Convert.ToDecimal(_LoanPAmntt1));

                    _Loan.Month = _loanStartdate;
                    _Loan.RepaymentNumber = i + 1;
                    _Loan.OpeningBalance = _LoanPAmntt1;
                    _Loan.LoanRepayment = pmt2;
                    _Loan.InterestCharged = _InterestCharged1;
                    _Loan.CapitalRepaid = _capitalRapid1;
                    _Loan.ClosingBalance = _ClosingBalance1;
                    _Loan.CapitalOutstanding = String.Format("{0:0.0}", (capitaloutstanding1 * 100)) + "%";
                    _Loan.InterestRate = InterestRate;
                    _lst.Add(_Loan);
                    i++;
                }
                return _lst;
            }

            catch
            {
                return _lst;
            }
        }

        public ActionResult GetLoanAmortiZation(string LoanPrincipleAmount, string AnnualInterestRate, string LoanPeriod, string OriginalRepaymentAmount, DateTime LoanStartDate)
        {
             if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
            List<LoanAmortiZd> _LoanAmortiZation = CalculateLoanAmortiZation(LoanPrincipleAmount, AnnualInterestRate, LoanPeriod, OriginalRepaymentAmount, LoanStartDate);
            return PartialView("_LoanAmortization", _LoanAmortiZation);
            }
             return RedirectToAction("Login", "Home");
        }

        public List<LoanAmortiZd> testloan()
        {

            try
            {
                string LoanPrincipleAmount = "103,000.00 ";
                //string AnnualInterestRate = "9.50";
                string _Intrest = "9.50";
                decimal InterestRate = Convert.ToDecimal(_Intrest);
                string LoanPeriod = " 360.00";
                //string OriginalRepaymentAmount = "903.90";
                string LoanStartDate = "1/15/1999";
                string RepaymentType = "End";  //"Beginning"; //"End";

                List<LoanAmortiZd> _lst = new List<LoanAmortiZd>();
                DateTime _Currntdate = DateTime.Now;
                decimal _LoanPeriod = Convert.ToDecimal(LoanPeriod.ToString());
                int LoanPeriod1 = Convert.ToInt32(_LoanPeriod);
                //int Type = 1;
                DateTime _loandate = Convert.ToDateTime(LoanStartDate.ToString());

                for (int i = 0; i < LoanPeriod1; i++)
                {
                    LoanAmortiZd _Loan = new LoanAmortiZd();
                    string _LoanPAmnt = LoanPrincipleAmount.Replace(",", "");
                    decimal _LoanPAmntt = Convert.ToDecimal(_LoanPAmnt.ToString());

                    string _month = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(_loandate.Month);
                    string _loanStartdate = _month + "-" + _loandate.Year;
                    _loandate = Convert.ToDateTime(_loandate.AddMonths(1));
                    decimal _InterestCharged = 0;
                    decimal _capitalRapid;
                    decimal _ClosingBalance;
                    decimal capitaloutstanding;
                    if (i == 0)
                    {
                        //double pmt = PMT(9.50, 360, 103000.00);
                        decimal pmt1;
                        int chkloanindex = (LoanPeriod1 + 1) - (i + 1);
                        if (chkloanindex == 0)
                        {
                            pmt1 = 0;
                        }
                        else
                        {
                            if (RepaymentType == "Beginning")
                            {
                                double pmt = PMT(Convert.ToDouble(InterestRate), chkloanindex, Convert.ToDouble(_LoanPAmntt), 1);

                                pmt1 = Decimal.Round(Convert.ToDecimal(pmt), 2);
                                _InterestCharged = 0;
                            }
                            else
                            {
                                double pmt = PMT(Convert.ToDouble(InterestRate), chkloanindex, Convert.ToDouble(_LoanPAmntt), 0);
                                pmt1 = Decimal.Round(Convert.ToDecimal(pmt), 2);
                                _InterestCharged = Decimal.Round(Convert.ToDecimal(_LoanPAmntt * (InterestRate / 100) / 12), 2); //(_LoanPAmntt * InterestRate / 12);
                            }

                        }
                        _capitalRapid = (pmt1 - _InterestCharged);

                        _ClosingBalance = (_LoanPAmntt - _capitalRapid);

                        capitaloutstanding = Math.Round((Convert.ToDecimal(_ClosingBalance) / Convert.ToDecimal(_LoanPAmntt)), 1);

                        _Loan.Month = _loanStartdate;
                        _Loan.RepaymentNumber = i + 1;
                        _Loan.OpeningBalance = _LoanPAmntt;
                        _Loan.LoanRepayment = pmt1;
                        _Loan.InterestCharged = _InterestCharged;
                        _Loan.CapitalRepaid = _capitalRapid;
                        _Loan.ClosingBalance = _ClosingBalance;
                        _Loan.CapitalOutstanding = (capitaloutstanding * 100) + "%";
                        _Loan.InterestRate = InterestRate;
                    }
                    else
                    {
                        decimal _LoanPAmntt1;
                        decimal pmt2;
                        decimal _InterestCharged1 = 0;
                        decimal _capitalRapid1;
                        decimal _ClosingBalance1;
                        decimal capitaloutstanding1;
                        decimal LoanRepay = Convert.ToDecimal(_lst[i - 1].ClosingBalance);
                        decimal _ChkloanRe = Decimal.Round(LoanRepay, 0);
                        if (_ChkloanRe > 0)
                        {
                            _LoanPAmntt1 = LoanRepay;
                        }
                        else
                        {
                            _LoanPAmntt1 = 0;
                        }

                        int chkLaonprd = LoanPeriod1 + 1 - (i + 1);
                        if (chkLaonprd == 0)
                        {
                            pmt2 = 0;
                        }
                        else
                        {
                            double pmt = PMT(Convert.ToDouble(InterestRate), chkLaonprd, Convert.ToDouble(_LoanPAmntt1), 0);
                            pmt2 = Decimal.Round(Convert.ToDecimal(pmt), 2, 0);

                        }
                        _InterestCharged1 = Decimal.Round(Convert.ToDecimal(_LoanPAmntt1 * (InterestRate / 100) / 12), 2);

                        _capitalRapid1 = (pmt2 - _InterestCharged1);

                        _ClosingBalance1 = (_LoanPAmntt1 - _capitalRapid1);

                        capitaloutstanding1 = (Convert.ToDecimal(_ClosingBalance1) / Convert.ToDecimal(_LoanPAmntt));

                        _Loan.Month = _loanStartdate;
                        _Loan.RepaymentNumber = i + 1;
                        _Loan.OpeningBalance = _LoanPAmntt1;
                        _Loan.LoanRepayment = pmt2;
                        _Loan.InterestCharged = _InterestCharged1;
                        _Loan.CapitalRepaid = _capitalRapid1;
                        _Loan.ClosingBalance = _ClosingBalance1;
                        _Loan.CapitalOutstanding = String.Format("{0:0.0}", (capitaloutstanding1 * 100)) + "%";
                        _Loan.InterestRate = InterestRate;
                        if (_Currntdate.Year == _loandate.Year && _Currntdate.Month == _loandate.Month)
                        {
                            //string tt = "";
                        }
                    }
                    _lst.Add(_Loan);

                }
                //ExportListedToExcel(_lst);
                return _lst;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        private void ExportListedToExcel(List<LoanAmortiZd> _lst)
        {
            try
            {
                //Creae an Excel application instance
                Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();

                Microsoft.Office.Interop.Excel._Workbook workbook = excelApp.Workbooks.Add(Type.Missing);
                Microsoft.Office.Interop.Excel._Worksheet excelWorkSheet = null;

                excelWorkSheet = workbook.ActiveSheet;
                excelWorkSheet.Name = "LoanAmortization";

                excelWorkSheet.Cells[1, 1] = "Month";
                excelWorkSheet.Cells[1, 2] = "RepaymentNumber";
                excelWorkSheet.Cells[1, 3] = "OpeningBalance";
                excelWorkSheet.Cells[1, 4] = "LoanRepayment";
                excelWorkSheet.Cells[1, 5] = "InterestCharged";
                excelWorkSheet.Cells[1, 6] = "CapitalRepaid";
                excelWorkSheet.Cells[1, 7] = "ClosingBalance";
                excelWorkSheet.Cells[1, 8] = "InterestRate";


                for (int j = 0; j < _lst.Count; j++)
                {
                    for (int k = 0; k < 8; k++)
                    {
                        switch (k)
                        {
                            case 0:
                                excelWorkSheet.Cells[j + 2, k + 1] = _lst[j].Month; ;
                                break;
                            case 1:
                                excelWorkSheet.Cells[j + 2, k + 1] = _lst[j].RepaymentNumber; ;
                                break;
                            case 2:
                                excelWorkSheet.Cells[j + 2, k + 1] = _lst[j].OpeningBalance; ;
                                break;
                            case 3:
                                excelWorkSheet.Cells[j + 2, k + 1] = _lst[j].LoanRepayment; ;
                                break;
                            case 4:
                                excelWorkSheet.Cells[j + 2, k + 1] = _lst[j].InterestCharged; ;
                                break;
                            case 5:
                                excelWorkSheet.Cells[j + 2, k + 1] = _lst[j].CapitalRepaid; ;
                                break;
                            case 6:
                                excelWorkSheet.Cells[j + 2, k + 1] = _lst[j].ClosingBalance; ;
                                break;
                            case 7:
                                excelWorkSheet.Cells[j + 2, k + 1] = _lst[j].InterestRate; ;
                                break;
                        }
                    }


                }

                String FilePath = Server.MapPath("~/TransCapitalRecords.ods");
                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.ClearContent();
                response.Clear();
                response.ContentType = "application/vnd.ms-excel";
                response.AddHeader("Content-Disposition", "attachment; filename=MyClaimsReport-" + DateTime.Now.ToShortDateString() + ".xls;");
                response.TransmitFile(FilePath);
                response.Flush();
                response.Close();

                workbook.Save();
                workbook.Close();
                excelApp.Quit();
            }
            catch (Exception Ex)
            {
                string _msg = Ex.Message;
                //string ext = "";
            }

        }

        public static double PMT(double yearlyInterestRate, int totalNumberOfMonths, double loanAmount, int Type)
        {
            //IF($D$5+1-C12=0,0,IF($D$8="Beginning",PMT(K12/12,$D$5+1-C12,-$D12,0,1),PMT(K12/12,$D$5+1-C12,-$D12,0,0)))
            var rate = (double)yearlyInterestRate / 100 / 12;
            var denominator = Math.Pow((1 + rate), totalNumberOfMonths) - 1;
            return (rate + (rate / denominator)) * loanAmount;
        }

       
        #endregion

        #region User
        public ActionResult Login()
        {
            var login = CheckLoginCookie();
            VMUser loginUser = new VMUser();
            if (login != null)
            {
                loginUser = loginUser.LoginUser(login.UserEmail, login.UserPassword);
                if (loginUser != null)
                {
                    System.Web.HttpContext.Current.Session["UserId"] = loginUser.UserId;
                    System.Web.HttpContext.Current.Session["UserEmail"] = loginUser.UserEmail;
                    System.Web.HttpContext.Current.Session["UserName"] = loginUser.UserName;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.Message = "Invalid Email / Password";
                }

            }
           
            return View(loginUser);
        }


        [HttpPost]
        public ActionResult Login(VMUser user, System.Web.Mvc.FormCollection collection)
        {
            VMUser loginUser = new VMUser();

            if (ModelState.IsValid)
            {
                var remember = collection["remember"];
                if (remember != null)
                {
                    CreateCookies(user.UserEmail, user.UserPassword);
                }
               
               

                if (user != null)
                {
                    loginUser = user.LoginUser(user.UserEmail, user.UserPassword);
                }
                if (loginUser != null)
                {
                    System.Web.HttpContext.Current.Session["UserId"] = loginUser.UserId;
                    System.Web.HttpContext.Current.Session["UserEmail"] = loginUser.UserEmail;
                    System.Web.HttpContext.Current.Session["UserName"] = loginUser.UserName;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.Message = "Invalid Email / Password";
                }
            }
            return View();
        }



        private void CreateCookies(string userName, string userPassword)
        {
            var myCookie = HttpContext.Request.Cookies["MyApp"] ?? new HttpCookie("MyApp");
            myCookie.Values["UserName"] = userName;
            myCookie.Values["UserPassword"] = userPassword;
            myCookie.Values["LastVisit"] = DateTime.Now.ToString(CultureInfo.InvariantCulture);
            myCookie.Expires = DateTime.Now.AddDays(365);
            HttpContext.Response.Cookies.Add(myCookie);
        }

        private VMUser CheckLoginCookie()
        {
            var httpCookie = HttpContext.Request.Cookies["MyApp"];
            if (httpCookie != null)
            {
                var username = httpCookie.Values["UserName"];
                var password = httpCookie.Values["UserPassword"];
                if (username!=null && password!=null)
                {
                    var login = new VMUser
                    {
                        UserEmail = username.ToString(),
                        UserPassword = password.ToString()
                    };

                    return login;
                }
            }
            return null;
        }

        public ActionResult Logout()
        {
            try
            {

                System.Web.HttpContext.Current.Session["UserId"] = null;
                System.Web.HttpContext.Current.Session["UserEmail"] = null;
                System.Web.HttpContext.Current.Session["UserName"] = null;
              
                Session.Abandon();
                return RedirectToAction("Login", "Home");
            }
            catch (Exception ex)
            {
                string _msg = ex.Message;
                return RedirectToAction("Login", "Home");
            }
        }
        #endregion

        #region Test Zone
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Test2()
        {
            //ComparePropertyMSAccess _CompPrMSAccess = new ComparePropertyMSAccess();
            //DataSet model = _CompPrMSAccess.GetCopareableProperty();


            return View();

        }
        public ActionResult Test211()
        {

            //string _url = "http://www.bcpa.net/RecInfo.asp?URL_Folio=514206081320";
            string _url = "http://www.bcpa.net/RecAddr.asp";
            int radioVal = 2;
            string Address = "3860 N 51 AVENUE, HOLLYWOOD";
            browser = new IEBrowser(true, resultEvent, _url, radioVal, Address);

            Thread.Sleep(300);

            string html = browser.HtmlResult;

            string LandFactor = "";
            string AdvBldgSF = "";
            string UnitsBEDBath = "";
            string _LegalDesc = "";
            string _FolioID = "";
            string _Milage = "";
            string _use = "";
            string _SiteAddress = "";
            string _PropertOwner = "";
            string _mailingAddress = "";
            string FolioSiteLink = "";
            //bool Compareable = true;
            //-----------------------Legl description --------------------------

            string _html = @"<html><body>" + html.ToString() + "</body></html>";
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(_html);

            System.Data.DataTable _dt = new System.Data.DataTable();

            _dt.Columns.Add("Year", typeof(int));
            _dt.Columns.Add("Land", typeof(String));
            _dt.Columns.Add("Building", typeof(String));
            _dt.Columns.Add("MarketValue", typeof(String));
            _dt.Columns.Add("AssessedValue", typeof(String));
            _dt.Columns.Add("AssessedTax", typeof(String));

            for (int k = 3; k <= 5; k++)
            {
                DataRow dr = _dt.NewRow();
                int _countColumn = 1;
                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[5]/tbody/tr[" + k + "]/td"))
                {
                    var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    switch (_countColumn)
                    {
                        case 1:
                            dr["Year"] = _text;
                            break;
                        case 2:
                            dr["Land"] = _text;
                            break;
                        case 3:
                            dr["Building"] = _text;
                            break;
                        case 4:
                            dr["MarketValue"] = _text;
                            break;
                        case 5:
                            dr["AssessedValue"] = _text;
                            break;
                        case 6:
                            dr["AssessedTax"] = _text;
                            break;
                    }

                    if (_countColumn >= 6)
                    {
                        _dt.Rows.Add(dr);
                        _countColumn = 0;
                    }
                    else
                        _countColumn++;
                }
            }

            //-----------------------Legl description --------------------------

            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[3]/tbody/tr[1]/td[2]"))
            {
                _LegalDesc = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                break;
            }
            //-----------------------FolioId --------------------------

            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[1]/tbody/tr/td[3]/table/tbody/tr[1]/td[2]"))
            {
                _FolioID = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                break;
            }
            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[1]/tbody/tr/td[3]/table/tbody/tr[2]/td[2]"))
            {
                _Milage = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                break;
            }
            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[1]/tbody/tr/td[3]/table/tbody/tr[3]/td[2]"))
            {
                _use = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                break;
            }

            //-----------------------Property details Address --------------------------

            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[1]/tbody/tr/td[1]/table/tbody/tr[1]/td[2]"))
            {
                string _SiteFilter = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                _SiteAddress = _SiteFilter.Split('\r')[0];
                break;
            }
            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[1]/tbody/tr/td[1]/table/tbody/tr[2]/td[2]"))
            {
                _PropertOwner = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                break;
            }
            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[1]/tbody/tr/td[1]/table/tbody/tr[3]/td[2]"))
            {
                _mailingAddress = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                break;
            }
            //-----------------------Land factors --------------------------

            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[9]/tbody/tr/td[2]/table/tbody/tr[3]/td[2]"))
            {
                LandFactor = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                break;
            }
            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[9]/tbody/tr/td[2]/table/tbody/tr[7]/td[2]"))
            {
                AdvBldgSF = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                break;
            }
            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[9]/tbody/tr/td[2]/table/tbody/tr[8]/td[2]"))
            {
                UnitsBEDBath = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                break;
            }
            //--------------Insert InTo database-----------------

            FolioSiteLink = _url;

            //string Result = _vmcase.InsertAssessedDetails(null, null, _SiteAddress, _PropertOwner, _mailingAddress, _FolioID, _Milage, _use, _LegalDesc, LandFactor, AdvBldgSF, UnitsBEDBath, FolioSiteLink, _dt, Compareable);

            return View();
        }





        public void testfolio1()
        {
            FirefoxProfile ffprofile = new FirefoxProfile();
            IWebDriver driver = new FirefoxDriver(ffprofile);
            try
            {
                driver.Manage().Window.Maximize();

                driver.Navigate().GoToUrl("http://www.bcpa.net/RecInfo.asp?URL_Folio=514124120801");

                Thread.Sleep(1000);

                //string Result = "";

                By xPathdetails = By.XPath("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[1]/tbody/tr/td[1]/table/tbody");
                By xPathdetailsval = By.XPath("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[1]/tbody/tr/td[3]/table/tbody");
                By xPathdLegalDesc = By.XPath("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[3]/tbody");
                By xPathEvents = By.XPath("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[9]/tbody/tr/td[2]/table/tbody");
                //By xPathEvents1 = By.XPath("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[5]/tbody");
                By xPathdYearly = By.XPath("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[5]/tbody");
                By xPathsaleHistory = By.XPath("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[9]/tbody/tr/td[1]/table/tbody");
                string LandFactor = "";
                string AdvBldgSF = "";
                string UnitsBEDBath = "";
                string _LegalDesc = "";
                string _FolioID = "";
                string _Milage = "";
                string _use = "";
                string _SiteAddress = "";
                string _PropertOwner = "";
                string _mailingAddress = "";
                string FolioSiteLink = "";
                //-----------------------Legl description --------------------------

                IWebElement EventsDocumentYearly = driver.FindElement(xPathdYearly);
                var innerHtmlYearly = EventsDocumentYearly.GetAttribute("innerHTML");

                string htmlYearly = @"<html><body>" + innerHtmlYearly.ToString() + "</body></html>";
                HtmlAgilityPack.HtmlDocument _docYearly = new HtmlAgilityPack.HtmlDocument();
                _docYearly.LoadHtml(htmlYearly);

                System.Data.DataTable _dt = new System.Data.DataTable();

                _dt.Columns.Add("Year", typeof(int));
                _dt.Columns.Add("Land", typeof(String));
                _dt.Columns.Add("Building", typeof(String));
                _dt.Columns.Add("MarketValue", typeof(String));
                _dt.Columns.Add("AssessedValue", typeof(String));
                _dt.Columns.Add("AssessedTax", typeof(String));

                for (int k = 3; k <= 5; k++)
                {
                    DataRow dr = _dt.NewRow();
                    int _countColumn = 1;
                    foreach (HtmlNode node in _docYearly.DocumentNode.SelectNodes("//tr[" + k + "]/td"))
                    {
                        var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                        switch (_countColumn)
                        {
                            case 1:
                                dr["Year"] = _text;
                                break;
                            case 2:
                                dr["Land"] = _text;
                                break;
                            case 3:
                                dr["Building"] = _text;
                                break;
                            case 4:
                                dr["MarketValue"] = _text;
                                break;
                            case 5:
                                dr["AssessedValue"] = _text;
                                break;
                            case 6:
                                dr["AssessedTax"] = _text;
                                break;
                        }

                        if (_countColumn >= 6)
                        {
                            _dt.Rows.Add(dr);
                            _countColumn = 0;
                        }
                        else
                            _countColumn++;
                    }
                }
                //====----------------SaleHistory--------------===//
                IWebElement EventsDocumentsaleHistory = driver.FindElement(xPathsaleHistory);
                var innerHtmlsaleHistory = EventsDocumentsaleHistory.GetAttribute("innerHTML");

                string htmlsaleHistory = @"<html><body>" + innerHtmlsaleHistory.ToString() + "</body></html>";
                HtmlAgilityPack.HtmlDocument _docsaleHistory = new HtmlAgilityPack.HtmlDocument();
                _docsaleHistory.LoadHtml(htmlsaleHistory);

                System.Data.DataTable _dtSaleHistory = new System.Data.DataTable();

                _dtSaleHistory.Columns.Add("SaleDate", typeof(String));
                _dtSaleHistory.Columns.Add("SaleType", typeof(String));
                _dtSaleHistory.Columns.Add("SalePrice", typeof(String));
                _dtSaleHistory.Columns.Add("SaleBookPage", typeof(String));

                for (int p = 3; p <= 5; p++)
                {
                    DataRow drSaleHistory = _dtSaleHistory.NewRow();
                    int _cntsaleColumn = 1;


                    foreach (HtmlNode node in _docsaleHistory.DocumentNode.SelectNodes("//tr[" + p + "]/td"))
                    {
                        var _text = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                        switch (_cntsaleColumn)
                        {
                            case 1:
                                drSaleHistory["SaleDate"] = _text;
                                break;
                            case 2:
                                drSaleHistory["SaleType"] = _text;
                                break;
                            case 3:
                                drSaleHistory["SalePrice"] = _text;
                                break;
                            case 4:
                                drSaleHistory["SaleBookPage"] = _text;
                                break;

                        }
                        if (_cntsaleColumn >= 4)
                        {
                            _dtSaleHistory.Rows.Add(drSaleHistory);
                            _cntsaleColumn = 0;
                        }
                        else
                            _cntsaleColumn++;
                    }
                }

                //-----------------------Legl description --------------------------
                IWebElement EventsDocumentLegal = driver.FindElement(xPathdLegalDesc);
                var innerHtmlLegal = EventsDocumentLegal.GetAttribute("innerHTML");

                string htmlLegal = @"<html><body>" + innerHtmlLegal.ToString() + "</body></html>";
                HtmlAgilityPack.HtmlDocument _doclegal = new HtmlAgilityPack.HtmlDocument();
                _doclegal.LoadHtml(htmlLegal);


                foreach (HtmlNode node in _doclegal.DocumentNode.SelectNodes("//tr[1]/td[2]"))
                {
                    _LegalDesc = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                }
                //-----------------------FolioId --------------------------
                IWebElement EventsDocumentID = driver.FindElement(xPathdetailsval);
                var innerHtmlID = EventsDocumentID.GetAttribute("innerHTML");

                string htmlID = @"<html><body>" + innerHtmlID.ToString() + "</body></html>";
                HtmlAgilityPack.HtmlDocument _docID = new HtmlAgilityPack.HtmlDocument();
                _docID.LoadHtml(htmlID);


                foreach (HtmlNode node in _docID.DocumentNode.SelectNodes("//tr[1]/td[2]"))
                {
                    _FolioID = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                }
                foreach (HtmlNode node in _docID.DocumentNode.SelectNodes("//tr[2]/td[2]"))
                {
                    _Milage = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                }
                foreach (HtmlNode node in _docID.DocumentNode.SelectNodes("//tr[3]/td[2]"))
                {
                    _use = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                }

                //-----------------------Property details Address --------------------------
                IWebElement EventsDocumentDetails = driver.FindElement(xPathdetails);
                var innerHtmlDetials = EventsDocumentDetails.GetAttribute("innerHTML");

                string htmldetails = @"<html><body>" + innerHtmlDetials.ToString() + "</body></html>";
                HtmlAgilityPack.HtmlDocument _docDetails = new HtmlAgilityPack.HtmlDocument();
                _docDetails.LoadHtml(htmldetails);


                foreach (HtmlNode node in _docDetails.DocumentNode.SelectNodes("//tr[1]/td[2]"))
                {
                    string _SiteFilter = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                    _SiteAddress = _SiteFilter.Split('\r')[0];

                }
                foreach (HtmlNode node in _docDetails.DocumentNode.SelectNodes("//tr[2]/td[2]"))
                {
                    _PropertOwner = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();

                }
                foreach (HtmlNode node in _docDetails.DocumentNode.SelectNodes("//tr[3]/td[2]"))
                {
                    _mailingAddress = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();

                }
                //-----------------------Land factors --------------------------
                IWebElement EventsDocument = driver.FindElement(xPathEvents);
                var innerHtml = EventsDocument.GetAttribute("innerHTML");

                string html = @"<html><body>" + innerHtml.ToString() + "</body></html>";
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(html);

                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//tr[3]/td[2]"))
                {
                    LandFactor = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();

                }

                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//tr[7]/td[2]"))
                {
                    AdvBldgSF = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();

                }
                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//tr[8]/td[2]"))
                {
                    UnitsBEDBath = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();

                }
                //--------------Insert InTo database-----------------

                FolioSiteLink = driver.Url;

                //Result = _vmcase.InsertOtherPropertyDetails(76, _SiteAddress, _PropertOwner, _mailingAddress, _FolioID, _Milage, _use, _LegalDesc, LandFactor, AdvBldgSF, UnitsBEDBath, FolioSiteLink, _dt, Compareable, _dtSaleHistory);

            }
            catch (Exception ex)
            {
                string _msg = ex.Message;
            }
        }

        public ActionResult Test21()
        {
            string LandFactor;
            string AdvBldgSF;
            string UnitsBEDBath;
            var binary = new FirefoxBinary(Server.MapPath("~/firefox.exe"));
            string path = Server.MapPath("~/firefox.exe");
            //bool Compareable = true;
            FirefoxProfile ffprofile = new FirefoxProfile();
            IWebDriver driver = new FirefoxDriver(ffprofile);
            driver.Manage().Window.Maximize();

            driver.Navigate().GoToUrl("http://www.bcpa.net/RecInfo.asp?URL_Folio=514124120801");
            Thread.Sleep(1000);


            By xPathEvents = By.XPath("//table[2]/tbody/tr/td[1]/table/tbody/tr/td[1]/table[9]/tbody/tr/td[2]/table/tbody");

            IWebElement EventsDocument = driver.FindElement(xPathEvents);
            var innerHtml = EventsDocument.GetAttribute("innerHTML");

            string html = @"<html><body>" + innerHtml.ToString() + "</body></html>";
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);
            var nodes = doc.DocumentNode.SelectNodes("//tr");
            if (nodes != null)
            {
                int totalRow = nodes.Count;

                for (int k = 1; k <= totalRow; k++)
                {
                    switch (k)
                    {
                        case 3:
                            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//tr[3]/td[2]"))
                            {
                                LandFactor = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                                break;
                            }
                            break;
                        case 7:
                            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//tr[7]/td[2]"))
                            {
                                AdvBldgSF = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                                break;
                            }
                            break;
                        case 8:
                            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//tr[8]/td[2]"))
                            {
                                UnitsBEDBath = Regex.Replace(node.InnerText, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&amp;|&gt;|&lt;", " ").Trim();
                                break;
                            }
                            break;
                    }
                }
            }








            //var txtFilePath = "C:/PDfFiles/Test/html.txt";
            //string innerHtml1 = System.IO.File.Exists(txtFilePath) ? System.IO.File.ReadAllText(txtFilePath) : string.Empty;
            // string html = @"<html><body>" + innerHtml1.ToString() + "</body></html>";
            //    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            //    doc.LoadHtml(html);
            //    var nodes = doc.DocumentNode.SelectNodes("//td[2]/b[1]");
            //    if (nodes != null)
            //    {
            //        foreach (HtmlNode node in nodes)
            //        {
            //            if (node.InnerText.ToLower().Contains("final judgment"))
            //            {

            //            }
            //        }
            //    }
            //int TotalPages = 13;

            //int _thredtime = Convert.ToInt32((TotalPages * 2) + "000" + "");
            //var _Keyword = "UNKNOWN TENANT #1 IN POSSESSION OF SUBJECT PROPERTY";
            //bool _check = _Keyword.Contains("Unknown Tenant");
            //bool _check1 = _Keyword.Contains("JOHN DOE");
            //bool _check2 = _Keyword.Contains("UNKNOWN TENANT");




            return View();
        }
        #endregion

        #region MiamiDade

        public ActionResult MiamiDade()
        {
            return View();
        }
        #endregion

        #region Bulk Emails

        public ActionResult BulkEmails(string County,int? UseCodeCategoryID, int? AssesedFrom, int? AssesedTo, decimal? LandFactorFrom, decimal? LandFactorTo, int? Zipcode, decimal? UnitsMin, decimal? UnitsMax, decimal? BedroomMin, decimal? BedroomMax, decimal? BathroomMin, decimal? BathroomMax)
        {
            List<UseCodeCategory> _PropertyType = _vmcase.GetPropertyTypeTab();
            ViewBag.PropertyType = _PropertyType;

            ViewBag.EmailList = _vmBulkEmails.GetEmailList(County, UseCodeCategoryID, AssesedFrom, AssesedTo, LandFactorFrom, LandFactorTo, Zipcode, UnitsMin, UnitsMax, BedroomMin, BedroomMax, BathroomMin, BathroomMax);

            return View();
        }

        [HttpPost]
        public ActionResult SendBulkEmail(string emails)
        {
            string[] emailArray = emails.Split(',');
            int successCount = 0;
            int failureCount = 0;



            System.Diagnostics.Stopwatch sWatch = new System.Diagnostics.Stopwatch();

            sWatch.Start();
            Parallel.ForEach(emailArray, row =>
            {
            //SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
            //NetworkCredential ncDNR = new NetworkCredential("maneesh.badhan@gmail.com", "Maneesh9034425715");
            //MailAddress maInfo = new MailAddress("maneesh.badhan@gmail.com", "Shark App");
            //MailAddress maDNR = new MailAddress("maneesh.badhan@gmail.com", "Shark App");
            //NetworkCredential ncInfo = new NetworkCredential("maneesh.badhan@gmail.com", "Maneesh9034425715");
            SmtpClient smtpClient = new SmtpClient("mail.teamstride.com", 25);
            NetworkCredential ncDNR = new NetworkCredential("support@teamstride.com", "Sup@ts0214!");
            MailAddress maInfo = new MailAddress("support@teamstride.com", "Shark App");
            MailAddress maDNR = new MailAddress("support@teamstride.com", "Shark App");
            NetworkCredential ncInfo = new NetworkCredential("support@teamstride.com", "Sup@ts0214!");

                string email = row.Trim();
                if (!string.IsNullOrEmpty(email))
                {
                    // string file = Server.MapPath("~/Mail.html");
                    string mailbody = "test email " + successCount.ToString();//System.IO.File.ReadAllText(file);
                    string to = email;
                    
                    MailAddress maNewUser = new MailAddress(to);
                    MailMessage msg = new MailMessage(maDNR, maNewUser);
                    msg.Subject = "Auto Response Email " + successCount.ToString();
                    msg.Body = mailbody;
                    
                    msg.IsBodyHtml = true;
                    
                    smtpClient.Credentials = ncDNR;
                    try
                    {
                        smtpClient.Send(msg);
                        successCount++;
                    }
                    catch (Exception ex)
                    {
                        string _msg = ex.Message;
                        failureCount++;
                    }
                }
            });
            sWatch.Stop();
            
            List<int> Counts = new List<int>();
            Counts.Add(successCount);
            Counts.Add(failureCount);
            return Json(Counts, JsonRequestBehavior.AllowGet);
        }

        #endregion Bulk Emails



        #region Sunbiz Details

        public ActionResult SunbizDetail()
        {
            return View();
        }


        public ActionResult LoadSunbizDetailData()
        {
            List<SunbizDetail> _tblSunbiz=new List<SunbizDetail>();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
          

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
           
            using (BrowardclerkEntities dc = new BrowardclerkEntities())
            {


                
                _tblSunbiz = _vmPalmCase.GetSunbizsearchDetailData();
                

            }
            //SORT
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            {
                bool desc = sortColumnDir == "asc";
                //_tblSunbiz = Extenstions.(_tblSunbiz, sortColumn, desc);
            }
            if (pageSize == -1)
            {
                pageSize = _tblSunbiz.Count();
            }
            recordsTotal = _tblSunbiz.Count();
            var data = _tblSunbiz.Skip(skip).Take(pageSize).ToList();
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPalmSunbizDetails(int SunbizCompanyID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<SunbizDetail> _VMGetSpokioOwnerList = _vmPalmCase.GetPalmSunbizDetails(SunbizCompanyID);
                
                return PartialView("_ShowSunbizOwnerList", _VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult GetBrowardCaseSunbizDetails(int SunbizDetailID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<BrowardCasesSunbizDetail> _VMGetSpokioOwnerList = _vmPalmCase.GetBrowardSunbizDetails(SunbizDetailID);

                return PartialView("_ShowBrowardCaseSunbizDetailsList", _VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home"); 
        }

        public ActionResult GetBrowardTaxIssueDetail(int SearchDefendantID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<BrowardTaxIsueDetails> _VMGetSpokioOwnerList = _vmPalmCase.GetBrowardTaxIssueDetail(SearchDefendantID);
                string constr = System.Configuration.ConfigurationManager.ConnectionStrings["ConnBrowardclerk"].ConnectionString;
               SqlConnection con = new SqlConnection(constr);
                con.Open();
                SqlCommand cmd = new SqlCommand("select distinct Totaltaxdue from tblBrowardtaxdatadetail where searchdefendantid='" + SearchDefendantID + "' and Totaltaxdue is not null and Totaltaxdue<>''", con);
                ViewBag.Totaldue = Convert.ToString(cmd.ExecuteScalar());
                con.Close();
                return PartialView("_ShowTaxDetailsForBrowardt", _VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult TaxDetailBrowardCaseYear(int TaxDetailID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                BrowardTaxIsueDetails _VMGetSpokioOwnerList = _vmPalmCase.GetYearBrowardTaxIssueDetail(TaxDetailID);

                return View( _VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home");
        }
        public ActionResult GetBrowardCaseLinkedInDetails(int OthAddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<BrowardCasesLinkedIntable> _VMGetSpokioOwnerList = _vmPalmCase.GetBrowardLinkedInSiteDetails(OthAddressID);

                return PartialView("_ShowBrowardCaseLinkedInDetailList", _VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home"); 
        }

        public ActionResult GetBrowardCaseTwitterDetails(int OthAddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<BrowardCasesTwittertable> _VMGetSpokioOwnerList = _vmPalmCase.GetBrowardCaseTwitterDetails(OthAddressID);

                return PartialView("_ShowBrowardCaseTwitterData", _VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home");
        }


        public ActionResult GetPalmBeachCaseTwitterData(int OthAddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<PalmBeachCasesTwittertable> _VMGetSpokioOwnerList = _vmPalmCase.GetPalmBeachCaseTwitterData(OthAddressID);

                return PartialView("_ShowBrowardCaseTwitterData", _VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home");
        }
        //public ActionResult SpokeoPalmOwnerProfileDetail(long spokioDetailsID, int PCBPalmDetailsID)
        //{
        //    if (System.Web.HttpContext.Current.Session["UserId"] != null)
        //    {
        //        SpokioDetail vmSpokioOwnerDetail = _vmPalmCase.GetPalmSpokioOwnerDetails(spokioDetailsID, PCBPalmDetailsID);
        //        return View(vmSpokioOwnerDetail);
        //    }
        //    return RedirectToAction("Login", "Home");
        //}

        public ActionResult SunbizOwnerViewDetails(int SunbizCompanyID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                SunbizDetail _VMGetSpokioOwnerList = _vmPalmCase.GetSunbizOwnerViewDetails(SunbizCompanyID);

                return View(_VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult BrowardCasesSunbizOwnerDetails(int SunbizDetailID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                BrowardCasesSunbizDetail _VMGetSpokioOwnerList = _vmPalmCase.GetBrowardSunbizDetailsOwner(SunbizDetailID);

                return View(_VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult BrowardCasesLinkedInOwnerDetails(int OthAddressID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                BrowardCasesLinkedIntable _VMGetSpokioOwnerList = _vmPalmCase.GetBrowardLinkedInSiteDetails_OwnerProfile(OthAddressID);

                return View(_VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home");
        }


        public ActionResult GetPalmBeachCaseSunbizDetails(int SunbizDetailID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<PalmBeachCasesSunbizDetail> _VMGetSpokioOwnerList = _vmPalmCase.GetPalmBeachSunbizDetails(SunbizDetailID);

                return PartialView("_ShowPalmBeachCaseSunbizDetailsList", _VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PalmBeachCasesSunbizOwnerDetails(int SunbizDetailID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                PalmBeachCasesSunbizDetail _VMGetSpokioOwnerList = _vmPalmCase.GetPalmBeachSunbizDetailsOwner(SunbizDetailID);

                return View(_VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home");
        }


        public ActionResult GetPalmBeachCaseLinkedInDetails(int PalmPCBDetailID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<PalmBeachCasesLinkedIntable> _VMGetSpokioOwnerList = _vmPalmCase.GetPalmBeachLinkedInSiteDetails(PalmPCBDetailID);

                return PartialView("_ShowPalmBeachCaseLinkedInDetailList", _VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home");
        }



        public ActionResult PalmBeachCasesLinkedInOwnerDetails(int PalmPCBDetailID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                PalmBeachCasesLinkedIntable _VMGetSpokioOwnerList = _vmPalmCase.GetPalmBeachLinkedInSiteProfileData(PalmPCBDetailID);

                return View(_VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home");
        }


        public ActionResult GetIndividualCaseLinkedInDetails(int SunbizCompanyID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<IndividualLinkedInData> _VMGetSpokioOwnerList = _vmPalmCase.GetIndividualLinkedInSiteDetails(SunbizCompanyID);

                return PartialView("_ShowIndividualLinkedInDetailList", _VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult GetIndividualCaseTwitterDetails(int SunbizCompanyID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<TwitterIndividualData> _VMGetSpokioOwnerList = _vmPalmCase.GetIndividualCaseTwitterDetails(SunbizCompanyID);

                return PartialView("_ShowIndividualCaseTwitterData", _VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home");
        }


        public ActionResult IndividualCasesLinkedInOwnerDetails(int SunbizCompanyID)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                IndividualLinkedInData _VMGetSpokioOwnerList = _vmPalmCase.GetIndividualLinkedInSiteDetails_OwnerProfile(SunbizCompanyID);

                return View(_VMGetSpokioOwnerList);
            }
            return RedirectToAction("Login", "Home");
        }




        public ActionResult FilterbyManualSearchvalue(string LocationAddresses, string MailingAddresses,  string Defendants, int FilterStatuss,string DirectorNames)
        {
          string FilterStatus = "";
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;

            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<SunbizDetail> _tblSrchDefandant = new List<SunbizDetail>();
              

                if (FilterStatuss == 1)
                {
                    FilterStatus = "Active";
                }
                else if (FilterStatuss == 2)
                {
                    FilterStatus = "InActive";
                }
               
                else if (FilterStatuss == 0)
                {
                    FilterStatus = "Active,InActive";
                }
               
                string MailingAddress;
                string LocationAddress;
                string FolioID;
                string DirectorName;
                string Defendant;

                if (LocationAddresses == "")
                {
                    LocationAddress = null;
                }
                else
                {
                    LocationAddress = LocationAddresses;
                }
                if (MailingAddresses == "")
                {
                    MailingAddress = null;
                }
                else
                {
                    MailingAddress = MailingAddresses;
                }
               
              
                if (Defendants == "")
                {
                    Defendant = null;
                }
                else
                {
                    Defendant = Defendants;
                }

                if (DirectorNames == "")
                {
                    DirectorName = null;
                }
                else
                {
                    DirectorName = DirectorNames;
                }
                try
                {
                    
                   _tblSrchDefandant = _vmcase.GetSunbizsearchFilteredData(LocationAddress, MailingAddress,  Defendant, FilterStatus, DirectorName).OrderByDescending(x => x.SunbizCompanyID).ToList();
                   
                    
                    //TempData["showDocReportLp"] = _tblSrchDefandant;
                }
                catch
                {

                }
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    bool desc = sortColumnDir == "asc";
                    //_tblSunbiz = Extenstions.(_tblSunbiz, sortColumn, desc);
                }
                if (pageSize == -1)
                {
                    pageSize = _tblSrchDefandant.Count();
                }
                recordsTotal = _tblSrchDefandant.Count();
                var data = _tblSrchDefandant.Skip(skip).Take(pageSize).ToList();
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);



            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ShowReportManual()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<SunbizDetail> _tblSrchDefandant = new List<SunbizDetail>();

                //_tblSrchDefandant = GetSrchDefandentLpCases(UseCodeCategoryID, SubTabId, CntRecord, _tblSrchDefandant);

                //TempData["showDocReportLp"] = _tblSrchDefandant;

                return PartialView("SunbizDetailFilters", _tblSrchDefandant);
            }
            return RedirectToAction("Login", "Home");
        }

        #endregion


        #region Change Password

        public ActionResult _ChangePasswordPartial()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                return View();
            }
           return RedirectToAction("Login", "Home"); 
        }


        public string  ChangePassword(string NewPassword, string ConfirmPassword)
        {
            
                if (String.Compare(NewPassword, ConfirmPassword) == 0)
                {
                int Userid = Convert.ToInt32(Session["UserId"]);
                string Confirm = _vmPalmCase.UserChangePassword(Userid, NewPassword);
                 return "1";

                }
            
            else
            {
                return "2"; 
            }

        }


        public ActionResult AddAdminUser()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                return View();
            }
            return RedirectToAction("Login", "Home");
        }



        public string AddAdminuserSave(string username, string useremail, string password, string phoneno, string county, string zipcode, string address)
        {

            
                int RoleID = 1;
                string Adduser = _vmPalmCase.Addadminuser(username, useremail, password, phoneno, county, zipcode, address, RoleID);
                return "1";
            
        }



        public ActionResult ChangeAdminuserPassword()
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                return View();
            }
            return RedirectToAction("Login", "Home");
        }


        public JsonResult GetAdminUserPasswordChange()
        {
            List<tblUser> list = new List<tblUser>();
            try
            {



                if (System.Web.HttpContext.Current.Session["UserId"] != null)
                {
                    int RoleID = 1;
                    List<tblUser> _VMGetSpokioOwnerList = _vmPalmCase.GetAdminUserPass(RoleID);
                    //return _VMGetSpokioOwnerList;

                    return Json(new { data = _VMGetSpokioOwnerList }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
            
        }


        public string AdminChangePassword(int AdminUser, string NewPassword)
        {

            
                int Userid = AdminUser;
                string Confirm = _vmPalmCase.UserChangePassword(Userid, NewPassword);
                return "1";
            

        }


        //public List<tblUser> GetAdminUserPasswordChange()
        //{
        //    List<tblUser> list = new List<tblUser>();
        //    try
        //    {

        //        string constr = System.Configuration.ConfigurationManager.ConnectionStrings["ConnBrowardclerk"].ConnectionString;

        //        SqlConnection con = new SqlConnection(constr);
        //        con.Open();


        //        string query = "select * from tblusers where RoleID=1";
        //        SqlCommand cmd = new SqlCommand(query, con);
        //        SqlDataAdapter adap = new SqlDataAdapter(cmd);
        //        System.Data.DataTable dt = new System.Data.DataTable();
        //        adap.Fill(dt);
        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            tblUser usr = new tblUser();
        //            usr.UserId = Convert.ToInt32(dr["UserId"]);
        //            usr.UserName = dr["UserName"].ToString();
        //            //  usr.Status = bool.Parse(dr["Status"].ToString());
        //            list.Add(usr);
        //        }
        //        con.Close();
        //        return list;
        //    }



        //    catch (Exception ex)
        //    {
        //        string msg = ex.Message;
        //        return null;
        //    }

        //    //  return list.ToArray();
        //}
        #endregion


        #region "Proforma"
        public ActionResult ShowProformadata(int? OTHAddressID, int SrchDefendantID)
        {
            int xyzx = Convert.ToInt32(Request.QueryString["OTHAddressID"]);
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<SrchDefandant> _lst = new List<SrchDefandant>();

                _lst = _vmcase.GetFinancialDataDetail(OTHAddressID);
                Session["SrchDefendantID"] = SrchDefendantID;
                // _lst = testloan();
                //_lst = GetAmortizationByID(OTHAddressID);
                //TempData["lstLoanAmortiZd"] = _lst;
                Session["lstFinancialdata"] = _lst;
                //GetBrowardTaxDetailsFinancial(SrchDefendantID);
                //List<LoanAmortiZd> _LoanAmortiZation = CalculateLoanAmortiZation(LoanPrincipleAmount, AnnualInterestRate, LoanPeriod, OriginalRepaymentAmount, LoanStartDate);
                // return PartialView("_LoanAmortization");
                return RedirectToAction("ProformaDetails");
            }
            return RedirectToAction("Login", "Home");

        }
        //public JsonResult GetBrowardTaxDetailsFinancial(int SearchDefendantID)
        public JsonResult GetBrowardTaxDetailsFinancial()
        {
            //if (System.Web.HttpContext.Current.Session["UserId"] != null)
            //{
            int SearchDefendantID = Convert.ToInt32(Session["SrchDefendantID"]);
                List<BrowardTaxIsueDetails> _VMGetSpokioOwnerList = _vmPalmCase.GetBrowardTaxIssueDetail(SearchDefendantID);
                string constr = System.Configuration.ConfigurationManager.ConnectionStrings["ConnBrowardclerk"].ConnectionString;
                SqlConnection con = new SqlConnection(constr);
                con.Open();
                SqlCommand cmd = new SqlCommand("select distinct Totaltaxdue from tblBrowardtaxdatadetail where searchdefendantid='" + SearchDefendantID + "' and Totaltaxdue is not null and Totaltaxdue<>''", con);
                //ViewBag.Totaldue = Convert.ToString(cmd.ExecuteScalar());
               string Total= Convert.ToString(cmd.ExecuteScalar());
             
              con.Close();
            var result = new { data = _VMGetSpokioOwnerList, data1 = Total };
            return Json(result, JsonRequestBehavior.AllowGet);
          
        }
        public ActionResult ProformaDetails()
        {
            double Subtotal ;
            double Estimatedsoftcost;
            double ClosingCost;
            double Purchasepriceestimator;
            double EffectiveRentpsf;
            double AnnualTax;
            double NOIstablized;
            double Valuation;
            double HoaFee;
            double RentZestim;
            double Marketvalue;
            double Refipayment;
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                List<SrchDefandant> _lst = new List<SrchDefandant>();
                int SearchDefendantID = Convert.ToInt32(Session["SrchDefendantID"]);
                //GetBrowardTaxDetailsFinancial(SearchDefendantID);
                if (Session["lstFinancialdata"] != null)
                {
                    _lst = (List<SrchDefandant>)Session["lstFinancialdata"];

                    foreach (var srchcase in _lst)
                    {
                        ViewBag.OthAddress = srchcase.OTHMailingAddress;
                        ViewBag.Documentype = srchcase.DocumentType;
                        ViewBag.CaseNumber = srchcase.CaseNumber;
                        ViewBag.Plaintiff = srchcase.Plaintiff;
                        ViewBag.FolioID = srchcase.OtherFolio;
                        ViewBag.Instrument = srchcase.instrumentnumber;
                        ViewBag.SaleDate = srchcase.SaleDate;
                        ViewBag.Saleprice = srchcase.saleprice;
                        ViewBag.Saletype = srchcase.SaleType;
                        ViewBag.Advbldg = srchcase.AdvBldgSF;
                        ViewBag.LandArea = srchcase.OTHLandFactor;
                        ViewBag.YearBuilt = srchcase.OTHEffYearBuilt;
                        ViewBag.PropertyOwner = srchcase.OTHPropertyOwner;
                        if (srchcase.EstOwnerEquity == "FULLY PAID")
                        {
                            ViewBag.Estownerequity = "FULLY PAID";
                        }
                       else if (srchcase.EstOwnerEquity == "")
                        {
                            ViewBag.Estownerequity = "";
                        }
                        else
                        {
                            ViewBag.Estownerequity = srchcase.EstOwnerEquity;

                        }
                        double RedfinMarketvalue;
                        ViewBag.ZillowMarketval = srchcase.ZillowPricedetail;
                        if (srchcase.RedfinPricedetail == null || srchcase.RedfinPricedetail == "")
                        {
                            RedfinMarketvalue = 0;

                        }
                        else
                        {
                            RedfinMarketvalue = Convert.ToDouble(srchcase.RedfinPricedetail.Replace(",", "").Replace("$", ""));
                        }
                        ViewBag.RedfinMarket = String.Format("{0:C}", (RedfinMarketvalue)); 
                        //ViewBag.RentZestimate = srchcase.Rentzestimate;
                        ViewBag.JustMarket = srchcase.OthMarketValue;
                        string totalSqft = srchcase.Totalsqft;
                        string Unitbedbathval = srchcase.UnitsBedBath;
                        string[] Unitbedbath1 = Unitbedbathval.Split('/');
                        string Unit = "";
                        string Bed = "";
                         string Bath = "";
                        string UnitsBedBath = Bed + " " + "beds" + " " + Bath + " " + "baths" + " " + totalSqft;
                        if (Unitbedbath1.Length == 3) {
                             Unit = Unitbedbath1[0];
                             Bed = Unitbedbath1[1];
                             Bath = Unitbedbath1[2];
                             UnitsBedBath = Bed + " " + "beds" + " " + Bath + " " + "baths" + " " + totalSqft;
                        }
                        else if (Unitbedbath1.Length == 1)
                        {
                            Unit = Unitbedbath1[0];
                            Bed = "";
                            Bath = "";
                            UnitsBedBath = Bed + " " + "beds" + " " + Bath + " " + "baths" + " " + totalSqft;
                        }

                       
                        ViewBag.Unit = Unit;
                        ViewBag.Bed = Bed;
                        ViewBag.Bath = Bath;
                        ViewBag.Bedbath = UnitsBedBath;
                        string propertytype = srchcase.PropertyType;
                        string Codetype = srchcase.Usecodetype;
                        string PropertyCodetype = "Broward" + "-" + propertytype + "-" + Codetype;
                        ViewBag.Propertycode = PropertyCodetype;
                        string Todaydate = DateTime.Now.ToString("dd/MM/yyyy");
                        ViewBag.Datetoday = Todaydate;
                        ViewBag.PropertyOwner = srchcase.OTHPropertyOwner;
                        if (srchcase.ZillowPricedetail == null || srchcase.ZillowPricedetail=="")
                        {
                            Marketvalue = 0;

                        }
                        else
                        {
                            Marketvalue = Convert.ToDouble(srchcase.ZillowPricedetail.Replace(",", "").Replace("$", "")); 
                        }
                        
                        ViewBag.EstimatedMarketPrice = String.Format("{0:C}", (Marketvalue)); 
                        Subtotal = (Marketvalue * 100) / 120;
                        ViewBag.SubTotal = String.Format("{0:C}", (Subtotal)); 
                        if(srchcase.HOARedfinDue== "No Record Found" || srchcase.HOARedfinDue==null)
                        {
                            HoaFee = 0;
                        }
                        else
                        {
                            HoaFee = Convert.ToDouble(srchcase.HOARedfinDue.Replace(",", "").Replace("$", ""));
                            
                        }
                        ViewBag.HOAFee = String.Format("{0:C}", (HoaFee));
                        Estimatedsoftcost = 500 + (HoaFee * 12);
                        ViewBag.Estimsoftcost = String.Format("{0:C}", (Estimatedsoftcost));
                        double EstimatedHardcost =Convert.ToDouble("3000");
                        ViewBag.Estimhardcost = "$"+EstimatedHardcost;
                        ClosingCost = Subtotal * 0.035;
                        ViewBag.CLosingCost = String.Format("{0:C}", (ClosingCost));
                        Purchasepriceestimator = (Subtotal - Estimatedsoftcost - EstimatedHardcost);
                        ViewBag.Purchasepriceestimator = String.Format("{0:C}", (Purchasepriceestimator));
                        if (srchcase.Rentzestimate == null || srchcase.Rentzestimate=="")
                        {
                            RentZestim = 0;

                        }
                        else
                        {
                            RentZestim = Convert.ToDouble(srchcase.Rentzestimate.Replace(",", "").Replace("$", "").Replace("/mo", ""));
                           
                        }
                        ViewBag.RentZestimate = String.Format("{0:C}", (RentZestim));
                        double Advfl = Convert.ToDouble(srchcase.AdvBldgSF);
                        double EffectiveRent = RentZestim / Advfl;
                        EffectiveRentpsf = EffectiveRent * 1.03;
                        ViewBag.EffectiveRentpsf = String.Format("{0:C}", (EffectiveRentpsf)); 
                        AnnualTax = Convert.ToDouble(srchcase.othAssessedtax.Replace(",", "").Replace("$", ""));
                        ViewBag.AnnualTax = String.Format("{0:C}", (AnnualTax));
                        double F32 = 0 * (1.065);
                        double F86 = EffectiveRent;
                        double D20 = F86 * 1.03;
                        double F20 = (D20 * (1.03));
                        double J39 = Convert.ToDouble(srchcase.AdvBldgSF);
                        double F22 = F20 * (J39 * 12);
                        double F24 = F22;
                        double F26 = -(0.065 * F24);
                        double F27 = -(F24 * 1.03);
                        double F29 = F24;
                        double F34 = F29 + F32;
                        double D43 = Marketvalue * 0.01;
                        double D44 = Marketvalue * 0.02;
                        double D45 = HoaFee * 12;
                        double D46 = AnnualTax;
                        double F48 = 500+ D43+D44+D45+D46;
                        double CAP = 6.50 / 100;
                        ViewBag.CAP = CAP;
                        NOIstablized = F34+F48;
                        ViewBag.NOIstablized = NOIstablized;
                        Valuation = NOIstablized / CAP;
                        ViewBag.Valuation = String.Format("{0:C}", (Valuation));
                        double PSFValue = Valuation / CAP;
                        ViewBag.PSfValue = String.Format("{0:C}", (PSFValue)); 
                        double saleprice = Convert.ToDouble(srchcase.saleprice.Replace(",", "").Replace("$", ""));
                        double PSFPurchase = saleprice / CAP;
                        ViewBag.PSFPurchase = String.Format("{0:C}", (PSFPurchase));
                        double PSFPurchaseVsValue = PSFValue - PSFPurchase;
                        ViewBag.PSFPurchaseVsValue = String.Format("{0:C}", (PSFPurchaseVsValue));
                        double PurchaseVsValue = Valuation - Purchasepriceestimator;
                        ViewBag.PurchaseVsValue = String.Format("{0:C}", (PurchaseVsValue));
                        if (srchcase.Refipayment == null || srchcase.Refipayment=="")
                        {
                            Refipayment = 0;
                        }
                        else
                        {
                            Refipayment = Convert.ToDouble(srchcase.Refipayment.Replace(",", "").Replace("$", "").Replace("/mo", ""));

                          
                        }
                        ViewBag.Refipayment = String.Format("{0:C}", (Refipayment));
                        double Tax= AnnualTax/12;
                        ViewBag.Tax = String.Format("{0:C}", (Tax));
                        double F50 = Purchasepriceestimator * 0.01;
                        double Insurance = F50 / 12;
                        ViewBag.Insurance = String.Format("{0:C}", (Insurance));
                        double F45 = RentZestim * 12;
                        double J51 = 0.08;
                        double F51 = F45 * J51;
                        double Maintenance = F51 / 12;
                        ViewBag.Maintenance = String.Format("{0:C}", (Maintenance));
                        double TotalExpense = Refipayment + HoaFee + Tax + Insurance + Maintenance;
                        double NetIncome = RentZestim - TotalExpense;
                        ViewBag.NetIncome= String.Format("{0:C}", (NetIncome));
                        ViewBag.TotalExpense = String.Format("{0:C}", (TotalExpense));
                        double Annual = F45;
                        ViewBag.Annual = String.Format("{0:C}", (Annual));
                        double Annua2 = Refipayment * 12;
                        ViewBag.Annua2 = String.Format("{0:C}", (Annua2));
                        double Annua3 = HoaFee * 12;
                        ViewBag.Annua3 = String.Format("{0:C}", (Annua3));
                        ViewBag.Annua4 = String.Format("{0:C}", (AnnualTax));
                        double Annua4 = Convert.ToDouble(AnnualTax);
                        double Annua5 = F50;
                        ViewBag.Annua5 = String.Format("{0:C}", (Annua5));
                        ViewBag.Annua6 = String.Format("{0:C}", (F51));
                        double Annua6 = F51;

                        double Annual7 = TotalExpense * 12;
                        ViewBag.Annual7 = String.Format("{0:C}", (Annual7));

                        double Annual8 = RentZestim - TotalExpense;
                       ViewBag.Annual8 = String.Format("{0:C}", (Annual8));

                        double Mortpercent = (Annua2 / Annual7)*100;
                        ViewBag.Mortpercent = String.Format("{0:C}", (Mortpercent)).Replace("$","")+"%";
                        double HOAPercent = (Annua3 / Annual7)*100;
                        ViewBag.HOAPercent = String.Format("{0:C}", (HOAPercent)).Replace("$", "") + "%";
                        double Taxperc = (Annua4 / Annual7)*100;
                        ViewBag.Taxperc = String.Format("{0:C}", (Taxperc)).Replace("$", "") + "%";
                        double Insuranceperc = (Annua5 / Annual7)*100;
                        ViewBag.Insuranceperc = String.Format("{0:C}", (Insuranceperc)).Replace("$", "") + "%";
                        double Maintenanceeperc = (Annua6 / Annual7)*100;
                        ViewBag.Maintenanceeperc = String.Format("{0:C}", (Maintenanceeperc)).Replace("$", "") + "%";
                        double ExpenseRev = (Annual7 / Annual)*100;
                        ViewBag.ExpenseRev = String.Format("{0:C}", (ExpenseRev)).Replace("$", "") + "%";
                        double Mortexpense = (Annual8 / Annual)*100;
                        ViewBag.Mortexpense= String.Format("{0:C}", (Mortexpense)).Replace("$", "") + "%";
                        if (srchcase.ZillMarkettemp==null || srchcase.ZillMarkettemp == "")
                        {
                            ViewBag.MarketTemp = "";
                        }
                        else
                        {
                            ViewBag.MarketTemp = srchcase.ZillMarkettemp;
                        }
                        if (srchcase.MedianZEstimate == null || srchcase.MedianZEstimate == "")
                        {
                            ViewBag.MedianZestimate = "";
                        }
                        else
                        {
                            ViewBag.MedianZestimate = srchcase.MedianZEstimate;

                        }
                        if (srchcase.ZillPastMonths == null || srchcase.ZillPastMonths == "")
                        {
                            ViewBag.Zillpast = "";
                        }
                        else
                        {
                            ViewBag.Zillpast = srchcase.ZillPastMonths;
                        }
                        double AnnualRent = RentZestim * 12;
                        ViewBag.AnnualRent = String.Format("{0:C}", (AnnualRent));
                        double Just= Convert.ToDouble(srchcase.OthMarketValue.Replace(",", "").Replace("$", ""));
                        double Equity;
                        if (srchcase.EstOwnerEquity == "FULLY PAID")
                        {
                            Equity = 0;
                        }
                        else
                        {
                            Equity = Convert.ToDouble(srchcase.EstOwnerEquity.Replace(",", "").Replace("$", ""));

                        }
                        
                        double Estdebt = Just - Equity;
                        ViewBag.Estdebt = String.Format("{0:C}", (Estdebt));
                        double EStmarket = Just + (Just * 0.15);
                        ViewBag.EStmarket = String.Format("{0:C}", (EStmarket));
                        double F80 = EStmarket - Equity;
                        double EstReturn = 1 - (F80 / EStmarket);
                        ViewBag.EstReturn = EstReturn + "%";
                        double Equityper = Equity / Just;
                        ViewBag.Equityper = Equityper + "%";
                        double diffquityMarket = EStmarket - Equity;
                        ViewBag.diffquityMarket = String.Format("{0:C}", (diffquityMarket));
                        double diffZilltoEstmarket = Marketvalue - EStmarket;
                        ViewBag.diffZilltoEstmarket = String.Format("{0:C}", (diffZilltoEstmarket));
                        double diffRedfintoEstmarket = RedfinMarketvalue - EStmarket;
                        ViewBag.diffRedfintoEstmarket = String.Format("{0:C}", (diffRedfintoEstmarket));
                        double MonthlyEffectiverent = RentZestim / Advfl;
                        ViewBag.MonthlyEffectiverent = String.Format("{0:C}", (MonthlyEffectiverent));
                        double EffectiveRentNew = AnnualRent / Advfl;
                        ViewBag.EffectiveRentNew = String.Format("{0:C}", (EffectiveRentNew));
                        double PerSquareroot1 = Just / Advfl;
                        ViewBag.PerSquareroot1 = String.Format("{0:C}", (PerSquareroot1));
                        double PerSquareroot2 = EStmarket / Advfl;
                        ViewBag.PerSquareroot2 = String.Format("{0:C}", (PerSquareroot2));
                        double PerSquareroot3 = diffquityMarket / Advfl;
                        ViewBag.PerSquareroot3 = String.Format("{0:C}", (PerSquareroot3));
                        double PerSquareroot4 = diffZilltoEstmarket / Advfl;
                        ViewBag.PerSquareroot4 = String.Format("{0:C}", (PerSquareroot4));
                        double PerSquareroot5 = diffRedfintoEstmarket / Advfl;
                        ViewBag.PerSquareroot5 = String.Format("{0:C}", (PerSquareroot5));
                        double ZillVsEstmarket = (diffZilltoEstmarket / EStmarket)*100;
                        ViewBag.ZillVsEstmarket = String.Format("{0:C}", (ZillVsEstmarket)).Replace("$", "") + "%"; 
                        double RedVsEstmarket = (diffRedfintoEstmarket / EStmarket)*100;
                        ViewBag.RedVsEstmarket = String.Format("{0:C}", (RedVsEstmarket)).Replace("$", "") + "%";
                        double Units = Convert.ToDouble(Unit);
                        double TotalSf = Convert.ToDouble(Advfl * Units);
                        ViewBag.TotalSf = TotalSf;
                        double Unitperc = (TotalSf / TotalSf)*100;
                        ViewBag.Unitperc = Unitperc + "%";
                        double CAP1 = (Annual / Purchasepriceestimator) * 100;
                        ViewBag.CAP1 = String.Format("{0:C}", (CAP1)).Replace("$", "") + "%";

                    }//ViewBag.othaddressid = _lst.da
                }
                return View(_lst);

            }
            return RedirectToAction("Login", "Home");
        }

        //public ActionResult LoanAmortizationaa()
        //{
        //    if (System.Web.HttpContext.Current.Session["UserId"] != null)
        //    {
        //        List<srch> _lst = new List<LoanAmortiZd>();
        //        if (Session["lstLoanAmortiZd"] != null)
        //        {
        //            _lst = (List<LoanAmortiZd>)Session["lstLoanAmortiZd"];

        //            ViewBag.Anualrate = Session["CurrentRate"];
        //            ViewBag.LoanPrincipleAmount = Session["LoanPrincipleAmount"];
        //            ViewBag.LoanStartDate = Session["LoanStartDate"];
        //            ViewBag.LoanPeriod = Session["LoanPeriod"];
        //            ViewBag.Downpayment = Session["Downpayment"];
        //            string currentMonthYear = DateTime.Now.ToString("MMM") + "-" + DateTime.Now.Year.ToString();
        //            ViewBag.CurrentMonthYear = currentMonthYear;
        //            int index = _lst.FindIndex(a => a.Month == currentMonthYear);

        //            ViewBag.CurrentDTPage = index / 10;
        //            //TempData["LoanSummary"] = LoanSummary(_lst);
        //            //LoanSummary(_lst);
        //        }



        //        return View(_lst);
        //    }
        //    return RedirectToAction("Login", "Home");
        //}
        #endregion
    }
}



