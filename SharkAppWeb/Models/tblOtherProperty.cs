//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SharkAppWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblOtherProperty
    {
        public tblOtherProperty()
        {
            this.tblContactInfoes = new HashSet<tblContactInfo>();
            this.tblExemptionTAXes = new HashSet<tblExemptionTAX>();
            this.tblMatchedDecsWords = new HashSet<tblMatchedDecsWord>();
            this.tblOtherAssessedValues = new HashSet<tblOtherAssessedValue>();
            this.tblOtherSaleHistories = new HashSet<tblOtherSaleHistory>();
            this.tblSpokioDetails = new HashSet<tblSpokioDetail>();
        }
    
        public int OTHAddressID { get; set; }
        public Nullable<int> SrchDefendantID { get; set; }
        public string OTHSiteAddressSrch { get; set; }
        public string OTHPropertyOwner { get; set; }
        public string OTHMailingAddress { get; set; }
        public string OTHAbbreviatedLegalDesc { get; set; }
        public string OTHFolioId { get; set; }
        public string OTHMilage { get; set; }
        public string OTHSrchUse { get; set; }
        public string OTHLandFactor { get; set; }
        public string OTHAdvBldgSF { get; set; }
        public string UnitsBEDBath { get; set; }
        public string OTHFolioSiteLink { get; set; }
        public Nullable<System.DateTime> OTHSearchDate { get; set; }
        public Nullable<bool> OTHCompareable { get; set; }
        public Nullable<bool> IsOTHCorrectProperty { get; set; }
        public string OTHCardUrl { get; set; }
        public string OTHSketchUrl { get; set; }
        public string OTHEffYearBuilt { get; set; }
        public string OwedOnMortgage { get; set; }
        public string DateOFMortgage { get; set; }
        public Nullable<bool> IsSpokioDetails { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public Nullable<int> SpokioStatus { get; set; }
    
        public virtual ICollection<tblContactInfo> tblContactInfoes { get; set; }
        public virtual ICollection<tblExemptionTAX> tblExemptionTAXes { get; set; }
        public virtual ICollection<tblMatchedDecsWord> tblMatchedDecsWords { get; set; }
        public virtual ICollection<tblOtherAssessedValue> tblOtherAssessedValues { get; set; }
        public virtual tblSrchDefandant tblSrchDefandant { get; set; }
        public virtual ICollection<tblOtherSaleHistory> tblOtherSaleHistories { get; set; }
        public virtual ICollection<tblSpokioDetail> tblSpokioDetails { get; set; }
    }
}
