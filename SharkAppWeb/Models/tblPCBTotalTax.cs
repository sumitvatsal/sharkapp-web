//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SharkAppWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblPCBTotalTax
    {
        public long PCBTotalTaxesID { get; set; }
        public Nullable<long> PCBPalmDetailsID { get; set; }
        public string TaxesTax { get; set; }
        public string AdValorem { get; set; }
        public string NonAdValorem { get; set; }
        public string TotalTaxes { get; set; }
    
        public virtual tblPCBPalmDetail tblPCBPalmDetail { get; set; }
    }
}
