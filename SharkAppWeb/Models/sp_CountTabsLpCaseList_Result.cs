//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SharkAppWeb.Models
{
    using System;
    
    public partial class sp_CountTabsLpCaseList_Result
    {
        public Nullable<int> Residential { get; set; }
        public Nullable<int> Commercial { get; set; }
        public Nullable<int> Industrial { get; set; }
        public Nullable<int> Agricultural { get; set; }
        public Nullable<int> Institutional { get; set; }
        public Nullable<int> Government { get; set; }
        public Nullable<int> Miscellaneous { get; set; }
        public Nullable<int> CentrallyAssessed { get; set; }
        public Nullable<int> Non_Agricultural { get; set; }
        public Nullable<int> ALLLEADS { get; set; }
        public Nullable<int> MultiProperty { get; set; }
        public Nullable<int> SetSubjtProperty { get; set; }
        public Nullable<int> NotFound { get; set; }
    }
}
