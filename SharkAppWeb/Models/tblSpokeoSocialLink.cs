//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SharkAppWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblSpokeoSocialLink
    {
        public long SpokeoSocialLinkID { get; set; }
        public Nullable<long> SpokeoDetailsID { get; set; }
        public Nullable<long> SpokeoResidentID { get; set; }
        public Nullable<long> SpokeoRelativeID { get; set; }
        public string SocialLinkUrl { get; set; }
        public string SocialSiteType { get; set; }
        public string SocialSiteName { get; set; }
        public string SocialName { get; set; }
    
        public virtual tblSpokeoRelative tblSpokeoRelative { get; set; }
        public virtual tblSpokeoResident tblSpokeoResident { get; set; }
        public virtual tblSpokioDetail tblSpokioDetail { get; set; }
    }
}
