//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SharkAppWeb.Models
{
    using System;
    
    public partial class sp_GetPropertydetailsByID_Result
    {
        public Nullable<decimal> SalePricePrSqFt { get; set; }
        public string SalePrice { get; set; }
        public string MarketValue { get; set; }
        public string AssessedValue { get; set; }
        public int AddressID { get; set; }
        public string SiteAddressSrch { get; set; }
        public string PropertyOwner { get; set; }
        public string MailingAddress { get; set; }
        public string AbbreviatedLegalDesc { get; set; }
        public string FolioId { get; set; }
        public string Milage { get; set; }
        public string SrchUse { get; set; }
        public string LandFactor { get; set; }
        public string AdvBldgSF { get; set; }
        public string UnitsBEDBath { get; set; }
        public string FolioSiteLink { get; set; }
        public Nullable<System.DateTime> SearchDate { get; set; }
        public Nullable<bool> Compareable { get; set; }
        public Nullable<int> SrchDefendantID { get; set; }
        public string CardUrl { get; set; }
        public string SketchUrl { get; set; }
        public string EffYearBuilt { get; set; }
    }
}
