//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SharkAppWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblExemptionTAX
    {
        public long ExemptionID { get; set; }
        public Nullable<int> OTHAddressID { get; set; }
        public string EXTitle { get; set; }
        public string EXCounty { get; set; }
        public string EXSchoolBoard { get; set; }
        public string EXMuncipal { get; set; }
        public string EXIndependent { get; set; }
    
        public virtual tblOtherProperty tblOtherProperty { get; set; }
    }
}
