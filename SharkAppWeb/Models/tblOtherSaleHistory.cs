//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SharkAppWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblOtherSaleHistory
    {
        public int OthSaleHistoryID { get; set; }
        public Nullable<int> OTHAddressID { get; set; }
        public Nullable<System.DateTime> OthSaleDate { get; set; }
        public string OthSaleType { get; set; }
        public string OthSalePrice { get; set; }
        public string OthSaleBookPage { get; set; }
    
        public virtual tblOtherProperty tblOtherProperty { get; set; }
    }
}
